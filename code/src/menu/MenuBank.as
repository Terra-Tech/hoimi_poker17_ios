package menu
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import Refill.RefillView;
	
	import base.TiDisableTextField;
	
	public class MenuBank extends Sprite
	{
		private var m_mc:Poker_Btn_Menu_Bank = new Poker_Btn_Menu_Bank();
		private var m_coRefillView:RefillView = RefillView.fnGetInstance();
		public function MenuBank()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_coRefillView);
			m_coRefillView.mouseChildren=false;
			m_coRefillView.mouseEnabled=false;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
			m_mc.addEventListener(MouseEvent.CLICK,fnClickBank);
		}
		
		private function fnClickBank(e:MouseEvent):void
		{
			m_coRefillView.refill();
		}
		
		
	}
}