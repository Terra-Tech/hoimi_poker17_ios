package 
{
	
	
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.Transmitter;
	import com.hoimi.util.CryptoHelper;
	import com.hoimi.util.EventManager;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLRequestMethod;
	
	public class SessionManager
	{
		static private var _instance:SessionManager;
		
		static public function get instance():SessionManager
		{
			if(!_instance)
				_instance = new SessionManager(new MySingleton);
			return _instance;
		}
		
		public function SessionManager(singleton:MySingleton){}
		
		private var URL:String = "http://localhost:8080/";
		private var SESSION_TIMEOUT:int = 10000;
		private var KEY:String = "24875447";
		
		private var _transmitter:Transmitter;
		private var _queue:Array;
		private var _session:SessionInfo;
		
		public function init(url:String, key:String, timeout:int):void
		{
			URL = url;
			SESSION_TIMEOUT = timeout;
			KEY = key;
			_transmitter = new Transmitter(SESSION_TIMEOUT);
			_queue = [];
			
			EventManager.instance.register(NetEvent.STATE_COMPLETE, onDone);
			EventManager.instance.register(NetEvent.STATE_IO_ERROR, onFail);
			EventManager.instance.register(NetEvent.STATE_TIMEOUT, onFail);
			EventManager.instance.register(NetEvent.STATE_FAIL, onFail);
		}
		
		private function onFail(e:NetEvent):void
		{
			if(e.protocol < 0) return;
			if(_session.onFail != null)_session.onFail(e);
		}
		
		private function onDone(e:NetEvent):void
		{
			if(e.protocol < 0) return;
			e.data = CryptoHelper.decrypt(KEY,e.data);
			if(_session.onDone != null)_session.onDone(e);
		}
		
		public function loop():void
		{
			if(!_transmitter.busy && _queue.length > 0)
			{
				_session = _queue.shift();
				_session.execute();
			}
			_transmitter.loop();
		}
		
		public function request(protocol:int, params:Object, onDone:Function = null, onFail:Function = null, timeout:int = -1):void
		{
			if(protocol < 0 || protocol >= Protocol.max)return;
			
//			var method:Function = (Protocol.METHOD[protocol] == URLRequestMethod.GET ? httpGet : httpPost);
			var method:Function = httpPost;
			var route:String = Protocol.ROUTE[protocol];
			
			_queue.push(new SessionInfo(protocol,params, route, method, timeout == -1 ? SESSION_TIMEOUT : timeout, onDone, onFail ));
		}
		
		private function httpGet(protocol:int, route:String, params:Array, timeout:int):void
		{
			_transmitter.get(protocol, URL+route, encryptParams(params));
		}
		
		private function httpPost(protocol:int, route:String, json:Object, timeout:int):void
		{
			_transmitter.post(protocol, URL+route, encryptJSON(json));
		}
		
		private function encryptParams(params:Array):Array
		{
			for(var i:int = 0 ; i< params.length ; i++)
			{
				params[i] = CryptoHelper.encrypt(KEY,params[i]);
			}
			
			return params;
		}
		
		private function encryptJSON(json:Object):String
		{
			/*for( var p:String in json )
			{
				json[p] = CryptoHelper.encrypt(KEY,json[p]);
			}*/
			return CryptoHelper.encrypt(KEY,JSON.stringify(json));
		}
	}	
}
class SessionInfo
{
	public var protocol:int;
	public var params:Object;
	public var route:String;
	public var method:Function;
	public var timeout:int;
	public var onDone:Function;
	public var onFail:Function;
	
	public function SessionInfo(protocol:int, params:Object, route:String, method:Function, timeout:int, onDone:Function, onFail:Function):void
	{
		this.protocol = protocol;
		this.params  = params;
		this.route = route;
		this.method = method;
		this.timeout = timeout;
		this.onDone = onDone;
		this.onFail = onFail;
	}
	
	public function execute():void
	{
		method(protocol, route, params, timeout);
	}
}
class MySingleton{}