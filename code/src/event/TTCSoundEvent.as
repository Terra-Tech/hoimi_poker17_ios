package event
{
	public class TTCSoundEvent extends TiBasicEvent
	{
		public static const SOUND_EVENT:String="SOUND_EVENT";
		public static const SOUND_PLAY_EVENT:String = "SOUND_PLAY_EVENT";
		public static const SOUND_PLAY_FINISHED_EVENT:String="SOUND_PLAY_FINISHED_EVENT";
		
		public function TTCSoundEvent(type:String, detail:String="", bubbles:Boolean=false)
		{
			super(type, detail, bubbles);
		}
	}
}