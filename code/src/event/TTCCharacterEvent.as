package event
{

	public class TTCCharacterEvent extends TiBasicEvent
	{
		public static const CHARACTER_EVENT:String="CHARACTER_EVENT"
		public static const CHARACTER_FADE_IN_FINISHED_EVENT:String="CHARACTER_FADE_IN_FINISHED_EVENT"
		public static const CHARACTER_FADE_OUT_FINISHED_EVENT:String="CHARACTER_FADE_OUT__FINISHED_EVENT"
		public static const CHARACTER_SAD_FINISHED_EVENT:String="CHARACTER_SAD_FINISHED_EVENT"
		public static const CHARACTER_HAPPY_FINISHED_EVENT:String="CHARACTER_HAPPY_FINISHED_EVENT"
		public static const CHARACTER_ATTACK_FINISH_EVENT:String="CHARACTER_ATTACK_FINISH_EVENT"
		public static const CHARACTER_HIT_FINISH_EVENT:String="CHARACTER_HIT_FINISH_EVENT"
		public static const CHARACTER_DIE_FINISH_EVENT:String="CHARACTER_DIE_FINISH_EVENT"
		public static const CHARACTER_HIT_EVENT:String="CHARACTER_HIT_EVENT"
		public static const CHARACTER_DIE_EVENT:String="CHARACTER_DIE_EVENT"
		public static const CHARACTER_SHOW_FINISHED_EVENT:String="CHARACTER_SHOW_FINISHED_EVENT"
		public static const CHARACTER_SHOW_EVENT:String="CHARACTER_SHOW_EVENT"
			
		public function TTCCharacterEvent(type:String, detail:String = "",bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, detail, bubbles, cancelable);
		}
	}
}