package base
{
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	

	public class TiCheckBox extends EventDispatcher
	{
		public function TiCheckBox()
		{
			super();
		}
		public function fnSetMcState(mc:MovieClip,tip:String=""):void
		{
			mc.tipstr = tip;
			mc.buttonMode = true;
			mc.gotoAndStop(1);
			mc.selected = false;
			mc.addEventListener(MouseEvent.MOUSE_DOWN,mc_down);
			mc.addEventListener(MouseEvent.MOUSE_UP,mc_up);
			mc.addEventListener(MouseEvent.MOUSE_OVER,mc_over);
			mc.addEventListener(MouseEvent.MOUSE_MOVE,mc_move);
			mc.addEventListener(MouseEvent.MOUSE_OUT,mc_out);
			mc.seled = false;
		}
		
		public function fnEnableMcState(mc:MovieClip):void
		{
			mc.buttonMode = true;
			mc.disable = false;
			mc.gotoAndStop(1);
			mc.seled=false;
		}
		public function fnDisableMcState(mc:MovieClip):void
		{
			mc.buttonMode = false;
			mc.disable = true;
			mc.gotoAndStop(1);
		}
		
		public function fnSetMcSelected(mc:MovieClip):void
		{
			mc.buttonMode = true;
			mc.disable = false;
			mc.gotoAndStop(3);
			mc.seled=true;
		}
		public  function dispatchMyEvent(tipstr:String,X:Number,Y:Number):void
		{
			var myEvent:TiButtonStateEvent = new TiButtonStateEvent(TiButtonStateEvent.BUTTON_EVENT_DISPLAY_TOOPTIP,null);
			myEvent.m_strMsg = tipstr;
			myEvent.m_nX = X;
			myEvent.m_nY = Y;
			dispatchEvent(myEvent);
		}

		
		
		private function mc_down(event:MouseEvent):void
		{
			
			var mc:MovieClip = MovieClip(event.currentTarget);
			if(mc.disable)
				return;
			if(mc.seled)
			{
				mc.gotoAndStop(1);
				mc.seled = false;
			}
			else
			{
				mc.gotoAndStop(3);
				mc.seled = true;
				mc.buttonMode=false;
			}
			if(mc.tipstr !="")
			{
				fnChangeTooltip(mc);
			}
			var myEvent:TiButtonStateEvent = new TiButtonStateEvent(TiButtonStateEvent.BUTTON_EVENT_MOUSE_CLICK_EVENT,null);
			myEvent.m_obj = mc;
			this.dispatchEvent(myEvent);
		}
		
		private function mc_up(event:MouseEvent):void
		{
			var mc:MovieClip = MovieClip(event.currentTarget)
			if(mc.disable)
				return;
			if(!mc.seled)
			{
				mc.gotoAndStop(2);
			}
		}
		
		private function mc_over(event:MouseEvent):void
		{
			
			var mc:MovieClip = MovieClip(event.currentTarget);
			if(mc.disable)
				return;
			if (! mc.seled)
			{
				mc.gotoAndStop(2);
				mc.buttonMode = true;
			}
			else
			{
				mc.buttonMode = true;
			}
			if(mc.tipstr !="")
			{
				fnChangeTooltip(mc);
			}
			var myEvent:TiButtonStateEvent = new TiButtonStateEvent(TiButtonStateEvent.BUTTON_EVENT_ROLLOVER_EVENT,null);
			myEvent.m_obj = mc;
			dispatchEvent(myEvent);
			
		}
		private function mc_move(event:MouseEvent):void
		{
			
			var mc:MovieClip = MovieClip(event.currentTarget);
			if(mc.disable)
				return;
			dispatchMyEvent(mc.tipstr,event.stageX,event.stageY+mc.height);
		}
		private function mc_out(event:MouseEvent):void
		{
			
			dispatchEvent(new TiButtonStateEvent(TiButtonStateEvent.BUTTON_EVENT_REMOVE_TOOPTIP,null));
			var mc:MovieClip = MovieClip(event.currentTarget);
			if(mc.disable)
				return;
			if (! mc.seled)
			{
				mc.gotoAndStop(1);
			}
			else
			{
				//mc.gotoAndStop(3)
			}
			if(mc.tipstr !="")
			{
				fnChangeTooltip(mc);
			}
		}
		private function fnChangeTooltip(mc:MovieClip):void
		{
			for (var i: int = 0; i < mc.numChildren; i++) 
			{
				var txt:Object;
				txt = mc.getChildAt(i);
				if (txt is TextField) 
				{
					if(mc.tipstr!="")
					{
						TextField(txt).text = mc.tipstr;
						TextField(txt).selectable = false;
					}
					
				}
			}
		}
	}
}