package com.hoimi.lib.time
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.getTimer;
	
	public class Clock extends EventDispatcher
	{
		static public const ALERT:String = "CLOCK_ALERT"; 
		static public const DONE:String = "CLOCK_DONE"; 
		private const ONE_SECOND:int = 1000; 
		private var _timestamp:int = -1;
		private var _interval:Number = Number.MAX_VALUE;
		private var _totalCount:int = 0;
		private var _pauseTimestamp:int = -1;
		
		public function get isPause():Boolean
		{
			return Boolean(_pauseTimestamp > -1);
		}
		
		public function get isRunning():Boolean
		{
			return Boolean(_timestamp > -1);
		}
		
		public function Clock()
		{
		}
		
		public function run():void
		{
			if(isPause)return;
			if(isRunning)
			{
				var now_:int = now();
				if(diff(now_,_timestamp) >= _interval )
				{
					_timestamp = now_;
					this.dispatchEvent(new Event(ALERT));
					if(--_totalCount == 0)
					{
						_timestamp = -1;
						this.dispatchEvent(new Event(DONE));
					}
				}
			}
		}
		
		public function pause():void
		{
			if(!isRunning)return;
			_pauseTimestamp = now();
		}
		
		public function resume():void
		{
			if(!isPause)return;
			var pass:int = now() - _pauseTimestamp;
			if(isRunning){
				_timestamp = intCycle(_timestamp+pass);
			}
			_pauseTimestamp = -1;
		}
		
		public function stop():void
		{
			_timestamp = -1;
			_pauseTimestamp = -1;
		}
		
		public function countdown(millisecond:int = 0,second:int = 0,minute:int = 0,hour:int = 0,day:int = 0):void
		{
			_timestamp = -1;
			_interval = ONE_SECOND;
			var time:Number = calculateTime(millisecond,second,minute,hour,day);
			_totalCount = Math.ceil(time / ONE_SECOND);
			if(_interval >0){
				_timestamp = now();
			}
		}
		
		public function alert(millisecond:int = 0,second:int = 0,minute:int = 0,hour:int = 0,day:int = 0):void
		{
			_timestamp = -1;
			_interval = calculateTime(millisecond,second,minute,hour,day);
			_totalCount = 1;
			if(_interval >0)_timestamp = now();
		}
		
		private function calculateTime(millisecond:int = 0,second:int = 0,minute:int = 0,hour:int = 0,day:int = 0):Number
		{
			var interval:Number = 0;
			interval += day*24*60*60*60*1000;
			interval += hour*60*60*1000;
			interval += minute*60*1000;
			interval += second*1000;
			interval += millisecond;
			
			return interval;
		}
		
		private function now():int
		{
			var n:int = getTimer();
			return intCycle(n);
		}
		
		private function diff(t1, t2):int
		{
//			over int MAX_VALUE
			if(t1 < t2){
				return (int.MAX_VALUE - t2 + t1);
			}else{
				return t1 - t2;
			}
		}
		
		private function intCycle(value:int):int
		{
			if(value<0)
				return value + int.MAX_VALUE+1;
			else
				return value;
		}
	}
}