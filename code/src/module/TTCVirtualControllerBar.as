package module
{
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import event.GameEvent;
	
	
	public class TTCVirtualControllerBar extends Sprite
	{
		public function TTCVirtualControllerBar()
		{
			super();
			
		}
		
		protected function fnExit(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_EXIT_EVENT);
			dispatchEvent(ev);
		}
		
		protected function fnPause(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_PAUSE_EVENT);
			dispatchEvent(ev);
		}
		
		protected function fnResume(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_RESUME_EVENT);
			dispatchEvent(ev);
		}
		
		protected function fnSkip(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_SKIP_EVENT);
			dispatchEvent(ev);
		}
		
		protected function fnReplay(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_REPLAY_EVENT);
			dispatchEvent(ev);
		}		
	
		protected function  fnMute(event:MouseEvent):void
		{
			var nVolume:Number = parseInt(LocalSaver.instance.getValue("MusicVolume"));
			if(nVolume == 0)
			{
				LocalSaver.instance.save("MusicVolume",1);
			}
			else
			{
				LocalSaver.instance.save("MusicVolume",0);
			}
			
			EventManager.instance.trigger(new Event("ChangeMusicVolume"));
		}
	}
}