package com.hoimi.lib.text
{
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	public class VText extends Sprite
	{
		private var _words:Array = [];
		private var _texts:Array = [];
		private var _size:int;
		private var _format:TextFormat;
		private var _gap:uint = 0;
		private var _spiltMax:uint;
		
		public function VText(words:String,font:String = "Times", size:int = 48, color:int = 0xffffff, gap:uint = 5, spiltMax:uint = 6)
		{
			super();
			_words = words.split("");
			_size = size;
			_format = new TextFormat(font,size,color);
			_gap = gap;
			_spiltMax = spiltMax;
			initTexts();
		}
		
		public function set size(value:uint):void
		{
			_format.size = value;
			initTexts();
		}
		
		public function set gap(value:uint):void
		{
			_gap = value;
		}
		
		public function get gap():uint
		{
			return _gap;
		}
		
		public function set text(value:String):void
		{
			_words = value.split("");
			initTexts();
		}
		
		public function setColor(index:int, color:uint):void
		{
			if(index > _texts.length) return;
			if(_texts[index] == null) return;
			var tf:TextFormat = new TextFormat(_format.font,_format.size,color);
			_texts[index].setTextFormat(tf);
		}
		
		public function setVisible(index:int, value:Boolean):void
		{
			if(index > _texts.length) return;
			if(_texts[index] == null) return;
			_texts[index].visible = value;
		}
		
		public function setBorder(index:int, color:uint):void
		{
			if(index > _texts.length) return;
			if(_texts[index] == null) return;
			_texts[index].border = true;
			_texts[index].borderColor = color;
		}
		
		public function setBackground(index:int, color:uint):void
		{
			if(index > _texts.length) return;
			if(_texts[index] == null) return;
			_texts[index].background = true;
			_texts[index].backgroundColor = color;
		}
		
		public function get fontHeight():Number
		{
			return _texts[0].height; 
		}
		
		public function get fontWidth():Number
		{
			return _texts[0].width; 
		}
		
		public function getFontXY(index:uint):Point
		{
			//return TextField(_texts[index]).localToGlobal(new Point(this.x,this.y));
			return new Point(_texts[index].x, _texts[index].y);
		}
		
		override public function set x(value:Number):void
		{
			super.x = value - (this.width / 2);
		}
		
		override public function set y(value:Number):void
		{
			super.y = value - (this.height / 2);
		}
		
		private function initTexts():void
		{
			this.removeChildren();
			var i_:int,len_:int;
			var text_:TextField;
			len_ = _words.length;
			var spilt_:Boolean = (len_ >= _spiltMax ? true : false); 
			//trace(" this x " + this.x + " y " + this.y );
			for(i_ = 0; i_ < len_; i_++)
			{
				text_ = new TextField();
				text_.selectable = false;
				text_.autoSize = TextFieldAutoSize.CENTER;
				text_.text = _words[i_];
				text_.setTextFormat(_format);
				text_.embedFonts = true;
				this.addChild(text_);
				if(spilt_)
				{
					if(i_ < len_ / 2)
					{
						text_.x = (text_.width * 1.5);
						text_.y = (_gap + text_.height) * i_;
					}
					else
					{
						
						text_.x = 0;//-(text_.width / 1.5);
						text_.y = (_gap + text_.height) * ((i_ % (len_ / 2) + 0.3));
					}
				}
				else
				{
					text_.x = 0 ;
					text_.y = (_gap + text_.height) * i_;
				}
				//trace("i " + i_ + " word " + text_.text + " x " + text_.x + " y " + text_.y);
				_texts[i_] = text_;
			}
		}
	}
}

