package ui
{
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	
	import flash.display.Sprite;
	
	import Enum.EnumLocalSave;
	
	import event.GameEvent;
	
	import item.HoimiExpSmall;
	
	import lib.TweenerBashAni;
	
	public class ExpBar extends Sprite
	{
		public var m_mc:Poker_Exp_Bar = new Poker_Exp_Bar();
		private var m_sp:Sprite = new Sprite();
		private var m_spLV:Sprite = new Sprite();
		private var m_spExp:Sprite = new Sprite();
		private var m_tweener:TweenerBashAni= new TweenerBashAni();
		private var exp:HoimiExpSmall = new HoimiExpSmall();
		private var lv:HoimiExpSmall = new HoimiExpSmall();
		private var expNext:HoimiExpSmall = new HoimiExpSmall();
		private var m_bLevelUp:Boolean =false;
		
		public function ExpBar()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_sp);
			m_sp.addChild(m_spLV);
			m_sp.addChild(m_spExp);
			m_spExp.y = 30
			//m_spExp.x = 600
		//	m_spExp.scaleX = 0.8
			//m_spExp.scaleY = 0.8
//			fnExpChange();
//			fnNickNameChange();
			EventManager.instance.register(GameEvent.GAME_EXP_CHANGE_EVENT, fnExpChange);
			EventManager.instance.register(GameEvent.GAME_NICKNAME_CHANGE_EVENT, fnNickNameChange);
//			m_mc.txt_name.text = GameConfig.game_playerInfo.nickname;
			m_mc.txt_name.mouseEnabled=false;
			
			
		}
		
		private function fnNickNameChange(e:GameEvent = null):void
		{
			m_mc.txt_name.text = GameConfig.game_playerInfo.nickname;
		}
		
		private function fnExpChange(e:GameEvent = null):void
		{
		//	m_tweener.fnStopTween();
			m_spLV.removeChildren();
			m_spExp.removeChildren();
			
			lv.fnSetValue(GameConfig.game_playerInfo.level);
			lv.y = 34
			if(GameConfig.game_playerInfo.level <10)
			{
				lv.x = 112
			}
			else if(GameConfig.game_playerInfo.level <100)
			{
				lv.x = 125
			}
			else
			{
				lv.x = 137
			}
			m_spLV.addChild(lv);
			var slash:Poker_Number_Exp_Small
			if(GameConfig.game_playerInfo.next_exp ==0)
			{
				exp.fnSetValue2(0);
				m_spExp.addChild(exp);
				slash = new Poker_Number_Exp_Small();
				slash.x = exp.width+22
				slash.gotoAndStop(13);
				m_spExp.addChild(slash);
				expNext.fnSetValue2(0);
				expNext.x = exp.width + slash.width
				m_spExp.addChild(expNext);
				m_spExp.x = 680 - m_spExp.width
				m_mc.exp.gotoAndStop(m_mc.exp.totalFrames);
			}
			else
			{
				exp.fnSetValue2(GameConfig.game_playerInfo.exp);
				m_spExp.addChild(exp);
				slash = new Poker_Number_Exp_Small();
				slash.x = exp.width+22
				slash.gotoAndStop(13);
				m_spExp.addChild(slash);
				expNext.fnSetValue2(GameConfig.game_playerInfo.next_exp);
				expNext.x = exp.width + slash.width
				m_spExp.addChild(expNext);
				m_spExp.x = 680 - m_spExp.width
				
				fnSetExpBar();
			}
			
			//fnSetResultExp(1000)
		}
		
		private function fnSetExpBar():void
		{
			var nExp:Number = GameConfig.game_playerInfo.exp;
			var nNextExp:Number =GameConfig.game_playerInfo.next_exp;
			var ratio:int = nExp/nNextExp * 100
			m_mc.exp.gotoAndStop(ratio);
		}
		
		
		
		public function fnDestory():void
		{
		//	EventManager.instance.remove(GameEvent.GAME_EXP_CHANGE_EVENT, fnExpChange);
		}
	}
}