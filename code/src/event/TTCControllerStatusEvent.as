package event
{
	import flash.events.Event;
	
	public class TTCControllerStatusEvent extends Event
	{
		public static var STATUS_EVENT:String="STATUS_EVENT"
		public var m_bPauseResumeEnable:Boolean = false;
		public var m_bSkipEnable:Boolean = false;
		public var m_bReplayEnable:Boolean = false;
		
		public function TTCControllerStatusEvent(type:String, m_bPauseResumeEnable:Boolean,m_bSkipEnable:Boolean,m_bReplayEnable:Boolean )
		{
			super(type);
			this.m_bPauseResumeEnable = m_bPauseResumeEnable;
			this.m_bSkipEnable = m_bSkipEnable;
			this.m_bReplayEnable = m_bReplayEnable;
		}
	}
}