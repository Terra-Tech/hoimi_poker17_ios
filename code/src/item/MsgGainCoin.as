package item
{
	import flash.display.Sprite;
	
	import handler.TxtLangTransfer;
	
	public class MsgGainCoin extends Sprite
	{
		private var m_mc:Poker_Msg_Gain_Coin = new Poker_Msg_Gain_Coin();
		private var m_coDiamondNumber:HoimiDiamond = new HoimiDiamond();
		private var m_coCoinNumber:HoimiCoin = new HoimiCoin();
		
		public function MsgGainCoin()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_coDiamondNumber);
			this.addChild(m_coCoinNumber);
			m_coDiamondNumber.x = -200;
			m_coDiamondNumber.y = 52
			m_coCoinNumber.x = 120
			m_coCoinNumber.y = 52
		
		
		}
		
		public function fnSetData(nDiamond:Number,nCoin:Number,nRate:Number):void
		{
			m_coDiamondNumber.fnSetValue2(nDiamond);
			m_coCoinNumber.fnSetValue2(nCoin);
			m_mc.txt_use.text = TxtLangTransfer.USE
			m_mc.txt_gain.text = TxtLangTransfer.GAIN
			m_mc.txt_rate.text = TxtLangTransfer.CRITICAL + "X "+nRate;
			if(nRate == 1)
			{
				m_mc.txt_rate.visible=false;
			}
			else
			{
				m_mc.txt_rate.visible = true
			}
		}
	}
}