package Prize.Model
{
	import com.hoimi.lib.net.JsonObject;

	public class PrizeAmount extends JsonObject
	{
		public var prize_amount:int = 0;
		
		public function PrizeAmount(data:* = null)
		{
			super(data);
		}
	}
}