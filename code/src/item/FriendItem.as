package item
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	
	import FB.Model.FacebookFriend;
	
	import handler.TxtLangTransfer;
	
	import lib.CoinValueTransfer;
	
	public class FriendItem extends Sprite
	{
		
		private var m_mc:Poker_Friends_Item_Bg = new Poker_Friends_Item_Bg();
		
		private var m_loader:Loader = new Loader();
		private var m_image:Array = [];
		private var _data:FacebookFriend;
		
		public function FriendItem(f:FacebookFriend)
		{
			super();
			this.addChild(m_mc);
			_data = f;
			m_mc.txt_name.text = f.name
			m_mc.txt_level.text = "LV "+f.level.toString();
			m_mc.txt_win_lose.text = f.win.toString() + TxtLangTransfer.WIN + "  " +f.lose.toString()+TxtLangTransfer.LOSE  
			if(f.win == 0)
			{
				m_mc.txt_rate.text = TxtLangTransfer.WIN_LOSE_RATE + ": 0.00%"
			}
			else
			{
				m_mc.txt_rate.text = TxtLangTransfer.WIN_LOSE_RATE + ": "+((f.win/(f.win+f.lose)) * 100).toFixed(2) + "%"
			}

			m_mc.txt_coin.text = CoinValueTransfer.fnCoinValueTransferTS(f.coin);
			loadPicture();
		}
		
		private function loadPicture():void{
			if(_data.image){
				_data.image.x = -this.width / 2;
				this.addChild(_data.image);
			}else{
				m_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeHandler);
				m_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
				m_loader.x = -this.width / 2;
				this.addChild(m_loader);
				m_loader.load(new URLRequest("https://graph.facebook.com/"+_data.id+"/picture?width=120&height=120"));
			}
		}
		
		protected function errorHandler(event:IOErrorEvent):void
		{
			trace('load fb pic error');
		}
		
		protected function completeHandler(event:Event):void
		{
			trace('load fb pic complete');
			_data.image = m_loader.content;
		}		
		
	}
}