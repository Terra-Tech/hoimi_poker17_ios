package handler
{
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.text.Font;
	
	import event.GameEvent;

	public class HoimiFont01LoadHandler extends EventDispatcher
	{
		private static var m_Instance:HoimiFont01LoadHandler;
		
		public var embeddedFont:Font;
		
		public function HoimiFont01LoadHandler()
		{
		}
		
		public static function fnGetInstance(): HoimiFont01LoadHandler 
		{
			if ( m_Instance == null )
			{
				m_Instance = new HoimiFont01LoadHandler();
			}
			return m_Instance;
		}
		
		public function fnLoadFont():void
		{
			fnLoadSwf();
		}
		
		private function fnLoadSwf():void
		{
			
			if(embeddedFont !=null)
			{
				trace("embeddedFont01 !=null");
				fnFontLoadComplete();
			}
			else
			{
				var myLoader:Loader = new Loader();                     // create a new instance of the Loader class
				var url:URLRequest = new URLRequest("resource/swf/Hoimi_Font_01.swf"); // in this case both SWFs are in the same folder 
				myLoader.load(url);                                     // load the SWF file
				myLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,fnSwfLoadCompleteEventHandler);
				myLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,fnIoErrorEventHandler);
				
			}
		}
		
		private function fnIoErrorEventHandler(e:IOErrorEvent):void
		{
			trace("fnLoadSwf error :" + e.text)
		}
		
		private function fnSwfLoadCompleteEventHandler(e:Event):void
		{
			trace("get Hoimi_Font_01");
			var loaderInfo:LoaderInfo = e.currentTarget as LoaderInfo
			loaderInfo.removeEventListener(Event.COMPLETE,fnSwfLoadCompleteEventHandler);
			if(loaderInfo.applicationDomain.hasDefinition("Hoimi_Font_01"))
			{
				var cls: Class = loaderInfo.applicationDomain.getDefinition( "Hoimi_Font_01" ) as Class;
				embeddedFont = new cls();
				Font.registerFont(cls); // very important to registerFont, embedFonts set true
				fnFontLoadComplete();
			}
			else
			{
				trace("error : getDefinitionByName null");	
			}
		}
		
		private function fnFontLoadComplete():void
		{
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_FONT_01_LOAD_COMPLETE_EVENT);
			dispatchEvent(ev);
		}
		
	}
}