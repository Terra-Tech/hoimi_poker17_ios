package module
{
	import com.hoimi.util.EventManager;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import event.TTCControllerStatusEvent;
	import event.GameEvent;
	
	

	public class TTCSectionStatusController extends EventDispatcher
	{
		private static var m_Instance:TTCSectionStatusController;
		public static const STATUS_OPENING:String = "STATUS_OPENING";
		public static const STATUS_GAME:String = "STATUS_GAME";
		public static const STATUS_SECTION:String = "STATUS_SECTION";
		public static const STATUS_SECTION_WITHOUT_SKIP:String = "STATUS_SECTION_WITHOUT_SKIP";
		public static const STATUS_TEST:String = "STATUS_TEST";
		public var m_bStatusPause:Boolean = false;
		private var bPauseResumeEnable:Boolean = false;
		private var bSkipEnable:Boolean = false;
		private var bReplayEnable:Boolean = false;
		
		public function TTCSectionStatusController()
		{
			fnInit();
		}
		
		public static function fnGetInstance(): TTCSectionStatusController 
		{
			if ( m_Instance == null )
			{
				m_Instance = new TTCSectionStatusController();
			}
			return m_Instance;
		}
		
		private function fnInit():void
		{
			EventManager.instance.register("ExitGame",fnExit)
			EventManager.instance.register("Resume", fnResume);
			EventManager.instance.register("Pause", fnPause);
			EventManager.instance.register("Replay", fnReplay);
			EventManager.instance.register("Skip", fnSkip);
		}
		
		private function fnResume(e:Event):void
		{
			m_bStatusPause = false;
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_RESUME_EVENT);
			dispatchEvent(ev);
		}
		
		private function fnPause(e:Event):void
		{
			m_bStatusPause = true;
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_PAUSE_EVENT);
			dispatchEvent(ev);
		}
		
		private function fnReplay(e:Event):void
		{
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_REPLAY_EVENT);
			dispatchEvent(ev);
		}
		
		private function fnSkip(e:Event):void
		{
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_SKIP_EVENT);
			dispatchEvent(ev);
		}
		
		private function fnExit(e:Event):void
		{
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_EXIT_EVENT);
			dispatchEvent(ev);
		}
		
		public function fnSetControllerStatus(status:String):void
		{
			bPauseResumeEnable = false;
			bSkipEnable = false;
			bReplayEnable = false;
			if(status == STATUS_GAME)
			{
				bPauseResumeEnable = true;
				bReplayEnable = true;
			}
			else if(status == STATUS_SECTION)
			{
				bPauseResumeEnable = true;
				bSkipEnable = true;
				bReplayEnable = true;
			}
			else if(status == STATUS_TEST)
			{
				bReplayEnable = true;
			}
			else if(status == STATUS_SECTION_WITHOUT_SKIP)
			{
				bPauseResumeEnable = true;
				bReplayEnable = true;
			}
			
			var ev:TTCControllerStatusEvent= new TTCControllerStatusEvent(TTCControllerStatusEvent.STATUS_EVENT,bPauseResumeEnable,bSkipEnable,bReplayEnable);
			EventManager.instance.trigger(ev);
	
		}
		
		public function fnDestory():void
		{
			EventManager.instance.remove("ExitGame",fnExit)
			EventManager.instance.remove("Resume", fnResume);
			EventManager.instance.remove("Pause", fnPause);
			EventManager.instance.remove("Replay", fnReplay);
			EventManager.instance.remove("Skip", fnSkip);
		}
		
	}
}