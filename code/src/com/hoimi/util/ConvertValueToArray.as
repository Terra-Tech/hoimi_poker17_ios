package com.hoimi.util
{
	public class ConvertValueToArray
	{
		public function ConvertValueToArray()
		{
		}
		
		public static function fnConvertValueToArray(value:int):Array
		{
			var ar:Array =new Array();
			for(var i:int=0 ; ;i++)
			{
				var str:String = value.toString().charAt(i); 
				if(str=="")
				{
					break;
				}
				else
				{
					ar.push(str);
				}
			}
			return ar;
		}
	}
}