package menu
{
	import flash.display.Sprite;
	
	import base.TiDisableTextField;
	
	public class MenuFriends extends Sprite
	{
		private var m_mc:Poker_Btn_Menu_Friends = new Poker_Btn_Menu_Friends();
		public function MenuFriends()
		{
			super();
			this.addChild(m_mc);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
		}
	}
}