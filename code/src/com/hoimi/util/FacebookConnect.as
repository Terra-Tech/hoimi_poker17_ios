package com.hoimi.util
{
	import com.freshplanet.ane.AirFacebook.Facebook;
	
	import flash.events.StatusEvent;
	

	public class FacebookConnect
	{
		public static const LOGIN_SUCESSED:String = "LoginSucess";
		public static const PERMISSION_READ:String = "Read";
		public static const PERMISSION_POST:String = "Post";
//		private static const APP_ID:String = "603750736373179";
//		private static const PERMISSIONS:Array = ["email", "user_about_me", "user_birthday", "user_hometown", "user_website", "offline_access", "read_stream", "publish_stream", "read_friendlists"];
//		private static const PUBLISH_PERMISSIONS:Array = ["publish_actions"];
		
//		private static const READ_PERMISSIONS:Array = ["read_stream", "read_friendlists"];
//		private static const POST_PERMISSIONS:Array = ["publish_stream"];
		
		private var _facebook:Facebook;
		private var _isConnected:Boolean;
		
//		private var _inited:Boolean;
		
		static private var _instance:FacebookConnect;
		
		static public function get instance():FacebookConnect
		{
			if(_instance == null)
			{
				_instance = new FacebookConnect(new FacebookConnectSingletonClass);
			}
			return _instance
		}
		
		public function FacebookConnect($singleton:FacebookConnectSingletonClass) 
		{}
		
		public function destroy():void
		{
			
		}
		
		public function get isConnected():Boolean
		{
			return _isConnected;
		}
		
		public function set isConnected($value:Boolean):void
		{
			//HighWay.debugTrace("\n set isConnected:"+$value, false);
			_isConnected = $value;
		}
		
		public function get isSessionOpen():Boolean
		{
			return _facebook.isSessionOpen;
		}
		
		public function get isSupported():Boolean
		{
			return Facebook.isSupported;
		}
		
		public function get accessToken():String
		{
			return _facebook.accessToken;
		}
		
		public function init($appID:String):void
		{
			if(Facebook.isSupported)
			{
				_facebook = Facebook.getInstance();
				_facebook.logEnabled = true;
				_facebook.addEventListener(StatusEvent.STATUS, handler_status, false, 0, true);
				_facebook.init($appID,false);
				showInfo('facebook.accessToken:', _facebook.accessToken + " \nsessionOpen:"+_facebook.isSessionOpen);
//				if(_facebook.isSessionOpen)
//				{
//					loginSuccess(0);
//					_facebook.dialog("oauth", null, handler_dialog, true);
//					requestInfo();
//				}
//				else
//				{
//					login();
//				}
			}
		}
		
		public function logout():void
		{
			_facebook.closeSessionAndClearTokenInformation();
			logoutSuccess();
		}
		/// callback($success:Boolean, $userCancelled:Boolean, $error:String = null)
		public function inviteFriends($callback:Function):void
		{
//			var content:FBAppInviteContent = new FBAppInviteContent();
//			content.appLinkUrl = "https://fb.me/"+FacebookConfig.appID;
//			content.previewImageUrl = "https://lh3.googleusercontent.com/DCjM-6gj-5MWbff5xvty3xxMTolke6Cfq2VTLdEpmqm6eVBt-gt0EjQspfEhYy7Q-6s=h310";
//			Facebook.getInstance().appInviteDialog(content, $callback);
			
			Facebook.getInstance().dialog('apprequests',{'title':'17Poker','message':'快來17Poker一起同樂吧！朋友越多獎金越多喔！'}, onInviteDialogFinished, true);
		}
		
		private function onInviteDialogFinished(obj:Object):void
		{
			trace('onInviteDialogFinished '+obj);
		}
		public function requestInfo($callback:Function):void
		{
			var p:Object = {fields:"name,email,gender,age_range,birthday,picture"};
			Facebook.getInstance().requestWithGraphPath("/me",p,"GET",$callback);
			//			_facebook.requestWithGraphPath("/me/friends", null, "GET", handler_requesetWithGraphPath);
		}
		
		/// callback($data:Object)
		public function reqFriends($callback:Function):void
		{
			var p:Object = {fields:"id,name,picture"};
			Facebook.getInstance().requestWithGraphPath("/me/friends",p,"GET",$callback);
		}
		
		public function post($params, $callback:Function):void
		{
//			var params:Object = { message:$message, name:$name, description:$description, link:$link, picture:$picture }
			_facebook.requestWithGraphPath("/me/feed", $params, "POST",$callback);
		}
		
		public function login($permissionType:String,$permissions:Array,$callback:Function):void
		{
			if(!_facebook.isSessionOpen)
			{
//				_facebook.openSessionWithReadPermissions(READ_PERMISSIONS, handler_openSessionWithPermissions);
//				_facebook.openSessionWithPublishPermissions(POST_PERMISSIONS, handler_openSessionWithPermissions);
				if($permissionType == FacebookConnect.PERMISSION_POST)
				{
					_facebook.openSessionWithPublishPermissions($permissions, $callback);
				}
				else
				{
					showInfo("login openSessionWithReadPermissions:"+$permissions);
					_facebook.openSessionWithReadPermissions($permissions, $callback);
				}
			}
			else
			{
				$callback(true,false,null);
				showInfo('isSessionOpen!');
			}
		}
		
		public function authorPostPermissions($permissions:Array):void
		{
			_facebook.reauthorizeSessionWithPublishPermissions($permissions);
		}
		
		private function handler_openSessionWithPermissions($success:Boolean, $userCancelled:Boolean, $error:String = null):void
		{
			if($success)
			{
				loginSuccess(1);
//				this.dispatchEvent(new Event(LOGIN_SUCESSED));
			}
			showInfo("success:", $success, ",userCancelled:", $userCancelled, ",error:", $error);
			
			//			_facebook.reauthorizeSessionWithPublishPermissions(POST_PERMISSIONS);
			
		}
		protected function handler_status($evt:StatusEvent):void
		{
			showInfo("statusEvent,type:", $evt.type,",code:", $evt.code,",level:", $evt.level);
		}
		
		private function handler_dialog($data:Object):void
		{
			showInfo('handler_dialog:', JSON.stringify($data));
		}
		
		private function handler_requesetWithGraphPath($data:Object):void
		{
			showInfo("handler_requesetWithGraphPath:", JSON.stringify($data));  
		}
		
		private function loginSuccess($step:int):void
		{
			showInfo("loginSuccess_" + $step);
			
//			post("App_Link");
			//				requestInfo();
//			_facebook.publishInstall(APP_ID);
		}
		
		private function logoutSuccess():void
		{
			showInfo("logoutSuccess !!");
		}
		
		private function showInfo(...$args):void
		{
			var msg_:String = "";
			for (var i:int = 0; i < $args.length; i++) 
			{
				msg_ += $args[i] + " ";
			}
			msg_ += "\n";
			//HighWay.debugTrace(msg_);
			trace(msg_);
		}
	}
}

class FacebookConnectSingletonClass{}
