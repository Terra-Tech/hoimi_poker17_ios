package section
{

	
	
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.freshplanet.lib.ui.util.RectangleSprite;
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	import com.soma.loader.ILoading;
	import com.soma.loader.SomaLoader;
	import com.soma.loader.SomaLoaderEvent;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	
	import Enum.EnumLocalSave;
	import Enum.EnumSection;
	
	import Prize.PrizeController;
	import Prize.PrizePanel;
	
	import base.TTCSectionBase;
	
	import event.GameEvent;
	
	import handler.MyLoading;
	import handler.MyScrollController;
	
	import item.GameTopBar;
	import item.Loading;
	
	import model.BuyingInfo;
	import model.FirstBetInfo;
	import model.GameTable;
	import model.OpenBetInfo;
	import model.PickInfo;
	import model.PlayerInfo;
	import model.SecondBetInfo;
	
	import ui.TableList;
	import ui.Teach;
	
	

	
	public class SectionLobby extends TTCSectionBase
	{
		private var m_sp:Sprite = new Sprite();
		private var m_bg:Poker_Lobby_Bg = new Poker_Lobby_Bg();
		
		private var m_coTableList:TableList = new TableList();
		
		
		
		public function SectionLobby()
		{
			super();
			this.addChild(m_bg);
			this.addChild(m_coTableList);
		
			EventManager.instance.register(GameEvent.GAME_SECTION_REFRESH,fnInitTable);	
			fnInit()
		
		}
		
		private function fnInit():void
		{
			GameConfig.game_1stBetInfo1 = null;
			GameConfig.game_table = null; 	
			GameConfig.game_openBetInfo = null
			GameConfig.game_pickInfo = null
			GameConfig.game_1stBetInfo2 = null;
			GameConfig.game_buyingInfo = null;
			GameConfig.game_2ndBetInfo= null;
		
			m_coTableList.fnInitTable();
			/*var loader:SomaLoader = new SomaLoader();
				var myLoading:MyLoading = new MyLoading();
			//	addChild(myLoading);
			//	loader.loading = myLoading as ILoading;
			// here is a shortcut:
			loader.addEventListener(SomaLoaderEvent.COMPLETE, itemComplete);
			loader.addEventListener(SomaLoaderEvent.QUEUE_COMPLETE, queueComplete);
			loader.loading = addChild(new MyLoading) as ILoading;
			loader.add("resource/icons/144.png");
			loader.add("resource/icons/36.png");
			loader.start();*/
		//	var myScroller:MyScrollController = new MyScrollController();
		//	this.addChild(myScroller);
			
		}
		
		private function fnInitTable(e:GameEvent):void
		{
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NEXT_SECTION_EVENT);
			ev.m_eventArgs =EnumSection.SECTION_LOBBY
			dispatchEvent(ev);	
				
		}
		
		/*private var nIndex:int = 0
		private function itemComplete(e:SomaLoaderEvent):void {
			var bitmap:Bitmap = e.item.file as Bitmap;
			bitmap.x = 360 
			bitmap.y =500 +nIndex *300
			nIndex++

			addChild(bitmap);
		}*/
		
		private function queueComplete(event:SomaLoaderEvent):void {
			trace("All the items have been loaded");
		}
	
	
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
		
		
	
		override public function fnResize():void
		{
			m_bg.x = 360 + GameConfig.object_adjuct_position_x;
			m_coTableList.x+= GameConfig.object_adjuct_position_x;
			
		}
		
		
		
		override public function fnPause():void
		{
			super.fnPause();
		}
		
		override public function fnResume():void
		{
			super.fnResume();
		}
		
		override public function fnSkip():void
		{
			super.fnSkip();
		}
		
		
		override public function fnReplay():void
		{
			super.fnReplay();
		}
		
		override public function fnExit():void
		{
			super.fnExit()
			EventManager.instance.remove(GameEvent.GAME_SECTION_REFRESH,fnInitTable);	
			m_coTableList.fnDestory();
			this.removeChildren();
		}
	}
}