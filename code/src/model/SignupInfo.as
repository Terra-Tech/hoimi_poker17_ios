package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class SignupInfo extends NetPacket
	{
		/**   signup
		 *  1:success {result:1}
		 * -2:already exist
		 * -3:name duplicate
		 **/
		public function SignupInfo(signup:String){
			super(signup);
		}
	}
}