package CoinBack.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class Withdraw extends JsonObject
	{
		public var coin:Number = 0;
		public var player_coin:Number = 0;
		
		public function Withdraw(str:String=null)
		{
			super(str);
		}
	}
}