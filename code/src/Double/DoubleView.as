package Double
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	
	import Double.Model.DoubleConfigs;
	import Double.Model.DoubleResult;
	
	import Enum.EnumLocalSave;
	import Enum.EnumMsg;
	import Enum.EnumStatus;
	
	import base.TiButtonState;
	import base.TiDisableTextField;
	
	import caurina.transitions.Tweener;
	
	import conf.musicConfig;
	
	import event.GameEvent;
	import event.GameMessageEvent;
	
	import handler.TTCMusicHandler;
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	import item.DecisionTiemr;
	import item.HoimiCoin;
	import item.MsgGameStatus;
	import item.Poker;
	
	import lib.UIShowHideAnimation;
	
	import ui.PlayerStatusFooter;
	
	public class DoubleView extends Sprite
	{
		private var _control:DoubleController = new DoubleController();
		private var _result:DoubleResult;
		private var _configs:DoubleConfigs;
		private var m_mc:Poker_Double = new Poker_Double();
		private var m_coFooter:DoublePlayerStatusFooter = new DoublePlayerStatusFooter();
		private var m_buttonState:TiButtonState = new TiButtonState();
		private var m_nBetOption:int = EnumDouble.JOKER;
		private var m_coCoin:HoimiCoin = new HoimiCoin();
		private var m_coGameStatus:MsgGameStatus = new MsgGameStatus();
		private var m_spCard:Sprite = new Sprite();
		private var m_spRecord:Sprite = new Sprite();
		private var m_mcPoker:Poker = new Poker(1);
		private var m_nBet:Number = 0;
		private var m_coDecisionTime:DecisionTiemr = new DecisionTiemr();
		private var m_nFinalCoin:Number = 0;	
		private var m_nCurrentCoin:Number = 0
		private var m_nAddTickCoin:int = 0;	
		private var m_timer:Timer = new Timer(10)	
			
		public function DoubleView()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_coDecisionTime);
			this.addChild(m_coCoin);
			this.addChild(m_spCard);
			this.addChild(m_spRecord);
			m_spCard.addChild(m_mcPoker);
			this.addChild(m_coFooter);
			this.addChild(m_coGameStatus);
			m_coFooter.y = -110
			m_coFooter.x = -360
			m_coGameStatus.y = 90
			m_coCoin.x = -50
			m_coCoin.y = 960
			m_spCard.x = 0
			m_spCard.y = 530;
			m_spRecord.x = -310
			m_spRecord.y = 242
			m_coDecisionTime.scaleX = 0.7
			m_coDecisionTime.scaleY = 0.7
			m_coDecisionTime.x = 200;
			m_coDecisionTime.y = 900
			m_mcPoker.fnCardHide();
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.black);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.red);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.joker);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.spade);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.club);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.heart);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.diamond);
			m_mc.black.circle.visible = false;
			m_mc.red.circle.visible = false;
			m_mc.joker.circle.visible = false;
			m_mc.spade.circle.visible = false;
			m_mc.club.circle.visible = false;
			m_mc.heart.circle.visible = false;
			m_mc.diamond.circle.visible = false;
			m_mc.black.addEventListener(MouseEvent.CLICK,fnClickBlack);
			m_mc.red.addEventListener(MouseEvent.CLICK,fnClickRed);
			m_mc.joker.addEventListener(MouseEvent.CLICK,fnClickJoker);
			m_mc.spade.addEventListener(MouseEvent.CLICK,fnClickSpade);
			m_mc.club.addEventListener(MouseEvent.CLICK,fnClickClub);
			m_mc.heart.addEventListener(MouseEvent.CLICK,fnClickHeart);
			m_mc.diamond.addEventListener(MouseEvent.CLICK,fnClickDiamond);
			m_coDecisionTime.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_mc.txt_result.visible=false;
			m_mc.txt_result.mouseEnabled =false;
			this.visible =false;
			fnDisableAllButton();
			fnSetMask();
		}
		
		private function fnGameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_TIME_FINISH)
			{
				fnClickJoker(null);
			}
		}
		
		private function fnSetMask():void
		{
			var masker:Sprite = new Sprite();
			masker.graphics.beginFill(0);
			masker.graphics.drawRect( -360 ,100 , 708 , 250);
			masker.graphics.endFill();
			this.addChild(masker);
			m_spRecord.mask = masker;
		}
	
		private function fnDisableAllButton():void
		{
			m_mc.black.mouseChildren =false;
			m_mc.black.mouseEnabled =false;
			m_mc.red.mouseChildren =false;
			m_mc.red.mouseEnabled =false;
			m_mc.joker.mouseChildren =false;
			m_mc.joker.mouseEnabled =false;
			m_mc.spade.mouseChildren =false;
			m_mc.spade.mouseEnabled =false;
			m_mc.club.mouseChildren =false;
			m_mc.club.mouseEnabled =false;
			m_mc.heart.mouseChildren =false;
			m_mc.heart.mouseEnabled =false;
			m_mc.diamond.mouseChildren =false;
			m_mc.diamond.mouseEnabled =false;
			m_buttonState.fnDisableAndDarkMcState(m_mc.black);
			m_buttonState.fnDisableAndDarkMcState(m_mc.red);
			m_buttonState.fnDisableAndDarkMcState(m_mc.joker);
			m_buttonState.fnDisableAndDarkMcState(m_mc.spade);
			m_buttonState.fnDisableAndDarkMcState(m_mc.club);
			m_buttonState.fnDisableAndDarkMcState(m_mc.heart);
			m_buttonState.fnDisableAndDarkMcState(m_mc.diamond);
		}
		
		private function fnEnableAllButton():void
		{
			m_mc.black.mouseChildren =true;
			m_mc.black.mouseEnabled =true;
			m_mc.red.mouseChildren =true;
			m_mc.red.mouseEnabled =true;
			m_mc.joker.mouseChildren =true;
			m_mc.joker.mouseEnabled =true;
			m_mc.spade.mouseChildren =true;
			m_mc.spade.mouseEnabled =true;
			m_mc.club.mouseChildren =true;
			m_mc.club.mouseEnabled =true;
			m_mc.heart.mouseChildren =true;
			m_mc.heart.mouseEnabled =true;
			m_mc.diamond.mouseChildren =true;
			m_mc.diamond.mouseEnabled =true;
			m_buttonState.fnEnableMcState(m_mc.black);
			m_buttonState.fnEnableMcState(m_mc.red);
			m_buttonState.fnEnableMcState(m_mc.joker);
			m_buttonState.fnEnableMcState(m_mc.spade);
			m_buttonState.fnEnableMcState(m_mc.club);
			m_buttonState.fnEnableMcState(m_mc.heart);
			m_buttonState.fnEnableMcState(m_mc.diamond);
		}
		
		private function fnClickBlack(e:MouseEvent):void
		{
			m_mc.black.circle.visible = true;
			fnDisableAllButton();
			m_nBetOption = EnumDouble.BLACK
			m_mc.txt_result.text = m_mc.txt_black.text	
			fnBet();
		}
		
		private function fnClickRed(e:MouseEvent):void
		{
			m_mc.red.circle.visible = true;
			fnDisableAllButton();
			m_nBetOption = EnumDouble.RED
			m_mc.txt_result.text = m_mc.txt_red.text	
			fnBet();
		}
		
		private function fnClickJoker(e:MouseEvent):void
		{
			m_mc.joker.circle.visible = true;
			fnDisableAllButton();
			m_nBetOption = EnumDouble.JOKER
			m_mc.txt_result.text = m_mc.txt_joker.text
			fnBet();
		}
		
		private function fnClickSpade(e:MouseEvent):void
		{
			m_mc.spade.circle.visible = true;
			fnDisableAllButton();
			m_nBetOption = EnumDouble.SPADE
			m_mc.txt_result.text = m_mc.txt_spade.text
			fnBet();
		}
		
		private function fnClickClub(e:MouseEvent):void
		{
			m_mc.club.circle.visible = true;
			fnDisableAllButton();
			m_nBetOption = EnumDouble.CLUB
			m_mc.txt_result.text = m_mc.txt_club.text
			fnBet();
		}
		
		private function fnClickHeart(e:MouseEvent):void
		{
			m_mc.heart.circle.visible = true;
			fnDisableAllButton();
			m_nBetOption = EnumDouble.HEART
			m_mc.txt_result.text = m_mc.txt_heart.text
			fnBet();
		}
		
		private function fnClickDiamond(e:MouseEvent):void
		{
			m_mc.diamond.circle.visible = true;
			fnDisableAllButton();
			m_nBetOption = EnumDouble.DIAMOND
			m_mc.txt_result.text = m_mc.txt_diamond.text
			fnBet();
		}
		
		public function fnDisplayPanel(nBet:Number,nWin:int,nLose:int):void
		{
			this.visible = true;
			TTCMusicHandler.fnGetInstance().fnSelectMusic(musicConfig.GAME_MUSIC_DOUBLE,0.5)
			m_nBet = nBet
			m_nCurrentCoin = nBet
			fnDoubleRecord();
			m_coFooter.fnCoinChange();
			m_coFooter.fnDiamondChange();
			
			m_coFooter.fnSetWinLoseRate(nWin,nLose);
			fnOnGetConfig();
		}
		
		private function fnDoubleRecord():void
		{
			if(LocalSaver.instance.getValue(EnumLocalSave.DOUBLE_RECORD) == undefined)
			{
				return;
			}
			var strRecord:String = LocalSaver.instance.getValue(EnumLocalSave.DOUBLE_RECORD);
			var ar:Array = strRecord.split(":");
			ar.reverse();
			var poker:Poker
			for(var i:int = 0;i< ar.length;i++)
			{
				poker = new Poker(ar[i])
				poker.scaleX = 0.6
				poker.scaleY = 0.6
				poker.x = i*(poker.width+2)
				poker.fnCardShow();
				m_spRecord.addChild(poker);
			}
		}
		
		private function fnOnGetConfig(e:GameEvent = null):void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnOnGetConfig);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			_control.fetchConfig(GameConfig.game_playerInfo.token,onRecvConf,null);
		}
		
		private function onRecvConf(e:NetEvent):void
		{
			//double conf {"result":1,"configs":[null,{"id":1,"name":"Joker","multiple":15,"cards":[16]},{"id":2,"name":"Black","multiple":2,"cards":[0,3,4,7,8,11,12,15]},{"id":3,"name":"Red","multiple":2,"cards":[1,2,5,6,9,10,13,14]},{"id":4,"name":"Spade","multiple":4,"cards":[3,7,11,15]},{"id":5,"name":"Heart","multiple":4,"cards":[2,6,10,14]},{"id":6,"name":"Diamond","multiple":4,"cards":[1,5,9,13]},{"id":7,"name":"Club","multiple":4,"cards":[0,4,8,12]}]}
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnOnGetConfig);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			trace("double conf " + e.data);
			switch(e.result){
				case 1:
					_configs = new DoubleConfigs(e.data);
					fnAfterGetConfig();
					break;
				default:
					trace("Error "+ e.result);
					break;
			}
		}
		
		private function fnAfterGetConfig():void
		{
			var nJoker:int = _configs.configs[1].multiple;
			var nBlack:int = _configs.configs[2].multiple;
			var nRed:int = _configs.configs[3].multiple;
			var nSpade:int = _configs.configs[4].multiple;
			var nHeart:int = _configs.configs[5].multiple;
			var nDiamond:int = _configs.configs[6].multiple;
			var nClub:int = _configs.configs[7].multiple;
			m_mc.txt_joker.mouseEnabled =false;
			m_mc.txt_black.mouseEnabled =false;
			m_mc.txt_red.mouseEnabled =false;
			m_mc.txt_spade.mouseEnabled =false;
			m_mc.txt_heart.mouseEnabled =false;
			m_mc.txt_diamond.mouseEnabled =false;
			m_mc.txt_club.mouseEnabled =false;
			m_mc.txt_joker.text = "x" + nJoker.toString();
			m_mc.txt_black.text = "x" +nBlack.toString();
			m_mc.txt_red.text = "x" +nRed.toString();
			m_mc.txt_spade.text = "x" +nSpade.toString();
			m_mc.txt_heart.text = "x" +nHeart.toString();
			m_mc.txt_diamond.text = "x" +nDiamond.toString();
			m_mc.txt_club.text = "x" +nClub.toString();
			m_mc.txt_bet.text = "壓注金額";
			m_coDecisionTime.fnStartResultTime();
			setTimeout(function():void 
			{
				m_coFooter.fnBetCoins(-m_nBet);
			}, 750)
			
			setTimeout(function():void 
			{
				m_coCoin.fnSetValue(m_nBet);
				TTCSoundHandler.fnGetInstance().fnEmbeddedSoundCoin();
				fnEnableAllButton();
			}, 1000)
		}
		
		private function fnBet(e:GameEvent=null):void
		{
			m_coDecisionTime.fnCountDownStop();
			m_coDecisionTime.removeEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnBet);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			_control.bet(GameConfig.game_playerInfo.token, GameConfig.game_openBetInfo.game_id, m_nBetOption, m_nBet, onSettle,null);
		}
		
		private function onSettle(e:NetEvent):void
		{
			trace("double settle " + e.data);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnBet);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			TTCMusicHandler.fnGetInstance().fnStopMusic();
			switch(e.result){
				case 1:
					_result = new DoubleResult(e.data);
					fnResult();
					break;
				case -5:
					trace("Money not enought!!");
					break;
				case -6:
					trace("Option not exist!!");
					break;
				default:
					trace("Error "+ e.result);
					break;
			}
		}
		
		private function fnResult():void
		{
			//double settle {"result":1,"game_id":141,"result_card":4,"win_lose":400,"player_coin":196721700}
			setTimeout(function():void 
			{
				m_mcPoker = new Poker(_result.result_card);
				m_mcPoker.fnCardShow()
				m_spCard.addChild(m_mcPoker);
				TTCSoundHandler.fnGetInstance().fnEmbeddedSound055();
				fnSaveRecord();
			}, 500)
			GameConfig.game_playerInfo.coin = _result.player_coin;
			if(_result.win_lose > 0 )
			{
				m_nFinalCoin =  _result.win_lose;
			}
			else
			{
				m_nFinalCoin =  0;
			}
		
			m_nAddTickCoin = (m_nFinalCoin - m_nCurrentCoin)/100
			if(m_nAddTickCoin >= 0)
			{
				setTimeout(function():void 
				{
					m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PLAYER_WIN;
					m_mc.txt_result.visible = true;
					TTCSoundHandler.fnGetInstance().fnEmbeddedSoundWin();
					m_timer.addEventListener(TimerEvent.TIMER,fnTimerTickPlus);
					m_timer.start();
				}, 1500)
				setTimeout(function():void 
				{
					fnHideDouble();
				}, 8000)
			}
			else
			{
				setTimeout(function():void 
				{
					m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PLAYER_LOSE;
					TTCSoundHandler.fnGetInstance().fnEmbeddedSoundLose();
					m_timer.addEventListener(TimerEvent.TIMER,fnTimerTickMinus);
					m_timer.start();
				}, 1500)
				setTimeout(function():void 
				{
					fnHideDouble();
				}, 5000)
			}
			
		}
		
		private function fnTimerTickPlus(e:TimerEvent):void
		{
			m_nCurrentCoin += m_nAddTickCoin
			if(m_nCurrentCoin >=  m_nFinalCoin)	
			{
				m_nCurrentCoin = m_nFinalCoin
				m_timer.stop();
				m_timer.removeEventListener(TimerEvent.TIMER,fnTimerTickPlus);
				Tweener.addTween(m_coCoin,{scaleX:1.5,scaleY:1.5,time:0.5,transition:"easeOutBack",onComplete:fnAfterCoinEffect});
			
			}
			m_coCoin.fnSetValue(m_nCurrentCoin);
		}
		
		private function fnTimerTickMinus(e:TimerEvent):void
		{
			m_nCurrentCoin += m_nAddTickCoin
			if(m_nCurrentCoin <=  m_nFinalCoin)	
			{
				m_nCurrentCoin = m_nFinalCoin
				m_timer.stop();
				m_timer.removeEventListener(TimerEvent.TIMER,fnTimerTickMinus);
			//	Tweener.addTween(m_coCoin,{scaleX:1.5,scaleY:1.5,time:0.5,transition:"easeOutBack",onComplete:fnAfterCoinEffect});
				
			}
			m_coCoin.fnSetValue(m_nCurrentCoin);
		}
		
		private function fnAfterCoinEffect():void
		{
			Tweener.addTween(m_coCoin,{scaleX:1,scaleY:1,time:0.5,transition:"easeOutBack",onComplete:fnAfterCoinEffect2});
		}
		
		private function fnAfterCoinEffect2():void
		{
			if(_result.win_lose>0)
			{
				m_coFooter.fnBetCoins(_result.win_lose);
			}
		}
		
		private function fnSaveRecord():void
		{
			var strRecord:String = LocalSaver.instance.getValue(EnumLocalSave.DOUBLE_RECORD);
			var ar:Array = new Array();
			if(strRecord)
			{
				ar = strRecord.split(":");
			}
			
			if(ar.length > 7)
			{
				ar.shift();
			}
			ar.push(_result.result_card);
			strRecord = "";
			for(var i:int = 0;i<ar.length;i++)
			{
				if(i == ar.length-1)
				{
					strRecord += ar[i]	
				}
				else
				{
					strRecord +=ar[i]+":"
				}
			}
			LocalSaver.instance.save(EnumLocalSave.DOUBLE_RECORD,strRecord);
			
			Tweener.addTween(m_spRecord,{x:-224.2,time:0.5,transition:"easeOutExpo",onComplete:fnAfterSaveRecord});
			
		}
		
		private function fnAfterSaveRecord():void
		{
			var poker:Poker = new Poker(_result.result_card);
			poker.fnCardShow();
			poker.x = -310
			poker.y = 242;
			poker.scaleX = 0;
			poker.scaleY = 0;
			Tweener.addTween(poker,{scaleX:0.6,scaleY:0.6,time:0.25,transition:"easeOutBack"});
			this.addChild(poker);
			var m_mcAni:Poker_Circle_Ani = new Poker_Circle_Ani();
			this.addChild(m_mcAni);
			m_mcAni.x = -310
			m_mcAni.y = 242
			m_mcAni.gotoAndPlay(2);
		}
		
		private function fnHideDouble():void
		{
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_DOUBLE_FINISH_EVENT,true);
			dispatchEvent(ev);
			this.visible =false;
		/*	TTCMusicHandler.fnGetInstance().fnSelectMusic(musicConfig.BATTLE_MUSIC,0.5);
			setTimeout(function():void 
			{
				TTCMusicHandler.fnGetInstance().fnPauseMusic();
				var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_DOUBLE_FINISH_EVENT,true);
				dispatchEvent(ev);
						
			}, 200)*/
			
		}

	
		
		public function fnResume():void
		{
			if(m_coDecisionTime.visible)
			{
				m_coDecisionTime.fnResume()
			}
		}
		
		public function fnPause():void
		{
			m_coDecisionTime.fnPause();
		}
	}
}