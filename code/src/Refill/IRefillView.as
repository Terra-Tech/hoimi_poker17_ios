package Refill
{
	public interface IRefillView
	{
		// 網路發生問題
		function netError(msg:String):void;
		/// Server 吐錯誤訊息
		function serverError(msg:String):void;
		/// 如打開領獎Panel show loading
		function showLoading():void;
		/// 如打開領獎Panel hide loading
		function hideLoading():void;
		/// 顯示時間統一function
		function showClock(hour:Number,minute:Number,second:Number):void
		/// 顯示現在可領的金額跟上限額
		function showCoin(current:Number, max:Number):void
		/// 領了多少錢
		function showRefill(refill_coin:Number, player_coin:Number):void
		/// 額度太地無法領
		function showNoMoney():void
	}
}