package ui
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import base.TiDisableTextField;
	
	import handler.TTCSoundHandler;
	
	public class HelpPanel extends Sprite
	{
		private var m_mc:Poker_Btn_Help = new Poker_Btn_Help();
		private var m_mcPanel:Poker_Help_Panel = new Poker_Help_Panel();
		
		public function HelpPanel()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_mcPanel);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
			m_mc.addEventListener(MouseEvent.CLICK,fnClickHelp);
			m_mc.x = 500
			m_mc.y = 140
			m_mcPanel.x = 360
			m_mcPanel.y = 80
			m_mcPanel.visible =false;
			m_mcPanel.addEventListener(MouseEvent.CLICK,fnClickPanel);
		}
		
		private function fnClickHelp(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			if(m_mcPanel.visible == true)
			{
				m_mcPanel.visible = false
			}
			else
			{
				m_mcPanel.visible = true
			}
		}
		
		private function fnClickPanel(e:MouseEvent):void
		{
			m_mcPanel.visible =false;
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
		}
	}
}