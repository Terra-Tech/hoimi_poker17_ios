package CoinBack
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	
	import flash.events.Event;
	import flash.utils.getTimer;
	
	import CoinBack.Model.Withdraw;
	
	import model.PlayerInfo;

	public class CoinBackController
	{	
		/// Panel 被打開
		static public const EVENT_COIN_BACK_WITHDRAW:String = 'CoinBackWithdraw';
		
		private const TIME_COUNTDOWN:int = 1000 * 10;
		private const WITHDRAW_MIN:int = 1000;
		private var _withdraw:Withdraw = new Withdraw();
		private var _timestamp:int = -1;
		private var _withdraw_timestamp:int = -1;
		private var _view:ICoinBackView;
		private var _now:int = 0;
		
		public function CoinBackController(view:ICoinBackView)
		{
			_view = view;
			EventManager.instance.register(EVENT_COIN_BACK_WITHDRAW, withdraw);
		}
		
		private function withdraw(e:Event):void
		{
			if(GameConfig.game_playerInfo.coin_back_pool < WITHDRAW_MIN){
				_view.showBelowWithdrawMin(WITHDRAW_MIN);
				return;
			}
			_now = getTimer();
			if(_withdraw_timestamp > 0 && _now - _withdraw_timestamp < TIME_COUNTDOWN){
				_view.showWithdraw(0, _withdraw.player_coin);
			}else{
				_withdraw_timestamp = _now;
				req_withdraw();	
			}
		}
		
		private function req_withdraw():void
		{
			_view.showLoading();
			SessionManager.instance.request(Protocol.COIN_BACK_WITHDRAW, {token:GameConfig.game_playerInfo.token}, onSuccess, onFail);
		}
		
		private function onSuccess(e:NetEvent):void{
			trace("onCoinBack success : " + e.data);
			_view.hideLoading();
			var net:NetPacket = new NetPacket(e.data);
			if(e.protocol == Protocol.COIN_BACK_WITHDRAW){
				switch(net.result)
				{
					case 1:
						_withdraw.byString(e.data);
						_view.showWithdraw(_withdraw.coin, _withdraw.player_coin);
						if(_withdraw.coin > 0)
						{
							GameConfig.game_playerInfo.coin_back_pool = 0;
							_view.showPool(0);
						}
						break;
					case -4:
						_view.showBelowWithdrawMin(WITHDRAW_MIN);
						break;
					default:
						_view.serverError("coin back inner error : " + net.result);
						break;
				}	
			}
		}
		
		private function onFail(e:NetEvent):void{
			if(e.protocol == Protocol.COIN_BACK_WITHDRAW){
				_withdraw_timestamp = -1;
			}
			_view.netError("coin back net error : " + e.protocol);
		}
		
	}
}