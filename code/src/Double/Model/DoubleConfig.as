package Double.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class DoubleConfig extends JsonObject
	{
		/// id ---> 1:Joker, 2:Black, 3:Red, 4:Spade, 5:Heart, 6:Diamond, 7:Club
		public var id:int;
		public var name:String;
		public var multiple:int;
		public var cards:Array;
		
		public function DoubleConfig(obj:*=null)
		{
			super(obj);
		}
	}
}