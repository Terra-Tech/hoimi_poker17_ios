package section
{
	import com.hoimi.util.EventManager;
	import com.hoimi.util.FacebookConnect;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import FB.FacebookController;
	import FB.Model.FacebookFriend;
	
	import base.TTCSectionBase;
	import base.TiButtonState;
	import base.TiDisableTextField;
	
	import event.GameEvent;
	
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	import item.FriendItem;
	import item.HoimiExpSmall;
	
	
	public class SectionFriends extends TTCSectionBase
	{
		private var m_mcBg:Poker_Friends_Bg = new Poker_Friends_Bg();
		private var m_mcFB:Poker_Btn_Facebook = new Poker_Btn_Facebook();
		private var m_fb:FacebookController = FacebookController.instance;
		private var m_mcNext:Poker_Btn_Friends_Arrow_Right = new Poker_Btn_Friends_Arrow_Right();
		private var m_mcPrevious:Poker_Btn_Friends_Arrow_Left = new Poker_Btn_Friends_Arrow_Left();
		private var m_spFriend:Sprite = new Sprite();
		private var m_sp:Sprite = new Sprite();
		private var m_nPageIndex:int = 1;
		private var m_nStageTotal:int = 0;
		private var m_nPageTotal:int = 0;	
		private var m_coPageCurrent:HoimiExpSmall = new HoimiExpSmall();
		private var m_coPageTotal:HoimiExpSmall = new HoimiExpSmall();
		private var m_coPageSlash:Poker_Number_Exp_Small = new Poker_Number_Exp_Small();
		private var m_mcInvite:Poker_Btn_Friends_Invite = new Poker_Btn_Friends_Invite();
		private var m_mcSortLevel:Poker_Btn_Friends_Sort_Level = new Poker_Btn_Friends_Sort_Level();
		private var m_mcSortWin:Poker_Btn_Friends_Sort_Win = new Poker_Btn_Friends_Sort_Win();
		private var m_mcSortRate:Poker_Btn_Friends_Sort_Rate = new Poker_Btn_Friends_Sort_Rate();
		private var m_mcSortCoin:Poker_Btn_Friends_Sort_Coin = new Poker_Btn_Friends_Sort_Coin();
		private var m_sortSp:Sprite = new Sprite();
		private var m_buttonState:TiButtonState = new TiButtonState();
		private var m_txtInviteMsg:TextField = new TextField();
		private var m_arSort:Array = new Array();
		
		public function SectionFriends()
		{
			super();
			this.addChild(m_mcBg);
			this.addChild(m_sp);
			m_sp.addChild(m_mcFB);
			m_sp.addChild(m_txtInviteMsg);
			m_sp.addChild(m_spFriend);
			m_sp.addChild(m_mcPrevious);
			m_sp.addChild(m_mcNext);
			m_sp.addChild(m_coPageCurrent);
			m_sp.addChild(m_coPageTotal);
			m_sp.addChild(m_coPageSlash);
			m_sp.addChild(m_mcInvite);
			m_sp.addChild(m_sortSp);
			m_sortSp.addChild(m_mcSortLevel);
			m_sortSp.addChild(m_mcSortWin);
			m_sortSp.addChild(m_mcSortRate);
			m_sortSp.addChild(m_mcSortCoin);
			m_mcSortCoin.x = 144
			m_mcSortLevel.x = 288
			m_mcSortWin.x = 432
			m_mcSortRate.x = 576
			m_sortSp.y = 225
			m_mcFB.x = 360
			m_mcFB.y = 300
			m_spFriend.y = 280	
			m_mcInvite.x = 360
			m_mcInvite.y = 1180
			m_txtInviteMsg.x = 0 
			m_txtInviteMsg.y = 1100	
			m_txtInviteMsg.defaultTextFormat = new TextFormat( "Arial", 30, 0xFFFFFF, false, false, false, null, null, TextFormatAlign.CENTER );
			m_txtInviteMsg.filters = [new DropShadowFilter(1,45,0x000000,1,1,1,1,1)];
			m_txtInviteMsg.width = 720
			m_txtInviteMsg.text = TxtLangTransfer.INVITE_FB_FRIEND;

			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcFB);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcInvite);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcNext);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcPrevious);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcSortLevel);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcSortWin);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcSortRate);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcSortCoin);
			m_mcInvite.addEventListener(MouseEvent.CLICK,fnClickInvite);
			m_mcSortCoin.addEventListener(MouseEvent.CLICK,fnClickSortCoin);
			m_mcSortLevel.addEventListener(MouseEvent.CLICK,fnClickSortLevel);
			m_mcSortWin.addEventListener(MouseEvent.CLICK,fnClickSortWin);
			m_mcSortRate.addEventListener(MouseEvent.CLICK,fnClickSortRate);
			
		
				
			m_mcNext.visible=false;
			m_mcPrevious.visible =false;
			m_coPageSlash.visible =false;
			m_coPageCurrent.visible =false;
			m_mcNext.x = 600
			m_mcNext.y = 1190
			m_mcPrevious.x = 120
			m_mcPrevious.y = 1190
			
			m_coPageCurrent.x = 340
			m_coPageCurrent.y = 1205
			m_coPageTotal.x = 380
			m_coPageTotal.y = 1205
			
			m_coPageSlash.x = 370
			m_coPageSlash.y = 1205
			m_coPageSlash.gotoAndStop(13);
			
			
			EventManager.instance.register(FacebookController.EVENT_FACEBOOK_LOGIN_FAIL, fnLoginFail);
			EventManager.instance.register(FacebookController.EVENT_FACEBOOK_LOGIN_CANCEL, fnLoginFail);
			EventManager.instance.register(FacebookController.EVENT_FACEBOOK_RECV_FRIENDS, fnSetPage);
		
			if(GameConfig.game_playerInfo.fb_id == null)
			{
				showFacebookButton();
				hideSortButton();
			}
			else
			{
				m_mcFB.visible =false;
				// triger controller open event
				this.addEventListener(Event.ADDED_TO_STAGE, onAdd);
			}
		}
		
		protected function onAdd(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAdd);
			this.addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			
			if(FacebookConnect.instance.isSupported){
				m_fb.open();
			}else{
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "facebook unsupport!"
				EventManager.instance.trigger(evError);
			}
		

		}
		
		private function fnLoginFail(e:Event):void
		{
			showFacebookButton();
		}
		
		private function fnClickSortCoin(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_buttonState.fnDisableAndDarkMcState(m_mcSortCoin);
			m_buttonState.fnEnableMcState(m_mcSortLevel);
			m_buttonState.fnEnableMcState(m_mcSortWin);
			m_buttonState.fnEnableMcState(m_mcSortRate);
			m_arSort = m_fb.friendsByCoin
			m_nPageIndex = 1
			showFriends(m_arSort);
		}
		
		private function fnClickSortLevel(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_buttonState.fnDisableAndDarkMcState(m_mcSortLevel);
			m_buttonState.fnEnableMcState(m_mcSortCoin);
			m_buttonState.fnEnableMcState(m_mcSortWin);
			m_buttonState.fnEnableMcState(m_mcSortRate);
			m_arSort = m_fb.friendsByLevel
			m_nPageIndex = 1
			showFriends(m_arSort);
		}
		
		private function fnClickSortWin(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_buttonState.fnDisableAndDarkMcState(m_mcSortWin);
			m_buttonState.fnEnableMcState(m_mcSortCoin);
			m_buttonState.fnEnableMcState(m_mcSortLevel);
			m_buttonState.fnEnableMcState(m_mcSortRate);
			m_arSort = m_fb.friendsByWin
			m_nPageIndex = 1
			showFriends(m_arSort);
		}
		
		private function fnClickSortRate(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_buttonState.fnDisableAndDarkMcState(m_mcSortRate);
			m_buttonState.fnEnableMcState(m_mcSortCoin);
			m_buttonState.fnEnableMcState(m_mcSortLevel);
			m_buttonState.fnEnableMcState(m_mcSortWin);
			m_arSort = m_fb.friendsByRate
			m_nPageIndex = 1
			showFriends(m_arSort);
		}
		
		private function fnClickFB(e:MouseEvent):void
		{
			if(FacebookConnect.instance.isSupported){
				hideFacebookButton();
				m_fb.open();
			}else{
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "facebook unsupport!"
				EventManager.instance.trigger(evError);
			}
		}
		
		private function fnSetPage(e:Event):void
		{
			var nTotal:int =  m_fb.friendLength;

			m_nStageTotal = nTotal;
			
			if(m_nStageTotal%7 == 0)
			{
				m_nPageTotal = int(nTotal/7);	
			}
			else
			{
				m_nPageTotal = int(nTotal/7) + 1;
			}
			
			if(m_nPageIndex > m_nPageTotal)
			{
				m_nPageIndex = m_nPageTotal
			}
			
			if(nTotal >7 )
			{
				m_mcNext.visible = true;
				m_mcPrevious.visible = true;
				m_mcNext.addEventListener(MouseEvent.CLICK,fnNextPage);
				m_mcPrevious.addEventListener(MouseEvent.CLICK,fnPreviousPage);
				m_coPageCurrent.fnSetValue(m_nPageIndex);
				m_coPageTotal.fnSetValue2(m_nPageTotal);
				m_coPageCurrent.visible =true;
				m_coPageSlash.visible = true
				m_coPageTotal.visible = true
			}
			
//			trace('show friends level' + m_fb.friendsByLevel);
//			trace('show friends rate' + m_fb.friendsByRate);
//			trace('show friends win' + m_fb.friendsByWin);
			//var dataFriend:FacebookFriend	
//			showFriends(ar)
			
			fnClickSortCoin(null);
		}
		
		private function showFriends(ar:Array):void
		{
			m_spFriend.removeChildren();
//			var mc:FacebookFriend	
			var nStart:int = (m_nPageIndex-1) * 7
			var nEnd:int = 	(m_nPageIndex-1) * 7 + 7
				
			if(nEnd > m_nStageTotal)
			{
				nEnd = m_nStageTotal
			}
			var itemFriend:FriendItem, f:FacebookFriend;
			for(var i:int= nStart ;i< nEnd;i++)
			{
				f = ar[i];
				itemFriend = new FriendItem(f);
				itemFriend.x = 360
				itemFriend.y = int(i- nStart)*(itemFriend.height + 10)
				m_spFriend.addChild(itemFriend);
			}
			this.showSortButton();
		}
	
		protected function onRemove(event:Event):void
		{
			EventManager.instance.remove(FacebookController.EVENT_FACEBOOK_RECV_FRIENDS, fnSetPage);
			EventManager.instance.remove(FacebookController.EVENT_FACEBOOK_LOGIN_FAIL, fnLoginFail);
			EventManager.instance.remove(FacebookController.EVENT_FACEBOOK_LOGIN_CANCEL, fnLoginFail);
			EventManager.instance.remove(FacebookController.EVENT_FACEBOOK_RECV_FRIENDS, showFriends);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			this.removeEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			this.addEventListener(Event.ADDED_TO_STAGE, onAdd);
		}		
		
		
	
	
		private function fnNextPage(e:MouseEvent):void
		{
			if(m_nPageIndex!=m_nPageTotal)
			{
				m_nPageIndex++
					m_coPageCurrent.fnSetValue(m_nPageIndex);
				TTCSoundHandler.fnGetInstance().fnEmbeddedSoundPage();
				showFriends(m_arSort);
			}
		}
		
		
		private function fnPreviousPage(e:MouseEvent):void
		{
			if(m_nPageIndex!=1)
			{
				m_nPageIndex--
					m_coPageCurrent.fnSetValue(m_nPageIndex)	
				TTCSoundHandler.fnGetInstance().fnEmbeddedSoundPage();
				showFriends(m_arSort);
			}
		}
		
		private function fnClickInvite(e:MouseEvent):void
		{
			m_fb.inviteFriends();
		}
		
		private function showFacebookButton():void
		{
			m_mcFB.visible = true;
			m_mcFB.addEventListener(MouseEvent.CLICK,fnClickFB);
		}
		
		private function hideFacebookButton():void
		{
			m_mcFB.visible = false;
			m_mcFB.removeEventListener(MouseEvent.CLICK,fnClickFB);
		}
		
		private function showSortButton():void
		{
			m_mcSortLevel.visible = true;
			m_mcSortWin.visible = true;
			m_mcSortRate.visible = true;
			m_mcSortCoin.visible = true;
			m_mcInvite.visible = true;
		}
		
		private function hideSortButton():void
		{
			m_mcSortLevel.visible = false;
			m_mcSortWin.visible = false;
			m_mcSortRate.visible = false;
			m_mcSortCoin.visible = false;
			m_mcInvite.visible = false;
		}
		
		override public function fnResize():void
		{
			m_mcBg.x = 360 +  GameConfig.object_adjuct_position_x 
			m_sp.x =  GameConfig.object_adjuct_position_x
			
			
		}
		
		override public function fnPause():void
		{
			super.fnPause();
		}
		
		override public function fnResume():void
		{
			super.fnResume();
		}
		
		override public function fnSkip():void
		{
			super.fnSkip();
		}
		
		
		override public function fnReplay():void
		{
			super.fnReplay();
		}
		
		override public function fnExit():void
		{
			super.fnExit()
			this.removeChildren();
		}
	}
}