package base
{
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	

	public class TiRadioGroup extends EventDispatcher
	{

		private var m_arGroup:Array;
		private var m_strSelectName:String;
		public function TiRadioGroup()
		{
			super();
			m_arGroup = new Array();
		}
		public function fnAddItem(mc:MovieClip,strTooltip:String=""):void
		{
			var cb:TiCheckBox = new TiCheckBox();
			cb.fnSetMcState(mc,strTooltip);
			cb.addEventListener(TiButtonStateEvent.BUTTON_EVENT_MOUSE_CLICK_EVENT,fnClick);
			cb.addEventListener(TiButtonStateEvent.BUTTON_EVENT_ROLLOVER_EVENT,fnOver)
			m_arGroup.push(mc);
		}
		private function fnUnCheckAll():void
		{
			for(var i:int=0;i<m_arGroup.length;i++)
			{
				var mc:MovieClip = MovieClip(m_arGroup[i]);
				mc.seled = false;
				mc.gotoAndStop(1);
			}
		}
		private function fnOver(e:TiButtonStateEvent):void
		{
			var mc:MovieClip = MovieClip(e.m_obj);
			mc.buttonMode=true
			if(mc.seled==true)
			{
				mc.buttonMode=false;
			}
		}
			
		private function fnClick(e:TiButtonStateEvent):void
		{	
			var mc:MovieClip = MovieClip(e.m_obj);
			mc.buttonMode=false;
			if(mc.seled == true)
			{
				fnUnCheckAll();
				mc.seled = true;
				mc.gotoAndStop(3);
				m_strSelectName = mc.name;
			}
			else
			{
				mc.seled = true;
				mc.gotoAndStop(3);
				m_strSelectName = mc.name;
			}
			var ev:TiButtonStateEvent = new TiButtonStateEvent(TiButtonStateEvent.BUTTON_EVENT_MOUSE_CLICK_EVENT);
			ev.m_eventArgs = m_strSelectName;
			dispatchEvent(ev);
		}
		private function fnGetMCByName(strName:String):MovieClip
		{
			for(var i:int=0;i<m_arGroup.length;i++)
			{
				var mc:MovieClip = MovieClip(m_arGroup[i]);
				if(mc.name == strName)
					return mc;
			}
			return null;
		}
		public function fnSetSelectName(MC:MovieClip):void
		{
			var strName:String = MC.name;
			var mc:MovieClip = fnGetMCByName(strName);
			if(!mc)
				return;
			fnUnCheckAll();
			mc.seled = true;
			mc.buttonMode = false;
			mc.gotoAndStop(3);
			m_strSelectName = strName;
		}
		public function fnGetSelectName():String
		{
			return m_strSelectName;
		}
		
		public function fnUnSelectAnyOne():void
		{
			m_strSelectName = "";
			fnUnCheckAll();
		}
	}
}