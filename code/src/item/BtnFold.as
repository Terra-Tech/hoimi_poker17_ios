package item
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import Enum.EnumLang;
	
	import base.TiButtonState;
	import base.TiDisableTextField;
	
	import lib.MultiLanguagleUISelect;
	
	public class BtnFold extends Sprite
	{
		private var m_mc:MovieClip
		private var m_buttonState:TiButtonState = new TiButtonState();
		public function BtnFold()
		{
			super();
			var mcFold:Poker_Btn_Fold = new Poker_Btn_Fold();
			var mc:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(mcFold);
			m_mc = mc;
			this.addChild(mcFold);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
		}
		
		public function fnBtnNormalState():void
		{
			m_mc.gotoAndStop(1);
		}
		
		public function fnDisableBtn():void
		{
			m_buttonState.fnDisableAndDarkMcState(m_mc);
		}
	}
}