package ui
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import base.TiDisableTextField;
	
	import event.GameEvent;
	
	import handler.HoimiFont01LoadHandler;
	
	import lib.VersionNumber;
	
	public class ErrorMessagePanel extends Sprite
	{
		private var m_mcMask:Poker_Mask_Base = new Poker_Mask_Base();
		private var m_mc:Poker_Msg_Error_Panel = new Poker_Msg_Error_Panel();
		public function ErrorMessagePanel()
		{
			super();
			this.addChild(m_mcMask);
			this.addChild(m_mc);
			m_mc.x = 360
			m_mc.y = 300
			m_mcMask.x = 360
			this.visible =false;
			EventManager.instance.register(GameEvent.GAME_ERROR_MESSAGE_EVENT,fnDisplayPanel);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_ok);
			m_mc.btn_ok.addEventListener(MouseEvent.CLICK,fnClickOk);
			m_mc.txt_msg.mouseEnabled=false;
			//HoimiFont01LoadHandler.fnGetInstance().addEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			//HoimiFont01LoadHandler.fnGetInstance().fnLoadFont();
		}
		
		private function GameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_FONT_01_LOAD_COMPLETE_EVENT)
			{
				fnEmbededFont();
			}
		}
		
		private function fnEmbededFont():void
		{
			HoimiFont01LoadHandler.fnGetInstance().removeEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			var tf:TextFormat = new TextFormat();
			tf.font = HoimiFont01LoadHandler.fnGetInstance().embeddedFont.fontName;
			//tf.size = 40;
			//tf.color = "0xFF8800";
			m_mc.txt_msg.embedFonts = true; // very important to set
			m_mc.txt_msg.defaultTextFormat = tf;
			
		}
		
	/*	private function fnSetMask():void
		{
			var masker:Sprite = new Sprite();
			masker.graphics.beginFill(0);
			masker.graphics.drawRect( 0 , 0 , 720 , 1280);
			masker.graphics.endFill();
			masker.alpha = 0
			this.addChild(masker);
			
		}*/
		
		private function fnDisplayPanel(e:GameEvent):void
		{
			this.visible =true;
			var gameID:int = 0;
			if(GameConfig.game_openBetInfo)
			{
				GameConfig.game_openBetInfo.game_id
				gameID = GameConfig.game_openBetInfo.game_id
			}
			
			var nickname:String;
			if(GameConfig.game_playerInfo){
				nickname = GameConfig.game_playerInfo.nickname;
			}
			
			m_mc.txt_msg.text = e.m_eventArgs + "\nName :"+nickname+"\nUUID: "+GameConfig.UUID + "\nGame Id :" + gameID+ "\nVersion :"+ VersionNumber.fnGetVersionNumber();
		}
		
		private function fnClickOk(e:MouseEvent):void
		{
			this.visible=false;
		}
	}
}