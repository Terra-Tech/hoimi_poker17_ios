package item
{
	import flash.display.Sprite;
	
	import caurina.transitions.Tweener;
	
	
	public class LevelUpArrow extends Sprite
	{
		private var m_mc:Poker_Arrow_Level_Up = new Poker_Arrow_Level_Up();
		
		public function LevelUpArrow()
		{
			super();
			this.addChild(m_mc);
			fnTweenEffect();
		}
		
		public function fnTweenEffect():void
		{
			Tweener.addTween(m_mc,{y:-15,time:0.2,transition:"linear",onComplete:fnAfterTweenEffect});
		}
		
		private function fnAfterTweenEffect():void
		{
			Tweener.addTween(m_mc,{y:0,time:0.2,transition:"linear",onComplete:fnTweenEffect});
		}
		
	}
}