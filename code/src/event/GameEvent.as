package event
{
	public class GameEvent extends TiBasicEvent
	{
		public static const GAME_EVENT:String="GAME_EVENT";
		public static const GAME_ADD_TO_STAGE_EVENT:String="GAME_ADD_TO_STAGE_EVENT";
		public static const GAME_NEXT_SECTION_EVENT:String="GAME_NEXT_SECTION_EVENT";
		public static const GAME_PREVIOUS_SECTION_EVENT:String="GAME_PREVIOUS_SECTION_EVENT";
		public static const GAME_TIME_FINISH:String="GAME_TIME_FINISH";
		public static const GAME_ANSWER_CORRECT_EVENT:String="GAME_ANSWER_CORRECT_EVENT";
		public static const GAME_ANSWER_ERROR_EVENT:String="GAME_ANSWER_ERROR_EVENT";
		public static const GAME_DIALOG_FINISHED_EVENT:String="GAME_DIALOG_FINISHED_EVENT";
		public static const GAME_START_EVENT:String="GAME_START_EVENT";
		public static const GAME_WIN_EVENT:String="GAME_WIN_EVENT";
		public static const GAME_LOSE_EVENT:String="GAME_LOSE_EVENT";
		public static const GAME_SHAKE_STAGE_EVENT:String="GAME_SHAKE_STAGE_EVENT";
		public static const GAME_DROP_SUCCESS_EVENT:String="GAME_DROP_SUCCESS_EVENT";
		public static const GAME_EXIT_EVENT:String="GAME_EXIT_EVENT";
		public static const GAME_REPLAY_EVENT:String="GAME_REPLAY_EVENT";
		public static const GAME_RETRY_EVENT:String="GAME_RETRY_EVENT";
		public static const GAME_RESUME_EVENT:String ="GAME_RESUME_EVENT";
		public static const GAME_PAUSE_EVENT:String ="GAME_PAUSE_EVENT";
		public static const GAME_SKIP_EVENT:String ="GAME_SKIP_EVENT";
		public static const GAME_SWF_LOAD_COMPLETE_EVENT:String ="GAME_SWF_LOAD_COMPLETE_EVENT";
		public static const GAME_SWF_ACT_EVENT:String="GAME_SWF_ACT_EVENT";
		public static const GAME_MOUSE_POSITION_EVENT:String ="GAME_MOUSE_POSITION_EVENT";
		public static const GAME_COIN_CHANGE_EVENT:String = "GAME_COIN_CHANGE_EVENT";
		public static const GAME_DIAMOND_CHANGE_EVENT:String = "GAME_DIAMOND_CHANGE_EVENT";
		public static const GAME_EXP_CHANGE_EVENT:String = "GAME_EXP_CHANGE_EVENT";
		public static const GAME_NUMBER_FONT_LOAD_COMPLETE_EVENT:String = "GAME_NUMBER_FONT_LOAD_COMPLETE_EVENT";
		public static const GAME_FONT_01_LOAD_COMPLETE_EVENT:String = "GAME_FONT_01_LOAD_COMPLETE_EVENT";
		public static const GAME_IAP_EVENT:String = "GAME_IAP_EVENT";
		public static const GAME_GAIN_COIN_EVENT:String = "GAME_GAIN_COIN_EVENT";
		public static const GAME_RECONNECT_EVENT:String = "GAME_RECONNECT_EVENT";
		public static const GAME_RECONNECT_CANCEL_CEVENT:String = "GAME_RECONNECT_CANCEL_CEVENT";
		public static const GAME_ERROR_MESSAGE_EVENT:String = "GAME_ERROR_MESSAGE_EVENT";
		public static const GAME_SIGNIN_EVENT:String = "GAME_SIGNIN_EVENT";
		public static const GAME_LOADING_DISPLAY_EVENT:String = "GAME_LOADING_DISPLAY_EVENT";
		public static const GAME_LOADING_HIDE_EVENT:String = "GAME_LOADING_HIDE_EVENT";
		public static const GAME_LEVEL_UP_EVENT:String = "GAME_LEVEL_UP_EVENT";
		public static const GAME_EXP_TWEEN_FINISH_EVENT:String = "GAME_EXP_TWEEN_FINISH_EVENT";
		public static const GAME_NICKNAME_CHANGE_EVENT:String = "GAME_NICKNAME_CHANGE_EVENT";
		public static const GAME_DIAMIOND_CHANGE_COIN_FINISH_EVENT:String = "GAME_DIAMIOND_CHANGE_COIN_FINISH_EVENT";
		public static const GAME_OPENING_LOAD_COMPLETE:String = "GAME_OPENING_LOAD_COMPLETE";
		public static const GAME_SECTION_REFRESH:String = "GAME_SECTION_REFRESH";
		public static const GAME_SKILL_RAISE:String = "GAME_SKILL_RAISE";
		public static const GAME_PLAYER_RECORD_CHANGE:String = "GAME_PLAYER_RECORD_CHANGE";
		public static const GAME_COIN_BACK_CHANGE:String = "GAME_COIN_BACK_CHANGE";
		public static const GAME_DOUBLE_FINISH_EVENT:String = "GAME_DOUBLE_FINISH_EVENT";

		public function GameEvent(type:String, detail:String="",bubbles:Boolean = false)
		{
			super(type, detail, bubbles);
		}
	}
}