package model
{
	import com.hoimi.util.EventManager;
	
	import flash.events.Event;
	
	import Skill.Model.Skill;
	
	import event.GameEvent;
	
	public class PlayerInfo
	{
		private var _data:Player;
		
		public function PlayerInfo(player:String)
		{
//			super(player);
			_data = new Player(player);
		}
		
		public function get exchange_rate():int
		{
			return _data.exchange_rate;
		}

		public function get nickname():String
		{
			return _data.nickname;
		}

		public function set nickname(value:String):void
		{
			_data.nickname = value;
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_NICKNAME_CHANGE_EVENT));
		}

		public function boardcast():void{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_COIN_CHANGE_EVENT));
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_DIAMOND_CHANGE_EVENT));
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_EXP_CHANGE_EVENT));
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_NICKNAME_CHANGE_EVENT));
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_COIN_BACK_CHANGE));
		}
		
		public function get coin_back_pool():Number
		{
			return _data.coin_back_pool;
		}

		public function set coin_back_pool(value:Number):void
		{
			_data.coin_back_pool = value;
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_COIN_BACK_CHANGE));
		}

		public function get fb_id():String
		{
			return _data.fb_id;
		}

		public function set fb_id(value:String):void
		{
			_data.fb_id = value;
		}

		public function get uncomplete_winlose():Number
		{
			return _data.uncomplete_winlose;
		}

		public function set uncomplete_winlose(value:Number):void
		{
			_data.uncomplete_winlose = value;
		}

		public function get uncomplete_game_id():int
		{
			return _data.uncomplete_game_id;
		}

		public function set uncomplete_game_id(value:int):void
		{
			_data.uncomplete_game_id = value;
		}

		public function get reward_diamond():int
		{
			return _data.reward_diamond;
		}

		public function set reward_diamond(value:int):void
		{
			_data.reward_diamond = value;
		}

		public function get reward_coin():Number
		{
			return _data.reward_coin;
		}

		public function set reward_coin(value:Number):void
		{
			_data.reward_coin = value;
		}

		public function get player_record():PlayerRecord
		{
			return _data.player_record;
		}

		public function set player_record(value:PlayerRecord):void
		{
			_data.player_record = value;
		}

		public function get refill_timestamp():Number
		{
			return _data.refill_timestamp;
		}

		public function set refill_timestamp(value:Number):void
		{
			_data.refill_timestamp = value;
		}

		public function get skills_next():Array
		{
			return _data.skills_next;
		}

		public function set skills_next(value:Array):void
		{
			_data.skills_next = value;
		}

		public function get skills():Array
		{
			return _data.skills;
		}

		public function set skills(value:Array):void
		{
			_data.skills = value;
		}
		
		public function skill_raise(index:int, current:Skill, next:Skill):void
		{
			if(index < 0 || index >= _data.skills.length)return;
			_data.skills[index] = current;
			_data.skills_next[index] = next;
			var ge:GameEvent = new GameEvent(GameEvent.GAME_SKILL_RAISE);
			ge.m_eventArgs = index;
			EventManager.instance.trigger(ge);
		}

		public function get expire():Number
		{
			return _data.expire;
		}

		public function set expire(value:Number):void
		{
			_data.expire = value;
		}

		public function get token():String
		{
			return _data.token;
		}

		public function set token(value:String):void
		{
			_data.token = value;
		}

		public function get next_exp():Number
		{
			return _data.next_exp;
		}

		public function set next_exp(value:Number):void
		{
			_data.next_exp = value;
		}

		public function get exp():Number
		{
			return _data.exp;
		}

		public function set exp(value:Number):void
		{
			_data.exp = value;
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_EXP_CHANGE_EVENT));
		}

		public function get level():Number
		{
			return _data.level;
		}

		public function set level(value:Number):void
		{
			_data.level = value;
		}

		public function get diamond():Number
		{
			return _data.diamond;
		}

		public function set diamond(value:Number):void
		{
			_data.diamond = value;
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_DIAMOND_CHANGE_EVENT));
		}

		public function get coin():Number
		{
			return _data.coin;
		}

		public function set coin(value:Number):void
		{
			_data.coin = value;
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_COIN_CHANGE_EVENT));
		}
	}
}