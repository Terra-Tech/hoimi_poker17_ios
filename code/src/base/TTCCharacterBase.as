package base
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	
	import Interface.ICharacter;
	
	import event.TTCCharacterEvent;

	public class TTCCharacterBase extends Sprite implements ICharacter
	{
		
		private var m_mcCharacter:MovieClip = new MovieClip();
		private var _m_nLife:int = 0 
		public function TTCCharacterBase()
		{
			
			
		}
		
		public function get m_nLife():int
		{
			return _m_nLife;
		}

		public function set m_nLife(value:int):void
		{
			_m_nLife = value;
		}

		protected function fnInit(mc:MovieClip,nLife:int=0):void
		{
			m_mcCharacter = mc;
			m_nLife = nLife
			fnHide();
			fnNormal();
		}
	
		
		private function fnHide():void
		{
			if(m_mcCharacter.normal)
			{
				m_mcCharacter.normal.visible =false;
				m_mcCharacter.normal.gotoAndStop(1);
			}
			if(m_mcCharacter.speak)
			{
				m_mcCharacter.speak.visible =false;
				m_mcCharacter.speak.gotoAndStop(1);
			}
			if(m_mcCharacter.happy)
			{
				m_mcCharacter.happy.visible =false;
				m_mcCharacter.happy.gotoAndStop(1);
			}
			if(m_mcCharacter.sad)
			{
				m_mcCharacter.sad.visible = false;
				m_mcCharacter.sad.gotoAndStop(1);
			}
			if(m_mcCharacter.fade_in)
			{
				m_mcCharacter.fade_in.visible = false
				m_mcCharacter.fade_in.gotoAndStop(1);
			}
			if(m_mcCharacter.fade_out)
			{
				m_mcCharacter.fade_out.visible =false;
				m_mcCharacter.fade_out.gotoAndStop(1);
			}
			if(m_mcCharacter.nut_fall)
			{
				m_mcCharacter.nut_fall.visible =false;
				m_mcCharacter.nut_fall.gotoAndStop(1);
			}
			if(	m_mcCharacter.attack)
			{
				m_mcCharacter.attack.visible =false;
				m_mcCharacter.attack.gotoAndStop(1);
			}
			if(	m_mcCharacter.hit)
			{
				m_mcCharacter.hit.visible =false;
				m_mcCharacter.hit.gotoAndStop(1);
			}
			if(m_mcCharacter.die)
			{
				m_mcCharacter.die.visible =false;
				m_mcCharacter.die.gotoAndStop(1);
			}
			if(m_mcCharacter.show)
			{
				m_mcCharacter.show.visible = false;
				m_mcCharacter.show.gotoAndStop(1);
			}
		}
		
		public function fnShow():void
		{
			fnHide();
			if(m_mcCharacter.show)
			{
				m_mcCharacter.show.visible =true;
				m_mcCharacter.show.gotoAndPlay(2);
				m_mcCharacter.show.addEventListener(Event.ENTER_FRAME,fnShowEnterFrame);
			}
		}
		
		private function fnShowEnterFrame(e:Event):void
		{
			var nTotalFrame:int = m_mcCharacter.show.totalFrames
			if(m_mcCharacter.show.currentFrameLabel == "SHOW")
			{
				var ev2:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_SHOW_EVENT,true);
				dispatchEvent(ev2);
			}
			if(nTotalFrame == m_mcCharacter.show.currentFrame)
			{
				m_mcCharacter.show.removeEventListener(Event.ENTER_FRAME,fnShowEnterFrame);
				m_mcCharacter.show.gotoAndStop(nTotalFrame);
				var ev:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_SHOW_FINISHED_EVENT,true);
				dispatchEvent(ev);
				fnNormal();
			}
		}
		
		public function fnNormal():void
		{
			fnHide();
			if(m_mcCharacter.normal)
			{
				m_mcCharacter.normal.visible =true;
				m_mcCharacter.normal.gotoAndPlay(2);
			}
		}
		
		public function fnSpeak():void
		{
			fnHide();
			m_mcCharacter.speak.visible =true;
			m_mcCharacter.speak.gotoAndPlay(2);
			
		}
		
		public function fnHappy():void
		{
			fnHide();
			m_mcCharacter.happy.visible =true;
			m_mcCharacter.happy.gotoAndPlay(2);
			m_mcCharacter.happy.addEventListener(Event.ENTER_FRAME,fnHappyEnterFrame);
		}
		
		private function fnHappyEnterFrame(e:Event):void
		{
			var nTotalFrame:int = m_mcCharacter.happy.totalFrames
			if(nTotalFrame == m_mcCharacter.happy.currentFrame)
			{
				m_mcCharacter.happy.gotoAndStop(nTotalFrame);
				m_mcCharacter.happy.removeEventListener(Event.ENTER_FRAME,fnHappyEnterFrame);
				var ev:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_HAPPY_FINISHED_EVENT,true);
				dispatchEvent(ev);
			}
		}
		
		public function fnSad():void
		{
			fnHide();
			m_mcCharacter.sad.visible =true;
			m_mcCharacter.sad.gotoAndPlay(2);
			m_mcCharacter.sad.addEventListener(Event.ENTER_FRAME,fnSadEnterFrame);
		}
		
		private function fnSadEnterFrame(e:Event):void
		{
			var nTotalFrame:int = m_mcCharacter.sad.totalFrames
			if(nTotalFrame == m_mcCharacter.sad.currentFrame)
			{
				m_mcCharacter.sad.gotoAndStop(nTotalFrame);
				m_mcCharacter.sad.removeEventListener(Event.ENTER_FRAME,fnSadEnterFrame);
				var ev:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_SAD_FINISHED_EVENT,true);
				dispatchEvent(ev);
			}
		}
		
		public function fnFadeIn():void
		{
			fnHide();
			m_mcCharacter.fade_in.visible =true;
			m_mcCharacter.fade_in.gotoAndPlay(2);
			m_mcCharacter.fade_in.addEventListener(Event.ENTER_FRAME,fnFadeInEnterFrame);
		}
		
		private function fnFadeInEnterFrame(e:Event):void
		{
			var nTotalFrame:int = m_mcCharacter.fade_in.totalFrames
			if(nTotalFrame == m_mcCharacter.fade_in.currentFrame)
			{
				m_mcCharacter.fade_in.gotoAndStop(nTotalFrame);
				m_mcCharacter.fade_in.removeEventListener(Event.ENTER_FRAME,fnFadeInEnterFrame);
				var ev:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_FADE_IN_FINISHED_EVENT,true);
				dispatchEvent(ev);
			}
		}
		
		public function fnFadeOut():void
		{
			fnHide();
			m_mcCharacter.fade_out.visible =true;
			m_mcCharacter.fade_out.gotoAndPlay(2);
			m_mcCharacter.fade_out.addEventListener(Event.ENTER_FRAME,fnFadeOutEnterFrame);

		}
		
		private function fnFadeOutEnterFrame(e:Event):void
		{
			var nTotalFrame:int = m_mcCharacter.fade_out.totalFrames
			if(nTotalFrame == m_mcCharacter.fade_out.currentFrame)
			{
				m_mcCharacter.fade_out.gotoAndStop(nTotalFrame);
				m_mcCharacter.fade_out.removeEventListener(Event.ENTER_FRAME,fnFadeOutEnterFrame);
				var ev:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_FADE_OUT_FINISHED_EVENT,true);
				dispatchEvent(ev);
			}
		}
		
		public function fnNutsFall():void
		{
			fnHide();
			m_mcCharacter.nut_fall.visible =true;
			m_mcCharacter.nut_fall.gotoAndPlay(2);
			
		}
		
		public function fnAttack():void
		{
			fnHide();
			m_mcCharacter.attack.visible =true;
			m_mcCharacter.attack.gotoAndPlay(2);
			m_mcCharacter.attack.addEventListener(Event.ENTER_FRAME,fnAttackEnterFrame);
		}
		
		private function fnAttackEnterFrame(e:Event):void
		{
			var mc:MovieClip = e.currentTarget as MovieClip
			if(mc.currentFrameLabel == "HIT")
			{
				var ev2:TTCCharacterEvent =new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_HIT_EVENT,true)
				dispatchEvent(ev2);
			}	
			
			var nTotalFrame:int = m_mcCharacter.attack.totalFrames
			if(nTotalFrame == m_mcCharacter.attack.currentFrame)
			{
				m_mcCharacter.attack.gotoAndStop(nTotalFrame);
				m_mcCharacter.attack.removeEventListener(Event.ENTER_FRAME,fnAttackEnterFrame);
				var ev:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_ATTACK_FINISH_EVENT,true);
				dispatchEvent(ev);
			}
		}
		
		public function fnHit():void
		{
			fnHide();
			m_mcCharacter.hit.visible =true;
			m_mcCharacter.hit.gotoAndPlay(2);
			m_mcCharacter.hit.addEventListener(Event.ENTER_FRAME,fnHitEnterFrame);
		}
		
		private function fnHitEnterFrame(e:Event):void
		{
			var nTotalFrame:int = m_mcCharacter.hit.totalFrames
			if(nTotalFrame == m_mcCharacter.hit.currentFrame)
			{
				m_mcCharacter.hit.gotoAndStop(nTotalFrame);
				m_mcCharacter.hit.removeEventListener(Event.ENTER_FRAME,fnHitEnterFrame);
				m_nLife--
				if(m_nLife == 0)
				{
					var ev:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_HIT_FINISH_EVENT,true);
					ev.m_eventArgs = m_nLife;
					dispatchEvent(ev);
					var ev2:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_DIE_EVENT,true);
					dispatchEvent(ev2);
					
				}
				else
				{
					var ev3:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_HIT_FINISH_EVENT,true);
					ev3.m_eventArgs = m_nLife;
					dispatchEvent(ev3);
				}
			
			}
		}
		
		public function fnDie():void
		{
			fnHide();
			m_mcCharacter.die.visible =true;
			m_mcCharacter.die.gotoAndPlay(2);
			m_mcCharacter.die.addEventListener(Event.ENTER_FRAME,fnDieEnterFrame);
		}
		
		private function fnDieEnterFrame(e:Event):void
		{
			var nTotalFrame:int = m_mcCharacter.die.totalFrames
			if(nTotalFrame == m_mcCharacter.die.currentFrame)
			{
				m_mcCharacter.die.gotoAndStop(nTotalFrame);
				m_mcCharacter.die.removeEventListener(Event.ENTER_FRAME,fnDieEnterFrame);
				var ev:TTCCharacterEvent = new TTCCharacterEvent(TTCCharacterEvent.CHARACTER_EVENT,TTCCharacterEvent.CHARACTER_DIE_FINISH_EVENT,true);
				dispatchEvent(ev);
			}
		}
		
		public function fnDestory():void
		{
			m_mcCharacter = null;
		}
	}
}