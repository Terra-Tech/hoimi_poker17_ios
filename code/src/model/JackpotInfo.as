package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class JackpotInfo extends NetPacket
	{
		public var jackpot_pool:Number;
		public var last_winner:String;
		public var last_bonus:Number;
		public function JackpotInfo(j:*){
			super(j);
		}
	}
}