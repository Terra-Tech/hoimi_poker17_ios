package item
{
	import com.hoimi.util.LocalSaver;
	
	import flash.display.Sprite;
	
	import Enum.EnumLocalSave;
	
	public class GamePassCoin extends Sprite
	{
		private var m_mcCoin:Porker_Coin = new Porker_Coin();
		private var m_sp:Sprite = new Sprite();
		public function GamePassCoin()
		{
			super();
			this.addChild(m_mcCoin);
			this.addChild(m_sp);
		}
		
		public function fnSetCoin(nValue:int):void
		{
			m_sp.removeChildren();
			var ar:Array = new Array();
			var n1:int = 0;
			var n2:int = 0;
			var n3:int = 0;
			var n4:int = 0;
			var n5:int = 0;
			
			if(nValue<10)
			{
				ar.push(11)
				ar.push(nValue);
			}
			else if(nValue < 100)
			{
				n1 = parseInt(nValue.toString().charAt(0));
				n2 = parseInt(nValue.toString().charAt(1));
				ar.push(11)
				ar.push(n1);
				ar.push(n2);
			}
			else if(nValue <1000)
			{
				n1 = parseInt(nValue.toString().charAt(0));
				n2 = parseInt(nValue.toString().charAt(1));
				n3 = parseInt(nValue.toString().charAt(2));
				ar.push(11)
				ar.push(n1);
				ar.push(n2);
				ar.push(n3);
			}
			else if(nValue < 10000)
			{
				n1 = parseInt(nValue.toString().charAt(0));
				n2 = parseInt(nValue.toString().charAt(1));
				n3 = parseInt(nValue.toString().charAt(2));
				n4 = parseInt(nValue.toString().charAt(3));
				ar.push(11)
				ar.push(n1);
				ar.push(n2);
				ar.push(n3);
				ar.push(n4);
			}
			else
			{
				n1 = parseInt(nValue.toString().charAt(0));
				n2 = parseInt(nValue.toString().charAt(1));
				n3 = parseInt(nValue.toString().charAt(2));
				n4 = parseInt(nValue.toString().charAt(3));
				n5 = parseInt(nValue.toString().charAt(4));
				ar.push(11)
				ar.push(n1);
				ar.push(n2);
				ar.push(n3);
				ar.push(n4);
				ar.push(n5);
			}
		/*	var number:Porker_Number_Coin
			for(var i:int =0 ; i< ar.length ;i++)
			{
				number = new Porker_Number_Coin();
				var index:int = ar[i]
				number.gotoAndStop(index+1)
				m_sp.addChild(number);
				number.x = 65 + i*22;
				number.y = 18
			}*/
		}
	}
}