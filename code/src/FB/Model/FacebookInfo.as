package FB.Model
{
	import com.hoimi.lib.net.JsonObject;

	public class FacebookInfo extends JsonObject
	{
		public var id:String;
		public var name:String;
		public var gender:String;
		public var age_range:AgeRange = new AgeRange();
		public var email:String;
		public var pictue:FacebookPicture = new FacebookPicture();
		
		public function FacebookInfo(fb_info:*)
		{
			super(fb_info);
		}
	}
}
import com.hoimi.lib.net.JsonObject;

class AgeRange extends JsonObject
{
	public var min:int;
	
	public function AgeRange(range:* = null){
		super(range);
	}
}