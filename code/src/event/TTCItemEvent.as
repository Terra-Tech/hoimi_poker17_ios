package event
{
	public class TTCItemEvent extends TiBasicEvent
	{
		public static const ITEM_EVENT:String="ITEM_EVENT";
		public static const ITEM_STATRT_EVENT:String="ITEM_STATRT_EVENT";
		public static const ITEM_PLAY_EVENT:String="ITEM_PLAY_EVENT";
		public static const ITEM_REPLAY_EVENT:String="ITEM_REPLAY_EVENT";
		public static const ITEM_RETRY_EVENT:String="ITEM_RETRY_EVENT";
		public static const ITEM_NEXT_PAGE_EVENT:String="ITEM_NEXT_PAGE_EVENT";
		public static const ITEM_321GO_EVENT:String="ITEM_321GO_EVENT";
		public static const ITEM_TITLE_EVENT:String="ITEM_TITLE_EVENT";
		
		
		public function TTCItemEvent(type:String, detail:String="")
		{
			super(type, detail);
		}
	}
}