package model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class PlayerRecord extends JsonObject
	{
		public var win:int;
		public var lose:int;
		public var rate:Number;
		
		public function PlayerRecord(record:*)
		{
			super(record);
		}
	}
}