package section
{
	
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import CoinBack.CoinBackView;
	
	import Skill.SkillController;
	import Skill.SkillRaiseEvent;
	import Skill.SkillView;
	import Skill.skillItem.SkillItem_01;
	
	import base.TTCSectionBase;
	
	import event.GameEvent;
	
	public class SectionSkill extends TTCSectionBase
	{
		private var m_mcBg:Poker_Skill_Bg = new Poker_Skill_Bg();
		private var m_coSkillView:SkillView = SkillView.fnGetInstance()
		
		
		public function SectionSkill()
		{
			super();
			
		
			m_coSkillView.init();
			
			this.addChild(m_mcBg);
			this.addChild(m_coSkillView);
			EventManager.instance.register(GameEvent.GAME_SECTION_REFRESH,fnRefresh);	
		}
		
		
		
		private function fnRefresh(e:GameEvent):void
		{
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NEXT_SECTION_EVENT);
			dispatchEvent(ev);
		}
		
		
		override public function fnResize():void
		{
			m_mcBg.x = 360 +  GameConfig.object_adjuct_position_x 
			m_coSkillView.x = GameConfig.object_adjuct_position_x 
			
		}
		
		override public function fnPause():void
		{
			super.fnPause();
		}
		
		override public function fnResume():void
		{
			super.fnResume();
		}
		
		override public function fnSkip():void
		{
			super.fnSkip();
		}
		
		
		override public function fnReplay():void
		{
			super.fnReplay();
		}
		
		override public function fnExit():void
		{
			m_coSkillView.destory();
			EventManager.instance.remove(GameEvent.GAME_SECTION_REFRESH,fnRefresh);	
			this.removeChildren();
			super.fnExit()
			
		}
	}
}