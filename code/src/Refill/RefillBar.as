package Refill
{
	import flash.display.Sprite;
	
	import item.HoimiCoin;
	
	public class RefillBar extends Sprite
	{
		private var m_mc:Poker_Refill_Bar = new Poker_Refill_Bar();
		private var m_coCoinValue:HoimiCoin = new HoimiCoin();
		private var m_coCoinValueMax:HoimiCoin = new HoimiCoin();
		
		public function RefillBar()
		{
			super();
			this.addChild(m_mc);
		
			m_coCoinValue.x = 0
			m_coCoinValueMax.x = 100
		
	
		}
		
		public function showClock(hour:Number, minute:Number, second:Number):void
		{
			var strHour:String="";
			var strMinute:String="";
			var strSecond:String="";
			if(hour == 0)
			{
				strHour ="00"
			}
			else if(hour < 10)
			{
				strHour ="0" + hour
			}
			else
			{
				strHour =hour.toString();
			}
			
			if(minute ==0)
			{
				strMinute = "00"
			}
			else if(minute<10)
			{
				strMinute = "0"+minute
			}
			else
			{
				strMinute = minute.toString();
			}
			
			if(second == 0)
			{
				strSecond = "00"
			}
			else if(second<10)
			{
				strSecond = "0" + second
			}
			else
			{
				strSecond = second.toString();
			}
			m_mc.txt_time.text = strHour+':'+strMinute+ ':'+strSecond;
		}
		
		public function showCoin(current:Number, max:Number):void
		{
		
			m_mc.txt_coin.text = current+" / "+max
			var nRate:int = (current/max)*100
			m_mc.slice.width = nRate*2	
		}
		
	
	}
}