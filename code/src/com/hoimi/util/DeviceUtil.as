package com.hoimi.util
{
	import flash.system.Capabilities;
	
	public class DeviceUtil
	{
		private static var _instance : DeviceUtil = null;
		
		public static function getInstance() : DeviceUtil 
		{
			return _instance ? _instance : new DeviceUtil();
		}
		
		public function DeviceUtil()
		{
			if ( !_instance ) {
				_instance = this;
			}
			else {
				throw Error( 'This is a singleton, use getInstance(), do not call the constructor directly.' );
			}
		}
		
		public function get isOnDevice() : Boolean {
			
			var value : Boolean = this.isOnIOS || this.isOnAndroid;
			return value;
		}
		
		public function get isOnIOS() : Boolean {
			
			var value : Boolean = Capabilities.manufacturer.indexOf( 'iOS' ) > -1;
			return value;
		}
		
		public function get isOnAndroid() : Boolean {
			
			var value : Boolean = Capabilities.manufacturer.indexOf( 'Android' ) > -1;
			return value;
		}
		
		/**
		 * Returns true if the user is running the app on a Debug Flash Player.
		 * Uses the Capabilities class
		 **/
		public function isDebugPlayer() : Boolean
		{
			return Capabilities.isDebugger;
		}
		
		/**
		 * Returns true if the swf is built in debug mode
		 **/
		public function isDebugBuild() : Boolean
		{
			var st:String = new Error().getStackTrace();
			return (st && st.search(/:[0-9]+]$/m) > -1);
		}
	}
}