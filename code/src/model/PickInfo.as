package model
{
	import com.hoimi.lib.net.NetPacket;

	public class PickInfo extends NetPacket
	{
		/** pick
		 * 1:{"result" : 1, 'game_id':92, 'whos_turn':1, 'enemy_pick':[13,5], 'enemy_max_multiple':3});
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 * -4:param miss
		 * -5:state error
		 **/ 
		public var game_id:int = 0;
		/// 0:Player, 1:Enemy
		public var whos_turn:int = 0;
		public var enemy_pick:Array = [];
		public var enemy_max_multiple:int = 0;
		
		public function PickInfo(pick:String)
		{
			super(pick);
		}
	}
}