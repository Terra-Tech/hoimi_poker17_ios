package lib
{
	
	import utils.LocalSaver;

	public class TTCActivedCheck
	{
		public function TTCActivedCheck()
		{
		}
		
		public static function fnActivedCheck():Boolean
		{
			var actived: * = LocalSaver.instance.getValue("actived");
			if (actived == null)
			{
				if(TTCModeCheck.isReleaseBuild())
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			return false; 
		}
	}
}