package com.hoimi.util
{
	import flash.events.Event;
	import flash.utils.Dictionary;
	
	public class EventManager
	{
		private static var _instance:EventManager = null;
		private var _funMap:Dictionary;
		
		public static function get instance():EventManager 
		{
			if (_instance == null)
			{
				_instance = new EventManager( new SingletonEnforcer() )
			}
			
			return _instance ;
		}
		
		public function EventManager(enforcer:SingletonEnforcer)
		{
			_funMap = new Dictionary(true);
		}
		
		public function register($key:String , $func:Function):void 
		{
			if($func == null)return;
			if(_funMap[$key] == null)
			{
				_funMap[$key] = [];
			}
			var ary_:Array = _funMap[$key];
			if(ary_.indexOf($func)==-1)
			{
				ary_.push($func);
			}
//			trace(ary_.lengt÷h);
//			trace(_funMap[$key]);
		}
		
		public function remove($key:String , $func:Function):void 
		{
//			trace("remove "+$key);
			if(_funMap[$key] == null)
			{
				return;
			}
			var ary_:Array = _funMap[$key];
			var idx_:int = ary_.indexOf($func);
			if(idx_==-1)
			{
				return;
			}
			ary_.splice(idx_,1);
		}
		
		public function trigger($event:Event):void
		{
//			trace(_funMap[$event.type]);
			if(_funMap[$event.type] == null)
			{
				return;
			}
			var ary_:Array = _funMap[$event.type];
			var i_:int,len_:int;
			var fun_:Function;
			len_ = ary_.length;
			for(i_ = 0 ;i_ < len_;i_++)
			{
				fun_ = ary_[i_];
				if(fun_==null) continue;
				fun_.call(null,$event);
			}
		}
		
		public function destroy():void
		{
//			trace("destroy");
			var ary_:Array;
			for each(ary_ in _funMap)
			{
				ary_ = [];
			}
			_funMap = new Dictionary(true);
		}
	}
}

class SingletonEnforcer{}