package handler
{
	import flash.display.Graphics;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	
	import base.TTCMusicBase;
	
	import event.GameEvent;
	
	public class TTCSwfHandler extends Sprite
	{
		private static var m_Instance:TTCSwfHandler;
		public var m_mcSwf:MovieClip = new MovieClip();
		private var m_strSwfUrl:String = "";
		private var m_strEndTag:String = "";
		private var m_strActTag:String = "";
		private var m_myLoader:Loader        // create a new instance of the Loader class
		public function TTCSwfHandler()
		{
			super();
		}
		
		public static function fnGetInstance(): TTCSwfHandler 
		{
			if ( m_Instance == null )
			{
				m_Instance = new TTCSwfHandler();
			}
			return m_Instance;
		}
		
		public function fnLoadSwf(strUrl:String,strEndTag:String="END",strActTag="ACT"):void
		{
			m_strEndTag = strEndTag
			m_strActTag = strActTag
			if(strUrl != m_strSwfUrl)
			{
				this.removeChildren(); 
				m_strSwfUrl = strUrl;
				m_myLoader = new Loader();              
				var url:URLRequest = new URLRequest(strUrl); // in this case both SWFs are in the same folder 
				m_myLoader.load(url);                                     // load the SWF file
				this.addChild(m_myLoader);      
				m_myLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,fnLoadSwfCompleteHandler);
				m_myLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,fnLoadSwfIOErrorHandler);
			}
			else
			{
				m_mcSwf.addEventListener(Event.ENTER_FRAME,fnEnterFrameHandler);
				TTCMusicHandler.fnGetInstance().fnAddSwf(m_mcSwf);
				var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_SWF_LOAD_COMPLETE_EVENT);
				dispatchEvent(ev);
			}
		}
		
		private function fnLoadSwfIOErrorHandler(e:IOErrorEvent):void
		{
			trace("SwfLad error : " + e.text)
		}
		
		private function fnLoadSwfCompleteHandler(e:Event):void
		{
			var mc:MovieClip = e.currentTarget.content as MovieClip; 
			m_mcSwf = mc;
			m_myLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE,fnLoadSwfCompleteHandler);
			m_myLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,fnLoadSwfIOErrorHandler);
			TTCMusicHandler.fnGetInstance().fnAddSwf(mc);
			mc.addEventListener(Event.ENTER_FRAME,fnEnterFrameHandler);
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_SWF_LOAD_COMPLETE_EVENT);
			dispatchEvent(ev);
		}
		
		private function fnEnterFrameHandler(e:Event):void
		{
			var mc:MovieClip = e.currentTarget as MovieClip
				//trace("mc.currentLabel"+mc.currentLabel)
			if(mc.currentLabel == m_strEndTag)
			{
				trace("swf end tag")
				mc.gotoAndStop(mc.currentFrame);
				mc.removeEventListener(Event.ENTER_FRAME,fnEnterFrameHandler);
				var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_SECTION_FINISH_EVENT);
				dispatchEvent(ev);
			}
			if(mc.currentLabel == m_strActTag)
			{
				trace("swf act tag")
				mc.gotoAndStop(mc.currentFrame);
				mc.removeEventListener(Event.ENTER_FRAME,fnEnterFrameHandler);
				var ev2:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_SWF_ACT_EVENT);
				ev2.m_eventArgs = m_mcSwf;
				dispatchEvent(ev2);
			}
				//trace(mc.currentScene.name);
				//trace(mc.currentLabel);
		}
		public function fnGoToAndPlay(frame:int,scene:String=null):void
		{
			TTCMusicHandler.fnGetInstance().fnAddSwf(m_mcSwf);
			m_mcSwf.gotoAndPlay(frame,scene);
			m_mcSwf.addEventListener(Event.ENTER_FRAME,fnEnterFrameHandler);
		}
		
		public function fnGotoAndPlayByLabel(label:String):void
		{
			TTCMusicHandler.fnGetInstance().fnAddSwf(m_mcSwf);
			m_mcSwf.gotoAndPlay(label);
			m_mcSwf.addEventListener(Event.ENTER_FRAME,fnEnterFrameHandler);
		}
		
		public function fnGoToAndPlayNextFrame():void
		{
			TTCMusicHandler.fnGetInstance().fnAddSwf(m_mcSwf);
			var nFrame:int = m_mcSwf.currentFrame
			nFrame++
			trace("label:"+m_mcSwf.currentLabel);
			m_mcSwf.gotoAndPlay(nFrame);
			trace("label:"+m_mcSwf.currentLabel);
			m_mcSwf.addEventListener(Event.ENTER_FRAME,fnEnterFrameHandler);
		}
		
		public function fnPause():void
		{
			m_mcSwf.gotoAndStop(m_mcSwf.currentFrame);
			
			for(var i :int = 0 ;i<m_mcSwf.numChildren;i++)
			{
				var mc:MovieClip = m_mcSwf.getChildAt(i) as MovieClip;
				if(mc)
				{
					mc.gotoAndStop(mc.currentFrame);
					for(var j :int = 0 ;j< mc.numChildren;j++)
					{
						var mc2:MovieClip = mc.getChildAt(j) as MovieClip;
						if(mc2)
						{
							mc2.gotoAndStop(mc2.currentFrame);
							for(var k :int = 0 ;k< mc2.numChildren;k++)
							{
								var mc3:MovieClip = mc2.getChildAt(k) as MovieClip;
								if(mc3)
								{
									mc3.gotoAndStop(mc3.currentFrame);
								}
							}
						}
					}
				}
			}
		}
		
		public function fnResume():void
		{
			m_mcSwf.gotoAndPlay(m_mcSwf.currentFrame);
			for(var i :int = 0 ;i<m_mcSwf.numChildren;i++)
			{
				var mc:MovieClip = m_mcSwf.getChildAt(i) as MovieClip;
				if(mc)
				{
					mc.gotoAndPlay(mc.currentFrame);
					for(var j :int = 0 ;j< mc.numChildren;j++)
					{
						var mc2:MovieClip = mc.getChildAt(j) as MovieClip;
						if(mc2)
						{
							mc2.gotoAndPlay(mc2.currentFrame);
							for(var k :int = 0 ;k< mc2.numChildren;k++)
							{
								var mc3:MovieClip = mc2.getChildAt(k) as MovieClip;
								if(mc3)
								{
									mc3.gotoAndPlay(mc3.currentFrame);
								}
							}
						}
					}
				}
			}
		}
		
		public function fnResumeButStopCurrentFrame():void
		{
			for(var i :int = 0 ;i<m_mcSwf.numChildren;i++)
			{
				var mc:MovieClip = m_mcSwf.getChildAt(i) as MovieClip;
				if(mc)
				{
					mc.gotoAndPlay(mc.currentFrame);
					for(var j :int = 0 ;j< mc.numChildren;j++)
					{
						var mc2:MovieClip = mc.getChildAt(j) as MovieClip;
						if(mc2)
						{
							mc2.gotoAndPlay(mc2.currentFrame);
							for(var k :int = 0 ;k< mc2.numChildren;k++)
							{
								var mc3:MovieClip = mc2.getChildAt(k) as MovieClip;
								if(mc3)
								{
									mc3.gotoAndPlay(mc3.currentFrame);
								}
							}
						}
					}
				}
			}
		}
		
		public function fnSkip():void
		{
			if(m_mcSwf)
			{
				m_mcSwf.stop();
				m_mcSwf.removeEventListener(Event.ENTER_FRAME,fnEnterFrameHandler);
			}
		}
		
		public function fnExit():void
		{
			if(m_mcSwf)
			{
				TTCMusicHandler.fnGetInstance().fnRemoveSwf();
				m_mcSwf.stop()
				m_mcSwf.removeEventListener(Event.ENTER_FRAME,fnEnterFrameHandler);
				m_mcSwf = null
			}
			m_strSwfUrl ="";
			m_strEndTag ="";
			if(m_myLoader)
			{
				m_myLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE,fnLoadSwfCompleteHandler);
				m_myLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR,fnLoadSwfIOErrorHandler);
				m_myLoader = null;
			}
		
			this.removeChildren(); 
		}

	}
}