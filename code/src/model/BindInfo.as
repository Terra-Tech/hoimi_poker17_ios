package model
{
	import com.hoimi.lib.net.NetPacket;

	public class BindInfo extends NetPacket
	{
		
		/**  bind
		 *    1 {"result" : 1, "uuid" : uuid}
		 *   -1 param miss
		 *   -2 already bind
		 *   -3 uuid not exist , signup first
		 *   -4 facebook already bind
		 */
		public var uuid:String;
		
		public function BindInfo(bind:String){
			super(bind);
		}
	}
}