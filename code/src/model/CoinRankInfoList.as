package model
{
	import com.hoimi.lib.net.NetPacket;

	public class CoinRankInfoList extends NetPacket
	{
		public var ranks:Array = [];
		public var self_rank:int = 0
			
		public function CoinRankInfoList(rankList:*)
		{
			super(rankList);
			var s:CoinRankInfo;
			
			for (var i:int in ranks)
			{
				s = new CoinRankInfo(ranks[i]);
				s.rank = i+1 
				ranks[i] = s;
			}
		}
	}
}