package ui
{
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.freshplanet.lib.ui.util.RectangleSprite;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	
	import base.TiDisableTextField;
	
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	public class RegulationPanel extends Sprite
	{
		private var m_mc:Poker_Regulation_Panel = new Poker_Regulation_Panel();
		private var m_txtRegulation:Poker_Regulation_TW = new Poker_Regulation_TW();
		private var m_sp:Sprite = new Sprite();
		private var _scroll:ScrollController;
		private var m_nMaskHeight:Number = 1090
		private var m_nDefaultSpY:Number = 40
		private var m_coinMsgSp:Sprite = new Sprite();	
		private var m_arList:Array
		private var m_scrollSp:Sprite = new Sprite();
		
		public function RegulationPanel()
		{
			super();
			this.addChild(m_mc);
			m_sp.addChild(m_txtRegulation);
			this.addChild(m_scrollSp);
			this.addEventListener(Event.ADDED_TO_STAGE,fnAddToStage);
			m_txtRegulation.mouseChildren =false;
			m_txtRegulation.mouseEnabled =false;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_close);
			m_mc.btn_close.addEventListener(MouseEvent.CLICK,fnClose);
			this.visible =false;
			m_scrollSp.x = -m_txtRegulation.width/2
		}
		
		private function fnAddToStage(e:Event):void
		{
			fnInitScroll();
		}
		
		public function fnDisplay():void
		{
			this.visible =true;
		}
		
		private function fnClose(e:MouseEvent):void
		{
			this.visible =false;
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound003();
		}
		
		private function fnInitScroll():void
		{
			var container:RectangleSprite = new RectangleSprite(0xFFFFFF, 0, m_nDefaultSpY, 0, m_nMaskHeight);//red background
			m_scrollSp.addChild(container);
			container.addChild(m_sp);
			var containerViewport:Rectangle = new Rectangle(0, 0, 720 , m_nMaskHeight);
			var containerRect:Rectangle = new Rectangle(0, 0, 720 , m_nMaskHeight);
			this._scroll = new ScrollController();
			this._scroll.horizontalScrollingEnabled = false;
			this._scroll.addScrollControll(m_sp, container, containerViewport,containerRect);
		}
		
	}
}