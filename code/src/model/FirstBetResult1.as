package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class FirstBetResult1 extends NetPacket
	{
		//{'result' : 1, 'game_id':game_id, 'winlose':winlose, 'player_coin':player_coin, 'player_cards':player_cards, 'enemy_cards':enemy_cards, 'player_reward':player_eval5.name, 'enemy_reward':enemy_eval5.name, 'level':player_level.level, 'exp':player_level.exp, 'next_exp':next_exp, 'reward_coin':reward_coin, 'reward_diamond':reward_diamond, 'jp_bonus':0}
		/** first_bet
		 * 1:{'result' : 1, 'game_id':92,'player_coin':2200, 'enemy_buying_amount':0}
		 * 2:{'result' : 2, 'game_id':92, 'winlose':100, 'player_coin':2100, 'level':9, 'exp':100, 'next_exp':50, 'reward_coin':100, 'reward_diamond':1, 'player_reward':'4Cards', 'jp_bonus':0, 'player_cards':[0,1,2,3,4], 'player_diamond':0}
		 * -3:token expire
		 **/ 
		public var game_id:int = -1;
		public var player_coin:Number = 0;
		public var enemy_buying_amount:int = 0;
		public var player_record:PlayerRecord = new PlayerRecord(null);
		
		public function FirstBetResult1(fb:String){
			
			super(fb);
		}
	}
}