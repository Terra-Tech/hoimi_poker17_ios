package Prize
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import base.TiDisableTextField;
	
	import event.GameEvent;
	
	import handler.HoimiFont01LoadHandler;
	import handler.TTCSoundHandler;
	
	public class PrizeMsgPanel extends Sprite
	{
		private var m_mc:Poker_Msg_Panel = new Poker_Msg_Panel();
		public function PrizeMsgPanel()
		{
			super();
			this.addChild(m_mc);
			m_mc.x = 360
			m_mc.y = 300
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_ok);
			m_mc.btn_no.visible = false;
			m_mc.btn_yes.visible = false;
			m_mc.btn_ok.addEventListener(MouseEvent.CLICK,fnOk);
			this.visible = false;
			m_mc.txt_msg.mouseEnabled=false;
			
			
		}
		
		
		
		
		private function fnSetMask():void
		{
			var masker:Sprite = new Sprite();
			masker.graphics.beginFill(0);
			masker.graphics.drawRect( 0 , 0 , 720 , 1280);
			masker.graphics.endFill();
			masker.alpha = 0
			this.addChild(masker);
			
		}
		
		
	
		
		private function fnOk(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			this.visible = false;
			
			
		}
		
		
		public function fnDisplayPanel(nCoin:Number):void
		{
			this.visible =true;
			m_mc.txt_msg.text = "獲得金幣"
		}
		
		
		
		
	}
}