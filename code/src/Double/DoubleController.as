package Double
{
	import Double.Model.DoubleSettle;

	public class DoubleController 
	{
		public function DoubleController()
		{
			super();
		}
		
		public function fetchConfig(token:String, onSuccess, onFail):void
		{
			SessionManager.instance.request(Protocol.DOUBLE_CONFIG,{token:token}, onSuccess, onFail);
		}
		
		public function bet(token:String, game_id:Number, bet_option:int, bet_amount:int, onSuccess, onFail):void
		{
			var settle:DoubleSettle = new DoubleSettle(token, game_id, bet_option, bet_amount);
			SessionManager.instance.request(Protocol.DOUBLE_BET, settle, onSuccess, onFail);
		}
	}
}