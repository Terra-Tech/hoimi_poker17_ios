package Refill
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	
	import handler.TTCSoundHandler;
	
	import item.ResultPanelCoin;
	
	
	public class RefillView extends Sprite implements IRefillView
	{
		private var m_coRefillBar:RefillBar = new RefillBar();
		private var m_mcHoimiCoin:Poker_Hoimi_Coin = new Poker_Hoimi_Coin();
		private var m_sp:Sprite = new Sprite();
		private var statusMsg:TextField = new TextField();
		
		public function RefillView()
		{
			this.addChild(m_mcHoimiCoin);
			this.addChild(m_coRefillBar);
			this.addChild(statusMsg);
			this.addChild(m_sp);
			m_mcHoimiCoin.x = 60
			m_mcHoimiCoin.y = 111.5
			m_coRefillBar.x = 80
			m_coRefillBar.y = 100
			m_mcHoimiCoin.scaleX = 0.3
			m_mcHoimiCoin.scaleY =0.3
				
			
			statusMsg.defaultTextFormat = new TextFormat( "Arial", 30, 0xFF0000, false, false, false, null, null, TextFormatAlign.CENTER );
			statusMsg.filters = [new DropShadowFilter(1,45,0xFFFFFF,1,1,1,1,1)];
			statusMsg.x = 22
			statusMsg.y = 150;
			statusMsg.width = 300
		}
		private static var m_Instance:RefillView;
		
		public static function fnGetInstance(): RefillView 
		{
			if ( m_Instance == null )
			{
				m_Instance = new RefillView();
			}
			return m_Instance;
		}
		
		
		public function init():void
		{
			EventManager.instance.trigger(new Event(RefillController.EVENT_REFILL_INIT));
		}
		
		public function destory():void
		{
			EventManager.instance.trigger(new Event(RefillController.EVENT_REFILL_DESTORY));
		}
		
		public function refill():void
		{
			EventManager.instance.trigger(new Event(RefillController.EVENT_REFILL));
		}
		
		public function fnPause():void
		{	
			EventManager.instance.trigger(new Event(RefillController.EVENT_REFILL_PAUSE));
		}
		
		public function fnResume():void
		{
			EventManager.instance.trigger(new Event(RefillController.EVENT_REFILL_RESUME));
		}
		
		public function netError(msg:String):void
		{
			trace('Refill net error ' + msg);
		}
		
		public function serverError(msg:String):void
		{
			trace('Refill server error' + msg);
		}
		
		public function showLoading():void
		{
			trace('showLoading loading...');
		}
		
		public function hideLoading():void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
		}
		
		public function showClock(hour:Number, minute:Number, second:Number):void
		{
			//trace('showClock '+hour.toString()+':'+minute.toString()+ ':'+second.toString());
			m_coRefillBar.showClock(hour, minute, second);
		}
		
		public function showCoin(current:Number, max:Number):void
		{
			//trace('showCoin '+current.toString()+'/'+max.toString());
			m_coRefillBar.showCoin(current,max);
		}
		
		public function showRefill(refill_coin:Number, player_coin:Number):void
		{
		//	trace('showRefill Coin:'+refill_coin.toString()+' Player Coin:'+player_coin.toString());
			GameConfig.game_playerInfo.coin = player_coin
			var m_coMsgGainCoin:ResultPanelCoin = new ResultPanelCoin();
			m_coMsgGainCoin.fnSetValue(refill_coin,true,false);
			m_coMsgGainCoin.x = 200
			m_coMsgGainCoin.y = 220
			
			TTCSoundHandler.fnGetInstance().fnEmbeddedSoundCoin();
			m_sp.addChild(m_coMsgGainCoin);
			setTimeout(function():void 
			{
				fnTweenEffect(m_coMsgGainCoin);
			}, 2000)
		}
		
		private function fnTweenEffect(mc:Sprite):void
		{
			Tweener.addTween(mc,{y:-150,time:1,alpha:0,transition:"easeInQuart",onComplete:fnTweenEffectFinish});
		}
		
		private function fnTweenEffectFinish():void
		{
			m_sp.removeChildren();
		}
		
		public function showNoMoney():void
		{
			trace('showNoMoney pls wait moment!');
			statusMsg.text = "餘額不足，無法領取!"
			setTimeout(function():void 
			{
				statusMsg.text="";
			}, 2000)
		}
	}
}