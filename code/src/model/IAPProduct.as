package model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class IAPProduct extends JsonObject
	{
		public var twd_60:int = 0;
		public var twd_150:int = 0;
		public var twd_300:int = 0;
		public var twd_600:int = 0;
		public var twd_1500:int = 0;
		public var twd_3000:int = 0;
		
		public function IAPProduct(product:*)
		{
			super(product);
		}
	}
}