package ui
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import Enum.EnumLang;
	import Enum.EnumMsg;
	
	import base.TiDisableTextField;
	
	import event.GameEvent;
	
	import handler.HoimiFont01LoadHandler;
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	public class ReconnectPanel extends Sprite
	{
		private var m_mc:Poker_Msg_Reconnect_Panel = new Poker_Msg_Reconnect_Panel();
		
		public function ReconnectPanel()
		{
			super();
			fnSetMask();
			this.addChild(m_mc);
			m_mc.x = 360
			m_mc.y = 300;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_reconnect);
			m_mc.btn_reconnect.addEventListener(MouseEvent.CLICK,fnReconnect);
			//m_mc.btn_reconnect2.addEventListener(MouseEvent.CLICK,fnReconnect);
		//	m_mc.btn_no.addEventListener(MouseEvent.CLICK,fnClickNo);
			this.visible =false;
			EventManager.instance.register(NetEvent.STATE_TIMEOUT,fnDisplayPanel);
			EventManager.instance.register(NetEvent.STATE_IO_ERROR,fnDisplayPanel);
			
		//	HoimiFont01LoadHandler.fnGetInstance().addEventListener(GameEvent.GAME_EVENT,GameEventHandler);
		//	HoimiFont01LoadHandler.fnGetInstance().fnLoadFont();
		}
		
		private function GameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_FONT_01_LOAD_COMPLETE_EVENT)
			{
				fnEmbededFont();
			}
		}
		
		private function fnEmbededFont():void
		{
			HoimiFont01LoadHandler.fnGetInstance().removeEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			var tf:TextFormat = new TextFormat();
			tf.font = HoimiFont01LoadHandler.fnGetInstance().embeddedFont.fontName;
			//tf.size = 40;
			//tf.color = "0xFF8800";
			m_mc.txt_msg.embedFonts = true; // very important to set
			m_mc.txt_msg.defaultTextFormat = tf;
				
			m_mc.txt_msg.text = TxtLangTransfer.RECONNECT
		}
		
		private function fnSetMask():void
		{
			var masker:Sprite = new Sprite();
			masker.graphics.beginFill(0);
			masker.graphics.drawRect( 0 , 0 , 720 , 1280);
			masker.graphics.endFill();
			masker.alpha = 0
			this.addChild(masker);
			
		}
		
		private function fnReconnect(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			this.visible =false;
			var ev:GameEvent = new GameEvent(GameEvent.GAME_RECONNECT_EVENT);
			EventManager.instance.trigger(ev);
		}
		
		private function fnDisplayPanel(e:NetEvent):void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			this.visible = true;
			switch(e.protocol)
			{
				case Protocol.PRIZE_AMOUNT:
				case Protocol.COIN_BACK_POOL:
				case Protocol.REFILL_TIMESTAMP:
					this.visible = false;
				break;	
			}
			
			
		}
	}
}