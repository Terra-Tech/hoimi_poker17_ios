package model
{
	import com.hoimi.lib.net.JsonObject;
	
	import Skill.Model.Skill;
	
	public class Player extends JsonObject
	{
		public var token:String;
		public var expire:Number;
		public var coin:Number;
		public var diamond:Number;
		public var level:int;
		public var exp:Number;
		public var next_exp:Number;
		// 0:refill_max_coin, 1:refill_pay_min, 2:exchange_crit_rate, 3:exchnage_crit_multiple, 4:exp_raise_multiple
		public var skills:Array = [];
		//銀行上限，銀行補幣，換錢爆擊率，換錢爆擊倍率，經驗值，返利
		public var skills_next:Array = [];
		public var refill_timestamp:Number;
		/// 戰績
		public var player_record:PlayerRecord = new PlayerRecord(null);
		/// 升級獎勵金幣
		public var reward_coin:Number;
		/// 升級獎勵鑽石
		public var reward_diamond:int;
		/// 未完成局的id -1:沒有未完成  有的會是>0的game_id(Ex:92)
		public var uncomplete_game_id:int;
		/// 未完成局輸了多少錢  0:沒有未完成  有的話會是負數(Ex:-1000)
		public var uncomplete_winlose:Number;
		/// facebook id 識別有無登入綁定，沒有則為null
		public var fb_id:String;
		/// 返利錢池
		public var coin_back_pool:Number;
		/// 玩家姓名
		public var nickname:String;
		/// 鑽石換金幣比率
		public var exchange_rate:int;
		
		public function Player(obj:*=null)
		{
			super(obj);
			
			var s:Skill,n:Skill;
			for (var i:int in skills){
				s = new Skill(skills[i]);
				skills[i] = s;
				n = new Skill(skills_next[i]);
				skills_next[i] = n;
			}
		}
	}
}