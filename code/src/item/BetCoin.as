package item
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.text.TextFormat;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	
	import handler.HoimiFontNumberLoadHandler;
	import handler.TTCSoundHandler;
	
	import lib.RandomMachine;
	import lib.TweenerBashAni;
	
	public class BetCoin extends Sprite
	{
		private var m_mcBetCoinBase:Poker_Coin_Bet_Base = new Poker_Coin_Bet_Base();
		private var m_coCoinValue:HoimiCoin = new HoimiCoin()
		private var m_tweenerPlayer:TweenerBashAni= new TweenerBashAni();
		private var m_tweenerEnemy:TweenerBashAni = new TweenerBashAni();
		private var m_coinSp:Sprite = new Sprite();
		private var m_arX:Array = [-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,0,1,2,3,4,5,6,7,8,9,10]
		
		private var m_nLastCoinValue:int = 0;
		
		private var m_bPlayer:Boolean=false;
		
		public function BetCoin(bPlayer)
		{
			super();
			m_bPlayer = bPlayer
			this.addChild(m_coinSp);	
			this.addChild(m_mcBetCoinBase)
			this.addChild(m_coCoinValue);
			this.addChild(m_tweenerEnemy);
			this.addChild(m_tweenerPlayer);
			m_coCoinValue.y = 15
			m_coCoinValue.fnDestory();
			HoimiFontNumberLoadHandler.fnGetInstance().addEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			HoimiFontNumberLoadHandler.fnGetInstance().fnLoadFont();
			m_mcBetCoinBase.visible=false;
			m_coCoinValue.visible=false;
		}
		
		private function GameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_NUMBER_FONT_LOAD_COMPLETE_EVENT)
			{
				fnSetCoinTextField();
			}
		}
		
		private function fnSetCoinTextField():void
		{
			HoimiFontNumberLoadHandler.fnGetInstance().removeEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			var tf:TextFormat = new TextFormat();
			tf.font = HoimiFontNumberLoadHandler.fnGetInstance().embeddedFont.fontName;
			tf.size = 38;
			//	tf.align = TextFormatAlign.RIGHT;
			//tf.color = "0xFFFF33";
			//m_mcBetCoin.coin.txt_coin.embedFonts = true; // very important to set
			//m_mcBetCoin.coin.txt_coin.defaultTextFormat = tf;
			//	txt.width = 150
			//txt.x = 37
			//txt.y = 2
		//	m_mcBetCoin.coin.txt_coin.mouseEnabled =false;
			//m_txtCoin.text = 99999+""
			//txt.text ="100" 
			
		}
		
		public function set m_nBetCoins(value:int):void
		{
			var newCoinValue:int = value - m_nLastCoinValue;
			m_nLastCoinValue = value
			fnAddBetAni(newCoinValue);
			
		}

		private function fnAddBetAni(nCoinValue:Number):void
		{
			var nConut:int = nCoinValue/GameConfig.game_table.bet
			var ar:Array = new Array();	
			for(var i:int = 0; i< nConut;i++)
			{
				var mcBet:Poker_Bet_Coin = new Poker_Bet_Coin();
				mcBet.coin_base.visible=false;
				ar.push(mcBet);
			}
			if(nConut>0)
			{
				if(m_bPlayer)
				{
					m_tweenerPlayer.fnSetInit(ar,92,500,0,0,1,1,1,0.25,"easeOutExpo",fnItemTweenerComplete,fnAllTweenFinish);
				}
				else
				{
					m_tweenerPlayer.fnSetInit(ar,92,-500,0,0,1,1,1,0.25,"easeOutExpo",fnItemTweenerComplete,fnAllTweenFinish);
				}
				
			}
			
		
		}
		
		private function fnItemTweenerComplete(nAll:int,nIndex:int):void
		{
			m_mcBetCoinBase.visible=true;
			m_coCoinValue.visible=true;
			var index:int = m_coCoinValue.m_nCoinValue/GameConfig.game_table.bet
			var mcBetCoin:Poker_Bet_Coin = new Poker_Bet_Coin();
			m_coinSp.addChild(mcBetCoin);
			mcBetCoin.coin_base.visible=false;
			TTCSoundHandler.fnGetInstance().fnEmbeddedSoundCoin();
			if(index == 0)
			{
				mcBetCoin.x = 0
			}
			else
			{
				mcBetCoin.x = RandomMachine.fnGetRanOneValue(m_arX);
			}
			mcBetCoin.y = -8* (index+1)
				//	m_mcBetCoin.gotoAndStop(index+1);
			m_tweenerPlayer.y = -8* (index+1)
			var value:int = m_nLastCoinValue - ((nAll -nIndex) * GameConfig.game_table.bet)
			m_coCoinValue.fnSetValue(value);
			m_coCoinValue.x = m_coCoinValue.width/2 - 9;
		}
		
		private function fnAllTweenFinish():void
		{
			
		}
		
	}
}