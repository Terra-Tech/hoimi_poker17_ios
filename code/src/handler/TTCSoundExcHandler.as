package handler
{
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.media.Sound;
	
	import event.TTCSoundEvent;
	
	public class TTCSoundExcHandler extends EventDispatcher
	{
		public function TTCSoundExcHandler(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		
		
		private function fnSoundPlay(sound:Sound):void
		{
			var ev:TTCSoundEvent = new TTCSoundEvent(TTCSoundEvent.SOUND_EVENT,TTCSoundEvent.SOUND_PLAY_EVENT);
			ev.m_eventArgs = sound;
			dispatchEvent(ev);
			
		}
		
		public function fnRanOneSound():void
		{
			var ran:int = Math.round(Math.random()*14)+1;
			
			
		}
	}
}