package item
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import base.TiDisableTextField;
	
	import event.GameEvent;
	
	import handler.TTCSoundHandler;
	
	import ui.ExpBar;
	
	public class GameTopBar extends Sprite
	{
		private var m_mc:Poker_Game_Top_Base = new Poker_Game_Top_Base();
		private var m_coCoinIAP:CoinIAP = new CoinIAP();
		private var m_coDiamondIAP:DiamondIAP = new DiamondIAP();
		private var m_coExpBar:ExpBar = new ExpBar();
		private var m_mcBack:Poker_Btn_Back = new Poker_Btn_Back();
		
		public function GameTopBar()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_mcBack);
			this.addChild(m_coDiamondIAP);
			this.addChild(m_coCoinIAP);
			this.addChild(m_coExpBar);
		
			m_mcBack.x = 25;
			m_mcBack.y = 8;
			m_coCoinIAP.x =  132
			m_coCoinIAP.y =  20
			m_coDiamondIAP.x = 510;
			m_coDiamondIAP.y = 20
			m_coExpBar.y = 81
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcBack);
			m_mcBack.addEventListener(MouseEvent.CLICK,fnClickBack);
			
			
		}
		
		
	
		
		public function fnHideBack():void
		{
			m_mcBack.visible=false;
		}
		
		public function fnShowBack():void
		{
			m_mcBack.visible=true;
		}
		
		private function fnClickBack(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_PREVIOUS_SECTION_EVENT);
			dispatchEvent(ev);
		}
		
	}
}