package menu
{
	import flash.display.Sprite;
	
	import base.TiDisableTextField;
	
	public class MenuRank extends Sprite
	{
		private var m_mc:Poker_Btn_Menu_Rank = new Poker_Btn_Menu_Rank();
		public function MenuRank()
		{
			super();
			this.addChild(m_mc);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
		}
	}
}