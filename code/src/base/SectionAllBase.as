package base
{
	
	
	import flash.display.Sprite;
	
	
	import event.GameEvent;
	
	import handler.TTCMusicHandler;
	import handler.TTCSoundHandler;
	import handler.TTCVoiceHandler;
	
	import lib.TTCModeCheck;
	import lib.TTCShakeStage;
	
	import module.TTCSectionStatusController;
	import module.TTCVirtualControllerBar;
	
	public class SectionAllBase extends Sprite 
	{
		protected var m_section:TTCSectionBase
		protected var m_sectionGp:Sprite = new Sprite();
		protected var m_statusController:TTCSectionStatusController = TTCSectionStatusController.fnGetInstance();
		protected var m_coController:TTCVirtualControllerBar 
		protected var m_strPreviousSection:String;
		protected var m_strNextSection:String;
		protected var m_strCurrentSection:String

		public function SectionAllBase()
		{
			super();
			var simpleSprite:Sprite = new Sprite();
			simpleSprite.x = 0;
			simpleSprite.y = 0;
			simpleSprite.graphics.lineStyle();
			simpleSprite.graphics.beginFill(0x000000);
			simpleSprite.graphics.drawRect(0,0,1200,1280);
			this.addChild(simpleSprite);
			if(m_statusController)
			{
				m_statusController.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);	
			}
			
			if(TTCModeCheck.isDebugBuild())
			{
				m_coController = new TTCVirtualControllerBar();
				m_coController.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			}
		}
		
		protected function fnGameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_NEXT_SECTION_EVENT)
			{
				if(e.m_eventArgs !=null)
				{
					m_strNextSection = e.m_eventArgs
				}
				fnNextSection();
				fnResize();
			}
			else if(e.detail == GameEvent.GAME_PREVIOUS_SECTION_EVENT)
			{
				fnPreviousSection();
				fnResize();
			}
			else if(e.detail == GameEvent.GAME_RETRY_EVENT)
			{
				
				
			}
			else if(e.detail == GameEvent.GAME_REPLAY_EVENT)
			{
				
			}
			else if(e.detail == GameEvent.GAME_PAUSE_EVENT)
			{
			
			
			}
			else if(e.detail == GameEvent.GAME_RESUME_EVENT)
			{
				
			
			}
			else if(e.detail == GameEvent.GAME_SKIP_EVENT)
			{
				
			}
			else if(e.detail == GameEvent.GAME_EXIT_EVENT)
			{
				fnExit();
			}
			else if(e.detail == GameEvent.GAME_SHAKE_STAGE_EVENT)
			{
				fnShakeStage();
			}
			else if(e.detail == GameEvent.GAME_ADD_TO_STAGE_EVENT)
			{trace("add to stage event")
				if(m_sectionGp.numChildren > 1)
				{
					m_sectionGp.removeChildAt(0);		
				}
			}
			trace("numchildren:"+m_sectionGp.numChildren)
		}
		
		protected function fnNextSection():void
		{
			fnExit();
			m_strCurrentSection = m_strNextSection
			fnSelectSection();	
		}
		
		protected function fnPreviousSection():void
		{
			fnExit();
			m_strCurrentSection = m_strPreviousSection
			fnSelectSection();	
		}
		
		protected function fnSelectSection():void
		{
			//be override
		}
		
		public function  fnResize():void
		{
			if(m_section)
			{
				m_section.fnResize();
			}
		}
		
		
		protected function fnExit():void
		{
		
			if(m_statusController)
			{
				m_statusController.fnDestory();
				m_statusController.removeEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);	
				m_statusController = null
			}
			if(m_coController)
			{
				m_coController.removeEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
				m_coController = null;
			}
			if(m_section)
			{
				m_section.fnExit();
				m_section.removeEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
				m_section = null;	
			}
			m_sectionGp.removeChildren();
		}
		
		
		protected function fnShakeStage():void
		{	if(m_section)
			{
				TTCShakeStage.fnShake(m_section);
			}
		}
		
		public function fnResume():void
		{
			TTCSoundHandler.fnGetInstance().fnResume();	
			TTCMusicHandler.fnGetInstance().fnResume();
			TTCVoiceHandler.fnGetInstance().fnResume();
			if(m_section)
			{
				m_section.fnResume()
			}
		}
		
		public function fnPause():void
		{
			TTCSoundHandler.fnGetInstance().fnPauseSound();	
			TTCMusicHandler.fnGetInstance().fnPauseMusic();
			TTCVoiceHandler.fnGetInstance().fnPauseVoice();
			if(m_section)
			{
				m_section.fnPause();
			}
		}
		
	
		
		
	}
}