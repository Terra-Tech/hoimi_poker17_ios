package com.hoimi.lib.net
{
	import com.hoimi.lib.time.Clock;
	import com.hoimi.util.EventManager;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;
	
	public class Transmitter extends URLLoader
	{	
		private var _timer:Clock;
		private var _timeout:int;
		private var _busy:Boolean;
		private var _protocol:int;
		private var _status:int;
		
		public function Transmitter(timeout:int)
		{
			super(null);
			_timer = new Clock();
			_timeout = timeout;
			_busy = false;
		}
		
		public function loop():void
		{
			_timer.run();
		}
		
		public function get busy():Boolean
		{
			return _busy;
		}
		
		public function get(protocol:int, url:String, params:Array):void
		{
			if(_busy)return;
			_busy = true;
			_protocol = protocol;
			var req:URLRequest = new URLRequest();
			req.method = URLRequestMethod.GET;
			if(params && params.length > 0)
			{
				for(var i:int = 0 ; i < params.length ; i++)
				{
					url += "/"+params[i];
				}
			}
			req.url = url;
			
			excute(req);
		}
		
		public function post(protocol:int, url:String, data:String):void
		{
			if(_busy)return;
			
			_busy = true;
			_protocol = protocol;
			var req:URLRequest = new URLRequest();
			req.method = URLRequestMethod.POST;
			var hdr:URLRequestHeader = new URLRequestHeader("Content-type", "application/json");
			req.requestHeaders.push(hdr);
			req.data = data;
			req.url = url;
			
			excute(req);
		}
		
		
		public function restore():void
		{
			_timer.stop();
			_timer.removeEventListener(Clock.ALERT, onTimeout);
			this.removeEventListener(Event.COMPLETE, onComplete);
			this.removeEventListener(IOErrorEvent.IO_ERROR, onIoError);
			this.removeEventListener(HTTPStatusEvent.HTTP_STATUS, onStatus);
			
			_busy = false;
		}
		
		private function onStatus(e:HTTPStatusEvent):void
		{
			_status = e.status;
		}
		
		private function onTimeout(e:Event):void 
		{
			restore();
			EventManager.instance.trigger(new NetEvent(NetEvent.STATE_TIMEOUT, _protocol, null));
		}
		
		private function onComplete(e:Event):void
		{
			restore();
			var data:String = e.target.data as String;
			if(_status == 200)
				EventManager.instance.trigger(new NetEvent(NetEvent.STATE_COMPLETE, _protocol, data));
			else
				EventManager.instance.trigger(new NetEvent(NetEvent.STATE_FAIL, _protocol, data));
		}
		
		private function onIoError(e:IOErrorEvent):void
		{
			restore();
			EventManager.instance.trigger(new NetEvent(NetEvent.STATE_IO_ERROR, _protocol, null));
		}
		
		private function excute(req:URLRequest):void
		{
			this.addEventListener(HTTPStatusEvent.HTTP_STATUS, onStatus, false, 0, true);
			this.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
			this.addEventListener(IOErrorEvent.IO_ERROR, onIoError, false, 0, true);
			this.dataFormat = URLLoaderDataFormat.TEXT;
			_timer.addEventListener(Clock.ALERT, onTimeout, false, 0, true);
			
			try
			{
				this.load(req);	
				_timer.alert(_timeout);
			}
			catch(err:Error)
			{
				trace("Unable to load get URL "+ err.message);			
			}
		}
	}
}