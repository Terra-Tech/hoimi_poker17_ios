package handler
{
	import com.hoimi.util.HttpHelper;
	import com.hoimi.util.Purchase;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	
	import Enum.EnumURL;

	public class IAPLicenseKeyHandler
	{
		public static var GOOGLE_IAP_LICENSE_KEY:Boolean = false;
		private static var m_callFn:Function=null; 
		public function IAPLicenseKeyHandler()
		{
		}
		
		public static function fnGetIAPLicenseKey(callFn:Function = null):void
		{
			m_callFn = callFn
		//	HttpHelper.get(EnumURL.URL_IAP_LICENSE_KEY_SERVER,"",fnIOError,fnStatus,fnTimeOut,fnComplete);
		}
		
		private static function fnIOError(e:IOErrorEvent):void
		{
			GOOGLE_IAP_LICENSE_KEY =false;
		}
		
		private static function fnStatus(status:int):void
		{
			if(status == 200 )
			{
				GOOGLE_IAP_LICENSE_KEY = true;
			}
		}
		
		private static function fnComplete(resp:String):void
		{
			if(GOOGLE_IAP_LICENSE_KEY ==false)
			{
				return;	
			}
			GOOGLE_IAP_LICENSE_KEY = true;
			//Purchase.instance.init(resp);
			if(m_callFn)
			{
				m_callFn();
			}
		}
		
		private static function fnTimeOut():void
		{
			
		}
	}
}