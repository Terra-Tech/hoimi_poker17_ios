package ui
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import Double.DoubleView;
	
	import Enum.EnumMsg;
	
	import base.TiButtonState;
	import base.TiDisableTextField;
	
	import conf.musicConfig;
	
	import event.GameEvent;
	import event.GameMessageEvent;
	
	import handler.TTCMusicHandler;
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	import item.DecisionTiemr;
	import item.ResultPanelCoin;
	import item.ResultPanelExp;
	import item.ResultPanelExpBar;
	
	import lib.MultiLanguagleUISelect;
	import lib.TweenerBashAni;
	
	public class ResultPanel extends Sprite
	{
		private var m_mcMask:Poker_Mask_Base = new Poker_Mask_Base();
		private var m_mc:Poker_Result_Panel = new Poker_Result_Panel();
		private var m_mcCoin:ResultPanelCoin = new ResultPanelCoin();
		private var m_coExpBar:ResultPanelExpBar = new ResultPanelExpBar();
		private var m_mcExp:ResultPanelExp = new ResultPanelExp();
		private var m_coLevelUp:LevelUpPanel = new LevelUpPanel();
		private var m_coDecisionTime:DecisionTiemr = new DecisionTiemr();
		private var m_mcLobby:MovieClip
		private var m_mcOther:MovieClip
		private var m_mcAgain:MovieClip
		private var m_mcDouble:MovieClip
		private var m_buttonState:TiButtonState = new TiButtonState();
		private var m_nNextExp:int = 0
		private var m_nLevel:int = 0;
		private var m_nRewardCoind:int = 0
		private var m_nRewardDiamond:int = 0
		private var m_txtSyetemCommissionMsg:TextField = new TextField();
		private var m_coDoublePanel:DoubleView = new DoubleView();
		private var m_nDoubleBet:Number = 0;
		
		public function ResultPanel()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_txtSyetemCommissionMsg);
			this.addChild(m_mcCoin);
			this.addChild(m_mcExp);
			this.addChild(m_coExpBar);
			this.addChild(m_coDecisionTime);
			this.addChild(m_mcMask);
			this.addChild(m_coLevelUp);
			this.addChild(m_coDoublePanel);
			m_coLevelUp.y = 100
			m_mcCoin.x = -200;
			m_mcCoin.y = -98;
			m_mcExp.x = -200;
			m_mcExp.y = 0;
			m_coExpBar.x = -360
			m_coExpBar.y = 60
			m_mcMask.y = -500
			m_mcMask.alpha = 0;
			m_mcMask.visible=true;
			m_coDecisionTime.scaleX = 0.7
			m_coDecisionTime.scaleY = 0.7
			m_coDecisionTime.x = 230;
			m_coDecisionTime.y = -97.5
			m_txtSyetemCommissionMsg.x = -360
			m_txtSyetemCommissionMsg.y = 240
			m_txtSyetemCommissionMsg.defaultTextFormat = new TextFormat( "Arial", 24, 0xFFFFFF, false, false, false, null, null, TextFormatAlign.CENTER );
			m_txtSyetemCommissionMsg.filters = [new DropShadowFilter(1,45,0x000000,1,1,1,1,1)];
			m_txtSyetemCommissionMsg.width = 720
			m_txtSyetemCommissionMsg.text = TxtLangTransfer.SYSTEM_COMMISSION.replace("{0}",GameConfig.game_playerInfo.skills[6].value*100);
			m_txtSyetemCommissionMsg.visible =false;
			m_txtSyetemCommissionMsg.mouseEnabled=false;
			m_coExpBar.m_mc.base.visible =false;
			m_coDoublePanel.y = -390
		//	m_coLevelUp.fnSetData(15,156,8);
			this.visible=false;
			m_coLevelUp.visible=false;
			var mcLobby:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mc.btn_lobby);
			m_mcLobby = mcLobby;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcLobby);
			m_mcLobby.addEventListener(MouseEvent.CLICK,fnClickLobby);
			var mcOther:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mc.btn_other);
			m_mcOther = mcOther;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcOther);
			var mcAgain:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mc.btn_again);
			m_mcAgain = mcAgain;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcAgain);
			var mcDouble:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mc.btn_double);
			m_mcDouble = mcDouble;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcDouble);
			m_coExpBar.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_coDecisionTime.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			//m_mcMask.addEventListener(MouseEvent.CLICK,fnClickMask);
			m_buttonState.fnDisableAndDarkMcState(m_mcAgain);
			m_buttonState.fnDisableAndDarkMcState(m_mcOther);
			m_buttonState.fnDisableAndDarkMcState(m_mcLobby);
			m_buttonState.fnDisableAndDarkMcState(m_mcDouble);
			m_mcDouble.visible = false;
		}
		
		private function fnGameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_LEVEL_UP_EVENT)
			{
				m_coLevelUp.visible = true;
				m_mcMask.visible=false;
				m_coLevelUp.fnSetData(m_nLevel,m_nRewardCoind,m_nRewardDiamond);
			}
			else if(e.detail == GameEvent.GAME_EXP_TWEEN_FINISH_EVENT)
			{
				m_mcMask.visible=false;
				fnCheckButtonEnable();
				m_coDecisionTime.fnStartResultTime();
			}
			else if(e.detail == GameEvent.GAME_TIME_FINISH)
			{
				if(GameConfig.game_playerInfo.coin < GameConfig.game_table.limit_min  || GameConfig.game_playerInfo.coin > GameConfig.game_table.limit_max)
				{
					var ev2:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
					ev2.m_strType = EnumMsg.MSG_TYPE_OK
					ev2.m_strMsg = TxtLangTransfer.PLEASE_EXIT_TABLE;
					ev2.m_callOkFn = fnPleaseExitTable
					EventManager.instance.trigger(ev2);
					
				}
				else
				{
					fnClickAgain(null);
				}
			}
			else if(e.detail == GameEvent.GAME_DOUBLE_FINISH_EVENT)
			{
				if(GameConfig.game_playerInfo.coin < GameConfig.game_table.limit_min  || GameConfig.game_playerInfo.coin > GameConfig.game_table.limit_max)
				{
					var ev3:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
					ev3.m_strType = EnumMsg.MSG_TYPE_OK
					ev3.m_strMsg = TxtLangTransfer.PLEASE_EXIT_TABLE;
					ev3.m_callOkFn = fnPleaseExitTable
					EventManager.instance.trigger(ev3);
					
				}
			}
			
		}
		
		private function fnPleaseExitTable():void
		{
			GameConfig.game_table = null;
			TTCMusicHandler.fnGetInstance().fnSelectMusic(musicConfig.GAME_MUSIC_MENU);
			var ev3:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_PREVIOUS_SECTION_EVENT,true)
			dispatchEvent(ev3);
		}
		
		private function fnClickLobby(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			TTCMusicHandler.fnGetInstance().fnSelectMusic(musicConfig.GAME_MUSIC_MENU);
			GameConfig.game_table = null;
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_PREVIOUS_SECTION_EVENT,true)
			dispatchEvent(ev);
		}
		
		private function fnClickOther(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			GameConfig.bAgain = false;
			GameConfig.bOther = true;
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NEXT_SECTION_EVENT,true)
			dispatchEvent(ev);
		}
		
		private function fnClickAgain(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			GameConfig.change_enemy_remind--
			if(GameConfig.change_enemy_remind <=0)
			{
				var ev2:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
				ev2.m_strMsg = "\n您的對手已經離開\n\n請重新尋找對手!"
				ev2.m_strType = EnumMsg.MSG_TYPE_OK
				EventManager.instance.trigger(ev2);	
				m_buttonState.fnDisableAndDarkMcState(m_mcAgain);
				m_mcAgain.removeEventListener(MouseEvent.CLICK,fnClickAgain);
				m_coDecisionTime.fnCountDownStop();
				m_coDecisionTime.visible=false;
			}
			else
			{
				GameConfig.bAgain = true;
				GameConfig.bOther = false;
				var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NEXT_SECTION_EVENT,true)
				dispatchEvent(ev);
			}
			
			
		}
		
		public function fnDisplayPanel(nCoin:int,nAddExp:int,nLevel:int,nNextExp,nRewardCoin:int,nRewardDiamond:int):void
		{
			this.visible = true;
			m_nLevel = nLevel
			m_nRewardCoind = nRewardCoin
			m_nRewardDiamond = nRewardDiamond
			m_mcCoin.fnSetValue(nCoin,true,false);
			m_mcExp.fnSetValue(nAddExp);
			m_coExpBar.fnSetResultExp(nLevel,nAddExp);
			if(nCoin>0)
			{
				m_nDoubleBet = nCoin
				m_txtSyetemCommissionMsg.visible = true;
				m_mcDouble.visible = true;
			}
		}
		
		private function fnClickMask(e:MouseEvent):void
		{
			m_mcMask.visible=false;
			//m_coExpBar.fnSetExpBar();
			/*var evExp:GameEvent = new GameEvent(GameEvent.GAME_EXP_CHANGE_EVENT)
				EventManager.instance.trigger(evExp);
			var evCoin:GameEvent = new GameEvent(GameEvent.GAME_COIN_CHANGE_EVENT)
				EventManager.instance.trigger(evCoin);
			var evDiamond:GameEvent = new GameEvent(GameEvent.GAME_DIAMOND_CHANGE_EVENT)
				EventManager.instance.trigger(evDiamond);*/
		}
		
		private function fnClickDouble(e:MouseEvent):void
		{
			m_buttonState.fnDisableAndDarkMcState(m_mcAgain);
			m_mcAgain.removeEventListener(MouseEvent.CLICK,fnClickAgain);
			m_coDecisionTime.fnCountDownStop();
			m_coDecisionTime.visible=false;
			m_mcDouble.visible=false;
			m_coDoublePanel.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_coDoublePanel.fnDisplayPanel(m_nDoubleBet,GameConfig.game_playerRecord.win,GameConfig.game_playerRecord.lose);
		}
		
		private function fnCheckButtonEnable():void
		{
			if(GameConfig.game_playerInfo.coin < GameConfig.game_table.limit_min || GameConfig.game_playerInfo.coin > GameConfig.game_table.limit_max)
			{
				m_buttonState.fnDisableAndDarkMcState(m_mcAgain);
				m_buttonState.fnDisableAndDarkMcState(m_mcOther);
				m_buttonState.fnDisableAndDarkMcState(m_mcDouble);
				m_mcAgain.mouseChildren=false;
				m_mcAgain.mouseEnabled=false;
				m_mcOther.mouseChildren=false;
				m_mcOther.mouseEnabled=false;
				m_mcDouble.mouseChildren=false;
				m_mcDouble.mouseEnabled=false;
				m_mcAgain.removeEventListener(MouseEvent.CLICK,fnClickAgain);
				m_mcOther.removeEventListener(MouseEvent.CLICK,fnClickOther);
				m_mcDouble.removeEventListener(MouseEvent.CLICK,fnClickDouble);
			}
			else
			{
				m_buttonState.fnEnableMcState(m_mcAgain);
				m_buttonState.fnEnableMcState(m_mcOther);
				m_buttonState.fnEnableMcState(m_mcDouble);
				m_mcAgain.addEventListener(MouseEvent.CLICK,fnClickAgain);
				m_mcOther.addEventListener(MouseEvent.CLICK,fnClickOther);
				m_mcDouble.addEventListener(MouseEvent.CLICK,fnClickDouble);
			}
			m_buttonState.fnEnableMcState(m_mcLobby);
		}
		
		public function fnResume():void
		{
			m_coDecisionTime.fnResume()
			if(m_coDoublePanel.visible)
			{
				m_coDoublePanel.fnResume();	
			}
		}
		
		public function fnPause():void
		{
			m_coDecisionTime.fnPause();
			if(m_coDoublePanel.visible)
			{
				m_coDoublePanel.fnPause();
			}
		}
		
		
		public function fnDestory():void
		{
			m_mcLobby.removeEventListener(MouseEvent.CLICK,fnClickLobby);
			m_mcAgain.removeEventListener(MouseEvent.CLICK,fnClickAgain);
			m_mcOther.removeEventListener(MouseEvent.CLICK,fnClickOther);
			m_mcDouble.removeEventListener(MouseEvent.CLICK,fnClickDouble);
			m_coDoublePanel.removeEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_coDecisionTime.removeEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_coDecisionTime.fnCountDownStop();
			m_coExpBar.fnDestory();
		}
	}
}