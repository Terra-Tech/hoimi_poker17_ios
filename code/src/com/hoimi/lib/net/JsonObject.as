package com.hoimi.lib.net
{
	public class JsonObject
	{
		public function JsonObject(obj:* = null)
		{
			input(obj);
		}
		
		public function input(value:*):void
		{
			if(value){
				if(value is String)
					byString(value);
				else if(value is Object)
					byObject(value);
			}
		}
		
		public function byString(str:String):void
		{
			try
			{
				byObject(JSON.parse(str));
			}
			catch(e:Error)
			{
				trace("parse error ");
			}
		}
		
		public function byObject(obj:Object):void
		{
			for (var p:String in obj)
			{
//				trace('byObject['+p+'] ' + this.hasOwnProperty(p));
				if(this.hasOwnProperty(p))
				{
					if(this[p] is JsonObject)
						this[p].input(obj[p]);
					else
						this[p] = obj[p];
				}
					
			}
		}
		
		public function toString():String
		{
			return JSON.stringify(this);
		}
	}
}