package model
{
	import com.hoimi.lib.net.JsonObject;

	public class GameTable extends JsonObject
	{
		/** table
		 * 1:{"result" : 1, 'table':[{id:1,bet:10,max_ratio1:3,max_ratio2:6,limit_min:100,limit_max:10000}]}
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 */
		public var id:int;
		public var name:String;
		public var bet:Number;
		public var max_ratio1:Number;
		public var max_ratio2:Number;
		public var limit_min:Number;
		public var limit_max:Number;
		public var exp_win:Number;
		public var exp_lose:Number;
		public var time_pick:Number;
		public var time_1st_bet:Number;
		public var time_buying:Number;
		public var time_2nd_bet:Number;
		public var time_result:int;
		
		public function GameTable(table:*){
			super(table);
		}
	}
}