package lib
{
	import flash.display.MovieClip;
	import flash.display.Sprite;

	public class TTCPauseResumeItem
	{
		public function TTCPauseResumeItem()
		{
		}
		
		public static function fnPauseItem(mcItem:Sprite):void
		{
		//	mcItem.gotoAndStop(mcItem.currentFrame);
			for(var i :int = 0 ;i<mcItem.numChildren;i++)
			{
				var mc:MovieClip = mcItem.getChildAt(i) as MovieClip;
				if(mc)
				{
					mc.gotoAndStop(mc.currentFrame);
					for(var j :int = 0 ;j< mc.numChildren;j++)
					{
						var mc2:MovieClip = mc.getChildAt(j) as MovieClip;
						if(mc2)
						{
							mc2.gotoAndStop(mc2.currentFrame);
							for(var k :int = 0 ;k< mc2.numChildren;k++)
							{
								var mc3:MovieClip = mc2.getChildAt(k) as MovieClip;
								if(mc3)
								{
									mc3.gotoAndStop(mc3.currentFrame);
								}
							}
						}
					}
				}
			}
		}
		
		public static function fnResumeItem(mcItem:Sprite):void
		{
			for(var i :int = 0 ;i<mcItem.numChildren;i++)
			{
				var mc:MovieClip = mcItem.getChildAt(i) as MovieClip;
				if(mc)
				{
					if(mc.currentFrame != mc.totalFrames  || mc.totalFrames == 1)
					{
						mc.gotoAndPlay(mc.currentFrame);
						
						for(var j :int = 0 ;j< mc.numChildren;j++)
						{
							var mc2:MovieClip = mc.getChildAt(j) as MovieClip;
							if(mc2)
							{
								mc2.gotoAndPlay(mc2.currentFrame);
								for(var k :int = 0 ;k< mc2.numChildren;k++)
								{
									var mc3:MovieClip = mc2.getChildAt(k) as MovieClip;
									if(mc3)
									{
										mc3.gotoAndPlay(mc3.currentFrame);
									}
								}
							}
						}
					}
					
				}
			}
		}
	}
}