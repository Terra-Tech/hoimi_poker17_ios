package Skill.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class SkillRaise extends JsonObject
	{
		/// 升級後的錢
		public var player_coin:Number = 0;
		public var player_diamond:Number = 0;
		/// 升級了哪個技能的index
		public var skill:int = 0;
		/// 升級後現在技能的資訊
		public var current_skill:Skill = new Skill();
		/// 升級後下一技能的資訊, 如current_skill已到最高級則next_skill為null
		public var next_skill:Skill = new Skill();
		
		public function SkillRaise(obj:*=null)
		{
			super(obj);
		}
	}
}