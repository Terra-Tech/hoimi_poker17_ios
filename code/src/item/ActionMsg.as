package item
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import Enum.EnumAction;
	import Enum.EnumLang;
	import Enum.EnumMsg;
	
	
	public class ActionMsg extends Sprite
	{
		private var m_mcAction:Poker_Msg_Action =new Poker_Msg_Action();
		private var m_mcAni:Poker_Circle_Ani = new Poker_Circle_Ani();
		private var m_mc:MovieClip
		private var m_timer:Timer = new Timer(3000,1)
		
		public function ActionMsg()
		{
			super();
			this.addChild(m_mcAction);
			this.addChild(m_mcAni);
			m_mcAni.scaleX=1.3
			m_mcAni.scaleY = 1.3
			m_mcAni.y = 40
			m_mcAction.EN.visible=false;
			m_mcAction.TW.visible=false;
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				m_mcAction.TW.visible=true;
				m_mc = m_mcAction.TW
			}
			else
			{
				m_mcAction.EN.visible=true;
				m_mc = m_mcAction.EN
			}
		}
		
		public function fnAction(str:String):void
		{
			switch(str)
			{
				case EnumAction.ACTION_RAISE:
					m_mc.gotoAndStop(1);
					break;
				case EnumAction.ACTION_CALL:
					m_mc.gotoAndStop(2);
					break;
				case EnumAction.ACTION_FOLD:
					m_mc.gotoAndStop(3);
					break;
					
			}
			m_mcAni.gotoAndPlay(2);
			//m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTimerComplete);
			//m_timer.start();
		}
		
		private function fnTimerComplete(e:TimerEvent):void
		{
			m_timer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnTimerComplete);
			m_mcAction.visible = false;
		}
	}
}