package com.hoimi.lib.fsm
{
	import flash.display.DisplayObject;

	public interface IHandler
	{
		function get stateMachine():StateMachine;
		
		function add(child:DisplayObject):void;
		
		function remove(child:DisplayObject):void;
		
		function to(state:String):void;
	}
}