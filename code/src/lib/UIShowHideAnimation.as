package lib
{
	import flash.display.Sprite;
	
	import caurina.transitions.Tweener;

	public class UIShowHideAnimation
	{
		public function UIShowHideAnimation()
		{
		}
		
		public static function fnTweenShow(sp:Sprite,fun:Function):void
		{
			sp.scaleX = 0;
			sp.scaleY = 0;
			Tweener.addTween(sp,{scaleX:1,scaleY:1,time:0.2,transition:"easeOutBack",onComplete:fun});
		}
		
		public static function fnTweenHide(sp:Sprite,fun:Function):void
		{
			Tweener.addTween(sp,{scaleX:0,scaleY:0,time:0.2,transition:"easeInBack",onComplete:fun});
		}
		
		
	}
}