package Enum
{
	import flash.display.Sprite;

	public class EnumAction 
	{
		public function EnumAction()
		{
		}
		public static const ACTION_RAISE:String = "raise";
		public static const ACTION_CALL:String = "call";
		public static const ACTION_FOLD:String = "fold";
	}
}