package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class LottoInfo extends NetPacket
	{/** lotto
		* 1:{'result' : 1, 'player_coin':wallet.coin, 'lotto_bet':lotto_bet}
		* -1:token param miss
		* -2:can't find token
		* -3:token expire
		* -4:param miss 
		* -5:lotto_multiple error
		* -6:game is over
		* -7:coin not enought
		*/
		public var player_coin:Number;
		public var lotto_bet:Number;
		public function LottoInfo(lotto:*){
			super(lotto);
		}
	}
}