package Prize
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	
	import flash.events.Event;
	import flash.utils.getTimer;
	
	import Prize.Model.Prize;
	import Prize.Model.PrizeAmount;
	import Prize.Model.PrizeInfo;
	import Prize.Model.PrizeList;
	
	import event.GameEvent;
	
	public class PrizeController
	{
		/// UI被加到螢幕上
		static public const EVENT_PRIZE_INIT:String = 'PrizeInit';
		/// UI被移除
		static public const EVENT_PRIZE_DESTORY:String = 'PrizeDestory';
		/// Panel 被打開
		static public const EVENT_PRIZE_SHOW:String = 'PrizeShow';
		/// Panel 被關掉
		static public const EVENT_PRIZE_HIDE:String = 'PrizeHide';
		/// 領獎
		static public const EVENT_PRIZE_GAIN:String = 'PrizeGain';
		
		private const TIME_COUNTDOWN:int = 60000 * 1;
		private var _view:IPrizeView;
		private var _timestamp:int = -1;
		private var _timestamp_list:int = -1;
		private var _amount:PrizeAmount = new PrizeAmount();
		private var _list:PrizeList = new PrizeList();
		private var _prize:Prize = new Prize();
		private var _now:int = 0;
		private var m_nFetchId:Number
		
		public function PrizeController(view:IPrizeView)
		{
			_view = view;
			
			EventManager.instance.register(EVENT_PRIZE_INIT, init);
		}
		
		public function loop():void
		{
			if(!_view.open)
			{
				_now = getTimer();
				if(_now - _timestamp > TIME_COUNTDOWN){
					_timestamp = _now;
					amount();	
				}
			}
		}
		
		private function destory(e:Event):void
		{
			EventManager.instance.remove(EVENT_PRIZE_DESTORY, destory);
			EventManager.instance.remove(EVENT_PRIZE_SHOW, show);
			EventManager.instance.remove(EVENT_PRIZE_HIDE, hide);
			EventManager.instance.remove(EVENT_PRIZE_GAIN, gain);
			EventManager.instance.register(EVENT_PRIZE_INIT, init);
		}
		
		private function init(e:Event):void
		{
			EventManager.instance.remove(EVENT_PRIZE_INIT, init);
			EventManager.instance.register(EVENT_PRIZE_DESTORY, destory);
			EventManager.instance.register(EVENT_PRIZE_SHOW, show);
			EventManager.instance.register(EVENT_PRIZE_HIDE, hide);
			EventManager.instance.register(EVENT_PRIZE_GAIN, gain);
			amount();
		}
		
		private function gain(e:Event):void
		{
			if(!_view.open)return;
			fetch();
		}
		
		private function fetch():void
		{
			if(_list.prizes.length>0)
			{
				var p:PrizeInfo = _list.prizes[_list.prizes.length-1]
				m_nFetchId = p.id
				fnNetFetch();
			}
		}
		
		private function fnNetFetch(e:GameEvent = null):void
		{
			_view.showLoading();
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnNetFetch);
			SessionManager.instance.request(Protocol.PRIZE, {token:GameConfig.game_playerInfo.token, prize_id:m_nFetchId}, onSuccess, onFail);
		}
		
		private function hide(e:Event):void
		{
			trace("Prize Hide");
		}
		
		private function show(e:Event):void
		{
			if(_view.open)return;
			if(_timestamp_list > 0 && _now - _timestamp_list < TIME_COUNTDOWN){
				_view.showList(_list.prizes);
			}else{
				_timestamp_list = _now;
				list();	
			}
		}
		
		private function amount():void
		{
		//	_view.showLoading();
			SessionManager.instance.request(Protocol.PRIZE_AMOUNT, {token:GameConfig.game_playerInfo.token}, onSuccess, onFail);
		}
		
		private function list(e:NetEvent=null):void
		{
			_view.showLoading();
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,list);
			SessionManager.instance.request(Protocol.PRIZE_LIST, {token:GameConfig.game_playerInfo.token}, onSuccess, onFail);
		}
		
		
		
		private function onSuccess(e:NetEvent):void{
			trace("on prize " + e.data);
			
			var net:NetPacket = new NetPacket(e.data);
			if(net.result == 1){
				switch(e.protocol){
					case Protocol.PRIZE_AMOUNT:
						_amount.byString(e.data);
						if(_amount.prize_amount > 0)
							_view.alert();
						break;
					case Protocol.PRIZE_LIST:
						EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,list);
						_list.byString(e.data);
						_view.showList(_list.prizes);
						_view.hideLoading();
						break;
					case Protocol.PRIZE:		
						EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnNetFetch);
						_prize.byString(e.data);
						_view.showGain(_prize);
						var p:PrizeInfo;
						var i:int, len:int = _list.prizes.length;
						for (i = 0; i< len; i++){
							p = _list.prizes[i]
							if(p.id == _prize.prize_id){
								_list.prizes.removeAt(i);
								break;
							}
						}
						_view.showList(_list.prizes);
						if(_list.prizes.length == 0){
							_view.gained();
						}
						else
						{
							fetch()
						}
						_view.hideLoading();
						break;
					default:
						break;
				}
				
			}else{
				_view.hideLoading();
				_view.serverError("prize amount inner error : " + net.result);
			}
		}
		
		private function onFail(e:NetEvent):void{
			_view.hideLoading();
			_view.netError("prize amount net error : " + e.protocol);
		}
	}
}