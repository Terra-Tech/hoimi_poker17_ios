package base
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import button.BtnBack;
	
	import event.MPEvent;
	
	import handler.TTCSoundHandler;
	
	public class TopBarBase extends Sprite
	{
		protected var m_mcTop:MP_Top_Bar_Bg = new MP_Top_Bar_Bg();
		protected var m_coBtnBack:BtnBack = new BtnBack();
		public function TopBarBase()
		{
			super();
			m_mcTop.x =-10
			m_coBtnBack.x = 20
			m_coBtnBack.y = 7	
			m_coBtnBack.addEventListener(MouseEvent.CLICK,fnClickEventHandler);
		}
		
		private function fnClickEventHandler(e:MouseEvent):void
		{
			GameConfig.stage_quiz = null;
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			var ev:MPEvent = new MPEvent(MPEvent.MP_EVENT,MPEvent.MP_BACK_EVENT);
			dispatchEvent(ev);
		}
		
		public function fnHideBack():void
		{
			m_coBtnBack.visible = false;
		}
		
		public function fnResize():void
		{
			m_coBtnBack.x +=GameConfig.object_adjuct_position_x;
		}
	}
}