package FB.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class FacebookFriends extends JsonObject
	{
		public var data:Array = [];
		public var summary:FacebookSummary = new FacebookSummary(null);
		public var paging:FacebookPaging = new FacebookPaging();
		public function FacebookFriends(obj:*=null)
		{
			super(obj);
			var i:int = 0, len:int = data.length, fber:FacebookFriend;
			for (;i<len;i++){
				fber = new FacebookFriend(data[i]);
				data[i] = fber;
			}
		}
	}
}