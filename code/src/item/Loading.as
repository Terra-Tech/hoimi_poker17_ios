package item
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	
	public class Loading extends Sprite
	{
		private var m_mc:Poker_Loading = new Poker_Loading();
		public function Loading()
		{
			super();
			EventManager.instance.register(GameEvent.GAME_LOADING_DISPLAY_EVENT,fnDisplayPanel);
			EventManager.instance.register(GameEvent.GAME_LOADING_HIDE_EVENT,fnHidePanel);
			m_mc.x = 360;
			m_mc.y = 600
			fnSetMask();
			this.addChild(m_mc);
			fnTweenCoin();
			this.visible = false;
		}
		
		private function fnSetMask():void
		{
			var masker:Sprite = new Sprite();
			masker.graphics.beginFill(0);
			masker.graphics.drawRect( 0 , 0 , 720 , 1280);
			masker.graphics.endFill();
			masker.alpha = 0
			this.addChild(masker);
			
		}
		
		private function fnTweenCoin():void
		{
			Tweener.addTween(m_mc,{y:550,time:0.5,transition:"easeOutQuart",onComplete:fnAfterTween});
		}
		
		private function fnAfterTween():void
		{
			Tweener.addTween(m_mc,{y:600,time:0.5,transition:"easeInQuart",onComplete:fnTweenCoin});
		}
		
		private function fnDisplayPanel(e:GameEvent):void
		{
			this.visible = true;
		}
		
		private function fnHidePanel(e:GameEvent):void
		{
			this.visible = false;
		}
	}
}