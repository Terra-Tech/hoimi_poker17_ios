package item
{
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	
	import FB.Model.FacebookFriend;
	
	import handler.TxtLangTransfer;
	
	import lib.CoinValueTransfer;
	
	import model.CoinRankInfo;
	
	public class RankItem extends Sprite
	{
		private var m_mc:Poker_Rank_Item_Bg = new Poker_Rank_Item_Bg();
		
		private var m_loader:Loader = new Loader();
		private var m_image:Array = [];
		private var _data:CoinRankInfo;
		
		public function RankItem(f:CoinRankInfo,bSelf:Boolean = false)
		{
			super();
			this.addChild(m_mc);
			if(!bSelf)
			{
				m_mc.self.visible = false;
			}
			this.addChild(m_mc);
			_data = f;
			if(f.rank)
			{
				m_mc.txt_rank.text = f.rank.toString();
			}
			else
			{
				m_mc.txt_rank.text = "-"
			}
			if(f.name)
			{
				if(f.name == "幹你娘")
				{
					m_mc.txt_name.text = "不雅ID"
				}
				else
				{
					m_mc.txt_name.text = f.name	
				}
			}
			else
			{
				m_mc.txt_name.text = "";
			}
			
			if(f.coin)
			{
				m_mc.txt_coin.text = CoinValueTransfer.fnCoinValueTransferTS(f.coin);
			}
			else
			{
				m_mc.txt_coin.text ="";
			}
			loadPicture();
		}
		
		private function loadPicture():void{
			if(_data.image){
				_data.image.x = -this.width / 2;
				this.addChild(_data.image);
			}else{
				m_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeHandler);
				m_loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
				m_loader.x = -this.width / 2;
				this.addChild(m_loader);
				m_loader.load(new URLRequest("https://graph.facebook.com/"+_data.fb_id+"/picture?width=120&height=120"));
			}
		}
		
		protected function errorHandler(event:IOErrorEvent):void
		{
			trace('load fb pic error');
		}
		
		protected function completeHandler(event:Event):void
		{
			trace('load fb pic complete');
			//_data.image = m_loader.content;
		}		
	}
}