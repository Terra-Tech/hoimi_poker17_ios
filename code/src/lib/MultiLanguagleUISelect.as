package lib
{
	import flash.display.MovieClip;
	
	import Enum.EnumLang;

	public class MultiLanguagleUISelect
	{
		public function MultiLanguagleUISelect()
		{
		}
		
		public static function fnMultiLanguageUi(mc:MovieClip):MovieClip
		{
			var mcNew:MovieClip 
			mc.TW.visible = false
			mc.EN.visible = false
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				mc.TW.visible = true
				mcNew = mc.TW
			}
			else
			{
				mc.EN.visible = true
				mcNew = mc.EN
			}
			return	mcNew
		}
	
	}
}