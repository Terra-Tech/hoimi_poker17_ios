package lib
{
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import caurina.transitions.Tweener;

	public class TweenerRepeat
	{
		public function TweenerRepeat()
		{
			super();
		}
		
		private var m_nValue:int= 0;
		private var m_nTime:Number
		private var m_callFnItem:Function
		private var m_callFn:Function;
		private var m_nCount:int = 0
		private var m_nTimer:Timer 
		
	
		public function fnSetInit(nCount:int,nTime:Number,nValue,callFnItem:Function, callFn:Function):void
		{
			
			m_nValue = nValue
			m_nCount = nCount;
			m_nTime = nTime * 1000
			m_nTimer = new Timer(m_nTime,m_nCount)
			m_callFnItem = callFnItem
			m_callFn = callFn;
			m_nTimer.addEventListener(TimerEvent.TIMER,fnTimerTickEvent);
			m_nTimer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTimerCompleteEvent);
			m_nTimer.start();
		}
		
		
		private function fnTimerTickEvent(e:TimerEvent):void
		{
			m_callFnItem.call(m_callFnItem,m_nValue);
		}
		
		private function fnTimerCompleteEvent(e:TimerEvent):void
		{
			m_nTimer.removeEventListener(TimerEvent.TIMER,fnTimerTickEvent);
			m_nTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnTimerCompleteEvent);
			m_nTimer.start();
			m_callFn();
		}
		
	
	}
}