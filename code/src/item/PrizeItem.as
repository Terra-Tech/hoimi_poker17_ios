package item
{
	import flash.display.Sprite;
	
	public class PrizeItem extends Sprite
	{
		private var m_mc:Poker_Prize_Item = new Poker_Prize_Item();
		
		public function PrizeItem(nIndex:int,strDate:String,nType:int,nAmount:Number,strFriend:String)
		{
			super();
			this.addChild(m_mc);
			if(nIndex%2 == 0)
			{
				m_mc.base.visible=false;
			}
			var strTime:String = strDate.slice(5);
			m_mc.txt_time.text = "時間:"+ strTime
			if(nType == 0 || nType == 1)
			{
				m_mc.txt_type.text = "物品:金幣" ;	
			}
			m_mc.txt_amount.text = "數量:"+nAmount.toString();
			var Msg:String="";
			if(nType == 0)
			{
				m_mc.txt_msg.text = "說明:FB好友"+strFriend+"登入遊戲為您帶來了金幣獎勵"
			}
			else if(nType == 1)
			{
				m_mc.txt_msg.text = "說明:Hoimi系統金幣獎勵"
			}
		 
			this.mouseChildren=false;
			this.mouseEnabled=false;
		}
	}
}