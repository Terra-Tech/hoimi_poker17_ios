package com.hoimi.util
{
	public class RegExpHelper
	{
		public function RegExpHelper(){}
		
		//判断是否为中文字符串                  
		public static function matchChinese(word:String):Boolean 
		{  
			return  match(/[\一-\龥]/, word);
		}
		//匹配邮件地址
		public static function matchEmail(email:String):Boolean 
		{  
			return  match(/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/, email);
		}
		//匹配网址                 
		public static function matchURL(url:String):Boolean 
		{  
			return  match(/[a-zA-z]+:\/\/[^\s]*/, url); //  或者/http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/
		} 
		//匹配电话号码                
		public static function matchPhone(phone:String):Boolean
		{
			return  match(/\d{3}-\d{8}|\d{4}-\d{7}/, phone); //匹配形式如 0511-6666666 或 021-66666666
		}
		//匹配QQ号              
		public static function matchQQ(qq:String):Boolean
		{
			return  match(/[1-9][0-9]{4,}/, qq); //腾讯QQ号从10000开始
		}
		//匹配邮政编码             
		public static function matchPost(post:String):Boolean
		{
			return  match(/[1-9]\d{5}(?!\d)/, post); 
		}
		//匹配身份证            
		public static function matchIDcard(idcard:String):Boolean
		{
			return  match(/\d{15}|\d{18}/, idcard); 
		}
		//匹配时间            
		public static function matchTime(time:String):Boolean
		{
			return  match(/([0-1]?[0-9]|2[0-3]):([0-5][0-9])/, time); 
		}
		//截取字符串            
		public static function interception(source:String,prefix:String,suffix:String):String
		{  
			var reg:RegExp=new RegExp("(?<="+prefix+").*?(?="+suffix+")","s");  
			var obj:String=reg.exec(source).toString();  
			return obj;  
		}
		//匹配多项          
		public static function matchMore(source:String,prefix:String,suffix:String):Array
		{  
			var reg:RegExp=new RegExp("(?<="+prefix+").*?(?="+suffix+")","g");  
			var obj:Array=source.match(reg)  
			return obj;  
		}  
		//匹配数字          
		public static function matchNumber(num:String):Boolean 
		{  
			return match(/^\d+$/, num);
		}
		//配對英文大小寫
		public static function matchEnglish(en:String):Boolean
		{
			return match(/[a-zA-Z]/,en);
		}
		//匹配手机          
		public static function matchMobile(num:String):Boolean 
		{   
			return match(/^0?1((3[0-9]{1})|(5[0-9]{1})|(8[0-9]{1})){1}[0-9]{8}$/, num);
		}
		//校验合法时间  
		public static function matchDate(num:String):Boolean 
		{ 
			return match(/\d{4}(\.|\/|\-)\d{1,2}(\.|\/|\-)\d{1,2}/, num);
		}
		//校验字符串：只能输入 min - max 个字母、数字、下划线(常用手校验用户名和密码)  
		public static function matchStrLength(str:String,min:uint,max:uint):Boolean 
		{   
			var reg:RegExp=new RegExp("^(\\w){"+min.toString()+","+max.toString()+"}$");  
			return match(reg, str);
		}
		//any match
		public static function match(pattern:RegExp,matchString:String):Boolean
		{
			return  Boolean(pattern.test(matchString));
		}
	}
}