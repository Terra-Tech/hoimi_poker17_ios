package section
{
	
	
	
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	
	import CoinBack.CoinBackController;
	import CoinBack.CoinBackView;
	
	import Enum.EnumLocalSave;
	import Enum.EnumMsg;
	import Enum.EnumSection;
	
	import Prize.PrizeController;
	import Prize.PrizePanel;
	
	import Refill.RefillController;
	import Refill.RefillView;
	
	import Skill.SkillController;
	import Skill.SkillView;
	
	import base.SectionAllBase;
	
	import conf.musicConfig;
	
	import event.GameEvent;
	import event.GameMessageEvent;
	
	import handler.TTCMusicHandler;
	import handler.TTCSoundHandler;
	import handler.TTCVoiceHandler;
	import handler.TxtLangTransfer;
	
	import item.GameTopBar;
	import item.Loading;
	import item.SoundPanel;
	
	import ui.ErrorMessagePanel;
	import ui.GainCoinPanel;
	import ui.GiftPanel;
	import ui.HelpPanel;
	import ui.IAPPanel;
	import ui.MessagePanel;
	import ui.ReconnectPanel;
	import ui.SettingPanel;
	import ui.Teach;
	
	
	
	public class SectionAll extends SectionAllBase
	{
	//	private var m_coExitPanel:ExitPanel = new ExitPanel();
		private var m_coReconnectPanel:ReconnectPanel = new ReconnectPanel();
		private var m_coMsgPanel:MessagePanel = new MessagePanel();
		private var m_coErrorMsgPanel:ErrorMessagePanel = new ErrorMessagePanel();
		private var m_coLoading:Loading = new Loading();
		private var m_coTopBar:GameTopBar = new GameTopBar();
		private var m_coIAPPanel:IAPPanel = new IAPPanel();
		private var m_coGainCoinPanel:GainCoinPanel = new GainCoinPanel();
		private var m_mcLogo:Poker_Logo = new Poker_Logo();
		private var m_coSettingPanel:SettingPanel = new SettingPanel();
		private var m_coPrizePanel:PrizePanel = new PrizePanel();
		private var _prize:PrizeController;
		private var _coinBack:CoinBackController;
		private var _refill:RefillController
		private var m_skillController:SkillController 
		private var m_coGiftPanel:GiftPanel = GiftPanel.fnGetInstance();

		public function SectionAll()
		{
			this.addChild(m_sectionGp);
		//	this.addChild(m_coExitPanel);
			m_strCurrentSection = EnumSection.SECTION_OPENING
			fnSelectSection();
			this.addChild(m_mcLogo);
			this.addChild(m_coTopBar);
			this.addChild(m_coPrizePanel);
			this.addChild(m_coGainCoinPanel);
			this.addChild(m_coSettingPanel);
			this.addChild(m_coIAPPanel);
			this.addChild(m_coMsgPanel);
			this.addChild(m_coGiftPanel);
			this.addChild(m_coReconnectPanel);
			this.addChild(m_coErrorMsgPanel);
			this.addChild(m_coLoading);
			m_coSettingPanel.visible=false;
			
			m_coTopBar.visible=false;
			m_coTopBar.y = 80
			m_coTopBar.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_coPrizePanel.y = 80;
			m_coPrizePanel.visible=false;
			this.addEventListener(Event.ENTER_FRAME, onLoop);
			EventManager.instance.register(GameEvent.GAME_OPENING_LOAD_COMPLETE,fnInitController);
			m_coGiftPanel.y = 400;
		}
		
		private function onLoop(e:Event):void
		{
			SessionManager.instance.loop();
			if(_prize)_prize.loop();
		}
		
		public function fnPhysicalBack():void
		{
			if(m_strCurrentSection != EnumSection.SECTION_BATTLE && m_strCurrentSection !=EnumSection.SECTION_MENU)
			{
				fnPreviousSection();
				fnResize();
			}
			else if(m_strCurrentSection == EnumSection.SECTION_MENU)
			{
				var ev:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
				ev.m_strMsg = TxtLangTransfer.EXIT_GAME
				ev.m_strType =  EnumMsg.MSG_TYPE_YES_NO
				ev.m_callYesFn = fnExitGame
				EventManager.instance.trigger(ev);
			}
		}
		
		private function fnExitGame():void
		{
			TTCMusicHandler.fnGetInstance().fnDestory();
			TTCSoundHandler.fnGetInstance().fnDestory();
			TTCVoiceHandler.fnGetInstance().fnDestory()
			EventManager.instance.destroy();
			NativeApplication.nativeApplication.exit();
		}
		
		override protected function fnSelectSection():void
		{
			m_section = null;
			switch(m_strCurrentSection)
			{
				case EnumSection.SECTION_OPENING:
					m_section = new SectionOpening();
					m_strNextSection = EnumSection.SECTION_MENU
					m_coTopBar.visible=false;
					m_coPrizePanel.visible=false;
					m_mcLogo.visible =false;
					break;
				case EnumSection.SECTION_MENU:
					m_section = new SectionMenu();
					m_strNextSection = EnumSection.SECTION_LOBBY
					//m_strPreviousSection = EnumSection.SECTION_MENU;
					m_coTopBar.visible=true;
					m_coTopBar.fnHideBack();
					m_coPrizePanel.visible=false;
					m_mcLogo.visible =true;
					m_coSettingPanel.visible =true;
					fnCheckTeach();
					break;
				case EnumSection.SECTION_LOBBY:
					m_section = new SectionLobby();
					m_strNextSection = EnumSection.SECTION_CONNECTING
					m_strPreviousSection = EnumSection.SECTION_MENU;
					m_coTopBar.visible=true;
					m_coTopBar.fnShowBack();
					m_coPrizePanel.visible=true;
					m_mcLogo.visible =true;
					m_coSettingPanel.visible =true;

					break;
				case EnumSection.SECTION_CONNECTING:
					m_section = new SectionConnecting();
					m_strNextSection = EnumSection.SECTION_BATTLE
					m_strPreviousSection = EnumSection.SECTION_LOBBY;
					m_coTopBar.visible=false;
					m_coPrizePanel.visible=false;
					m_mcLogo.visible =false;
					m_coSettingPanel.visible =false;

					break;
				case EnumSection.SECTION_BATTLE:
					m_section = new SectionBattle();
					m_strNextSection = EnumSection.SECTION_CONNECTING;
					m_strPreviousSection = EnumSection.SECTION_LOBBY
					m_coTopBar.visible=false;
					m_coPrizePanel.visible=false;
					m_mcLogo.visible =true;
					m_coSettingPanel.visible =true;
					break;
				case EnumSection.SECTION_FRIENDS:
					m_section = new SectionFriends();
					m_strNextSection = EnumSection.SECTION_FRIENDS;
					m_strPreviousSection = EnumSection.SECTION_MENU
					m_coTopBar.visible=true;
					m_coTopBar.fnShowBack();
					m_coPrizePanel.visible=false;
					m_mcLogo.visible =true;
					m_coSettingPanel.visible =true;
					break;
				case EnumSection.SECTION_RANK:
					m_section = new SectionRank();
					m_strNextSection = EnumSection.SECTION_RANK;
					m_strPreviousSection = EnumSection.SECTION_MENU
					m_coTopBar.visible=true;
					m_coTopBar.fnShowBack();
					m_coPrizePanel.visible=false;
					m_mcLogo.visible =true;
					m_coSettingPanel.visible =true;
					break;
				case EnumSection.SECTION_SKILL:
					m_section = new SectionSkill();
					m_strNextSection = EnumSection.SECTION_SKILL;
					m_strPreviousSection = EnumSection.SECTION_MENU
					m_coTopBar.visible=true;
					m_coTopBar.fnShowBack();
					m_coPrizePanel.visible=false;
					m_mcLogo.visible =true;
					m_coSettingPanel.visible =true;
					break;
				
			}
			if(m_section ==null)
			{
				return;
			}
			m_section.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_sectionGp.addChild(m_section);
		}
		
		public function onResize():void
		{
			m_coLoading.x = GameConfig.object_adjuct_position_x;
			m_coReconnectPanel.x = GameConfig.object_adjuct_position_x;
			m_coMsgPanel.x = GameConfig.object_adjuct_position_x;
			m_coErrorMsgPanel.x = GameConfig.object_adjuct_position_x;
			m_coTopBar.x = GameConfig.object_adjuct_position_x;
			m_coIAPPanel.x = GameConfig.object_adjuct_position_x;
			m_coGainCoinPanel.x = GameConfig.object_adjuct_position_x;
			m_mcLogo.x = 360+GameConfig.object_adjuct_position_x
			m_coPrizePanel.x = GameConfig.object_adjuct_position_x;
			m_coSettingPanel.x =  GameConfig.object_adjuct_position_x;
			m_coGiftPanel.x = 360 + GameConfig.object_adjuct_position_x;
		}
		
		private function fnInitController(e:GameEvent):void
		{
			EventManager.instance.remove(GameEvent.GAME_OPENING_LOAD_COMPLETE,fnInitController);
			_prize = new PrizeController(m_coPrizePanel);
			EventManager.instance.trigger(new Event(PrizeController.EVENT_PRIZE_INIT));
			_coinBack = new CoinBackController(CoinBackView.fnGetInstance());
			_refill = new RefillController(RefillView.fnGetInstance());
			RefillView.fnGetInstance().init();
			m_skillController = new SkillController(SkillView.fnGetInstance());
			TTCMusicHandler.fnGetInstance().fnSelectMusic(musicConfig.GAME_MUSIC_MENU,0.5);
		}
		
		private function fnCheckTeach():void
		{
			if(GameConfig.first_sign_in)
			{
				GameConfig.first_sign_in = false
				LocalSaver.instance.save(EnumLocalSave.FIRST_SIGN_IN,EnumLocalSave.FIRST_SIGN_IN);	
				var teachPanel:Teach = new Teach();
				this.addChild(teachPanel);
				teachPanel.x = 360 + GameConfig.object_adjuct_position_x;
			}
		}
	}
}