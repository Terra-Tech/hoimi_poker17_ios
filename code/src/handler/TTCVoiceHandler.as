package handler
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.utils.Timer;
	
	import event.TTCVoiceEvent;
	
	import util.collection.TiHashMap;

	public class TTCVoiceHandler extends EventDispatcher
	{
		private static var m_Instance:TTCVoiceHandler;
		private var _m_nVolume:Number = 1;
		private var m_voiceXmlList:Array = new Array();
		private var m_bDownloadComplete:Boolean=false;
		private var m_voiceCollectionLoader:TiHashMap = new TiHashMap();
		private var m_sound:Sound = new Sound();
		private var m_tGetVoiceXmlTimer:Timer=new Timer(500); 
		private var m_nSelectedSound:int=0
		private var m_voice:Sound
		private var m_soundChannel:SoundChannel
		private var m_strSection:String ="";
		private var m_nPosition:Number=0;
		
		public function TTCVoiceHandler()
		{
			
		}
		
		public function get m_nVolume():Number
		{
			return _m_nVolume;
		}

		public function set m_nVolume(value:Number):void
		{
			_m_nVolume = value;
			fnVolumeChange();
		}

		public static function fnGetInstance(): TTCVoiceHandler 
		{
			if ( m_Instance == null )
			{
				m_Instance = new TTCVoiceHandler();
			}
			return m_Instance;
		}
		
		
		
		public function fnLoadXMLFile( url: String ,section:String ): void 
		{
			m_nSelectedSound=0;
			m_bDownloadComplete=false;
			m_voiceXmlList = new Array();
			m_voiceCollectionLoader = new TiHashMap();
			m_strSection = section;
			var myLoader:URLLoader = new URLLoader( new URLRequest( url ) );
			myLoader.addEventListener(Event.COMPLETE , handleLoadVoiceConfigXMLComplete );
			myLoader.addEventListener( IOErrorEvent.IO_ERROR, handleIOErr );
			
		}
		
		private function handleLoadVoiceConfigXMLComplete(e:Event): void 
		{
			var voiceConfig:XML = new XML(e.target.data);
			
			fnLoadVoiceList(voiceConfig);
		}
		
		private function fnLoadVoiceList(voiceConfig:XML):void
		{
			if(m_strSection =="")
			{
				return;
			}
			var xmlList:XMLList=voiceConfig[m_strSection];
			if(!xmlList || xmlList.length()==0)
			{
				m_voiceXmlList=null;
				return
			}
			var voiceXML:XML=xmlList[0]
			for(var i:int=0;i<voiceXML.voice.length();i++)
			{
				m_voiceXmlList.push({name:voiceXML.voice[i].@name,url:voiceXML.voice[i].@url});	
			}
			
			var ev:TTCVoiceEvent = new TTCVoiceEvent(TTCVoiceEvent.VOICE_EVENT,TTCVoiceEvent.VOICE_CONFIT_COMPLETE);
		//	dispatchEvent(ev);
			fnDownloadStart();
		}
		
		public function fnDownloadStart():void
		{trace("download start");
			if(m_bDownloadComplete)
			{
				return;
			}
			m_voiceCollectionLoader=new TiHashMap();
			fnVoiceDownLoad();
		}
		private function fnGetVoiceXmlTimerHandler(e:Event):void
		{
			fnVoiceDownLoad();
		}
		private function fnVoiceDownLoad():void
		{
			trace("voice download")
			m_sound=new Sound();
			if(!m_voiceXmlList)
			{
				m_tGetVoiceXmlTimer.addEventListener(TimerEvent.TIMER,fnGetVoiceXmlTimerHandler);
				m_tGetVoiceXmlTimer.start();
				return;
			}
			else
			{
				m_tGetVoiceXmlTimer.removeEventListener(TimerEvent.TIMER,fnGetVoiceXmlTimerHandler);
				m_tGetVoiceXmlTimer.stop();
			}
			if(m_nSelectedSound >= m_voiceXmlList.length)
			{
				fnVoiceDownFinish();
				return;
			}
			var request:URLRequest= new URLRequest(m_voiceXmlList[m_nSelectedSound].url);
			m_sound.load(request);
			m_sound.addEventListener( IOErrorEvent.IO_ERROR, handleIOErr );
			m_sound.addEventListener(Event.COMPLETE,fnDownloadVoiceComplete)
		}
		
		private function fnDownloadVoiceComplete(e:Event = null):void
		{
			if(m_voiceXmlList == null)
			{
				return;
			}
			var key:String=m_voiceXmlList[m_nSelectedSound].name
			var value:String = m_voiceXmlList[m_nSelectedSound].name
				trace("key:"+key)
				trace("value:"+value);
			m_voiceCollectionLoader.put(key,e.currentTarget);
			if(m_nSelectedSound<m_voiceXmlList.length-1)
			{
				m_nSelectedSound++
				fnVoiceDownLoad();
			}
			else
			{
				fnVoiceDownFinish();
			}
			
		}
		
		private function handleIOErr(e:Event):void
		{
			trace("voice IO error:"+e.currentTarget)
			if(m_nSelectedSound<m_voiceXmlList.length-1)
			{
				m_nSelectedSound++
				fnVoiceDownLoad();
			}
			else
			{
				fnVoiceDownFinish();
			}
		}
		private function fnVoiceDownFinish():void
		{
			m_bDownloadComplete=true;
			var ev:TTCVoiceEvent = new TTCVoiceEvent(TTCVoiceEvent.VOICE_EVENT,TTCVoiceEvent.VOICE_DOWNLOAD_COMPLETE);
			dispatchEvent(ev);
		}
		
		
		public function fnPlayVoice(strName:String):void
		{
			if(m_voiceCollectionLoader)
			{
				if(m_voice)
				{
					m_voice = null;
					m_voice = new Sound();
				}
				if(m_soundChannel)
				{
					m_soundChannel.stop();
					m_soundChannel = new SoundChannel();
				}
				m_voice=m_voiceCollectionLoader.get(strName);
				if(m_voice)
				{
					m_soundChannel=m_voice.play();
					m_soundChannel.soundTransform=new SoundTransform(m_nVolume);
					m_soundChannel.addEventListener(Event.SOUND_COMPLETE,fnVoicePlayFinished);
				}
				else
				{
					trace("fnPlayVoice resource null :" + strName);
					var ev:TTCVoiceEvent = new TTCVoiceEvent(TTCVoiceEvent.VOICE_EVENT,TTCVoiceEvent.VOICE_NULL);
					ev.m_eventArgs = strName;
					dispatchEvent(ev);
				}
				
			}
		}
		
		private function fnVoicePlayFinished(e:Event):void
		{
			if(m_soundChannel)
			m_soundChannel.removeEventListener(Event.SOUND_COMPLETE,fnVoicePlayFinished);
			var ev:TTCVoiceEvent = new TTCVoiceEvent(TTCVoiceEvent.VOICE_EVENT,TTCVoiceEvent.VOICE_PLAY_FINISHED);
			dispatchEvent(ev);
		}
		
		private function fnVolumeChange():void
		{
			if(m_soundChannel)
			{
				m_soundChannel.soundTransform=new SoundTransform(m_nVolume)
			}
		}	
		
		public function fnStopVoice():void
		{
			if(m_voice)
			{
				m_voice = null	
			}
			if(m_soundChannel)
			{
				m_soundChannel.stop();
				m_soundChannel = null;	
			}
			
		}
		
		public function fnPauseVoice():void
		{
			if(m_soundChannel)
			{
				m_nPosition = m_soundChannel.position;
				m_soundChannel.stop();
			}
		}
		
		public function fnResume():void
		{
			if(m_voice && m_soundChannel)
			{
				m_soundChannel = m_voice.play(m_nPosition,1,new SoundTransform(m_nVolume));
			}
		}
		
		public function fnDestory():void
		{
			if(m_tGetVoiceXmlTimer)
			{
				m_tGetVoiceXmlTimer.stop();
				m_tGetVoiceXmlTimer.removeEventListener(TimerEvent.TIMER,fnGetVoiceXmlTimerHandler);
			}
			if(m_sound)
			{
				m_sound.removeEventListener( IOErrorEvent.IO_ERROR, handleIOErr );
				m_sound.removeEventListener(Event.COMPLETE,fnDownloadVoiceComplete)
				m_sound = new Sound();
			}
			if(m_soundChannel)
			{
				m_soundChannel.removeEventListener(Event.SOUND_COMPLETE,fnVoicePlayFinished);			
				m_soundChannel.stop();
				m_soundChannel = null;
			}
			m_nPosition = 0;
			m_nSelectedSound = 0;
		}
		
		public function fnReCreateInstance():void
		{
			fnDestory();
			m_Instance = new TTCVoiceHandler();
		}

	}
}