package Prize.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class PrizeInfo extends JsonObject
	{
		public var id:Number = 0;
		public var uuid:String;
		public var item_id:int;
		public var amount:int;
		public var friend:String;
		public var create_date:String;
		
		public function PrizeInfo(p:*){
			super(p);
		}
	}
}