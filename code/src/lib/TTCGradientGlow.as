package lib
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.BitmapFilterType;
	import flash.filters.GradientGlowFilter;
	
	import util.collection.TiHashMap;

	public class TTCGradientGlow
	{
		private var m_gradientGlow: GradientGlowFilter = new GradientGlowFilter();
		private var m_bEnable:Boolean = false;
		private var m_mc:Sprite
		private var m_hash:TiHashMap = new TiHashMap();
		
		public function TTCGradientGlow()
		{
			
		}
		
		public function fnAddGlowMC(mc:Sprite):void
		{
			initGradientGlow();
			m_mc = mc;
			m_hash.put(mc.name,mc);
			mc.mouseChildren =false;
			mc.addEventListener(MouseEvent.MOUSE_DOWN,fnMouseOver);
			mc.addEventListener(MouseEvent.MOUSE_OVER,fnMouseOver);	
			mc.addEventListener(MouseEvent.MOUSE_MOVE,fnMouseOver);
			mc.addEventListener(MouseEvent.MOUSE_OUT,fnMouseOut);	
			mc.addEventListener(MouseEvent.MOUSE_UP,fnMouseOut);	
		}
		
		public function fnRemoveGlowMC(mc:Sprite):void
		{
			var sp:Sprite = m_hash.getValue(mc.name);
			if(sp)
			{
				sp.filters = []
				sp.mouseChildren =false;
				sp.removeEventListener(MouseEvent.MOUSE_DOWN,fnMouseOver);
				sp.removeEventListener(MouseEvent.MOUSE_OVER,fnMouseOver);	
				sp.removeEventListener(MouseEvent.MOUSE_MOVE,fnMouseOver);
				sp.removeEventListener(MouseEvent.MOUSE_OUT,fnMouseOut);	
				sp.removeEventListener(MouseEvent.MOUSE_UP,fnMouseOut);	
				sp = null;
			}
		
		}
		
		
		private function initGradientGlow():void
		{
			m_gradientGlow.distance = 0;
			m_gradientGlow.angle = 45;
			m_gradientGlow.colors = [0xFFFFFF, 0xFFFFFF, 0xAAAAAA];
			m_gradientGlow.alphas = [0, 1, 1, 1, 1];
			m_gradientGlow.ratios = [0, 161, 255];
			m_gradientGlow.blurX = 10;
			m_gradientGlow.blurY = 10;
			m_gradientGlow.strength = 5;
			m_gradientGlow.quality = BitmapFilterQuality.HIGH;
			m_gradientGlow.type = BitmapFilterType.OUTER;
			m_gradientGlow.knockout = false;
		}
		
		private function fnMouseOver(e:MouseEvent):void
		{
			if(m_bEnable)
			{
				return;
			}
			var mc:Sprite = e.currentTarget as Sprite;
			mc.filters = [m_gradientGlow]
		}
		
		private function fnMouseOut(e:MouseEvent):void
		{
			if(m_bEnable)
			{
				return;
			}
			var mc:Sprite = e.currentTarget as Sprite;
			mc.filters = []
		}
		
		public function fnGlowbEnable(bValue:Boolean):void
		{
			m_bEnable = bValue;
			if(bValue)
			{
				m_mc.filters = [m_gradientGlow]
			}
			else
			{
				m_mc.filters= []			
			}
		}
		
		public function fnDestory():void
		{
			for each( var sp:Sprite in m_hash)
			{
				if(sp)
				{
					fnRemoveGlowMC(sp);
				}
			}
			m_mc = null;
			m_hash.clear();
			
		}
	}
}