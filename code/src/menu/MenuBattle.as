package menu
{
	import flash.display.Sprite;
	
	import base.TiDisableTextField;
	
	public class MenuBattle extends Sprite
	{
		private var m_mc:Poker_Btn_Menu_Battle = new Poker_Btn_Menu_Battle();
		public function MenuBattle()
		{
			super();
			this.addChild(m_mc);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
		}
	}
}