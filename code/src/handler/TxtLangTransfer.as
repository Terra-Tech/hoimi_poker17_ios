package handler
{
	import Enum.EnumLang;
	import Enum.EnumMsg;

	public class TxtLangTransfer
	{
		public function TxtLangTransfer()
		{
		}
		
		public static function get PRESS_SCRREN_START_GAME():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.PRESS_SCREEN_START_GAME_TW
			}
			else
			{
				return EnumMsg.PRESS_SCREEN_START_GAME_EN
			}
			return 	EnumMsg.PRESS_SCREEN_START_GAME_EN	
		}
		
		public static function get INPUT_NICKNAME():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.INPUT_NICKNAME_TW
			}
			else
			{
				return EnumMsg.INPUT_NICKNAME_EN
			}
			return 	EnumMsg.INPUT_NICKNAME_EN	
		}
		
		public static function get RECONNECT():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.RECONNECT_TW
			}
			else
			{
				return EnumMsg.RECONNECT_EN
			}
			return  EnumMsg.RECONNECT_EN;
		}
		
		public static function get NICKNAME_ALREADY_USE():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.NICKNAME_ALREADY_USE_TW
			}
			else
			{
				return EnumMsg.NICKNAME_ALREADY_USE_EN
			}
			return 	EnumMsg.NICKNAME_ALREADY_USE_EN	
		}
		
		public static function get NICKNAME_EXCEED_LIMIT():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.NICKNAME_EXCEED_LIMIT_TW
			}
			else
			{
				return EnumMsg.NICKNAME_EXCEED_LIMIT_EN
			}
			return 	EnumMsg.NICKNAME_EXCEED_LIMIT_EN	
		}
		
		public static function get NICKNAME_ILLEGAL():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.NICKNAME_ILLEGAL_TW
			}
			else
			{
				return EnumMsg.NICKNAME_ILLEGAL_EN
			}
			return 	EnumMsg.NICKNAME_ILLEGAL_EN
		}
		
		public static function get REGULATION_UNCHECK():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.REGULATION_UNCHECK_TW
			}
			else
			{
				return EnumMsg.REGULATION_UNCHECK_EN
			}
			return 	EnumMsg.REGULATION_UNCHECK_EN
		}
		
		public static function get COIN_LIMIT():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.COIN_LIMIT_TW
			}
			else
			{
				return EnumMsg.COIN_LIMIT_EN
			}
			return 	EnumMsg.COIN_LIMIT_EN
		}
		
		public static function get MIN_LIMIT():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.MIN_LIMIT_TW
			}
			else
			{
				return EnumMsg.MIN_LIMIT_EN
			}
			return 	EnumMsg.MIN_LIMIT_EN
		}
		
		public static function get MAX_LIMIT():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.MAX_LIMIT_TW
			}
			else
			{
				return EnumMsg.MIN_LIMIT_EN
			}
			return 	EnumMsg.MAX_LIMIT_EN
		}
		
		public static function get BASE_BET():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.BASE_BET_TW
			}
			else
			{
				return EnumMsg.BASE_BET_EN
			}
			return 	EnumMsg.BASE_BET_EN
		}
		
		public static function get OPEN_BET():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.OPEN_BET_TW
			}
			else
			{
				return EnumMsg.OPEN_BET_EN
			}
			return 	EnumMsg.OPEN_BET_EN
		}
		
		public static function get PICK_TIME():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.PICK_TIME_TW
			}
			else
			{
				return EnumMsg.PICK_TIME_EN
			}
			return 	EnumMsg.PICK_TIME_EN
		}
		
		public static function get FIRST_SECOND_BET_TIME():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.FIRST_SECOND_BET_TIME_TW
			}
			else
			{
				return EnumMsg.FIRST_SECOND_BET_TIME_EN
			}
			return 	EnumMsg.FIRST_SECOND_BET_TIME_EN
		}
		
		public static function get BUYING():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.BUYING_TW
			}
			else
			{
				return EnumMsg.BUYING_EN
			}
			return 	EnumMsg.BUYING_EN
		}
		
		public static function get CALL_DEAL():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.CALL_DEAL_TW
			}
			else
			{
				return EnumMsg.CALL_DEAL_EN
			}
			return 	EnumMsg.CALL_DEAL_EN
		}
		
		public static function get SEARCHING_ENEMY():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.SEARCHING_ENEMY_TW
			}
			else
			{
				return EnumMsg.SEARCHING_ENEMY_EN
			}
			return 	EnumMsg.SEARCHING_ENEMY_EN
		}
		
		public static function get CONNECTING():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.CONNECTING_TW;
			}
			else
			{
				return EnumMsg.CONNECTING_EN
			}
			return 	EnumMsg.CONNECTING_EN
		}
		
		public static function get WIN():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.WIN_TW;
			}
			else
			{
				return EnumMsg.WIN_EN
			}
			return 	EnumMsg.WIN_EN
		}
		
		public static function get LOSE():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.LOSE_TW;
			}
			else
			{
				return EnumMsg.LOSE_EN
			}
			return 	EnumMsg.LOSE_EN
		}
		
		public static function get WIN_LOSE_RATE():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.WIN_LOSE_RATE_TW;
			}
			else
			{
				return EnumMsg.WIN_LOSE_RATE_EN;
			}
			return 	EnumMsg.WIN_LOSE_RATE_EN
		}
		
		public static function get IAP_DIAMOND_BONUS():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.IAP_DIAMOND_BONUS_TW;
			}
			else
			{
				return EnumMsg.IAP_DIAMOND_BONUS_EN;
			}
			return 	EnumMsg.IAP_DIAMOND_BONUS_EN
		}
		
		public static function get USE():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.USE_TW
			}
			else
			{
				return EnumMsg.USE_EN;
			}
			return 	EnumMsg.USE_EN;
		}
		
		public static function get GAIN():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.GAIN_TW
			}
			else
			{
				return EnumMsg.GAIN_EN;
			}
			return 	EnumMsg.GAIN_EN;
		}
		
		public static function get CRITICAL():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.CRITICAL_TW
			}
			else
			{
				return EnumMsg.CRITICAL_EN;
			}
			return 	EnumMsg.CRITICAL_EN;
		}
		
		public static function get COIN_OVER_MAX_LIMIT():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.COIN_OVER_MAX_LIMIT_TW
			}
			else
			{
				return EnumMsg.COIN_OVER_MAX_LIMIT_EN;
			}
			return 	EnumMsg.COIN_OVER_MAX_LIMIT_EN;
		}
		
		public static function get COIN_NOT_ENOUGH():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.COIN_NOT_ENOUGH_TW
			}
			else
			{
				return EnumMsg.COIN_NOT_ENOUGH_EN;
			}
			return 	EnumMsg.COIN_NOT_ENOUGH_EN;
		}
		
		public static function get DIAMOND_NOT_ENOUGH():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.DIAMOND_NOT_ENOUGH_TW
			}
			else
			{
				return EnumMsg.DIAMOND_NOT_ENOUGH_EN;
			}
			return 	EnumMsg.DIAMOND_NOT_ENOUGH_EN;
		}
		
		public static function get DIAMOND_EXCHANGE_COIN():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.DIAMOND_EXCHANGE_COIN_TW
			}
			else
			{
				return EnumMsg.DIAMOND_EXCHANGE_COIN_EN;
			}
			return 	EnumMsg.DIAMOND_EXCHANGE_COIN_EN;
		}
		
		public static function get SKIIL_RAISE():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.SKIIL_RAISE_TW
			}
			else
			{
				return EnumMsg.SKIIL_RAISE_EN;
			}
			return 	EnumMsg.SKIIL_RAISE_EN;
		}
		
		public static function get PLEASE_EXIT_TABLE():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.PLEASE_EXIT_TABLE_TW
			}
			else
			{
				return EnumMsg.PLEASE_EXIT_TABLE_EN;
			}
			return 	EnumMsg.PLEASE_EXIT_TABLE_EN;
		}
		
		public static function get GIVE_RATE():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.GIVE_RATE_TW
			}
			else
			{
				return EnumMsg.GIVE_RATE_EN;
			}
			return 	EnumMsg.GIVE_RATE_EN;
		}
		
		public static function get LINE_SHARE():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.LINE_SHARE_TW;
			}
			else
			{
				return EnumMsg.LINE_SHARE_EN;
			}
			return 	EnumMsg.LINE_SHARE_EN;
		}
		
		public static function get INVITE_FB_FRIEND():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.INVITE_FB_FRIEND_TW;
			}
			else
			{
				return EnumMsg.INVITE_FB_FRIEND_EN;
			}
			return 	EnumMsg.INVITE_FB_FRIEND_EN;
		}
		
		public static function get SYSTEM_COMMISSION():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.SYSTEM_COMMISSION_TW;
			}
			else
			{
				return EnumMsg.SYSTEM_COMMISSION_EN;
			}
			return 	EnumMsg.SYSTEM_COMMISSION_EN;
		}
		
		public static function get EXIT_GAME():String
		{
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				return EnumMsg.EXIT_GAME_TW;
			}
			else
			{
				return EnumMsg.EXIT_GAME_EN;
			}
			return 	EnumMsg.EXIT_GAME_EN;
		}
	}
}