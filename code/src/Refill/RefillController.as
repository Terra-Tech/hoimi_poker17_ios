package Refill
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	import Refill.Model.RefillCoin;
	import Refill.Model.RefillSync;
	
	import Skill.Model.Skill;
	
	import event.GameEvent;
	
	import model.PlayerInfo;

	public class RefillController
	{
		/// UI被加到螢幕上
		static public const EVENT_REFILL_INIT:String = 'RefillInit';
		/// UI被移除
		static public const EVENT_REFILL_DESTORY:String = 'RefillDestory';
		/// 領取
		static public const EVENT_REFILL:String = 'Refill';
		/// Pause when app deactive
		static public const EVENT_REFILL_PAUSE:String = 'RefillPause';
		/// Resume when app active
		static public const EVENT_REFILL_RESUME:String = 'RefillResume';
		
		private const ONE_SECOND:int = 1000;
		private const ONE_MINUTE:int = 60;
		private const ONE_HOUR:int = ONE_MINUTE * 60;
		private const ONE_DAY:int = 24;
		private var _view:IRefillView;
		private var _refill:RefillCoin = new RefillCoin();
		private var _sync:RefillSync = new RefillSync();
		private var _timestamp:Number = -1;
		private var _skill:Array = [];
		private var _countdown:Timer = new Timer(ONE_SECOND);
		private var _left:Number = 0;
		private var _isPause:Boolean = false;

		
		public function RefillController(view:IRefillView)
		{
			_view = view;
			EventManager.instance.register(EVENT_REFILL_INIT, init);
		}
		
		public function loop():void
		{
		}
		
		private function destory(e:Event):void
		{
			_countdown.removeEventListener(TimerEvent.TIMER, onCountdown);
			_countdown.removeEventListener(TimerEvent.TIMER_COMPLETE, onCountdownFinish);
			EventManager.instance.remove(EVENT_REFILL_DESTORY, destory);
			EventManager.instance.remove(EVENT_REFILL, refill);
			EventManager.instance.remove(EVENT_REFILL_PAUSE, pause);
			EventManager.instance.remove(EVENT_REFILL_RESUME, resume);
			EventManager.instance.remove(GameEvent.GAME_SKILL_RAISE, onRaise);
			EventManager.instance.register(EVENT_REFILL_INIT, init);
		}
		
		private function init(e:Event):void
		{
			EventManager.instance.remove(EVENT_REFILL_INIT, init);
			EventManager.instance.register(EVENT_REFILL_DESTORY, destory);
			EventManager.instance.register(EVENT_REFILL, refill);
			EventManager.instance.register(EVENT_REFILL_PAUSE, pause);
			EventManager.instance.register(EVENT_REFILL_RESUME, resume);
			EventManager.instance.register(GameEvent.GAME_SKILL_RAISE, onRaise);
			_countdown.addEventListener(TimerEvent.TIMER, onCountdown);
			_countdown.addEventListener(TimerEvent.TIMER_COMPLETE, onCountdownFinish);
			syncTime();
		}
		
		private function onRaise(e:GameEvent):void
		{
			if(e.m_eventArgs < 0 || e.m_eventArgs > 1){
				return;
			}
			if(!_isPause){
				_countdown.stop();
			}
			syncTime();
		}
		
		private function pause(e:Event):void
		{
			_isPause = true;
			_countdown.stop();
			LocalSaver.instance.save('refill_pause_time', getTimer());
//			trace("refill pause");
		}
		
		private function resume(e:Event):void
		{
			_isPause = false;
//			trace("refill resume");
			var time:* = LocalSaver.instance.getValue('refill_pause_time');
			if(time != null){
				var pass_sec:int = Math.floor((getTimer() - time) / ONE_SECOND);
//				trace("pass_sec " + pass_sec);
				_left -= pass_sec;
				calculate();
				if(_left > 0)
					countdown(_left);
			}
		}
		
		private function syncTime():void
		{
			_view.showLoading();
			SessionManager.instance.request(Protocol.REFILL_TIMESTAMP, {token:GameConfig.game_playerInfo.token}, onSuccess, onFail);
		}
		
		private function refill(e:Event):void
		{
			var now:int = getTimer();
			var max_min:int = (max_min_skill.value - 1) * ONE_MINUTE;
			if(_timestamp > 0 && now - _timestamp < ONE_MINUTE || _left > max_min){
				_view.showNoMoney();
			}else{
				_timestamp = getTimer();
				_view.showLoading();
				onRefill();
			}
		}
		
		private function onRefill(e:GameEvent=null):void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,onRefill);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			SessionManager.instance.request(Protocol.USER_REFILL, {token:GameConfig.game_playerInfo.token}, onSuccess, onFail);
		}
		
		private function countdown(repeat:int):void
		{
			_countdown.reset();
			_countdown.repeatCount = repeat;
			_countdown.start();
		}
		
		protected function onCountdown(event:TimerEvent):void
		{
			--_left;
			calculate();
		}
		
		protected function onCountdownFinish(event:TimerEvent):void
		{
			_countdown.stop();
		}
		
		private function calculate():void
		{
			var h:int, m:int, s:int;
			if(_left > 0){
				var max_min:int = (max_min_skill.value - 1);
				h = int(Math.floor((_left / ONE_HOUR) % ONE_DAY));
				m = int(Math.floor((_left / ONE_MINUTE) % ONE_MINUTE));
				s = int(Math.floor(_left % ONE_MINUTE));
				var coin:Number = (max_min - Math.floor(_left / ONE_MINUTE)) * pay_min_skill.value;
				_view.showClock(h,m,s);
				_view.showCoin( coin < 0 ? 0 : coin, max_min_skill.value * pay_min_skill.value);
			}else{
				_view.showClock(0,0,0);
				_view.showCoin(max_min_skill.value * pay_min_skill.value , max_min_skill.value * pay_min_skill.value);
			}
		}
		
		private function onRefillTimestamp():void{
//			var now:Date = new Date();
			var interval:int = _sync.second_pass;
//			trace('Refii interval '+interval);
			var max_sec:Number = max_min_skill.value * ONE_MINUTE;
//			trace('Refii max_sec '+max_sec);
			if(interval > max_sec){
				_left = 0;
				calculate();
			}else{
				_left = max_sec - interval;
				calculate();
				if(!_isPause)countdown(_left);
			}
		}
		
		private function onSuccess(e:NetEvent):void{
//			trace("onRefill success : " + e.data);
			var net:NetPacket = new NetPacket(e.data);
			if(net.result == 1){
				switch(e.protocol){
					case Protocol.REFILL_TIMESTAMP:
						_sync.byString(e.data);
						onRefillTimestamp();
						/*var now:Date = new Date();
						var interval:Number = Math.floor((now.time - _sync.timestamp) / ONE_SECOND);
						var max_sec:Number = max_min_skill.value * ONE_MINUTE;
						if(interval > max_sec){
							_left = 0;
							calculate();
						}else{
							_left = max_sec - interval;
							calculate();
							countdown(_left);
						}*/
						break;
					case Protocol.USER_REFILL:	
						EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,onRefill);
						_view.hideLoading();
						_countdown.stop();
						_refill.byString(e.data);
						_view.showRefill(_refill.refill_coin, _refill.player_coin);
						_left = max_min_skill.value * ONE_MINUTE;
						calculate();
						countdown(_left);
						break;
					default:
						break;
				}
			}else if(net.result == -4 && e.protocol == Protocol.USER_REFILL){
				_view.showNoMoney();
			}else{
				_view.serverError("protocol :"+e.protocol+" inner error : " + net.result);
			}
		}
		
		private function onFail(e:NetEvent):void{
			_view.netError("net error : " + e.protocol);
		}
		
		private function get max_min_skill():Skill
		{
			return GameConfig.game_playerInfo.skills[0]; 
		}
		
		private function get pay_min_skill():Skill
		{
			return GameConfig.game_playerInfo.skills[1];
		}
	}
}