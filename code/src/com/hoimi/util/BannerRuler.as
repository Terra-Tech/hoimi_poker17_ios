package com.hoimi.util
{
	import flash.system.Capabilities;

	public class BannerRuler
	{
		static private var ResolutionUnit:Number = 160;
		static private var Inch720:Number = 720/ResolutionUnit;
		/*
		MDPI : 160 DPI
		HDPI = 1.5 x MDPI = 240 DPI
		XHDPI = 2 x MDPI = 320 DPI
		XXHDPI = 3 X MDPI = 480 DPI
		XXXHDPI = 4 X MDPI = 640 DPI
		*/
		public function BannerRuler()
		{
		}
		
		public static function is90DPHeight():Boolean
		{
			var dpi:Number = Capabilities.screenDPI;
			var height:Number = Capabilities.screenResolutionY;
			
			return (dpi/ResolutionUnit) > Inch720 ? true : false;
		}
	}
}