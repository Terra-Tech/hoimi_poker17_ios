package model
{
	import com.hoimi.lib.net.NetPacket;
	/** draw
	 * -4:sn miss
	 * -5:no such sn
	 * -6:already taken
	 * -7:expired
	 * -8:draw over limit times
	 */

	public class SNPrize extends NetPacket
	{
		public var coin:Number;
		public var diamond:Number;
		public var player_coin:Number;
		public var player_diamond:Number;
		public function SNPrize(data:String){
			super(data);
		}

	}
}