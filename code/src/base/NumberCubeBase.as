package base
{
	import com.hoimi.util.LocalSaver;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import handler.TTCSoundHandler;
	
	public class NumberCubeBase extends Sprite
	{
		public var m_nPlayerAns:int = -1
		public var m_nCubeIndex:int = -1;
		public var m_po:Point = new Point();
		public var m_bCanSelect:Boolean =false;
		public function NumberCubeBase()
		{
			super();
		}
		
		public function fnCubeSelect():void
		{
			
		}
		
		public function fnSelectNumber(nValue:int):void
		{
		
		}
		
		public function fnSelectMemo(nValue:int):void
		{
		
		}
		
		public function fnHintNumber():void
		{
		
		}
		
		public function fnDelNumber():void
		{
		}
		
		public function fnDisSelectCube():void
		{
		}
		
		public function fnGamePass():void
		{
			
			
		}
		
	
	}
}