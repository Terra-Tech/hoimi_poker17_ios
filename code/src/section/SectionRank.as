package section
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import FB.Model.FacebookFriend;
	
	import base.TTCSectionBase;
	import base.TiButtonState;
	import base.TiDisableTextField;
	
	import event.GameEvent;
	
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	import item.FriendItem;
	import item.HoimiExpSmall;
	import item.RankItem;
	
	import model.CoinRankInfo;
	import model.CoinRankInfoList;
	
	public class SectionRank extends TTCSectionBase
	{
		private var m_mcBg:Poker_Friends_Bg = new Poker_Friends_Bg();
		private var m_sp:Sprite = new Sprite();
		private var m_nPageIndex:int = 1;
		private var m_nStageTotal:int = 0;
		private var m_nPageTotal:int = 0;	
		private var m_coPageCurrent:HoimiExpSmall = new HoimiExpSmall();
		private var m_coPageTotal:HoimiExpSmall = new HoimiExpSmall();
		private var m_coPageSlash:Poker_Number_Exp_Small = new Poker_Number_Exp_Small();
		private var m_sortSp:Sprite = new Sprite();
		private var m_spFriend:Sprite = new Sprite();
		private var m_mcNext:Poker_Btn_Friends_Arrow_Right = new Poker_Btn_Friends_Arrow_Right();
		private var m_mcPrevious:Poker_Btn_Friends_Arrow_Left = new Poker_Btn_Friends_Arrow_Left();
		private var m_mcSortCoin:Poker_Btn_Friends_Sort_Coin = new Poker_Btn_Friends_Sort_Coin();
		private var m_buttonState:TiButtonState = new TiButtonState();
		private var m_arSort:Array = new Array();
		private var m_seftRankItem:RankItem 
		
		public function SectionRank()
		{
			super();
			this.addChild(m_mcBg);
			this.addChild(m_sp);
			m_sp.addChild(m_spFriend);
			m_sp.addChild(m_mcPrevious);
			m_sp.addChild(m_mcNext);
			m_sp.addChild(m_coPageCurrent);
			m_sp.addChild(m_coPageTotal);
			m_sp.addChild(m_coPageSlash);
			m_sp.addChild(m_sortSp);
			
			m_sortSp.addChild(m_mcSortCoin);
			m_sortSp.y = 225
			m_mcSortCoin.x = 360
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcNext);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcPrevious);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcSortCoin);
			m_spFriend.y = 280
			m_sortSp.visible = true;
			m_mcNext.visible=false;
			m_mcPrevious.visible =false;
			m_coPageSlash.visible =false;
			m_coPageCurrent.visible =false;
			m_mcNext.x = 600
			m_mcNext.y = 1190
			m_mcPrevious.x = 120
			m_mcPrevious.y = 1190
			m_coPageCurrent.x = 340
			m_coPageCurrent.y = 1230
			m_coPageTotal.x = 380
			m_coPageTotal.y = 1230
			
			m_coPageSlash.x = 370
			m_coPageSlash.y = 1230
			m_coPageSlash.gotoAndStop(13);
			
			this.addEventListener(Event.ADDED_TO_STAGE,fnAddToStage);
		}
		
		private function fnAddToStage(e:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,fnAddToStage);
			fnRankRequest();
		}
		
		private function fnRankRequest(e:GameEvent = null):void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnRankRequest);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			SessionManager.instance.request(Protocol.MONEY_RANK, {token:GameConfig.game_playerInfo.token}, onRank, null);

		}
		
		private function onRank(e:NetEvent):void
		{
			/*onRank {"result":1,"ranks":
				[{"uuid":"A8C29F5AAAFE186D","coin":183524347,"name":"jay","fb_id":"10206208188189937"},
					{"uuid":"0C01FCBA0CA706B5","coin":502300,"name":"Tryyu","fb_id":null},
					{"uuid":"AB5147ACAB985943","coin":225822,"name":"Pipi Yen","fb_id":"1027500957309992"},
					{"uuid":"BC0650CABD69D795","coin":102000,"name":"YA  YA","fb_id":""},
					{"uuid":"BC02E6EABD6DC15E","coin":84675,"name":"okla","fb_id":"527200757454298"},
					{"uuid":"172B705F17877AB8","coin":39800,"name":"Tryuiop","fb_id":null},
					{"uuid":"BA34E6F8BAE91070","coin":16140,"name":"Aries Lee","fb_id":"10201408743257000"},
					{"uuid":"1C4C50DB1CE063EA","coin":7700,"name":"Guest63EA","fb_id":null},
					{"uuid":"C4AC19B6C69D2CF7","coin":7600,"name":"TestCoinBack","fb_id":null},
					{"uuid":"0D30E7490D9715D6","coin":4500,"name":"Guest15D6","fb_id":null},
					{"uuid":"0F91FA270DA9E228","coin":4500,"name":"GuestE228","fb_id":null},
					{"uuid":"0FC040040D87D77D","coin":4500,"name":"GuestD77D","fb_id":null},
					{"uuid":"0FCDDC580D849B04","coin":4500,"name":"Guest9B04","fb_id":null},
					{"uuid":"118CDC9A11405029","coin":4500,"name":"Guest5029","fb_id":null},
					{"uuid":"15E357D717B51FAA","coin":4500,"name":"Guest1FAA","fb_id":null},
					{"uuid":"15EB2D6C17B41A67","coin":4500,"name":"Guest1A67","fb_id":null},
					{"uuid":"0F9150130DA9199B","coin":4500,"name":"Guest199B","fb_id":null},
					{"uuid":"0D35EFC20D920461","coin":4500,"name":"Guest0461","fb_id":null},
					{"uuid":"0D30F4730D9701C9","coin":3700,"name":"Guest01C9","fb_id":null},
					{"uuid":"EA2BFA31E818620D","coin":3300,"name":"TestSpeed","fb_id":null},
					{"uuid":"F4378C95F546FBE9","coin":2820,"name":"YaYa","fb_id":null},
					{"uuid":"F430B3C0F541C4F6","coin":2350,"name":"YaYa","fb_id":null},
					{"uuid":"0DC787850D60675B","coin":2180,"name":"Guest675B","fb_id":null},
					{"uuid":"F5BF8061F51D0DE1","coin":2150,"name":"jay","fb_id":null},
					{"uuid":"F7D297A8F75D34B5","coin":2000,"name":"Dalireal Lee","fb_id":"10204634075126513"},
					{"uuid":"F7349F4AF5075994","coin":2000,"name":"kau","fb_id":null},
					{"uuid":"F70E3B47F5388739","coin":2000,"name":"jay","fb_id":null},
					{"uuid":"F7043E3EF531D4F8","coin":2000,"name":"wwwee","fb_id":null},
					{"uuid":"0F6242680D2684A4","coin":2000,"name":"Guest84A4","fb_id":null},
					{"uuid":"0DFA2BEB0D5DEEEB","coin":2000,"name":"GuestEEEB","fb_id":null}],"self_rank":4}*/
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnRankRequest);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			trace("onRank "+e.data);
			if(e.result == 1)
			{
				var coinRankInfoList:CoinRankInfoList =  new CoinRankInfoList(e.data); 
				m_arSort = coinRankInfoList.ranks;
				fnSetPage();
				fnClickSortCoin(null)
				var self:CoinRankInfo = new CoinRankInfo(null);
				self.coin = GameConfig.game_playerInfo.coin;
				self.name = GameConfig.game_playerInfo.nickname;
				self.rank = coinRankInfoList.self_rank;
				self.fb_id = GameConfig.game_playerInfo.fb_id;
				m_seftRankItem = new RankItem(self,true);
				m_seftRankItem.x = 360
				m_seftRankItem.y = 1060
				m_sp.addChild(m_seftRankItem);
			}
			else
			{
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "onSignin "+ e.result
				EventManager.instance.trigger(evError);
			}
		}

		
		
		
		private function fnSetPage():void
		{
			var nTotal:int =  m_arSort.length;
			m_nStageTotal = nTotal;
			
			if(m_nStageTotal%6 == 0)
			{
				m_nPageTotal = int(nTotal/6);	
			}
			else
			{
				m_nPageTotal = int(nTotal/6) + 1;
			}
			
			if(m_nPageIndex > m_nPageTotal)
			{
				m_nPageIndex = m_nPageTotal
			}
			
			if(nTotal >6 )
			{
				m_mcNext.visible = true;
				m_mcPrevious.visible = true;
				m_mcNext.addEventListener(MouseEvent.CLICK,fnNextPage);
				m_mcPrevious.addEventListener(MouseEvent.CLICK,fnPreviousPage);
				m_coPageCurrent.fnSetValue(m_nPageIndex);
				m_coPageTotal.fnSetValue2(m_nPageTotal);
				m_coPageCurrent.visible =true;
				m_coPageSlash.visible = true
				m_coPageTotal.visible = true
			}
		}
		
		private function fnClickSortCoin(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_buttonState.fnDisableAndDarkMcState(m_mcSortCoin);
			m_nPageIndex = 1
			showFriends(m_arSort);
		}
		
		private function fnNextPage(e:MouseEvent):void
		{
			if(m_nPageIndex!=m_nPageTotal)
			{
				m_nPageIndex++
					m_coPageCurrent.fnSetValue(m_nPageIndex);
				TTCSoundHandler.fnGetInstance().fnEmbeddedSoundPage();
				
				showFriends(m_arSort);
			}
		}
		
		
		private function fnPreviousPage(e:MouseEvent):void
		{
			if(m_nPageIndex!=1)
			{
				m_nPageIndex--
					m_coPageCurrent.fnSetValue(m_nPageIndex)	
				TTCSoundHandler.fnGetInstance().fnEmbeddedSoundPage();
				showFriends(m_arSort);
			}
		}
		
		private function showFriends(ar:Array):void
		{
			m_spFriend.removeChildren();
			//			var mc:FacebookFriend	
			var nStart:int = (m_nPageIndex-1) * 6
			var nEnd:int = 	(m_nPageIndex-1) * 6 + 6
			
			if(nEnd > m_nStageTotal)
			{
				nEnd = m_nStageTotal
			}
			var itemFriend:RankItem, f:CoinRankInfo;
			for(var i:int= nStart ;i< nEnd;i++)
			{
				f = ar[i];
				itemFriend = new RankItem(f);
				itemFriend.x = 360
				itemFriend.y = int(i- nStart)*(itemFriend.height + 10)
				m_spFriend.addChild(itemFriend);
			}
			m_sortSp.visible = true;
		}
		
		
		
		override public function fnResize():void
		{
			m_mcBg.x = 360 +  GameConfig.object_adjuct_position_x 
			m_sp.x =  GameConfig.object_adjuct_position_x
			
			
		}
		
		override public function fnPause():void
		{
			super.fnPause();
		}
		
		override public function fnResume():void
		{
			super.fnResume();
		}
		
		override public function fnSkip():void
		{
			super.fnSkip();
		}
		
		
		override public function fnReplay():void
		{
			super.fnReplay();
		}
		
		override public function fnExit():void
		{
			super.fnExit()
			this.removeChildren();
		}
	}
}