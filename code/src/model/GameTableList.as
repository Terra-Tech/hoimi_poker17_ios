package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class GameTableList extends NetPacket
	{
		public var table:Array = [];
		public function GameTableList(iap:*)
		{
			super(iap);
			var s:GameTable;
			
			for (var i:int in table)
			{
				s = new GameTable(table[i]);
				table[i] = s;
			}
		}
	}
}