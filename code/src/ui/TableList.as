package ui
{
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.freshplanet.lib.ui.util.RectangleSprite;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	
	import caurina.transitions.Tweener;
	
	import item.TableItemRenderer;
	
	import model.GameTable;
	
	public class TableList extends Sprite
	{
		private var m_sp:Sprite = new Sprite();
		private var m_scrollArea:Sprite = new Sprite();
		private var m_nDefaultSpY:Number = 240
		private var m_po:Point = new Point();
		private var m_nMove:Number = 0	
		private var m_nMovePlus:Number = 0;	
		private var m_nClearMoveValueTimer:Timer = new Timer(100,1);
		private var m_nMoveUpOriginalY:Number = 0
		private var m_nMoveOutBoundMaxValue:Number = 50	
		private var m_nDuratinTime:Number = 0;	
		private var m_nItemGapY:Number = 10	
		private var m_nMaskHeight:Number = 1020
		private var _scroll:ScrollController;
		
		public function TableList()
		{
			super();
		
		//	m_sp.addEventListener(MouseEvent.MOUSE_DOWN,fnMouseDown);
		//	m_sp.y = m_nDefaultSpY
		//	fnSetMask();
			this.addChild(m_sp);
		}
		
		
		
		private function fnSetMask():void
		{
			var masker:Sprite = new Sprite();
			masker.graphics.beginFill(0);
			masker.graphics.drawRect( 0 , m_nDefaultSpY-20 , 720 , m_nMaskHeight);
			masker.graphics.endFill();
			this.addChild(masker);
			m_sp.mask = masker;
		}
		
		private function fnMouseDown(e:MouseEvent):void
		{
			if(mouseY < m_nDefaultSpY)
			{
				return;
			}
			Tweener.pauseAllTweens();
			Tweener.removeAllTweens();
			m_po.y = mouseY
			m_nMove = 0;
			m_nMovePlus =0;
			stage.addEventListener(MouseEvent.MOUSE_UP,fnMouseUp)
			stage.addEventListener(MouseEvent.MOUSE_MOVE,fnMouseMove);
			m_nClearMoveValueTimer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTimerComplete);
			m_nClearMoveValueTimer.start();
		}
		
		private function fnMouseMove(e:MouseEvent):void
		{
			if(m_nMove !=0)
			{
				m_nClearMoveValueTimer.reset()
				m_nClearMoveValueTimer.start()
			}
			//	trace("mouseY :" + mouseY)
			var nMove:Number = 0
			nMove = mouseY - m_po.y
			m_nMove = nMove
			
			trace("nMove :" + nMove)
			m_po.y = mouseY
			
			m_sp.y += nMove
			if(m_sp.y > m_nDefaultSpY + m_nMoveOutBoundMaxValue)
			{	//down
				m_sp.y = m_nDefaultSpY + m_nMoveOutBoundMaxValue;
			}
			
			if(m_sp.y  < m_nMoveUpOriginalY -m_nMoveOutBoundMaxValue)
			{//up
				m_sp.y = m_nMoveUpOriginalY - m_nMoveOutBoundMaxValue
				
			}
		
		}
	
		private function fnTimerComplete(e:TimerEvent):void
		{
			m_nMove = 0;
			m_nMovePlus =0;
			trace("time complete, m_nMove = 0")
		}
		
		private function fnMouseUp(e:MouseEvent):void
		{
			if(stage)
			{
				stage.removeEventListener(MouseEvent.MOUSE_UP,fnMouseUp)
				stage.removeEventListener(MouseEvent.MOUSE_MOVE,fnMouseMove);
			}
			
			m_nClearMoveValueTimer.stop();
			m_nClearMoveValueTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnTimerComplete);
			fnCalculateMovePlus(m_nMove);
			//var nMoveAll:Number = m_sp.y+m_nMovePlus
			var nOutValue:Number = 8
			var nNewY:Number
			//trace("nMove2:"+nMoveAll);
			var nTempNewY:Number =  m_sp.y + m_nMovePlus
			if(m_nMove == 0)
			{
				if(m_sp.y < m_nMoveUpOriginalY)
				{// final move down
					Tweener.addTween(m_sp, {y:m_nMoveUpOriginalY,time:0.15,transition:"easeOutBack"});
				}
				else if(m_sp.y  > m_nDefaultSpY)
				{// final move up
					Tweener.addTween(m_sp, {y:m_nDefaultSpY,time:0.15,transition:"easeOutBack"});
				}
				return;
			}
			if(m_nMove < 0 )
			{
				if(nTempNewY  < m_nMoveUpOriginalY  )
				{	//move up
					if(m_sp.numChildren > 8)
					{
						
						nNewY =  m_nMoveUpOriginalY + nOutValue
					}
					else
					{
						nNewY = m_nDefaultSpY + nOutValue
					}
					Tweener.addTween(m_sp, {y:nNewY,time:m_nDuratinTime,transition:"easeOutExpo",onComplete:fnTweenUpComplete});
					
				}
				else
				{
					nNewY = nTempNewY
					Tweener.addTween(m_sp, {y:nNewY,time:m_nDuratinTime,transition:"easeOutExpo"});
					
				}
			}
			else
			{
				if(nTempNewY   > m_nDefaultSpY )
				{	//down
					nNewY = m_nDefaultSpY - nOutValue
					Tweener.addTween(m_sp, {y:nNewY,time:m_nDuratinTime,transition:"easeOutExpo",onComplete:fnTweenDownComplete});
					
				}
				else
				{
					nNewY = nTempNewY
					Tweener.addTween(m_sp, {y:nNewY,time:m_nDuratinTime,transition:"easeOutExpo"});
					
				}
			}
			trace("nNewY :" + nNewY);
		}
		
		private function fnCalculateMovePlus(nValue:Number):void
		{
			trace("nValue :"+ nValue);
			if(m_sp.numChildren <= 8)
			{
				m_nDuratinTime = 0.25
				m_nMovePlus = nValue*1
			}
			else if(m_sp.numChildren <= 25)
			{
				if(nValue < -20 || nValue >20)
				{
					m_nDuratinTime = 0.25
					m_nMovePlus = nValue*2	
				}
				else if(nValue < -50 || nValue >50)
				{
					m_nDuratinTime = 0.35
					m_nMovePlus = nValue*4	
				}
				else
				{
					m_nDuratinTime = 0.45
					m_nMovePlus = nValue*5	
				}
				
			}
			else if(m_sp.numChildren <= 50)
			{
				if(nValue > -20 && nValue < 0 || nValue> 0 && nValue <20)
				{
					m_nDuratinTime = 0.25
					m_nMovePlus = nValue*1.5	
				}
				else if(nValue > -50 && nValue < 0 ||  nValue> 0  && nValue <50)
				{
					m_nDuratinTime = 0.5
					m_nMovePlus = nValue*2	
				}
				else
				{
					m_nDuratinTime = 0.75
					m_nMovePlus = nValue*2.5	
				}
			}
			else if(m_sp.numChildren <= 75)
			{
				if(nValue > -20 && nValue < 0 || nValue> 0 && nValue <20)
				{
					m_nDuratinTime = 0.25
					m_nMovePlus = nValue*2	
				}
				else if(nValue > -50 && nValue < 0 ||  nValue> 0  && nValue <50)
				{
					m_nDuratinTime = 0.75
					m_nMovePlus = nValue*4	
				}
				else
				{
					m_nDuratinTime = 1
					m_nMovePlus = nValue*5	
				}
			}
			else if(m_sp.numChildren <= 100)
			{
				if(nValue > -20 && nValue < 0 || nValue> 0 && nValue <20)
				{
					m_nDuratinTime = 0.25
					m_nMovePlus = nValue*2	
				}
				else if(nValue > -50 && nValue < 0 ||  nValue> 0  && nValue <50)
				{
					m_nDuratinTime = 0.75
					m_nMovePlus = nValue*4	
				}
				else
				{
					m_nDuratinTime = 1
					m_nMovePlus = nValue*8	
				}
			}
			else
			{
				if(nValue > -20 && nValue < 0 || nValue> 0 && nValue <20)
				{
					m_nDuratinTime = 0.25
					m_nMovePlus = nValue*2	
				}
				else if(nValue > -50 && nValue < 0 ||  nValue> 0  && nValue <50)
				{
					m_nDuratinTime = 0.75
					m_nMovePlus = nValue*5	
				}
				else
				{
					m_nDuratinTime = 1
					m_nMovePlus = nValue*10	
				}
			}
		}
		
		
		private function fnTweenUpComplete():void
		{
			Tweener.addTween(m_sp, {y:m_nMoveUpOriginalY,time:0.25,transition:"easeOutBack",onComplete:fnAfterTweenUpComplete});
		}
		
		private function fnAfterTweenUpComplete():void
		{
			
		}
		
		private function fnTweenDownComplete():void
		{
			
			Tweener.addTween(m_sp, {y:m_nDefaultSpY,time:0.25,transition:"easeOutBack",onComplete:fnAfterTweenDownComplete});
		}
		
		private function fnAfterTweenDownComplete():void
		{
			
		}
		
		public function fnInitTable():void
		{
			
			m_sp.removeChildren();
			var ar:Array = GameConfig.game_tableList;
			var tableData:GameTable
			var arNew:Array = new Array();
			for(var j:int = 0; j < ar.length;j++)
			{
				tableData = ar[j]
				if(GameConfig.game_playerInfo.coin >= tableData.limit_min && GameConfig.game_playerInfo.coin <=  tableData.limit_max)
				{
					arNew.push(ar[j]);
				}
			}
			if(arNew.length ==0)
			{
				arNew.push(ar[0]);
			}
			tableData = arNew[arNew.length-1]
			var nMaxRableId:int = tableData.id
			tableData = ar[nMaxRableId]
			if(tableData)
			{
				arNew.push(tableData);
			}
			tableData = arNew[0]
			if(tableData.id != 1)
			{
				arNew.reverse();
				tableData = ar[tableData.id-2]
				arNew.push(tableData);
			}
			else
			{
				arNew.reverse();	
			}
			
			var table:TableItemRenderer
			for(var i:int = 0;i<arNew.length;i++)
			{
				tableData = arNew[i]
				table = new TableItemRenderer(tableData.id,tableData.limit_min,tableData.limit_max,tableData.bet);
				table.cacheAsBitmap = true
				table.y =  i * (table.height+m_nItemGapY)
				m_sp.addChild(table);
			}
			
			if(m_sp.height > 1280 - m_nDefaultSpY)
			{
				m_nMoveUpOriginalY = m_nDefaultSpY -( m_sp.height - m_nMaskHeight)
				trace(m_nMoveUpOriginalY);
			}
			else
			{
				m_nMoveUpOriginalY = m_nDefaultSpY
			}
			if(ar.length>0)
			{
				fnInitScroll();
			}
		
		}
		
		private function fnInitScroll():void
		{
			var container:RectangleSprite = new RectangleSprite(0xFFFFFF, 0, m_nDefaultSpY, 0, m_nMaskHeight);//red background
			this.addChild(container);
			container.addChild(m_sp);
			var containerViewport:Rectangle = new Rectangle(0, 0, 720 , m_nMaskHeight);
			var containerRect:Rectangle = new Rectangle(0, 0, 720 , m_nMaskHeight);
			this._scroll = new ScrollController();
			this._scroll.horizontalScrollingEnabled = false;
			this._scroll.addScrollControll(m_sp, container, containerViewport,containerRect);
		}
		
		public function fnDestory():void
		{
			
			var table:TableItemRenderer
			for(var i:int = 0;i<m_sp.numChildren;i++)
			{
				table = m_sp.getChildAt(i) as TableItemRenderer;
				table.fnDestory();
			}
		}
	}
}