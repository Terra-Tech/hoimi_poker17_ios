package event
{
	public class TTCVoiceEvent extends TiBasicEvent
	{
		public static const VOICE_EVENT:String="VOICE_EVENT";
		public static const VOICE_CONFIT_COMPLETE:String="VOICE_CONFIT_COMPLETE";
		public static const VOICE_DOWNLOAD_COMPLETE:String="VOICE_DOWNLOAD_COMPLETE";
		public static const VOICE_PLAY_FINISHED:String="VOICE_PLAY_FINISHED";
		public static const VOICE_NULL:String="VOICE_NULL";
		
		public function TTCVoiceEvent(type:String, detail:String="")
		{
			super(type, detail);
		}
	}
}