package com.hoimi.util
{
	import com.freshplanet.ane.AirInAppPurchase.InAppPurchase;
	import com.freshplanet.ane.AirInAppPurchase.InAppPurchaseEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class Purchase extends EventDispatcher
	{
		static public const PURCHASE_INIT_SUCCESS:String = 'PurchaseInitSuccess';
		static public const PURCHASE_INIT_FAIL:String = 'PurchaseInitFail';
		
		protected var _iap:InAppPurchase;

		public function get bInit():Boolean
		{
			return _bInit;
		}

		private var _purchaseSuccess:Function = null;
		private var _purchaseFail:Function = null;
		private var _productID:String = "";
		private var _bInit:Boolean = false;
		
		static private var _instance:Purchase;
		
		public function Purchase(s:singleton)
		{	
			_iap = InAppPurchase.getInstance();
			_iap.addEventListener(InAppPurchaseEvent.PRODUCT_INFO_ERROR, onInfoError);
			_iap.addEventListener(InAppPurchaseEvent.PRODUCT_INFO_RECEIVED, onInfoReceived);
			_iap.addEventListener(InAppPurchaseEvent.RESTORE_INFO_RECEIVED, onRestoreReceived);
			_iap.addEventListener(InAppPurchaseEvent.PURCHASE_ENABLED, onEnabled);
			_iap.addEventListener(InAppPurchaseEvent.PURCHASE_DISABLED, onDisabled);
		}
		
		protected function onDisabled(e:InAppPurchaseEvent):void
		{
			trace("XXX onDisabled [" + e.data + "]");
		}
		
		protected function onEnabled(e:InAppPurchaseEvent):void
		{
			trace("XXX onEnabled [" + e.data + "]");
		}
		
		protected function onRestoreReceived(e:InAppPurchaseEvent):void
		{
			trace("XXX onRestoreReceived [" + e.data + "]");	
		}
		
		protected function onInfoReceived(e:InAppPurchaseEvent):void
		{
			trace("XXX onInfoReceived [" + e.data + "]");	
		}
		
		protected function onInfoError(e:InAppPurchaseEvent):void
		{
			trace("XXX onInfoError [" + e.data + "]");
		}
		
		static public function get instance():Purchase
		{
			if(_instance == null)
				_instance = new Purchase(new singleton);
			return _instance;
		}

		public function init(key:String):void
		{
			if(!_iap.isInAppPurchaseSupported)
				trace("not support!!");//throw new Error("in app purchase not suppotr!");
			else{
				_iap.init(key,false);
				_bInit = true;
				this.dispatchEvent(new Event("PurchaseInitSuccess"));
			}
		}
		
		public function inAppPurchase(item_id:String, onSuccess:Function, onFail:Function):void
		{
			if(!_iap.isInAppPurchaseSupported)
				trace("not support!!");
			else
			{
				_iap.addEventListener(InAppPurchaseEvent.PURCHASE_SUCCESSFULL, onPurchaseSuccess, false, 0, true);
				_iap.addEventListener(InAppPurchaseEvent.PURCHASE_ERROR, onPurchaseError, false, 0, true);
				_purchaseSuccess = onSuccess;
				_purchaseFail = onFail;
				_iap.makePurchase(item_id);
			}
			
		}
		
		public function fixConsume(iapId:String,rev:Function, err:Function):void
		{
			_iap.addEventListener(InAppPurchaseEvent.PRODUCT_INFO_RECEIVED, onFix, false, 0, true);
			_iap.addEventListener(InAppPurchaseEvent.PRODUCT_INFO_ERROR, onFix, false, 0, true);
			_iap.getProductsInfo([iapId],[]);
			
			function onFix(e:InAppPurchaseEvent):void
			{
				if(e.type == InAppPurchaseEvent.PRODUCT_INFO_RECEIVED)
				{
					var data_:Object = JSON.parse(e.data);
					var first_:Object = data_['purchases'][0];
					if(first_ == null)
					{
						return;
					}
					const receipt:Object = first_["receipt"];
					//out('JSON.stringify(receipt): ' + JSON.stringify(receipt));
					//out(this, 'receipt["signedData"]', receipt["signedData"]);
					const signedData:Object = JSON.parse(receipt["signedData"]);
					//out(this, 'signedData["productId"]', signedData["productId"]);
					
					// download the purchase (finish the transaction)
					_iap.removePurchaseFromQueue(signedData["productId"],JSON.stringify(receipt));
					rev(" consume end ");
				}
				else
				{
					err(e.type+" = " + e.data);
				}
			}
		}
		
		public function destory():void
		{
			_iap.removeEventListener(InAppPurchaseEvent.PURCHASE_SUCCESSFULL, onPurchaseSuccess);
			_iap.removeEventListener(InAppPurchaseEvent.PURCHASE_ERROR, onPurchaseError);
		}
		
		protected function onInitSuccess(event:InAppPurchaseEvent):void
		{
			//you can restore previously purchased items here
			trace("Init ok ");
			_bInit = true;
			this.dispatchEvent(new Event("PurchaseInitSuccess"));
		}
		
		protected function onInitError(event:InAppPurchaseEvent):void
		{
			trace(event.data); //trace error message
			this.dispatchEvent(new Event("PurchaseInitFail"));
		}
		
		protected function onPurchaseSuccess(event:InAppPurchaseEvent):void
		{	
			/*
			IAP 訊息
			
			Success Msg: 
			{"receiptType":"GooglePlay",
			 "receipt":{"signedData":
						"{\"orderId\":\"GPA.1369-6730-4879-51223\",
						\"packageName\":\"air.com.hoimi.MathxMath\",
						\"productId\":\"twd_150\",
						\"purchaseTime\":1441684922346,
						\"purchaseState\":0,
						\"purchaseToken\":\"mccabhjcoegionogekjeebia.AO-J1OzIpGkGiZfmvszNFWFeLT6_ZSWRv9RDYpzw561HJrFYOGTBxkJHmdYEX1VGNzAnVZhAxCvUtRqi12QYG5YG3fC1_GNOOSgCHZn4vAlZrn8IdDpiUiVjMQT5dKxCIU0q0Cxzl8MA\"}",
			 "signature":"ja9p4ttvmsDMuIXQmLrpVQcMAX7L\/6d\/w2kwvTVUeuNa9maSRV+VADuXH+D6n7zAvDIfeh5USeNlyw4rlO3Ob05bhKnCjXJwyDLEF7gayBXr8OXJ41oUn0kG0XWbXAlkwfaJy9nGSkA5l5VZUelsTJlDZRDvnnX+OOAqyOuVTPRWzW3qj3Jk7Qx52+yLstSB0jI5KcP0kxAA3\/XDEAlEqhszmNmDrgafQafoOdxU7CQYTcYR3IugcuPyHt2dOVLV\/DRv9bl4Q3X6DT5RbD8aB9QjP1wx3Fodd62JMTFFrj8bOWwkifxbLwfjmuA\/PoLfuQTHOZ\/7LhD8LGEEmY404w=="},
			 "productId":"twd_150"
			}
			*/			
			
			/*
			Success Msg: 
			{"receiptType":"GooglePlay",
			"receipt":{"signedData":"
						{\"orderId\":\"GPA.1360-6590-4596-47650\",
						\"packageName\":\"air.com.hoimi.MathxMath\",
						\"productId\":\"twd_3000\",
						\"purchaseTime\":1441685540719,
						\"purchaseState\":0,
						\"purchaseToken\":\"eokmmgcdjhemgaalhhhijhpk.AO-J1OxJBMX81bBuLLi3zqEsAQfDqhjGbJm6ihcV6Uq5yJGjsT17-eAXZlYOJ1CP6oKpauCa4ZMvUL_VdB4GqijAr_yIEY5eEy9XNR3Y51hqjtYPCKfrxW828NyxmlA7PHjX7PNLl2nm\"}",
			"signature":"i3NyoTWwXusvD7ZCwC5wvuEygwHB0j7xGuFqLTjhkYrqWZB2pk92DPJVmwcxC1wy9qGPgbGmAOU7a\/czRhEl\/Zr4GsNg5puIQ2gggp+1bgT9vJWP45cryP\/Z6bqmJ0RiP1uNH36YSD47ynW7OwOqII6PCItwUk6UfVuMOTO7q87GGVq9D77g3BFrw5gt52GjaEnraBcwn9u1ZCRCQ6vYiTCCwU59Ran87w0Uqm1Fi5IogEuxbOK9RDtuny+205sg5if7luzFiJ\/MQ\/xJOZ6YRtfRbDOIpyFXP4wlJFOHNSwDJzbWFi95\/7dujEDB9owUYbRaRQO2zl8wRUhjkO2cEw=="},
			"productId":"twd_3000"
			}
			*/
			var data_:Object
//			if(_purchaseSuccess != null)
//			{
//				data_= JSON.parse(event.data);
//				_purchaseSuccess(data_);
//			}
			try
			{
				
				data_= JSON.parse(event.data);
				if(_purchaseSuccess != null)
					_purchaseSuccess(data_);
			}
			catch(e:Error) 
			{
				_purchaseFail("data JSON.parse error :" + event.data);
			}
			
			var receipt:Object 
			var signedData:Object
			
			//out('JSON.stringify(receipt): ' + JSON.stringify(receipt));
			//out(this, 'receipt["signedData"]', receipt["signedData"]);
			try
			{
				receipt = data_['receipt'];
//				signedData = JSON.parse(receipt["signedData"]);
			}
			catch(e:Error)
			{
				_purchaseFail("signedData JSON.parse error :" + event.data);
			}
		
			//out(this, 'signedData["productId"]', signedData["productId"]);
			
			// download the purchase (finish the transaction)
			try
			{
				_iap.removePurchaseFromQueue(data_["productId"],JSON.stringify(receipt));
			}
			catch(e:Error)
			{
				_purchaseFail("removePurchaseFromQueue error :" + event.data);
			}
			
		}
		
		protected function onPurchaseAlreadyHave(event:InAppPurchaseEvent):void
		{
			trace("already have : "+event.data); //product id
		}
		
		protected function onPurchaseError(event:InAppPurchaseEvent):void
		{
			trace(event.data); //trace error message
			if(_purchaseFail != null)_purchaseFail(event.data);
			//if(event.data.indexOf("reponse 7") > -1)
		}
	}
}
class singleton{}