package module
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	
	import Enum.EnumStatus;
	
	import event.GameEvent;
	import event.NetConnectionEvent;
	import event.PokerEvent;
	
	import item.ActionMsg;
	import item.BetCoin;
	import item.CardResult;
	
	import ui.PlayerPorkerAll;
	import model.PickInfo;

	public class PlayerModule extends Sprite
	{
		private var _m_nCoin:Number = 0;
		private var _m_strAction:String = "";
		private var m_coPorkerPlayer:PlayerPorkerAll = new PlayerPorkerAll();
		private var m_coMsgTurn:Poker_Msg_Your_Turn = new Poker_Msg_Your_Turn();
		private var m_coBetCoin:BetCoin = new BetCoin(true);
		private var m_coActionMsg:ActionMsg = new ActionMsg();
		public var m_bBuyingConfirm:Boolean=false;
		private var m_coCardResult:CardResult = new CardResult();	
		private var m_arSend:Array = new Array();
		private var m_spBetCoinActionTime:Sprite = new Sprite();
		private var m_spBuyinTime:Sprite = new Sprite();
		
		public function PlayerModule()
		{
			this.addChild(m_spBuyinTime);
			this.addChild(m_coBetCoin);
			this.addChild(m_coPorkerPlayer);
			this.addChild(m_spBetCoinActionTime);
			this.addChild(m_coMsgTurn);
			this.addChild(m_coActionMsg);
			this.addChild(m_coCardResult);
			m_coPorkerPlayer.y = 852
			m_coPorkerPlayer.x = 350
			m_coBetCoin.x = 360
			m_coBetCoin.y = 695;
			m_coMsgTurn.x = 100
			m_coMsgTurn.y = 700;
			m_coActionMsg.x = 360
			m_coActionMsg.y = 550
			m_coCardResult.y = 1000
			m_coMsgTurn.visible =false;
			m_coBetCoin.visible=false;
			m_coActionMsg.visible=false
			m_coCardResult.visible=false;
		}

		public function get m_strAction():String
		{
			return _m_strAction;
		}

		public function set m_strAction(value:String):void
		{
			_m_strAction = value;
		}

		public function get m_nCoin():Number
		{
			return _m_nCoin;
		}

		public function set m_nCoin(value:Number):void
		{
			if(value > 0 )
			{
				_m_nCoin += value;
				m_coBetCoin.m_nBetCoins = _m_nCoin
			}
		}

		public function fnGameStart():void
		{
			_m_nCoin = 0 ;
			m_coPorkerPlayer.fnNewGame();
			m_coMsgTurn.visible=false;
			m_coBetCoin.m_nBetCoins = 0
			m_coBetCoin.visible=true;
		}
		
	
		
		public function fnDeal(arCard:Array):void
		{
			m_coPorkerPlayer.fnDeal(arCard,true);
		}
		
		public  function fnPickCard():void
		{
			m_coPorkerPlayer.fnPickCard()
		}
		
		public function fnPickCardFinish():void
		{
			var arCard:Array = m_coPorkerPlayer.fnShowPickCard();
			m_arSend = new Array();	
			m_arSend.push(arCard[0]);//Card A index
			m_arSend.push(arCard[3]);//card b index
			fnNetPick();
		}
		
		private function fnNetPick(e:GameEvent = null):void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnNetPick);
			SessionManager.instance.request(Protocol.GAME_PICK, {token:GameConfig.game_playerInfo.token, game_id:GameConfig.game_openBetInfo.game_id, player_pick:m_arSend},onPick,onNet);

		}
		
		private function onPick(e:NetEvent):void
		{
			trace('onPick '+e.data);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnNetPick);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				//onPick {"result":1,"game_id":60,"whos_turn":0,"enemy_pick":["2","3"],"enemy_max_multiple":1}
				//0 player 1 enemy
				
				GameConfig.game_pickInfo = new PickInfo(e.data);
				var ev:NetConnectionEvent = new NetConnectionEvent(NetConnectionEvent.NET_EVENT,NetConnectionEvent.NET_PICK_FINISH_EVENT);
				dispatchEvent(ev);
			}
			else
			{
				var ev2:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				ev2.m_eventArgs = "onPick "+data
				EventManager.instance.trigger(ev2);
			}
			
		}
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
		
		public function fnPlayerDecision():void
		{
			m_coMsgTurn.visible =true
			m_coActionMsg.visible=false
			m_spBetCoinActionTime.addChild(m_coBetCoin)
		}
		
		public function fnAction(str:String):void
		{
			_m_strAction = str
			m_coActionMsg.fnAction(str);
			m_coActionMsg.visible=true
			m_coMsgTurn.visible=false
		}
		
		public function fnHideDecision():void
		{
			m_coActionMsg.visible=false
		}
		
		public function fnBuyingTime():void
		{
			m_coMsgTurn.visible =true
			m_coPorkerPlayer.fnBuyingTime();
			m_spBuyinTime.addChild(m_coBetCoin);
		}
		
		public function fnBuyingConfirm():void
		{
			m_coPorkerPlayer.fnBuyingConfirm();
			m_bBuyingConfirm = true;
			m_coMsgTurn.visible =false
		}
		
		public function fnShowCardResult():void
		{
			m_coCardResult.visible =true;
			m_coCardResult.fnShowResult(GameConfig.game_2ndBetInfo.player_reward);
		}
		
		
		
	}
}