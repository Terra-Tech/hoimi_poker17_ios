package Enum
{
	public class EnumMsg
	{
		public function EnumMsg()
		{
		}
		
		public static const MSG_TYPE_OK:String = "MSG_TYPE_OK";
		public static const MSG_TYPE_YES_NO:String = "MSG_TYPE_YES_NO";
		public static const PRESS_SCREEN_START_GAME_TW:String = "請任意點擊螢幕開始遊戲"
		public static const PRESS_SCREEN_START_GAME_EN:String = "Press any screen to start game"
		public static const INPUT_NICKNAME_TW:String = "請輸入暱稱\n限10個中文字或英數字";
		public static const INPUT_NICKNAME_EN:String = "Please enter a nickname.\nNo more than 10 English letters or numbers.";
		public static const RECONNECT_TW:String = "網路異常\n請確認網路流暢後再重新連線";
		public static const RECONNECT_EN:String = "Please make sure the network smoothly, then re-try the connection again.";
		public static const NICKNAME_ALREADY_USE_TW:String = "此暱稱已被使用!";
		public static const NICKNAME_ALREADY_USE_EN:String = "This nickname is already in use!";
		public static const NICKNAME_EXCEED_LIMIT_TW:String = "超過限制長度!";
		public static const NICKNAME_EXCEED_LIMIT_EN:String = "Exceed limit!";
		public static const NICKNAME_ILLEGAL_TW:String = "有非法字元請重新輸入!";
		public static const NICKNAME_ILLEGAL_EN:String = "Invalid character!";
		public static const REGULATION_UNCHECK_TW:String = "未勾選遊戲管理規章同意書";
		public static const REGULATION_UNCHECK_EN:String = "Management Regulations Unchecked";
		public static const RRSULATRION_TXT_TW:String = "";
		public static const COIN_LIMIT_TW:String = "財產限制:";
		public static const COIN_LIMIT_EN:String = "Coin Limit:";
		public static const MIN_LIMIT_TW:String = "最小 ";
		public static const MIN_LIMIT_EN:String = "Min ";
		public static const MAX_LIMIT_TW:String = "最大 ";
		public static const MAX_LIMIT_EN:String = "Man ";
		public static const BASE_BET_TW:String = "底注 ";
		public static const BASE_BET_EN:String = "Bet ";
		public static const OPEN_BET_TW:String = "遊戲開始";
		public static const OPEN_BET_EN:String = "Game Start";
		public static const PICK_TIME_TW:String = "請點選出兩張要翻開的牌";
		public static const PICK_TIME_EN:String = "Please select two cards to open";
		public static const FIRST_SECOND_BET_TIME_TW:String = "請選擇加注、跟注或放棄"
		public static const FIRST_SECOND_BET_TIME_EN:String = "Please select Raise, Call or Give up"
		public static const BUYING_TW:String = "請選擇要換的牌，可選擇 0 ~ 3 張";
		public static const BUYING_EN:String = "Please select the card you want to change, you can choose from 0 to 3";
		public static const CALL_DEAL_TW:String = "賭注成立";
		public static const CALL_DEAL_EN:String = "Call Deal";
		public static const SEARCHING_ENEMY_TW:String = "尋找對手中..."
		public static const SEARCHING_ENEMY_EN:String = "Searching Enemy..."
		public static const CONNECTING_TW:String = "連接中...";
		public static const CONNECTING_EN:String = "Connecting...";
		public static const WIN_TW:String = "勝";
		public static const WIN_EN:String = "Win";
		public static const LOSE_TW:String = "負"
		public static const LOSE_EN:String = "Lose"
		public static const WIN_LOSE_RATE_TW:String = "勝率"
		public static const WIN_LOSE_RATE_EN:String = "Rate"
		public static const IAP_DIAMOND_BONUS_TW:String = "額外加贈鑽石(限購乙次)"
		public static const IAP_DIAMOND_BONUS_EN:String = "Bonus Diamonds(once)"
		public static const USE_TW:String = "使用"
		public static const USE_EN:String = "Use"
		public static const GAIN_TW:String = "獲得"
		public static const GAIN_EN:String = "Gain"
		public static const CRITICAL_TW:String = "爆擊"
		public static const CRITICAL_EN:String = "Crit"
		public static const COIN_OVER_MAX_LIMIT_TW:String = "\n\n金幣數量高於房間最高限制。";	
		public static const COIN_OVER_MAX_LIMIT_EN:String = "\n\nCoin over max limit.";
		public static const DIAMOND_NOT_ENOUGH_TW:String = "鑽石數量不足\n\n是否前往儲值?";	
		public static const DIAMOND_NOT_ENOUGH_EN:String = "Diamond not enough. \n\nDo you want to buy some diamonds?";	
		public static const COIN_NOT_ENOUGH_TW:String = "金幣數量不足\n\n是否前往鑽石換取金幣頁面?";	
		public static const COIN_NOT_ENOUGH_EN:String = "Coin not enough. \n\nDo you want to gain some coins?";
		public static const DIAMOND_EXCHANGE_COIN_TW:String = "使用少量鑽石換取大量金幣！\n\n確定使用?";
		public static const DIAMOND_EXCHANGE_COIN_EN:String = "Exchange a few diamonds for a large amount of coins!";
		public static const SKIIL_RAISE_TW:String = "升級此技能！\n\n確定使用?";
		public static const SKIIL_RAISE_EN:String = "\n\nDo you want to raise this skill?";
		public static const PLEASE_EXIT_TABLE_TW:String = "\n哎呀!您的財產不符合房間限制條件，請重新選擇房間繼續遊玩";
		public static const PLEASE_EXIT_TABLE_EN:String = "\nOops ! Your property is not match the room restrictions, please reselect a room to play";
		public static const GIVE_RATE_TW:String = "如果覺得遊戲有趣，請給我們一個好評，謝謝您的支持喔！";
		public static const GIVE_RATE_EN:String = "Like this game? Give us a comment！";
		public static const LINE_SHARE_TW:String = "邀請你來玩17Poker，來一場謀略對決的最佳一對一撲克遊戲吧！";
		public static const LINE_SHARE_EN:String = "Come to play 17Poker，the best tactics poker game!";
		public static const INVITE_FB_FRIEND_TW:String = "邀請FB好友，只要好友加入遊戲\n每多一位就可以獲得10萬金幣喔！";
		public static const INVITE_FB_FRIEND_EN:String = "Invite FB friends , just friends to join the game \nEach one more friend you can get 100000 gold Oh !";
		public static const SYSTEM_COMMISSION_TW:String = "系統收取{0}%金幣"
		public static const SYSTEM_COMMISSION_EN:String = "system charged {0}% coins"
		public static const EXIT_GAME_TW:String = "\n離開遊戲?"
		public static const EXIT_GAME_EN:String = "\nExit Game?"
	}
}