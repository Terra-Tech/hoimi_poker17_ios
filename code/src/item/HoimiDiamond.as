package item
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	
	import event.GameEvent;
	
	import lib.TiConvertNumber;
	
	public class HoimiDiamond extends Sprite
	{
		private var m_sp:Sprite = new Sprite();
		public var m_nDiamond:Number
		public function HoimiDiamond()
		{
			super();
			this.addChild(m_sp);
		//	EventManager.instance.register(GameEvent.GAME_DIAMOND_CHANGE_EVENT, fnDiamondChange);
			//fnSetValue(12345678)
		}
		
		
		
		public function fnSetValue(value:Number):void
		{
			m_nDiamond = value
			m_sp.removeChildren();
			var sp:Sprite = TiConvertNumber.fnConvertValueToUI(value,Poker_Number_Diamond_Small,0,false,true);
			m_sp.addChild(sp);
		
		}
		
		public function fnSetValue2(value:Number):void
		{
			m_nDiamond = value
			m_sp.removeChildren();
			var sp:Sprite = TiConvertNumber.fnConvertValueToUI(value,Poker_Number_Diamond_Small,0,false,true);
			m_sp.addChild(sp);
			sp.x += sp.width
		}
		

		
		public function fnDestory():void
		{
		//	EventManager.instance.remove(GameEvent.GAME_DIAMOND_CHANGE_EVENT, fnDiamondChange);
		}
	}
}