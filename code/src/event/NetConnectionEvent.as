package event
{
	public class NetConnectionEvent extends TiBasicEvent
	{
		public static const NET_EVENT:String = "NET_EVENT";
		public static const NET_PICK_FINISH_EVENT:String = "NET_PICK_FINISH_EVENT";
		
		public function NetConnectionEvent(type:String, detail:String="", bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, detail, bubbles, cancelable);
		}
	}
}