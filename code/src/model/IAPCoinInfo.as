package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class IAPCoinInfo extends NetPacket
	{
		/** iap coin
		 * 1:{"result" : 1, 'product_id':twd_150, 'vaild':true, player_coin:15000}
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 * -4:param miss
		 * -5:data lost
		 */
		public var product_id:String;
		public var valid:Boolean = false;
		public var player_coin:int = 0;
		public function IAPCoinInfo(iap:String)
		{
			super(iap);
		}
	}
}