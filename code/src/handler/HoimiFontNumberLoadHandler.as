package handler
{
	import flash.display.Loader;
	import flash.display.LoaderInfo;
	import flash.display.SWFVersion;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.text.Font;
	
	import event.GameEvent;
	
	

	public class HoimiFontNumberLoadHandler extends EventDispatcher
	{
		private static var m_Instance:HoimiFontNumberLoadHandler;
		[Embed(source="/resource/ttf/Montserrat-Bold.ttf",fontName = "Arial_Hebrew_Scholar",mimeType = "application/x-font",fontWeight="normal",fontStyle="normal",advancedAntiAliasing="true",embedAsCFF="false")]
		public var embeddedFont:Class;
		//public var embeddedFont:Font;
		public var embeddedFontName:String ="Arial_Hebrew_Scholar";
		
		public function HoimiFontNumberLoadHandler()
		{
			
		}
		
		
		public static function fnGetInstance(): HoimiFontNumberLoadHandler 
		{
			if ( m_Instance == null )
			{
				m_Instance = new HoimiFontNumberLoadHandler();
			}
			return m_Instance;
		}
		
		
		public function fnLoadFont():void
		{
				fnLoadSwf();
		}
		
		private function fnLoadSwf():void
		{
			
			if(embeddedFont !=null)
			{
				trace("embeddedFont !=null");
				fnFontLoadComplete();
			}
			else
			{
				var context:LoaderContext = new LoaderContext(false,ApplicationDomain.currentDomain);
				var myLoader:Loader = new Loader();    
				// create a new instance of the Loader class
				var url:URLRequest = new URLRequest("resource/swf/Hoimi_Font_Number.swf"); // in this case both SWFs are in the same folder 
				myLoader.load(url,context);                                     // load the SWF file
				myLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,fnSwfLoadCompleteEventHandler);
				myLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,fnIoErrorEventHandler);
				
			}
		}
		
		private function fnIoErrorEventHandler(e:IOErrorEvent):void
		{
			trace("fnLoadSwf error :" + e.text)
		}
		
		private function fnSwfLoadCompleteEventHandler(e:Event):void
		{
			trace("get Hoimi_Font_Number");
			var loaderInfo:LoaderInfo = e.currentTarget as LoaderInfo
			loaderInfo.removeEventListener(Event.COMPLETE,fnSwfLoadCompleteEventHandler);
			if(loaderInfo.applicationDomain.hasDefinition("Arial_Hebrew_Scholar"))
			{
				var cls: Class = loaderInfo.applicationDomain.getDefinition( "Arial_Hebrew_Scholar" ) as Class;
				embeddedFont = new cls();
				Font.registerFont(cls); // very important to registerFont, embedFonts set true
				fnFontLoadComplete();
			}
			else
			{
				trace("error : getDefinitionByName null");	
			}
		}
		
		private function fnFontLoadComplete():void
		{
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NUMBER_FONT_LOAD_COMPLETE_EVENT);
			dispatchEvent(ev);
		}
	}
}