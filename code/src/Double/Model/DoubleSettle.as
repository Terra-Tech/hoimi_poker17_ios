package Double.Model
{
	public class DoubleSettle
	{
		public var token:String;
		public var game_id:Number;
		public var bet_option:int;
		public var bet_amount:Number;
		
		public function DoubleSettle(token:String, game_id:Number, bet_option:int, bet_amount:Number)
		{
			this.token = token;
			this.game_id = game_id;
			this.bet_option = bet_option;
			this.bet_amount = bet_amount;
		}
	}
}