package handler
{
	import flash.events.EventDispatcher;
	import flash.media.Sound;
	
	import event.TTCSoundEvent;

	public class TTCSoundLevelHandler extends EventDispatcher
	{
		public function TTCSoundLevelHandler()
		{
		}
		
		
		private function fnSoundPlay(sound:Sound):void
		{
			var ev:TTCSoundEvent = new TTCSoundEvent(TTCSoundEvent.SOUND_EVENT,TTCSoundEvent.SOUND_PLAY_EVENT);
			ev.m_eventArgs = sound;
			dispatchEvent(ev);
			
		}
		
		public function fnSoundLevel(value:int):void
		{
			
		}
	}
}