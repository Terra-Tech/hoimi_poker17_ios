package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class VersionInfo extends NetPacket
	{
		/** version get 
		 * 1:{"result" : 1, 'version' : {value,:1.0.2,message:New version is release need update ?, url:market://details?id=air.com.hoimi.CorrectWord&reviewId=0}}
		 * -1:param miss
		 */

		public var version:Version = new Version();
		
		public function VersionInfo(str:String=null)
		{
			super(str);
		}
	}
}