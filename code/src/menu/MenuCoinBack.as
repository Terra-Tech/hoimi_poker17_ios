package menu
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import CoinBack.CoinBackController;
	import CoinBack.CoinBackView;
	
	import base.TiDisableTextField;
	
	
	public class MenuCoinBack extends Sprite
	{
		private var m_mc:Poker_Btn_Menu_Coin_Back = new Poker_Btn_Menu_Coin_Back();
		private var m_coCoinBackView:CoinBackView = CoinBackView.fnGetInstance()
		
		
		public function MenuCoinBack()
		{
			
			super();
			this.addChild(m_mc);
			this.addChild(m_coCoinBackView);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
			m_mc.addEventListener(MouseEvent.CLICK,fnClickCoinBack);
			m_coCoinBackView.mouseChildren=false
			m_coCoinBackView.mouseEnabled=false;
			m_coCoinBackView.showPool(GameConfig.game_playerInfo.coin_back_pool);
		}
		
		private function fnClickCoinBack(e:MouseEvent):void
		{
			EventManager.instance.trigger(new Event(CoinBackController.EVENT_COIN_BACK_WITHDRAW));
		}
		
		
	}
}