package lib
{
	public class CoinValueTransfer
	{
		public function CoinValueTransfer()
		{
		}
		
		public static function fnCoinValueTransferUnit(value:Number):String
		{
			var nTransfer:String=""
			if(value < 100)
			{
				nTransfer = value.toString(); 
			}
			else if(value >= 1000 && value < 1000000)
			{
				nTransfer = value/1000 + "K"
			}
			else if(value >= 1000000 && value < 1000000000)
			{
				nTransfer = value/1000000 + "M"
			}
			else 
			{
				nTransfer = value/1000000000 + "B"
			}
			return nTransfer;
		}
		
		public static function fnCoinValueTransferUnitTW(value:Number):String
		{
			var nTransfer:String=""
			if(value < 10000)
			{
				nTransfer = value.toString(); 
			}
			else if(value >= 10000 && value < 100000000)
			{
				nTransfer = value/10000 + "萬"
			}
			else 
			{
				nTransfer = value/100000000 + "億"
			}
			return nTransfer;
		}
		
		public static function fnCoinValueTransferTS(nNumber:Number):String
		{
			var ar:Array =new Array();
			for(var i:int=0 ; ;i++)
			{
				var str:String = nNumber.toString().charAt(i); 
				if(str=="")
				{
					break;
				}
				else
				{
					ar.push(str);
				}
			}
			
			
			ar.reverse();
			var arNew:Array = new Array();
			for(var j:int = 0;j<ar.length;j++)
			{
				if((j+1)%3==0 && j!=ar.length-1)
				{
					arNew.push(ar[j]);
					arNew.push(",");
				}
				else
				{
					arNew.push(ar[j]);
				}
			}
			arNew.reverse();
			var strNumber:String="";
			for(var k:int = 0;k<arNew.length;k++)
			{
				strNumber += arNew[k];
			}
			
			return strNumber
		}
		
		
	}
}