package Enum
{
	public class EnumLocalSave
	{
		public function EnumLocalSave()
		{
		}
		public static const MUSIC_MUTE:String = "MUSIC_MUTE";
		public static const SOUND_MUTE:String = "SOUND_MUTE";
		public static const NICKNAME:String = "NICKNAME";
		public static const APP_RATE:String="APP_RATE";
		public static const NO_AD:String = "NO_AD"
		public static const UUID:String = "UUID";	
		public static const FIRST_SIGN_IN:String = "FIRST_SIGN_IN";
		public static const DOUBLE_RECORD:String = "DOUBLE_RECORD";
	}
}