package item
{
	
	
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	
	import flash.utils.Timer;
	
	
	import base.TiDisableTextField;
	
	
	import event.GameEvent;
	
	import handler.TTCSoundHandler;
	
	

	public class CoinIAP extends Sprite
	{
		private var m_coCoinValue:HoimiCoin = new HoimiCoin();
		private var m_mcBtnIAP:Poker_Btn_IAP = new Poker_Btn_IAP();
		private var m_nFinalCoin:Number = 0;	
		private var m_nCurrentCoin:Number = 0
		private var m_nAddTickCoin:int = 0;	
		private var m_timer:Timer = new Timer(10);
		private var m_nAniTick:int = 50
			
		public function CoinIAP()
		{
			this.addChild(m_mcBtnIAP);
			this.addChild(m_coCoinValue);
			m_coCoinValue.mouseEnabled=false;
			m_coCoinValue.mouseChildren=false;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcBtnIAP);
			m_mcBtnIAP.addEventListener(MouseEvent.CLICK,fnClickHandler);
			m_coCoinValue.y = 23
			m_coCoinValue.x = 295
			EventManager.instance.register(GameEvent.GAME_COIN_CHANGE_EVENT, fnCoinChange);
		}
		
		private function fnCoinChange(e:GameEvent):void
		{
			//m_coCoinValue.fnSetValue(GameConfig.game_playerInfo.coin)
			m_timer.stop();
			m_timer.removeEventListener(TimerEvent.TIMER,fnTimerTickPlus);
			m_timer.removeEventListener(TimerEvent.TIMER,fnTimerTickMinus);
			
			m_nFinalCoin = GameConfig.game_playerInfo.coin
			var nWinLose:Number = m_nFinalCoin - m_nCurrentCoin
			if(nWinLose > 0)
			{
				if(nWinLose > m_nAniTick)
				{
					m_nAddTickCoin = nWinLose/m_nAniTick;
				}
				else
				{
					m_nAddTickCoin = 1
				}
				m_timer.addEventListener(TimerEvent.TIMER,fnTimerTickPlus);
				m_timer.start();
			}
			else if(nWinLose < 0 )
			{
				if(nWinLose < -m_nAniTick)
				{
					m_nAddTickCoin = nWinLose/m_nAniTick;
				}
				else
				{
					m_nAddTickCoin = -1
				}
				m_timer.addEventListener(TimerEvent.TIMER,fnTimerTickMinus);
				m_timer.start();
			}
		}
		
		private function fnTimerTickPlus(e:TimerEvent):void
		{
			m_nCurrentCoin += m_nAddTickCoin
			if(m_nCurrentCoin >=  m_nFinalCoin)	
			{
				m_nCurrentCoin = m_nFinalCoin
				m_timer.stop();
				m_timer.removeEventListener(TimerEvent.TIMER,fnTimerTickPlus);
				
			}
			m_coCoinValue.fnSetValue(m_nCurrentCoin);
		}
		
		private function fnTimerTickMinus(e:TimerEvent):void
		{
			m_nCurrentCoin += m_nAddTickCoin
			if(m_nCurrentCoin <=  m_nFinalCoin)	
			{
				m_nCurrentCoin = m_nFinalCoin
				m_timer.stop();
				m_timer.removeEventListener(TimerEvent.TIMER,fnTimerTickMinus);
			}
			m_coCoinValue.fnSetValue(m_nCurrentCoin);
		}
	
		private function fnClickHandler(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			EventManager.instance.trigger( new GameEvent(GameEvent.GAME_GAIN_COIN_EVENT));
			
		}
		
		
		
		public function fnDestory():void
		{
		}
	}
}