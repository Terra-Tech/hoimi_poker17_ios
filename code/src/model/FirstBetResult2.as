package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class FirstBetResult2 extends NetPacket
	{
		public var game_id:int = -1;
		public var winlose:Number = 0;
		public var player_coin:Number = 0;
		public var player_diamond:int = 0;
		public var level:int = 0;
		public var exp:int;
		public var next_exp:int;
		/// 升級獎勵
		public var reward_coin:int;
		/// 升級獎勵
		public var reward_diamond:int;
		/// 牌型
		public var player_reward:String;
		/// JP 獎勵
		public var jp_bonus:Number;
		
		public var player_cards:Array = [];
		/// 戰績
		public var player_record:PlayerRecord = new PlayerRecord(null);
		/// 返利錢池
		public var coin_back_pool:Number;
		
		
		public function FirstBetResult2(bet:String)
		{
			super(bet);
		}
	}
}