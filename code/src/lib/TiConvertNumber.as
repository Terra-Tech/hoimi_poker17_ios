package lib
{
	import com.hoimi.util.ConvertValueToArray;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;

	public class TiConvertNumber
	{
		public function TiConvertNumber()
		{
		}
		
		public static function fnConvertValueToArray(nNumber:Number,bUseTS:Boolean):Array
		{
			var ar:Array =new Array();
			for(var i:int=0 ; ;i++)
			{
				var str:String = nNumber.toString().charAt(i); 
				if(str=="")
				{
					break;
				}
				else
				{
					ar.push(str);
				}
			}
			if(bUseTS)
			{
				ar.reverse();
				var arNew:Array = new Array();
				for(var j:int = 0;j<ar.length;j++)
				{
					if((j+1)%3==0 && j!=ar.length-1)
					{
						arNew.push(ar[j]);
						arNew.push(",");
					}
					else
					{
						arNew.push(ar[j]);
					}
				}
				arNew.reverse();
				ar = arNew;
			}
			return ar;
		}
		
		public static function fnConvertValueToUI(nNum:Number,cls:Class,nGap:Number,bUsePlusMinus:Boolean,bUseTS:Boolean):Sprite
		{
			var ar:Array = fnConvertValueToArray(nNum,bUseTS);
			ar.reverse();
			if(nNum>0 && bUsePlusMinus)
			{
				
				ar.push("+");
			}
			else if(nNum < 0 && !bUsePlusMinus)
			{
				ar.shift();
			}
			var strValue:String
			var mc:MovieClip
			var sp:Sprite = new Sprite();
			for(var i:int = 0;i<ar.length;i++)
			{
				mc = new cls();
				mc.mouseEnabled =false;
				mc.mouseChildren =false;
			
				strValue = ar[i] 
				if(strValue == "0")
				{
					mc.gotoAndStop(10);
					mc.x -= nGap+sp.width
				}
				else if(strValue == "+")
				{
					mc.gotoAndStop(11);
					mc.x -= nGap+sp.width
				}
				else if(strValue == "-")
				{
					mc.gotoAndStop(12);
					mc.x -= nGap+sp.width
				}
				else if(strValue == ",")
				{
					mc.gotoAndStop(13)
					mc.x -= (sp.width-(mc.width/2))
				}
				else
				{
					mc.gotoAndStop(int(strValue));	
					mc.x -= nGap+sp.width
				}
				
				sp.addChild(mc);
			}
			return sp;
		}
	}
}