package com.hoimi.util
{
	import com.adobe.serialization.json.JSON;
	
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.utils.Timer;

	public class HttpHelper
	{
		public function HttpHelper()
		{
			throw new Error("can't be new instance");
		}
		
		static public function get(url:String, params:String="", ioErrCB:Function=null, statusCB:Function=null,timeoutCB:Function=null,completeCB:Function=null):void
		{
			/*if(!DeviceUtil.getInstance().isOnDevice)
			{
				url = url.replace("www.hoimigame.com", "127.0.0.1");
			}*/
			trace("get url "+ url + params);
			var req:URLRequest = new URLRequest();
			req.method = URLRequestMethod.GET;
			req.url = url+params;
			
			var lodaer:URLLoader = new URLLoader();
			
			lodaer.addEventListener(Event.COMPLETE, onComplete, false, 0, true);
			lodaer.addEventListener(IOErrorEvent.IO_ERROR, onIoError, false, 0, true);
			lodaer.addEventListener(HTTPStatusEvent.HTTP_STATUS, onStatus, false, 0, true);
			
			var timeoutAmount:int = 3000; // wait 3 seconds

			//add a timer to check for HTTP request timeout
			var loadTimer:Timer = new Timer(timeoutAmount);
			loadTimer.addEventListener(TimerEvent.TIMER, onTimeout, false, 0, true);
			try
			{
				lodaer.load(req);	
				loadTimer.start();
			}
			catch(err:Error)
			{
				trace("Unable to load get URL "+ err.message);			
			}
			
			function onComplete(e:Event):void
			{
				removeAllListener();
				trace("get url complete data:"+ e.target.data);
				if(completeCB != null)
				{
					
					completeCB(e.target.data);
				}
					
			}
			
			function onIoError(e:IOErrorEvent):void
			{
				removeAllListener();
				trace("url get IOError");
				if(ioErrCB != null)
				{
					ioErrCB();
				}
			}
			
			function onStatus(e:HTTPStatusEvent):void
			{
				trace("url get status :" + e.status);
				if(statusCB != null)
				{
					statusCB(e.status);
				}
			}
			
			function onTimeout(e:TimerEvent):void 
			{
				removeAllListener();
				trace("url get time out");
				if(timeoutCB)
				{
					timeoutCB();
				}
				
			}
			
			function removeAllListener():void
			{
				//remove timer
				loadTimer.stop();
				loadTimer.removeEventListener(TimerEvent.TIMER, onTimeout);
				loadTimer = null;
				
				lodaer.removeEventListener(Event.COMPLETE, onComplete);
				lodaer.removeEventListener(IOErrorEvent.IO_ERROR, onIoError);
				lodaer.removeEventListener(HTTPStatusEvent.HTTP_STATUS, onStatus);
			}
		}
		
		static public function post(url:String, json:Object =null,fnIOError:Function=null,fnStatus:Function=null,fnTimeOut:Function=null,fnComplete:Function=null):void
		{
			/*if(!DeviceUtil.getInstance().isOnDevice)
			{
				url = url.replace("www.hoimigame.com", "127.0.0.1");
				//return;
			}*/
			
			trace("post url "+ url + "params:"+ com.adobe.serialization.json.JSON.encode(json));
			//var vars:URLVariables = new URLVariables();
			//vars.data = jsonParams;
			var p:String ="";
			if(json)
			{
				p = com.adobe.serialization.json.JSON.encode(json);
			}
			
			var req:URLRequest = new URLRequest();
			req.method = URLRequestMethod.POST;
			var hdr:URLRequestHeader = new URLRequestHeader("Content-type", "application/json");
			req.requestHeaders.push(hdr);
			req.data = p
			req.url = url;
			
			var lodaer:URLLoader = new URLLoader();
			lodaer.addEventListener(IOErrorEvent.IO_ERROR, onioerr, false, 0, true);
			lodaer.addEventListener(HTTPStatusEvent.HTTP_STATUS, onstatus, false, 0, true);
			lodaer.addEventListener(Event.COMPLETE, completeHandler, false, 0, true);
			function onioerr(e:IOErrorEvent):void
			{
				//EventManager.instance.trigger(new LogEvent("post "+url+" io error " + e.toString()));
				if(fnIOError)
				{
					trace("IO Error")
					fnIOError();
				}
					
			}
			function onstatus(e:HTTPStatusEvent):void
			{
				//EventManager.instance.trigger(new LogEvent("post "+url+" status " + e.toString()));
				trace("url :"+url+"resp status :" + e.status);
				if(fnStatus)
				{
					fnStatus(e.status);	
				}
				
			}
			function completeHandler(e:Event):void
			{
				trace("url :"+url+"resp succes :" + e.target.data);
				if(fnComplete)
				{
					fnComplete(e.target.data);	
				}
				
				//EventManager.instance.trigger(new LogEvent("post "+url+" complete " + e.target.data));
			}
			function removeAllListener():void
			{
				//remove timer
				loadTimer.stop();
				loadTimer.removeEventListener(TimerEvent.TIMER, onTimeout);
				loadTimer = null;
				
				lodaer.removeEventListener(Event.COMPLETE, completeHandler);
				lodaer.removeEventListener(IOErrorEvent.IO_ERROR, onioerr);
				lodaer.removeEventListener(HTTPStatusEvent.HTTP_STATUS, onstatus);
			}
			function onTimeout(e:TimerEvent):void 
			{
				removeAllListener();
				trace("post time out");
				if(fnTimeOut)
				{
					fnTimeOut();	
				}
				
			}
			try
			{
				lodaer.load(req);	
				loadTimer.start();
			}
			catch(err:Error)
			{
				trace("Unable to load post URL "+ err.message);	
			}
			
			var timeoutAmount:int = 3000; // wait 3 seconds
			
			//add a timer to check for HTTP request timeout
			var loadTimer:Timer = new Timer(timeoutAmount);
			loadTimer.addEventListener(TimerEvent.TIMER, onTimeout, false, 0, true);
			
		}
	}
}