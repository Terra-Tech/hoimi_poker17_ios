package item
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	import Enum.EnumMsg;
	
	import base.TiButtonState;
	import base.TiDisableTextField;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	import event.GameMessageEvent;
	
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	import handler.YesNoPanelHandler;
	
	import model.ExchangeInfo;
	
	public class GainCoinItem extends Sprite
	{
		private var m_mc:Poker_Btn_Gain_Coin_Item = new Poker_Btn_Gain_Coin_Item();
		private var m_coDiamondNumber:HoimiDiamond = new HoimiDiamond();
		private var m_coCoinNumber:HoimiCoin = new HoimiCoin();
		private var m_buttonState:TiButtonState = new TiButtonState(); 
		private var m_sp:Sprite = new Sprite();
		public var m_bSelected:Boolean =false;
		
		public function GainCoinItem(nDiamond:Number , nCoin:Number)
		{
			super();
			this.addChild(m_mc);
			m_mc.diamond.addChild(m_coDiamondNumber);
			m_mc.coin.addChild(m_coCoinNumber);
			this.addChild(m_sp);
		//	m_coDiamondNumber.x = -175;
	//		m_coDiamondNumber.y = 100;
		//	m_coCoinNumber.x = 105;
	//		m_coCoinNumber.y = 100
			m_coDiamondNumber.fnSetValue(nDiamond);
			m_coCoinNumber.fnSetValue(nCoin);
			m_coDiamondNumber.mouseChildren=false;
			m_coDiamondNumber.mouseEnabled=false;
			m_coCoinNumber.mouseChildren=false;
			m_coCoinNumber.mouseEnabled=false;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
			m_mc.addEventListener(MouseEvent.CLICK,fnClickItem);
			
		}
		
		public function fnCheckButtonEnable():void
		{
			if(m_coDiamondNumber.m_nDiamond > GameConfig.game_playerInfo.diamond)
			{
				m_buttonState.fnDisableAndDarkMcState(m_mc)
				m_mc.addEventListener(MouseEvent.CLICK,fnClickItemIap);
				m_mc.removeEventListener(MouseEvent.CLICK,fnClickItem);
			}
			else
			{
				m_buttonState.fnEnableMcState(m_mc)
				m_mc.removeEventListener(MouseEvent.CLICK,fnClickItemIap);
				m_mc.addEventListener(MouseEvent.CLICK,fnClickItem);
			}
		}
		
		private function fnClickItemIap(e:MouseEvent):void
		{
			YesNoPanelHandler.fnDiamondNotEnough();
		}
		
		private function fnClickItem(e:MouseEvent):void
		{
			if(m_bSelected)
			{
				exchange();
			}
			else
			{
				TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
				var ev:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
				ev.m_strMsg = TxtLangTransfer.DIAMOND_EXCHANGE_COIN
				ev.m_strType = EnumMsg.MSG_TYPE_YES_NO;
				ev.m_callYesFn = exchange;
				EventManager.instance.trigger(ev);
			}
		}
		
		
		private function exchange():void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,exchange);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			SessionManager.instance.request(Protocol.USER_EXCHANGE, {token:GameConfig.game_playerInfo.token, exchange_diamond:m_coDiamondNumber.m_nDiamond}, onExchange, onNet);
		} 
		
		private function onExchange(e:NetEvent):void
		{
			trace('onExchange ' +e.data);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,exchange);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				var ex:ExchangeInfo = new ExchangeInfo(e.data);
				var nGain:Number = ex.player_coin - GameConfig.game_playerInfo.coin
				fnSetData(nGain);
				GameConfig.game_playerInfo.coin = ex.player_coin;
				GameConfig.game_playerInfo.diamond = ex.player_diamond;
				var ar:Array = new Array();
				ar.push(m_coDiamondNumber.m_nDiamond)
				ar.push(nGain);
				ar.push(ex.exchange_rate);
				var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_DIAMIOND_CHANGE_COIN_FINISH_EVENT)
				ev.m_eventArgs = ar
				dispatchEvent(ev);
				
			}
			
			//diamond(ex.player_diamond);
			//coin(ex.player_coin);
		}
		
		private function fnSetData(nCoin:Number):void
		{
			var m_coMsgGainCoin:ResultPanelCoin = new ResultPanelCoin();
			m_coMsgGainCoin.fnSetValue(nCoin,true,false);
			m_coMsgGainCoin.x = -200
			m_coMsgGainCoin.y = 50
			m_sp.addChild(m_coMsgGainCoin);
			setTimeout(function():void 
			{
				fnTweenEffect(m_coMsgGainCoin);
			}, 2000)
		}
		
		private function fnTweenEffect(mc:Sprite):void
		{
			Tweener.addTween(mc,{y:-150,time:1,alpha:0,transition:"easeInQuart",onComplete:fnTweenEffectFinish});
		}
		
		private function fnTweenEffectFinish():void
		{
			m_sp.removeChildren();
		}
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
		
	}
}