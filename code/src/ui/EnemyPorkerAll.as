package ui
{
	import flash.display.Sprite;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	import event.PokerEvent;
	
	import handler.TTCSoundHandler;
	
	import item.Poker;
	
	import lib.RandomMachine;
	
	public class EnemyPorkerAll extends Sprite
	{
		private var m_sp:Sprite = new Sprite();
		private var m_cardA:Poker
		private var m_cardB:Poker
	
		private var m_nIndexCard:int = 0;
		private var m_arCard:Array 
		private var m_arBuyingCard:Array
		
		public function EnemyPorkerAll()
		{
			super();
			this.addChild(m_sp);
		}
		
		public function fnNewGame():void
		{
			m_sp.removeChildren();
		}
		
		public function fnDeal(ar:Array,bPlayer:Boolean):void
		{
			m_sp.removeChildren();
			m_arCard = new Array();
			m_nIndexCard = 0;
			var porker:Poker
			for(var i:int = 0 ; i< 5 ;i++)
			{
				porker = new Poker(ar[i])
				/*if(bPlayer)	
				{
					porker.fnCardShow();
				}*/
				porker.x = (i-2)*142+10
				m_arCard.push(porker);
			//	m_sp.addChild(porker);
				if(i==3)
				{
					m_cardA = porker
				}
				else if(i ==4)
				{
					m_cardB = porker
				}
				m_sp.mouseChildren = false;
				m_sp.mouseEnabled = false;
			}
			fnTweenCardAddToStage();
		}
		
		private function fnTweenCardAddToStage():void
		{
			var card:Poker = m_arCard[m_nIndexCard] as Poker
			var nX:Number = card.x
			card.x = 720
			m_sp.addChild(card);
			Tweener.addTween(card,{x:nX,time:0.3,transition:"easeOutBack",onComplete:fnTweenCardAddtoStageFinish});
		}
		
		private function fnTweenCardAddtoStageFinish():void
		{
			m_nIndexCard++
			if(m_nIndexCard<5)
			{
				fnTweenCardAddToStage();
			}
		}
		
		
		public function fnShowPickCard():void
		{
			m_sp.mouseChildren = false;
			m_sp.mouseEnabled = false;
			var ar:Array = GameConfig.game_pickInfo.enemy_pick
			m_cardA.fnEnemyCardShow(ar[0]);
			m_cardB.fnEnemyCardShow(ar[1])
		/*	var ar:Array = new Array();
			ar.push(m_cardA.m_nCardIndex,m_cardA.m_nDivid,m_cardA.m_nMod);
			ar.push(m_cardB.m_nCardIndex,m_cardB.m_nDivid,m_cardB.m_nMod);*/
			
		}
		
		public function fnBuyingConfirm():void
		{
			var nBuying:int =  GameConfig.game_1stBetInfo1.enemy_buying_amount
			trace("nBuying:"+nBuying);
			m_arBuyingCard = new Array();
			if(nBuying > 0)
			{
				var arRan:Array= RandomMachine.fnGetRanArray([0,1,2],nBuying);
				arRan.sort();
				for(var i:int = 0 ;i< arRan.length ;i++)
				{
					var nIndex:int = arRan[i]
					var card:Poker = m_arCard[nIndex]
					Tweener.addTween(card,{x:card.x+1000,time:0.15,transition:"easeOutBack"});
					m_arBuyingCard.push(card);
				}
				m_nIndexCard= 0;
				setTimeout(function():void
				{
					fnBuyingCardAddToStage()
				},500)
			
			}
			else
			{
				setTimeout(function():void
				{
					var ev:PokerEvent =new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_ENEMY_BUYING_CONFIRM_EVENT,true);
					dispatchEvent(ev);
				},500)
			}
		}
		
		private function fnBuyingCardAddToStage():void
		{
			if(m_nIndexCard< m_arBuyingCard.length)
			{
				var card:Poker = m_arBuyingCard[m_nIndexCard] as Poker
				card.visible=true
				fnBuyingCardAddToStageTween(card)
			}
			else 
			{
				var ev:PokerEvent =new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_ENEMY_BUYING_CONFIRM_EVENT,true);
				dispatchEvent(ev);
			}
		
		}
		
		private function fnBuyingCardAddToStageTween(card:Poker):void
		{
			m_nIndexCard++
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound055()
			m_sp.addChild(card);
			var nX:Number = card.x-1000
			card.x = -720
			Tweener.addTween(card,{x:nX,time:0.3,transition:"easeOutBack",onComplete:fnBuyingCardAddToStage});
		}
		
		public function fnOpenCard():void
		{
			var porker:Poker 
			var arCards:Array = GameConfig.game_2ndBetInfo.enemy_cards
			/*var arCards:Array = new Array();
			for(var k:int = 0; k< ar.length;k++)
			{
				var num:int = ar[k]
				arCards.push(num);
			}*/
				
				
			m_sp.removeChildren();
			m_arCard.length = 0;
			var arEnemyCard:Array = new Array();
			for(var i:int =0;i<arCards.length;i++)
			{
				if(arCards[i] == m_cardA.m_nCardIndex || arCards[i] == m_cardB.m_nCardIndex)
				{
					//do nothing
				}
				else
				{
					arEnemyCard.push(arCards[i]);
				}
				
			}
			arEnemyCard.push(m_cardA.m_nCardIndex);
			arEnemyCard.push(m_cardB.m_nCardIndex);
			
			for(var j:int = 0;j<5;j++)
			{
				porker = new Poker(arEnemyCard[j]);
				porker.x = (j-2)*142+10
				porker.fnCardShow();
				m_arCard.push(porker);
				m_sp.addChild(porker);
			}
			setTimeout(function():void 
			{
				fnSortCardHandler(GameConfig.game_2ndBetInfo.enemy_cards,m_arCard);
			}, 500)
			
			setTimeout(function():void 
			{
				var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_ENEMY_OPEN_CARD_FINISH_EVENT,true);
				dispatchEvent(ev)
			},1500)
			
		}
		
		
		private function fnSortCardHandler(arCardIndex:Array,arCard:Array):void
		{
			
			//GameConfig.game_openBetInfo.player_cards.reverse();
			var porker:Poker
			var arNew:Array = new Array();
			for(var i:int = 0 ; i< 5 ;i++)
			{
				var nIndex:int = arCardIndex[i]
				for(var j:int = 0; j<5 ;j++)
				{
					porker = arCard[j] as Poker
					if(nIndex == porker.m_nCardIndex)
					{
						var newX:Number = (i-2)*142 +10
						porker.m_nCardIndexAtSort = i	
						fnTweenSortCard(porker,newX);
						arNew.push(porker);
					}
				}
			}
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound055();
			m_arCard = arNew;
		}
		
		private function fnTweenSortCard(card:Poker,nX:Number):void
		{
			Tweener.addTween(card,{x:nX,time:0.15,transition:"easeOutExpo"});
		}
	}
}