package ui
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	import com.hoimi.util.RegExpHelper;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import Enum.EnumLocalSave;
	
	import FB.FacebookController;
	
	import base.TiDisableTextField;
	
	
	import event.GameEvent;
	
	import handler.HoimiFont01LoadHandler;
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	import lib.MultiLanguagleUISelect;
	
	import model.FBSignupInfo;
	import model.SignupInfo;
	
	public class NicknamePanel extends Sprite
	{
		private var m_bg:Poker_Lobby_Bg = new Poker_Lobby_Bg();
		private var m_mc:Poker_Nickname_Panel = new Poker_Nickname_Panel();
		private var m_nMaxCHars:int = 14;
		private var m_nKeyTotal:int = 0;
		private var m_strNickname:String="";
		private var m_coRegulationPanel:RegulationPanel
		
		public function NicknamePanel()
		{
			super();
		//	this.addChild(m_bg);
			
				
			
		}
		
		private function GameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_FONT_01_LOAD_COMPLETE_EVENT)
			{
				fnEmbededFont();
			}
		}
		
		private function fnEmbededFont():void
		{
			HoimiFont01LoadHandler.fnGetInstance().removeEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			var tf:TextFormat = new TextFormat();
			tf.font = HoimiFont01LoadHandler.fnGetInstance().embeddedFont.fontName;
			//tf.size = 40;
			//tf.color = "0xFF8800";
			m_mc.txt_msg.embedFonts = true; // very important to set
			m_mc.txt_msg.defaultTextFormat = tf;
			m_mc.txt_error.embedFonts = true;
			m_mc.txt_error.defaultTextFormat = tf
			m_mc.txt_input.embedFonts = true;
			m_mc.txt_input.defaultTextFormat = tf
			m_mc.txt_msg.text = TxtLangTransfer.INPUT_NICKNAME
		
		}
		
		public function fnDisplayPanel():void
		{
			this.addChild(m_mc);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_fb);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_regulation);
		
			m_mc.btn_fb.addEventListener(MouseEvent.CLICK,fnClickFB);
			//	HoimiFont01LoadHandler.fnGetInstance().addEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			//	HoimiFont01LoadHandler.fnGetInstance().fnLoadFont();
			m_mc.txt_error.visible = false;
			m_mc.txt_input.maxChars = m_nMaxCHars;
			this.addEventListener(KeyboardEvent.KEY_DOWN,onKey);
			m_mc.regulation.addEventListener(MouseEvent.CLICK,fnRegulationClick);
			m_mc.btn_regulation.addEventListener(MouseEvent.CLICK,fnQuestionClick);
			m_mc.txt_input.addEventListener(Event.CHANGE,fnMessageChangingEvent)
			m_mc.txt_msg.text = TxtLangTransfer.INPUT_NICKNAME
			this.visible = true;
			var strUUID:String = GameConfig.UUID
			m_mc.txt_input.text = "Guest"+strUUID.slice(12,16); 
			
			var mcPlay:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mc.btn_ok);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(mcPlay);
			this.addChild(mcPlay);
			mcPlay.x = m_mc.btn_ok.x
			mcPlay.y = m_mc.btn_ok.y
			mcPlay.addEventListener(MouseEvent.CLICK,fnClickOk);
		}
		
		private function fnMessageChangingEvent(e:Event):void
		{
			if(m_nKeyTotal > m_nMaxCHars)
			{
				e.preventDefault();
			}
		}
		
		private function fnClickFB(e:MouseEvent):void
		{
			if(m_mc.regulation.check.visible ==false)
			{
				m_mc.txt_error.visible=true;
				m_mc.txt_error.text = TxtLangTransfer.REGULATION_UNCHECK
				return;
			}
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			EventManager.instance.register(FacebookController.EVENT_FACEBOOK_COMPLETE, onFBdata);
			FacebookController.instance.open();
		}
		
		private function onFBdata(e:Event):void
		{
			EventManager.instance.remove(FacebookController.EVENT_FACEBOOK_COMPLETE, onFBdata);
			m_strNickname = FacebookController.instance.self.name;
			fb_signup();
		}
		
		private function fb_signup(e:GameEvent=null):void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,signup);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			var obj:Object = new Object();
			obj.uuid = GameConfig.UUID;
			obj.device = GameConfig.DEVICE;
			obj.fb_id = FacebookController.instance.self.id;
			obj.fb_name = FacebookController.instance.self.name;
			obj.gender = FacebookController.instance.info.gender;
			obj.age_range_min = FacebookController.instance.info.age_range.min;
			obj.email = FacebookController.instance.info.email;
			obj.friends = FacebookController.instance.friendIDs;
			SessionManager.instance.request(Protocol.FACEBOOK_SIGNUP, obj, onFBSignup, onNet);
		}
		
		private function onFBSignup(e:NetEvent):void
		{
			trace('onFBSignup '+e.data);
			var ev:GameEvent;
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,signup);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			var packet:FBSignupInfo = new FBSignupInfo(e.data);
			if(packet.result == 1){
				GameConfig.UUID = packet.uuid;
				LocalSaver.instance.save(EnumLocalSave.FIRST_SIGN_IN,EnumLocalSave.FIRST_SIGN_IN);
				ev = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_SIGNIN_EVENT);
				dispatchEvent(ev);
			}else if(packet.result == 2){
				ev = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_SIGNIN_EVENT);
				LocalSaver.instance.save(EnumLocalSave.FIRST_SIGN_IN,EnumLocalSave.FIRST_SIGN_IN);
				
				dispatchEvent(ev);
			}else{
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "on FB Signup "+e.data;
				EventManager.instance.trigger(evError);
			}
		}
		
		private function onKey(e:KeyboardEvent):void
		{
		
			trace(e.keyCode)
			var nChars:int = m_mc.txt_input.length;
			trace("nChars :" + nChars);
			if(e.keyCode==8)
			{
				m_nKeyTotal = nChars - 1
				//m_nKeyTotal++
				if(m_nKeyTotal < 0)
				{
					m_nKeyTotal = 0
				}
			}
			else
			{
				m_nKeyTotal ++ 
				if(m_nKeyTotal >= m_nMaxCHars)
				{
					m_nKeyTotal = m_nMaxCHars
				}
			}
			
			trace("m_nKeyTotal :" +m_nKeyTotal)
			trace("m_nMaxCHars :" +m_nMaxCHars)
		}
		
		private function fnClickOk(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			m_mc.txt_error.visible = false;
			if(m_mc.txt_input.text == "")
			{
				return;
			}
			
			if( m_mc.txt_input.length > m_nMaxCHars)
			{
				m_mc.txt_error.visible=true;
				m_mc.txt_error.text = TxtLangTransfer.NICKNAME_EXCEED_LIMIT
			
				return;
			}
			var nick:String = m_mc.txt_input.text
			var len:uint = nick.length;
			var cnt:uint = 0;
			var c:String;
			while(cnt < len)
			{
				c = nick.charAt(cnt);
				if(!RegExpHelper.matchChinese(c) && !RegExpHelper.matchEnglish(c) && !RegExpHelper.matchNumber(c))
				{
					m_mc.txt_error.visible=true;
					m_mc.txt_error.text = TxtLangTransfer.NICKNAME_ILLEGAL;
					
					return;
				}
				++cnt;
			}
			if(m_mc.regulation.check.visible ==false)
			{
				m_mc.txt_error.visible=true;
				m_mc.txt_error.text = TxtLangTransfer.REGULATION_UNCHECK
				return;
			}
			m_strNickname = m_mc.txt_input.text
			signup();
		
		}
		
		private function signup(e:GameEvent=null):void
		{
			m_mc.btn_ok.removeEventListener(MouseEvent.CLICK,fnClickOk);
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,signup);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			var obj:Object = new Object();
			obj.uuid = GameConfig.UUID; 
			obj.name = m_strNickname;
			obj.device = GameConfig.DEVICE;
			SessionManager.instance.request(Protocol.USER_SIGNUP, obj, onSignup, onNet);
			
		}
		
		/**   signup
		 *  1:success {result:1}
		 * -2:already exist
		 * -3:name duplicate
		 **/
		
		private function onSignup(e:NetEvent):void
		{
			trace('onSignUp '+e.data);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,signup);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			var packet:SignupInfo = new SignupInfo(e.data);
			if(packet.result == 1)
			{
				trace("user sign up success");
				LocalSaver.instance.save(EnumLocalSave.FIRST_SIGN_IN,EnumLocalSave.FIRST_SIGN_IN);
				var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_SIGNIN_EVENT);
				dispatchEvent(ev);
				
			}
			else if(packet.result == -2)
			{
				m_mc.btn_ok.addEventListener(MouseEvent.CLICK,fnClickOk);
			}
			else if(packet.result == -3)
			{
				m_mc.btn_ok.addEventListener(MouseEvent.CLICK,fnClickOk);
				m_mc.txt_error.visible = true;
				m_mc.txt_error.text = TxtLangTransfer.NICKNAME_ALREADY_USE
				
			}
		}
		
		private function fnRegulationClick(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			if(m_mc.regulation.check.visible == false)
			{
				m_mc.regulation.check.visible = true
			}
			else
			{
				m_mc.regulation.check.visible =false;
			}
		}
		
		private function fnQuestionClick(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			if(m_coRegulationPanel == null)
			{
				m_coRegulationPanel = new RegulationPanel();
				m_coRegulationPanel.y = -50
				this.addChild(m_coRegulationPanel);
			}
			m_coRegulationPanel.fnDisplay();
		}
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
	}
}