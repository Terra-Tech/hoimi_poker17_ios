package module
{
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	
	import event.PokerEvent;
	
	
	import item.ActionMsg;
	import item.BetCoin;
	import item.CardResult;
	
	import lib.RandomMachine;
	
	import ui.EnemyPorkerAll;
	
	public class EnemyModule extends Sprite
	{
		private var _m_nCoin:Number = 0;
		private var _m_strAction:String = "";
		private var m_coPorkerEnemy:EnemyPorkerAll = new EnemyPorkerAll();
		private var m_coBetCoin:BetCoin = new BetCoin(false);
		private var m_coActionMsg:ActionMsg = new ActionMsg();
		private var m_coMsgTurn:Poker_Msg_Enemy_Turn = new Poker_Msg_Enemy_Turn();
		private var m_EnemyTurnTimer:Timer
		public var m_bBuyingConfirm:Boolean=false;
		private var m_mcEnemyName:Poker_Enemy_Name = new Poker_Enemy_Name();
		private var m_nMinMutiple:int = 0;
		public var m_nActionMaxMutiple:int = 0
		private var m_coCardResult:CardResult = new CardResult();	
		private var m_nMaxMultiple:int = 0;
		private var m_nEnemyCanBetMaxMultiple:int = 0
		private var m_nPlayerMultiple:int = 0	
	
		
		public function EnemyModule()
		{
			super();
			this.addChild(m_mcEnemyName);
			this.addChild(m_coPorkerEnemy);
			this.addChild(m_coBetCoin);
			this.addChild(m_coMsgTurn);
			this.addChild(m_coActionMsg);
			this.addChild(m_coCardResult);
			m_coBetCoin.x=360
			m_coBetCoin.y = 395;
			m_coPorkerEnemy.y = 212.35
			m_coPorkerEnemy.x = 350
			m_coActionMsg.x = 360
			m_coActionMsg.y = 444
			m_coMsgTurn.x = 100
			m_coMsgTurn.y = 360
			m_coCardResult.y = 360
			m_coBetCoin.visible =false;
			m_coMsgTurn.visible=false;
			m_coActionMsg.visible=false
			m_coCardResult.visible=false;
		}
		
		public function fnSetName(strName:String,nLevel:int):void
		{
			m_mcEnemyName.txt_name.text = strName;
			m_mcEnemyName.txt_level.text ="LV- "+ nLevel
			m_mcEnemyName.txt_game_id.text = "ID: "+GameConfig.game_openBetInfo.game_id;
		}
		
		public function get m_strAction():String
		{
			return _m_strAction;
		}
		
		public function set m_strAction(value:String):void
		{
			_m_strAction = value;
		}
		
		public function get m_nCoin():Number
		{
			return _m_nCoin;
		}
		
		public function set m_nCoin(value:Number):void
		{
			if(value > 0 )
			{
				_m_nCoin += value;
				m_coBetCoin.m_nBetCoins = _m_nCoin
			}
			
		}
		
		public function fnGameStart():void
		{
			_m_nCoin = 0
			m_coPorkerEnemy.fnNewGame();
			m_coBetCoin.m_nBetCoins = 0
			m_coBetCoin.visible =true
			m_coMsgTurn.visible=false;
		}
		
		public function fnDeal(arCard:Array):void
		{
			m_coPorkerEnemy.fnDeal(arCard,false)
		}
		
		public function fnPickCardFinish():void
		{
			m_coPorkerEnemy.fnShowPickCard()
		}
		
		public function fnEnemyDecision(nEnemyMultiple:int,nPlayerMultiple:int,nMaxRatio,nDecisionTime:Number):void
		{
			m_coMsgTurn.visible =true
			m_coActionMsg.visible=false
			//m_nEnemyCanBetMaxValue = GameConfig.game_table.bet + (GameConfig.game_table.bet * nMaxMultiple)
			m_nEnemyCanBetMaxMultiple = nMaxRatio
		//	m_nMaxMultiple = nMaxMultiple - m_nActionMaxMutiple
			//m_nMinMutiple = nPlayerMultiple
			m_nMinMutiple = nPlayerMultiple -nEnemyMultiple
			m_nMaxMultiple = nMaxRatio- nEnemyMultiple
			m_nPlayerMultiple = nPlayerMultiple
			var ar:Array = new Array();
		//	var ev:PorkerEvent = new PorkerEvent(PorkerEvent.PORKER_EVENT,PorkerEvent.PORKER_ENEMY_CALL_EVENT); 
			//dispatchEvent(ev);
		//	return;
			for(var i:int =1 ;i<nDecisionTime;i++)
			{
				ar.push(i*1000);
			}
			var nTime:int = RandomMachine.fnGetRanOneValue(ar);
			m_EnemyTurnTimer = new Timer(nTime,1)
			m_EnemyTurnTimer.addEventListener(TimerEvent.TIMER_COMPLETE,fnEnemyTimerComplete);
			if(!GameConfig.appPause)
			{
				m_EnemyTurnTimer.start();	
			}
			
		}
		
		private function fnEnemyTimerComplete(e:TimerEvent):void
		{
			m_EnemyTurnTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnEnemyTimerComplete);
			//var strAction:String = m_EnemyActionHandler.fnBetChoice();
			//var nMaxMultiple:int = GameConfig.game_pickInfo.enemy_max_multiple;
			var ev:PokerEvent
			
			var nRaise:int 
			
			if(m_nMaxMultiple <= 0)
			{trace("enemy fold m_nMaxMultiple:"+m_nMaxMultiple);
				ev = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_ENEMY_FOLD_EVENT); 
				dispatchEvent(ev);
				return;
			}
			trace("m_nActionMaxMutiple:"+m_nActionMaxMutiple);
			trace("m_nEnemyCanBetMaxMultiple:"+m_nEnemyCanBetMaxMultiple);
			trace("m_nPlayerMutiple:"+m_nPlayerMultiple);
			trace("m_nMaxMultiple:"+m_nMaxMultiple);
			if(m_nEnemyCanBetMaxMultiple < m_nPlayerMultiple )
			{
				ev = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_ENEMY_FOLD_EVENT); 
				dispatchEvent(ev);
			}
			else if(m_nMinMutiple > m_nMaxMultiple)
			{
				m_nCoin =  GameConfig.game_table.bet * m_nMaxMultiple
				m_nActionMaxMutiple += m_nMaxMultiple
				ev = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_ENEMY_CALL_EVENT); 
				ev.m_eventArgs = m_nMaxMultiple 
				dispatchEvent(ev);
			}
			else
			{
			//	var nPlayerMultiple:int = ((m_nPlayerMutiple-GameConfig.game_table.bet)/GameConfig.game_table.bet ) - m_nActionMaxMutiple
				var arRan:Array = new Array();
				for(var j:int = m_nMinMutiple ; j <= m_nMaxMultiple;j++)
				{
					arRan.push(j);
				}
				nRaise = RandomMachine.fnGetRanOneValue(arRan);
				if(nRaise > 0)
				{
					m_nCoin =  GameConfig.game_table.bet * nRaise
					m_nActionMaxMutiple += nRaise
				}
				
				trace("fnEnemyTimerComplete m_nActionMaxMutiple: " + m_nActionMaxMutiple);
				if(m_nActionMaxMutiple == m_nPlayerMultiple)
				{
					ev = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_ENEMY_CALL_EVENT); 
					ev.m_eventArgs = nRaise 
				}
				else
				{
					ev = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_ENEMY_RAISE_EVENT); 
					ev.m_eventArgs = nRaise
					
				}
				dispatchEvent(ev);
			}
		}
		
		
		public function fnAction(str:String):void
		{
			_m_strAction = str
			m_coActionMsg.fnAction(str);
			m_coMsgTurn.visible=false
			m_coActionMsg.visible=true
		}
		
		public function fnHideDecision():void
		{
			m_coActionMsg.visible=false
		}
		
		public function fnBuying():void
		{
			m_coMsgTurn.visible=true;
			var ar:Array = new Array();
			for(var i:int =1 ;i<GameConfig.game_table.time_buying;i++)
			{
				ar.push(i*1000);
			}
			var nTime:int = RandomMachine.fnGetRanOneValue(ar);
			m_EnemyTurnTimer = new Timer(nTime,1)
			m_EnemyTurnTimer.addEventListener(TimerEvent.TIMER_COMPLETE,fnBuyingTimeComplete);
			if(!GameConfig.appPause)
			{
				m_EnemyTurnTimer.start();	
			}
		}
		
		private function fnBuyingTimeComplete(e:TimerEvent):void
		{
			m_EnemyTurnTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnBuyingTimeComplete);
			m_coPorkerEnemy.fnBuyingConfirm();
			m_coMsgTurn.visible=false;
			m_bBuyingConfirm = true;
		
		}
		
		public function fnOpenCard():void
		{
			m_coPorkerEnemy.fnOpenCard();
		}
		
		
		public function fnShowCardResult():void
		{
			m_coCardResult.visible =true;
			m_coCardResult.fnShowResult(GameConfig.game_2ndBetInfo.enemy_reward);
		//	setTimeout(function():void 
		//	{
				//var ev:PorkerEvent = new PorkerEvent(PorkerEvent.PORKER_EVENT,PorkerEvent.PORKER_ENEMY_OPEN_CARD_FINISH_EVENT);
				//dispatchEvent(ev)
		//	},1000)
		}
		
		public function fnResume():void
		{
			if(m_EnemyTurnTimer && !m_EnemyTurnTimer.running && m_coMsgTurn.visible)
			{
				m_EnemyTurnTimer.start();
			}
		}
		
		public function fnPause():void
		{
			if(m_EnemyTurnTimer && m_EnemyTurnTimer.running)
			{
				m_EnemyTurnTimer.stop();
			}
		}
		
		public function fnDestory():void
		{
			if(m_EnemyTurnTimer)
			{
				m_EnemyTurnTimer.stop();
				m_EnemyTurnTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnEnemyTimerComplete);
				m_EnemyTurnTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnBuyingTimeComplete);
				m_EnemyTurnTimer = null;
			}
			
			
		}
		
	}
}