package ui
{

	
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.util.DeviceUtil;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.HttpHelper;
	import com.hoimi.util.Purchase;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	
	import Enum.EnumIAP;
	
	import base.TiDisableTextField;
	
	import event.GameEvent;
	
	import handler.HoimiFontNumberLoadHandler;
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	import item.HoimiDiamond;
	
	import model.IAPInfo;
	
	public class IAPPanel extends Sprite
	{
		private var m_mc:Poker_Iap_Panel = new Poker_Iap_Panel();
		private var m_mcMask:Poker_Mask_Base = new Poker_Mask_Base();
		private var m_tf:TextFormat = new TextFormat();
		private var m_intIAPSuccessDiamond:int = 0;
		private var m_bNoAD:Boolean = false;
		private var txtField:TextField = new TextField();
		private var m_strIAP:String = "";
		private var m_ObSendIapVarify:Object
		private var m_spDiamond:Sprite = new Sprite();
		
		public function IAPPanel()
		{
			super();
			this.addChild(m_mcMask);
			this.addChild(m_mc);
			this.addChild(m_spDiamond);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_change)
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_close);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.iap_01);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.iap_02);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.iap_03);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.iap_04);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.iap_05);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.iap_06);
			this.visible = false;
			m_mcMask.x = 360
			m_mcMask.alpha = 0.5
			m_mc.y = 160;
			m_mc.x = 360
			HoimiFontNumberLoadHandler.fnGetInstance().addEventListener(GameEvent.GAME_EVENT,MpEventHandler);
			HoimiFontNumberLoadHandler.fnGetInstance().fnLoadFont()
			m_mc.btn_close.addEventListener(MouseEvent.CLICK,fnClose);
			EventManager.instance.register(GameEvent.GAME_IAP_EVENT,fnDisplayIapPanel);
			m_mc.btn_change.addEventListener(MouseEvent.CLICK,fnChangeCoinPanel);
//			fnFixConsume();
			
		}
		
		private function fnChangeCoinPanel(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			this.visible =false;
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_GAIN_COIN_EVENT));
		}
	
		private function fnDisplayIapPanel(e:GameEvent):void
		{
			if(GameConfig.game_iapKey.publicKey)
			{
				if(Purchase.instance.bInit){
					fnAddEventListener();
					fnSetIAPContent();
				}else{
					Purchase.instance.addEventListener(Purchase.PURCHASE_INIT_SUCCESS, onInitSuccess);
					Purchase.instance.init(GameConfig.game_iapKey.publicKey);
//					fnFixConsume();
				}
			}
			this.visible = true;
			
		}
		
		protected function onInitSuccess(event:Event):void
		{
			Purchase.instance.removeEventListener(Purchase.PURCHASE_INIT_SUCCESS, onInitSuccess);
			fnAddEventListener();
			fnSetIAPContent();
		}		
		
		private function MpEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_NUMBER_FONT_LOAD_COMPLETE_EVENT)
			{
				fnSetIAPMoney();
			}
			
		}
		
		private function fnSetIAPMoney():void
		{
			var ar:Array = [1.99,4.99,9.99,19.99,49.99,99.99]
		
			//m_tf.font = HoimiFontNumberLoadHandler.fnGetInstance().embeddedFont.fontName;
			m_tf.font = HoimiFontNumberLoadHandler.fnGetInstance().embeddedFontName
				//m_tf.size = 50;
			m_tf.color = "0xCC0000";
			
			for(var i:int = 0 ; i<  6 ; i++)
			{
				var	mc:MovieClip =  m_mc["iap_0"+(i+1)]
				mc.diamond.gotoAndStop(i+1);
				mc.txt_diamond_bonus.text = TxtLangTransfer.IAP_DIAMOND_BONUS;
				mc.txt_iap.embedFonts = true; // very important to set
				mc.txt_iap.defaultTextFormat = m_tf;
				mc.txt_iap.text = "$" + ar[i]  
				TextField(mc.txt_iap).autoSize = TextFieldAutoSize.RIGHT;
			}
		}
		
		private function fnSetIAPContent():void
		{
			m_spDiamond.removeChildren();
			for(var i:int = 0 ; i<  6 ; i++)
			{
				var mcDiamond:HoimiDiamond = new HoimiDiamond();
				mcDiamond.x = 230;
				mcDiamond.y = (i * 143) +329
				m_spDiamond.addChild(mcDiamond);
				mcDiamond.mouseChildren=false;
				mcDiamond.mouseEnabled=false;
				var mcDiamondBonus:HoimiDiamond = new HoimiDiamond();
				mcDiamondBonus.x = 530;
				mcDiamondBonus.y = (i * 142.5) +387
				m_spDiamond.addChild(mcDiamondBonus);
				if(i == 0)
				{
					mcDiamond.fnSetValue2(GameConfig.game_iapProductInfo.product.twd_60);
					mcDiamondBonus.fnSetValue2(GameConfig.game_iapProductInfo.bonus.twd_60);
					if(GameConfig.game_iapProductInfo.bonus.twd_60 == 0)
					{
						mcDiamondBonus.visible =false;
						m_mc.iap_01.txt_diamond_bonus.visible =false;
						m_mc.iap_01.first_time.visible =false;
						m_mc.iap_01.promote.visible=false;
					}
				}
				else if(i == 1)
				{
					mcDiamond.fnSetValue2(GameConfig.game_iapProductInfo.product.twd_150);
					mcDiamondBonus.fnSetValue2(GameConfig.game_iapProductInfo.bonus.twd_150);
					if(GameConfig.game_iapProductInfo.bonus.twd_150 == 0)
					{
						mcDiamondBonus.visible =false;
						m_mc.iap_02.txt_diamond_bonus.visible =false;
						m_mc.iap_02.first_time.visible =false;
						m_mc.iap_02.promote.visible=false;
					}
				}
				else if(i == 2)
				{
					mcDiamond.fnSetValue2(GameConfig.game_iapProductInfo.product.twd_300);
					mcDiamondBonus.fnSetValue2(GameConfig.game_iapProductInfo.bonus.twd_300);
					if(GameConfig.game_iapProductInfo.bonus.twd_300 == 0)
					{
						mcDiamondBonus.visible =false;
						m_mc.iap_03.txt_diamond_bonus.visible =false;
						m_mc.iap_03.first_time.visible =false;
						m_mc.iap_03.promote.visible=false;
					}
				}
				else if(i == 3)
				{
					mcDiamond.fnSetValue2(GameConfig.game_iapProductInfo.product.twd_600);
					mcDiamondBonus.fnSetValue2(GameConfig.game_iapProductInfo.bonus.twd_600);
					if(GameConfig.game_iapProductInfo.bonus.twd_600 == 0)
					{
						mcDiamondBonus.visible =false;
						m_mc.iap_04.txt_diamond_bonus.visible =false;
						m_mc.iap_04.first_time.visible =false;
						m_mc.iap_04.promote.visible=false;
					}
				}
				else if(i == 4)
				{
					mcDiamond.fnSetValue2(GameConfig.game_iapProductInfo.product.twd_1500);
					mcDiamondBonus.fnSetValue2(GameConfig.game_iapProductInfo.bonus.twd_1500);
					if(GameConfig.game_iapProductInfo.bonus.twd_1500 == 0)
					{
						mcDiamondBonus.visible =false;
						m_mc.iap_05.txt_diamond_bonus.visible =false;
						m_mc.iap_05.first_time.visible =false;
					}
				}
				else if(i == 5)
				{
					mcDiamond.fnSetValue2(GameConfig.game_iapProductInfo.product.twd_3000);
					mcDiamondBonus.fnSetValue2(GameConfig.game_iapProductInfo.bonus.twd_3000);
					if(GameConfig.game_iapProductInfo.bonus.twd_3000 == 0)
					{
						mcDiamondBonus.visible =false;
						m_mc.iap_06.txt_diamond_bonus.visible =false;
						m_mc.iap_06.first_time.visible =false;
					}
				}
			}
			
		}
		
		private function fnFixConsume():void
		{
			Purchase.instance.fixConsume(EnumIAP.IAP_01_ID,fnfix,fnfix);
			Purchase.instance.fixConsume(EnumIAP.IAP_02_ID,fnfix,fnfix);
			Purchase.instance.fixConsume(EnumIAP.IAP_03_ID,fnfix,fnfix);
			Purchase.instance.fixConsume(EnumIAP.IAP_04_ID,fnfix,fnfix);
			Purchase.instance.fixConsume(EnumIAP.IAP_05_ID,fnfix,fnfix);
			Purchase.instance.fixConsume(EnumIAP.IAP_06_ID,fnfix,fnfix);
		}
		
		private function fnfix(str:String):void
		{
		}
		
		private function fnAddEventListener():void
		{
			m_mc.iap_01.addEventListener(MouseEvent.CLICK,fnIap01Click);
			m_mc.iap_02.addEventListener(MouseEvent.CLICK,fnIap02Click);
			m_mc.iap_03.addEventListener(MouseEvent.CLICK,fnIap03Click);
			m_mc.iap_04.addEventListener(MouseEvent.CLICK,fnIap04Click);
			m_mc.iap_05.addEventListener(MouseEvent.CLICK,fnIap05Click);
			m_mc.iap_06.addEventListener(MouseEvent.CLICK,fnIap06Click);
		}
		
		
		private function fnRemoveEventListener():void
		{
			m_mc.iap_01.removeEventListener(MouseEvent.CLICK,fnIap01Click);
			m_mc.iap_02.removeEventListener(MouseEvent.CLICK,fnIap02Click);
			m_mc.iap_03.removeEventListener(MouseEvent.CLICK,fnIap03Click);
			m_mc.iap_04.removeEventListener(MouseEvent.CLICK,fnIap04Click);
			m_mc.iap_05.removeEventListener(MouseEvent.CLICK,fnIap05Click);
			m_mc.iap_06.removeEventListener(MouseEvent.CLICK,fnIap06Click);
		}
		
		private function fnClose(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound003();
			fnRemoveEventListener();
			this.visible = false;
		}
		
		
		private function fnIap01Click(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_intIAPSuccessDiamond = GameConfig.game_iapProductInfo.product.twd_60 + GameConfig.game_iapProductInfo.bonus.twd_60
			m_bNoAD = true;
			m_strIAP = EnumIAP.IAP_01_ID
			Purchase.instance.inAppPurchase(EnumIAP.IAP_01_ID,fnIAPSuccess,fnIAPFail);
			
		}
		
		private function fnIap02Click(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_intIAPSuccessDiamond = GameConfig.game_iapProductInfo.product.twd_150 + GameConfig.game_iapProductInfo.bonus.twd_150
			m_bNoAD =true;
			m_strIAP = EnumIAP.IAP_02_ID
			Purchase.instance.inAppPurchase(EnumIAP.IAP_02_ID,fnIAPSuccess,fnIAPFail);
		}
		
		private function fnIap03Click(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_intIAPSuccessDiamond = GameConfig.game_iapProductInfo.product.twd_300 + GameConfig.game_iapProductInfo.bonus.twd_300
			m_bNoAD =true;
			m_strIAP = EnumIAP.IAP_03_ID
			Purchase.instance.inAppPurchase(EnumIAP.IAP_03_ID,fnIAPSuccess,fnIAPFail);
		}
		
		private function fnIap04Click(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_intIAPSuccessDiamond = GameConfig.game_iapProductInfo.product.twd_600 + GameConfig.game_iapProductInfo.bonus.twd_600
			m_bNoAD =true;
			m_strIAP = EnumIAP.IAP_04_ID
			Purchase.instance.inAppPurchase(EnumIAP.IAP_04_ID,fnIAPSuccess,fnIAPFail);
		}
		
		private function fnIap05Click(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_intIAPSuccessDiamond = GameConfig.game_iapProductInfo.product.twd_1500 + GameConfig.game_iapProductInfo.bonus.twd_1500
			m_bNoAD =true;
			m_strIAP = EnumIAP.IAP_05_ID
			Purchase.instance.inAppPurchase(EnumIAP.IAP_05_ID,fnIAPSuccess,fnIAPFail);
		}
		
		private function fnIap06Click(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_intIAPSuccessDiamond = GameConfig.game_iapProductInfo.product.twd_3000 + GameConfig.game_iapProductInfo.bonus.twd_3000
			m_bNoAD =true;
			m_strIAP = EnumIAP.IAP_06_ID
			Purchase.instance.inAppPurchase(EnumIAP.IAP_06_ID,fnIAPSuccess,fnIAPFail);
		}
		
		public function fnDestory():void
		{
			EventManager.instance.remove(GameEvent.GAME_IAP_EVENT,fnDisplayIapPanel);
		}
	
		
	
		
		private function fnIAPSuccess(msg:Object):void
		{
				/*var send:Object = {
				var send:Object = {
				token:GameConfig.game_playerInfo.token,
				receipt_type:"GooglePlay",
				receipt:{"signedData":"{\"orderId\":\"GPA.1369-6730-4879-51223\",\"packageName\":\"air.com.hoimi.porker17\",\"productId\":\""+m_strIAP+"\",\"purchaseTime\":1441684922346,\"purchaseState\":0,\"purchaseToken\":\"mccabhjcoegionogekjeebia.AO-J1OzIpGkGiZfmvszNFWFeLT6_ZSWRv9RDYpzw561HJrFYOGTBxkJHmdYEX1VGNzAnVZhAxCvUtRqi12QYG5YG3fC1_GNOOSgCHZn4vAlZrn8IdDpiUiVjMQT5dKxCIU0q0Cxzl8MA\"}",
						"signature":GameConfig.game_iapKey.publicKey}
							
					}*/
		//	printMsg('IAP success [' + JSON.stringify(msg)+']');
			
			m_ObSendIapVarify = {
				token:GameConfig.game_playerInfo.token,
					receipt_type:msg.receiptType,
					receipt:msg.receipt,
					product_id:msg.productId
			}
				
		
			fnNetVarifyIAP();

		}
		
		private function fnNetVarifyIAP(e:GameEvent = null):void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnNetVarifyIAP);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			SessionManager.instance.request(Protocol.IAP, m_ObSendIapVarify, fnIAPVarifyComplete,onNet);
		}
		
		private function fnIAPVarifyComplete(e:NetEvent):void
		{
			trace('onIAP ' + e.data);
			//var i:IAPInfo = new IAPInfo(e.data);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnNetVarifyIAP);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			var data:IAPInfo = new IAPInfo(e.data);
			if(data.result ==1)
			{
				GameConfig.game_playerInfo.diamond = data.player_diamond;

				switch(m_strIAP)
				{
					case EnumIAP.IAP_01_ID:
						GameConfig.game_iapProductInfo.bonus.twd_60 = 0;
						break;
					case EnumIAP.IAP_02_ID:
						GameConfig.game_iapProductInfo.bonus.twd_150 = 0;
						break;
					case EnumIAP.IAP_03_ID:
						GameConfig.game_iapProductInfo.bonus.twd_300 = 0;
						break;
					case EnumIAP.IAP_04_ID:
						GameConfig.game_iapProductInfo.bonus.twd_600 = 0;
						break;
					case EnumIAP.IAP_05_ID:
						GameConfig.game_iapProductInfo.bonus.twd_1500 = 0;
						break;
					case EnumIAP.IAP_06_ID:
						GameConfig.game_iapProductInfo.bonus.twd_3000 = 0;
						break;
				}
				fnSetIAPContent();
				fnSetIAPMoney();
				
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_SECTION_REFRESH));
			}
			else
			{
				printMsg('IAP verified : ' + e.data);
			}
		}
		
		private function printMsg(msg:String):void
		{
			var ev:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT);
			ev.m_eventArgs = "onIAP\n"+msg
			EventManager.instance.trigger(ev);
		}

		
		private function fnIAPVarifyFail():void
		{
			trace("IAP varify fail")
		}
		
		private function fnIAPFail(msg:String):void
		{ 
			/*var tf:TextFormat = new TextFormat();
			tf.size = 24
			tf.color = "0xFF0000"
			
			txtField.defaultTextFormat = tf
			txtField.width = 720;
			txtField.height = 1000
			txtField.text = "Fail Msg: " + msg 
			txtField.x = -360
			txtField.y = 50
			txtField.multiline = true;
			txtField.wordWrap = true;
			txtField.mouseEnabled =false;
			
			m_intIAPSuccessCoin = 0;
			fnAddEventListener();
			this.addChild(txtField);
			
			*/
		}
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
	}
}