package Skill
{
	import Skill.Model.SkillRaise;

	public interface ISkillView
	{
		// 網路發生問題
		function netError(msg:String):void;
		/// Server 吐錯誤訊息
		function serverError(msg:String):void;
		/// 如打開領獎Panel show loading
		function showLoading():void;
		/// 如打開領獎Panel hide loading
		function hideLoading():void;
		/// 要升級的技能不存在
		function showIndexError():void
		/// coin 或 diamond 不夠
		function showCoinNotEnought():void
		function showDiamondNotEnought():void
		/// 已到最高級
		function showArrivedTopLevel():void
		/// 升級成功
		function showRaiseSuccess(skill:SkillRaise):void
	}
}