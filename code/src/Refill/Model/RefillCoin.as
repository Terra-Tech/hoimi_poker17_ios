package Refill.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class RefillCoin extends JsonObject
	{
		public var refill_coin:int = 0;
		public var player_coin:Number = 0;
		
		public function RefillCoin(obj:*=null)
		{
			super(obj);
		}
	}
}