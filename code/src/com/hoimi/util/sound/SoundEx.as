﻿package com.hoimi.util.sound
{
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;
	import com.greensock.TweenMax;
	
	public class SoundEx
	{
		private var _sound: Sound;
		private var _channel: SoundChannel;
		private var _lastPos: int;

		public function SoundEx(sound: Sound)
		{
			_sound = sound;
		}

		public function play(startPos: int, loop: int, volume: Number, fadeIn: Boolean): void
		{
			if (fadeIn)
			{
				_channel = _sound.play(startPos, loop, new SoundTransform(0, 0));
				TweenMax.to(_channel, 3,
				{
					volume: volume
				});
			}
			else
			{
				_channel = _sound.play(startPos, loop, new SoundTransform(volume, 0));
			}
		}

		public function stop(keepLast: Boolean, fadeOut: Boolean): void
		{
			if (keepLast)
				_lastPos = -1;
			if (fadeOut)
				TweenMax.to(_channel, 3,
				{
					volume: 0,
					onComplete: stopSound
				});
			else
			{
				stopSound();
			}
		}
		
		public function get lastPostion():int
		{
			return _lastPos;
		}
		
		public function set volume(value:Number):void
		{
			if(value < 0 || value > 1) return;
			if(_channel)
			{
				var sndTF:SoundTransform = new SoundTransform(value,0);
				_channel.soundTransform = sndTF;
			}
		}

		private function stopSound(): void
		{
			_lastPos = (_lastPos == -1 ? _channel.position : 0);
			_channel.stop();
		}
	}

}