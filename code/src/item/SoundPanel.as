package item
{
	import com.hoimi.util.DeviceUtil;
	import com.hoimi.util.LocalSaver;
	import com.myflashlab.air.extensions.volume.Volume;
	import com.myflashlab.air.extensions.volume.VolumeEvent;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import Enum.EnumLocalSave;
	
	import base.TiDisableTextField;
	
	import handler.TTCMusicHandler;
	import handler.TTCSoundHandler;
	
	
	public class SoundPanel extends Sprite
	{
		
		private var m_mcSoundOn:Poker_Btn_Sound_On = new Poker_Btn_Sound_On();
		private var m_mcSoundOff:Poker_Btn_Sound_Off = new Poker_Btn_Sound_Off();
		private var m_mcMusicOn:Poker_Btn_Music_On = new Poker_Btn_Music_On();
		private var m_mcMusicOff:Poker_Btn_Music_Off = new Poker_Btn_Music_Off();
		private var m_silentMusicMute:Boolean = false;
		private var m_silentSoundMute:Boolean = false;
		public function SoundPanel()
		{
			super();
			
			this.addChild(m_mcMusicOn);
			this.addChild(m_mcMusicOff);
			m_mcMusicOff.visible =false;
			this.addChild(m_mcSoundOn);
			this.addChild(m_mcSoundOff);
			m_mcSoundOff.visible=false;
			m_mcMusicOn.x = 0;
			m_mcMusicOff.x = 0;
			m_mcSoundOn.x = 200;
			m_mcSoundOff.x = 200;

			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcSoundOff);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcSoundOn);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcMusicOn);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcMusicOff);
			m_mcMusicOn.addEventListener(MouseEvent.CLICK,fnMusicOnClick);
			m_mcMusicOff.addEventListener(MouseEvent.CLICK,fnMusicOffClick);
			m_mcSoundOn.addEventListener(MouseEvent.CLICK,fnSoundOnClick);
			m_mcSoundOff.addEventListener(MouseEvent.CLICK,fnSoundOffClick);
			fnInit()
		}
		
		private function fnInit():void
		{
			if(LocalSaver.instance.getValue(EnumLocalSave.MUSIC_MUTE) == undefined)
			{
				LocalSaver.instance.save(EnumLocalSave.MUSIC_MUTE,false);
			}
			var bMusicMute:Boolean = LocalSaver.instance.getValue(EnumLocalSave.MUSIC_MUTE);
			TTCMusicHandler.fnGetInstance().m_bMute = bMusicMute;
			if(bMusicMute)
			{
				m_mcMusicOn.visible = false;
				m_mcMusicOff.visible =true;
			}
			m_silentMusicMute = bMusicMute;
			if(LocalSaver.instance.getValue(EnumLocalSave.SOUND_MUTE) == undefined)
			{
				LocalSaver.instance.save(EnumLocalSave.SOUND_MUTE,false);
			}
			var bSoundMute:Boolean = LocalSaver.instance.getValue(EnumLocalSave.SOUND_MUTE);
			TTCSoundHandler.fnGetInstance().m_bMute  = bSoundMute
			if(bSoundMute)
			{
				m_mcSoundOn.visible = false;
				m_mcSoundOff.visible =true;
			}
			if(DeviceUtil.getInstance().isOnIOS)
			{
				m_silentSoundMute = bSoundMute;
				Volume.init();
				Volume.service.addEventListener(VolumeEvent.VOLUME_CHANGE, onDeviceVolumeChanged);
				Volume.service.addEventListener(VolumeEvent.MUTE_STATE, onDeviceMuteChanged);
				Volume.service.addEventListener(VolumeEvent.ERROR, onError);
			}
		}
		
		protected function onError(e:VolumeEvent):void
		{
			trace("onError = " + e.param);
		}
		
		protected function onDeviceMuteChanged(e:VolumeEvent):void
		{
			trace("is device mute? " + e.param);
			if(e.param){
				m_silentSoundMute = TTCSoundHandler.fnGetInstance().m_bMute;
				m_silentMusicMute = TTCMusicHandler.fnGetInstance().m_bMute;
				fnSoundOnClick(null);
				fnMusicOnClick(null);
			}else{
				if(!m_silentMusicMute)
					fnMusicOffClick(null);
				if(!m_silentSoundMute)
					fnSoundOffClick(null);
			}
			
		}
		
		protected function onDeviceVolumeChanged(e:VolumeEvent):void
		{
			trace("volume = " + e.param);
		}
		
		private function fnMusicOnClick(e:MouseEvent):void
		{
			m_mcMusicOn.visible = false;
			m_mcMusicOff.visible =true;
			TTCMusicHandler.fnGetInstance().m_bMute = true;
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			LocalSaver.instance.save(EnumLocalSave.MUSIC_MUTE,true);
		}
		
		private function fnMusicOffClick(e:MouseEvent):void
		{
			m_mcMusicOn.visible = true;
			m_mcMusicOff.visible =false;
			TTCMusicHandler.fnGetInstance().m_bMute = false;
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			LocalSaver.instance.save(EnumLocalSave.MUSIC_MUTE,false);
		}
		
		private function fnSoundOnClick(e:MouseEvent):void
		{
			m_mcSoundOn.visible = false;
			m_mcSoundOff.visible =true;
			TTCSoundHandler.fnGetInstance().m_bMute = true;
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			LocalSaver.instance.save(EnumLocalSave.SOUND_MUTE,true);
		}
		
		private function fnSoundOffClick(e:MouseEvent):void
		{
			m_mcSoundOn.visible = true;
			m_mcSoundOff.visible =false;
			TTCSoundHandler.fnGetInstance().m_bMute = false;
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			LocalSaver.instance.save(EnumLocalSave.SOUND_MUTE,false);
		}
		
	
	}
}