package section
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	import com.hoimi.util.UUID;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.setTimeout;
	
	import Enum.EnumDevice;
	import Enum.EnumLocalSave;
	import Enum.EnumMsg;
	
	import base.TTCSectionBase;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	import event.GameMessageEvent;

	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	import lib.TTCModeCheck;
	import lib.VersionNumber;
	
	import model.AFSButton;
	import model.BuyingInfo;
	import model.FirstBetInfo;
	import model.GameTableList;
	import model.IAPKey;
	import model.IAPProductInfo;
	import model.OpenBetInfo;
	import model.PickInfo;
	import model.PlayerInfo;
	import model.SecondBetInfo;
	import model.VersionInfo;
	
	import ui.NicknamePanel;
	
	
	public class SectionOpening extends TTCSectionBase
	{
		private var m_sp:Sprite = new Sprite();
	
		private var _openBetInfo:OpenBetInfo;
		private var _firstBetInfo:FirstBetInfo;
		private var _pickInfo:PickInfo;
		private var _buyingInfo:BuyingInfo;
		private var _secondBetInfo:SecondBetInfo;
		private var _tableId:int;
		private var btnsList:Vector.<AFSButton>;
		private var btns:Object;
		
		private var statusBar:Sprite;
		
		private var statusCoin:TextField;
		private var statusDiamond:TextField;
		
		private var _iapProduct:Array = [];
		private var m_strNickname:String = "msaterJay";
		private var m_coNickname:NicknamePanel = new NicknamePanel();
		private var m_txtFieldVersion:TextField = new TextField();
		private var m_txtFieldID:TextField = new TextField();
		private var m_bg:Poker_Opening_Bg = new Poker_Opening_Bg();
		private var m_mcSplash:Hoimi_Spalsh = new Hoimi_Spalsh();
		private var m_versionInfo:VersionInfo
		
		public function SectionOpening()
		{
			super();
			this.addChild(m_bg);
			this.addChild(m_txtFieldVersion);
			this.addChild(m_txtFieldID);
			this.addChild(m_coNickname);
			this.addChild(m_sp);
			fnDisplayVersionNumber();
			fnDisplayUUID();
		
			m_coNickname.visible=false;
			m_coNickname.y = 100
			m_bg.txt_msg.visible=false;
			m_bg.txt_msg.mouseChildren=false;
			m_bg.txt_msg.mouseEnabled=false;
			m_bg.txt_msg.txt_msg.text = TxtLangTransfer.PRESS_SCRREN_START_GAME;
			
			var simpleSprite:Sprite = new Sprite();
			simpleSprite.x = 0;
			simpleSprite.y = 0;
			simpleSprite.graphics.lineStyle();
			simpleSprite.graphics.beginFill(0xffffff);
			simpleSprite.graphics.drawRect(0,0,900,1280);
			m_sp.addChild(simpleSprite);
			m_sp.addChild(m_mcSplash);
			this.addEventListener(Event.ADDED_TO_STAGE,fnAddToStage);
		}
		
		private function fnAddToStage(e:Event):void
		{
			if(TTCModeCheck.isDebugBuild() || GameConfig.DEVICE == EnumDevice.IOS)
			{
				fnAfterTweenSplash();
			}
			else
			{
				setTimeout(function():void {fnTweenSplash()}, 3000)
			}
		}
		
		private function fnDisplayVersionNumber():void
		{
			var tf:TextFormat = new TextFormat();
			tf.size = 30;
			tf.color = "0x888888"
			m_txtFieldVersion.defaultTextFormat = tf;
			tf.align = TextFormatAlign.RIGHT;
			m_txtFieldVersion.y = 1240
			m_txtFieldVersion.width = 150;
			m_txtFieldVersion.text ="v"+ VersionNumber.fnGetVersionNumber();
		}
		
		private function fnTweenSplash():void
		{
			Tweener.addTween(m_mcSplash,{alpha:0,time:1.5,transition:"easeOutQuart",onComplete:fnAfterTweenSplash});
		}
		
		private function fnAfterTweenSplash():void
		{
			m_sp.removeChildren();
			SessionManager.instance.init(GameConfig.SERVER_URL, "24875447", GameConfig.SESSION_TIME);
			fnVersion();
			//Login();
		}
		
		private function fnVersion(e:GameEvent = null):void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnVersion);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			SessionManager.instance.request(Protocol.VERSION, {os:GameConfig.DEVICE,language:GameConfig.server_Language}, onVersion, onNet);
		}
		
		private function onVersion(e:NetEvent):void
		{
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnVersion);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			trace('onVersion : ' + e.data);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				m_versionInfo = new VersionInfo(e.data);
				var arServer:Array = m_versionInfo.version.value.split(".");	
				var arLocal:Array = VersionNumber.fnGetVersionNumber().split(".");
				var nServer:int = VersionNumber.fnGetVersionWeights(arServer[0],arServer[1],arServer[2]);
				var nLocaler:int = VersionNumber.fnGetVersionWeights(arLocal[0],arLocal[1],arLocal[2]);
				if(nServer >  nLocaler)
				{
					var ev:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
					ev.m_strMsg = m_versionInfo.version.message;
					ev.m_strType = EnumMsg.MSG_TYPE_OK;
					ev.m_callOkFn = fnNavigateToURL
					EventManager.instance.trigger(ev);	
				}
				else
				{
					Login();
				}
			}
			else
			{
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "onSignin "+ data.result
				EventManager.instance.trigger(evError);
			}
		}
		
		private function fnNavigateToURL():void
		{
			var request:URLRequest = new URLRequest(m_versionInfo.version.url);
			navigateToURL(request, "_blank");
			
		}
		
		private function  Login():void
		{
			m_mcSplash.visible=false;
		
			
			this.addEventListener(Event.ENTER_FRAME, onLoop);
			if(LocalSaver.instance.getValue(EnumLocalSave.FIRST_SIGN_IN) == undefined)
			{
				GameConfig.first_sign_in = true;
				m_coNickname.fnDisplayPanel();
				m_coNickname.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			}
			else
			{
				signin();
			}
			
		}
		
		private function fnGameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_SIGNIN_EVENT)
			{
				m_coNickname.removeEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
				signin();
			}
		}
		
	
		private function onLoop(e:Event):void
		{
			SessionManager.instance.loop();
		}
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
		
		
		
	
		private function fnDisplayUUID():void
		{
			var strUUID:String = "";
			if(LocalSaver.instance.getValue(EnumLocalSave.UUID) == undefined)
			{
				strUUID = UUID.instance.gen();
				GameConfig.UUID = strUUID;
			
				//var obj:Object = {uuid:strUUID,stay_page:"First Sign In"};
				//HttpHelper.post(EnumURL.URL_USER_LOG,obj);
			}
			else
			{
				strUUID = LocalSaver.instance.getValue(EnumLocalSave.UUID);
				GameConfig.UUID = strUUID;
			}
			var tf:TextFormat = new TextFormat();
			tf.size = 30;
			tf.color = "0x888888"
			m_txtFieldID.defaultTextFormat = tf;
			m_txtFieldID.x = 5
			m_txtFieldID.y = 1240
			m_txtFieldID.width = 500;
			m_txtFieldID.text ="ID:"+ strUUID;
		}
		
		private function signin(e:GameEvent=null):void
		{
			m_coNickname.visible=false;
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,signin);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			var strVersion:String = VersionNumber.fnGetVersionNumber();
			SessionManager.instance.request(Protocol.USER_SIGNIN, {uuid:GameConfig.UUID, device:GameConfig.DEVICE, version:strVersion},onSignin,onNet);
			
		}
		
		private function onSignin(e:NetEvent):void
		{
			trace('onSignin ' + e.data);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,signin);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				GameConfig.game_playerInfo = new PlayerInfo(e.data);
				GameConfig.game_playerInfo.boardcast();
				GameConfig.game_playerRecord = GameConfig.game_playerInfo.player_record
			//	var d:Date = new Date();
				//d.time = GameConfig.game_playerInfo.refill_timestamp;
			//	trace(' last login ' + d.toString());
				
				fetchGameTables();
				
			}
			else
			{
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "onSignin "+ data.result
				EventManager.instance.trigger(evError);
			}
			
			
		}
		
		private function fetchGameTables(e:GameEvent=null):void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fetchGameTables);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			trace("fetchGameTables token :" + GameConfig.game_playerInfo.token);
		//	if(GameConfig.game_playerInfo.token)
		//	{
				SessionManager.instance.request(Protocol.GAME_TABLE, {token:GameConfig.game_playerInfo.token},onRecvGameTables,onNet);
		//	}
		}
		
		/** table
		 * 1:{"result" : 1, 'table':tables}
		 * -101:token miss
		 * -103:token expire
		 */
		private function onRecvGameTables(e:NetEvent):void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fetchGameTables);
			trace('onRecvTB ' + e.data);
			
			var data:GameTableList = new GameTableList(e.data);
			if(data.result == 1)
			{
				GameConfig.game_tableList = data.table
				m_bg.txt_msg.visible=true;
				m_bg.addEventListener(MouseEvent.CLICK,fnStartGame);
				iapGetProduct();
			}
			else
			{
				var ev2:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				ev2.m_eventArgs = "onRecvTB " +e.data
				EventManager.instance.trigger(ev2);
			}
			
		
		}
		
		
		
		/// ================ IAP =============================
		
		private function iapGetProduct(e:GameEvent = null):void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,iapGetProduct);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			SessionManager.instance.request(Protocol.IAP_PRODUCT, {token:GameConfig.game_playerInfo.token},onIapGetProduct,onNet);
		}
		
		private function onIapGetProduct(e:NetEvent):void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,iapGetProduct);
			trace('onIapGetProduct ' + e.data);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				GameConfig.game_iapProductInfo = new IAPProductInfo(e.data);
//				iapGetKey();
				GameConfig.game_iapKey = new IAPKey("");
				GameConfig.game_iapKey.publicKey = "ios";
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_OPENING_LOAD_COMPLETE));
				fnTweenEffect();
			}
		}
		
		//skip got key
		/*private function iapGetKey(e:GameEvent=null):void
		{
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,iapGetKey);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			SessionManager.instance.request(Protocol.IAP_KEY, {token:GameConfig.game_playerInfo.token},onIapGetKey,onNet);
		}
		
		private function onIapGetKey(e:NetEvent):void
		{
			trace('onIapGetKey ' + e.data);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,iapGetKey);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				GameConfig.game_iapKey = new IAPKey(e.data);
//				Purchase.instance.init(GameConfig.game_iapKey.publicKey);
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_OPENING_LOAD_COMPLETE));
				fnTweenEffect();
			}

		}*/
		
		
		private function fnStartGame(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound206()
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT, GameEvent.GAME_NEXT_SECTION_EVENT)
			dispatchEvent(ev);
		}
		
		private function fnTweenEffect():void
		{
			Tweener.addTween(m_bg.txt_msg,{time:1.5,alpha:0.1,transition:"linear",onComplete:fnAfterTweenEffect});
		}
		
		private function fnAfterTweenEffect():void
		{
			Tweener.addTween(m_bg.txt_msg,{time:1.5,alpha:1,transition:"linear",onComplete:fnTweenEffect});
		}
		
		
		override public function fnResize():void
		{
			m_bg.x = 360 +  GameConfig.object_adjuct_position_x
			m_coNickname.x = 360+ GameConfig.object_adjuct_position_x
			m_txtFieldVersion.x = 600 + GameConfig.object_adjuct_position_x*2
			m_mcSplash.x = 360 + GameConfig.object_adjuct_position_x
		}
		
		
		override public function fnExit():void
		{
			super.fnExit()
			this.removeChildren();
		}
		
		
	}
}