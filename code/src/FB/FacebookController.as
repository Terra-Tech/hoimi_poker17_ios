package FB
{
	import com.freshplanet.ane.AirFacebook.Facebook;
//	import com.freshplanet.ane.AirNativeShare.AirNativeShare;
//	import com.freshplanet.ane.AirNativeShare.AirNativeShareObject;
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.lib.net.Transmitter;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.FacebookConnect;
	import com.hoimi.util.LocalSaver;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	
	import Enum.EnumLocalSave;
	
	import FB.Model.Bind;
	import FB.Model.FacebookFriend;
	import FB.Model.FacebookFriends;
	import FB.Model.FacebookInfo;
	
	import event.GameEvent;
	
	import handler.TxtLangTransfer;
	
	import lib.VersionNumber;
	
	import model.PlayerInfo;
	import model.SignupInfo;

	public class FacebookController
	{
		/// 登入發生錯誤
		static public const EVENT_FACEBOOK_LOGIN_FAIL:String = 'EVENT_FACEBOOK_LOGIN_FAIL';
		/// 登入User cancel
		static public const EVENT_FACEBOOK_LOGIN_CANCEL:String = 'EVENT_FACEBOOK_LOGIN_CANCEL';
		/// 要求FB個人資訊中
		static public const EVENT_FACEBOOK_WAIT_REQ_INFO:String = 'EVENT_FACEBOOK_WAIT_REQ_INFO';
		/// 要求FB個人資訓完成
		static public const EVENT_FACEBOOK_RECV_INFO:String = 'EVENT_FACEBOOK_RECV_INFO';
		/// 要求FB好友中
		static public const EVENT_FACEBOOK_WAIT_REQ_FRIENDS:String = 'EVENT_FACEBOOK_WAIT_REQ_FRIENDS';
		/// 要求FB好友完成
		static public const EVENT_FACEBOOK_RECV_FRIENDS:String = 'EVENT_FACEBOOK_RECV_FRIENDS';
		/// 要求FB動作完成
		static public const EVENT_FACEBOOK_COMPLETE:String = 'EVENT_FACEBOOK_COMPLETE';
		
		public static const READ_PERMISSIONS:Array = ["email","public_profile","user_friends"];
		
		static public function get instance():FacebookController
		{
			if(_instance == null){
				_instance = new FacebookController( new fbsingleton);
			}
			return _instance;
		}
		
		private static var _instance:FacebookController;
		
		private var _fbInfo:FacebookInfo = new FacebookInfo(null);
		private var _self:FacebookFriend = new FacebookFriend();
		private var _fbFriends:Array = [];
		private var _fbRank:Array = [];
		private var _http:Transmitter = new Transmitter(GameConfig.SESSION_TIME);
		private var _index:int = 0;
		private var _bSignin:Boolean = false;
		private var _timestamp:int = -1;

		public function get friendsByLevel():Array
		{
			return _fbRank.sortOn('level', Array.DESCENDING | Array.NUMERIC);
		}
		
		public function get friendsByRate():Array
		{
			return _fbRank.sortOn('rate', Array.DESCENDING | Array.NUMERIC);
		}
		
		public function get friendsByWin():Array
		{
			return _fbRank.sortOn('win', Array.DESCENDING | Array.NUMERIC);
		}
		
		public function get friendsByCoin():Array
		{
			return _fbRank.sortOn('coin', Array.DESCENDING | Array.NUMERIC);
		}
		
		public function get friendLength():uint
		{
			return _fbRank.length;
		}
		
		public function get friendIDs():Array
		{
			var ids:Array = [];
			for each(var f:FacebookFriend in _fbFriends){
				ids.push(f.id);
			}
			return ids;
		}
		
		public function get self():FacebookFriend
		{
			return _self;
		}
		
		public function get info():FacebookInfo
		{
			return _fbInfo;
		}
		
		public function FacebookController(slgt:fbsingleton)
		{
		}
		///首頁做登入
		public function open():void
		{
			///已經登入過要過好有資料
			if(_bSignin){
				selfFillInfo();
				
				var now:int = getTimer();
				if(_timestamp > 0 && now - _timestamp < 60000){
					EventManager.instance.trigger(new Event(FacebookController.EVENT_FACEBOOK_RECV_FRIENDS));
				}else{
					_timestamp = now;
					EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
					if(GameConfig.game_playerInfo.fb_id == null){
						bind();
					}else{
						_index = 0;
						reqFriendRecord();
					}
				}
			}else{ 
				_fbFriends = [];
				_fbRank = [];
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
				FacebookConnect.instance.login(FacebookConnect.PERMISSION_READ, FacebookController.READ_PERMISSIONS, onSessionOpened);
			}
		}
		
		public function inviteFriends():void
		{
			FacebookConnect.instance.inviteFriends(onInviteFriends);
		}
		
		private function onInviteFriends($success:Boolean, $userCancelled:Boolean, $error:String = null):void
		{
			trace("onInviteFriends : Success = "+ $success+ " $userCancelled = " + $userCancelled + " $error = "+ $error );  
		}
		
		private function onSessionOpened(success:Boolean, userCancelled:Boolean, error:String):void
		{
			
			if (!success && error)
			{
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
				EventManager.instance.trigger(new Event(EVENT_FACEBOOK_LOGIN_FAIL));
				return;
			}
			
			if(userCancelled){
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
				EventManager.instance.trigger(new Event(EVENT_FACEBOOK_LOGIN_CANCEL));
				return;
			}
			
			if(success)
			{
				_bSignin = true;
				reqInfo();
			}
		}
		
		private function reqInfo():void
		{
			trace('reqInfo');
			EventManager.instance.trigger(new Event(EVENT_FACEBOOK_WAIT_REQ_INFO));
			FacebookConnect.instance.requestInfo(onRecvInfo);
		}
		
		private function onRecvInfo($data:Object):void
		{
			trace("onRecvInfo : ", JSON.stringify($data));  
			_fbInfo.byObject($data);
			_self = new FacebookFriend($data);
			selfFillInfo();
//			GameConfig.game_playerInfo.fb_id = _self.id;
			_fbRank.push(_self);
			EventManager.instance.trigger(new Event(EVENT_FACEBOOK_RECV_INFO));
			reqFriends();
		}
		
		private function reqFriends():void
		{
			trace('reqFriends');
			EventManager.instance.trigger(new Event(EVENT_FACEBOOK_WAIT_REQ_FRIENDS));
			FacebookConnect.instance.reqFriends(onRecvFBFriends);
		}
		
		private function onRecvFBFriends($data:Object):void
		{
			trace("onRecvFBFriends : ", JSON.stringify($data));  
			var fbfs:FacebookFriends = new FacebookFriends($data);
			if(fbfs.data.length > 0){
				_fbFriends = _fbFriends.concat(fbfs.data.slice());
				_fbRank = _fbRank.concat(fbfs.data.slice());	
			}
		
			if(fbfs.paging != null && fbfs.paging.next != null){
				reqNextPage(fbfs.paging.next);
			}else{
				/// from opening
				if(GameConfig.game_playerInfo == null){
					EventManager.instance.trigger(new Event(FacebookController.EVENT_FACEBOOK_COMPLETE));
				}else if(GameConfig.game_playerInfo.fb_id == null){
					bind();
				}else{
					_index = 0;
					reqFriendRecord();
				}
			}
		}
		
		private function reqFriendRecord(e:GameEvent = null):void
		{
			if(_fbFriends.length == 0){
				EventManager.instance.trigger(new Event(FacebookController.EVENT_FACEBOOK_RECV_FRIENDS));
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
				return;
			}
			trace('reqFriendRecord '+_fbFriends.length);
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,reqFriendRecord);
//			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			var fber:FacebookFriend = _fbFriends[_index];
			SessionManager.instance.request(Protocol.FACEBOOK, {token:GameConfig.game_playerInfo.token, fb_id:fber.id}, onFriendRecord ,onFail);
			
		}
		
		private function onFriendRecord(e:NetEvent):void
		{
			trace('onFriendRecord ' +e.data);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,signin);

			var fb:Object = JSON.parse(e.data);
			if(fb.result == 1){
				_fbFriends[_index].byObject(fb);
				if(++_index == _fbFriends.length){
					EventManager.instance.trigger(new Event(FacebookController.EVENT_FACEBOOK_RECV_FRIENDS));
					EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
				}else{
					reqFriendRecord();	
				}
			}else{
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
				trace(' request fb friend error ['+ e.data + ']');
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "onFriendRecord "+ fb.result
				EventManager.instance.trigger(evError);
			}
		}	
		
		private function reqNextPage(url:String):void{
			trace('reqNextPage');
			EventManager.instance.register(NetEvent.STATE_COMPLETE, onRecvNextPage);
			EventManager.instance.register(NetEvent.STATE_IO_ERROR, onFail);
			EventManager.instance.register(NetEvent.STATE_TIMEOUT, onFail);
			EventManager.instance.register(NetEvent.STATE_FAIL, onFail);
			_http.get(-999,url,[]);
		}
		
		private function onRecvNextPage(e:NetEvent):void
		{
			if(e.protocol == -999){
				trace('onRecvNextPage ');
				EventManager.instance.remove(NetEvent.STATE_COMPLETE, onRecvNextPage);
				EventManager.instance.remove(NetEvent.STATE_IO_ERROR, onFail);
				EventManager.instance.remove(NetEvent.STATE_TIMEOUT, onFail);
				EventManager.instance.remove(NetEvent.STATE_FAIL, onFail);
				onRecvFBFriends(JSON.stringify(e.data));
			}
		}
		
		private function bind():void
		{
			trace('bind');
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,bind);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			var fb_ids:Array = [];
			for each(var f:FacebookFriend in _fbFriends){
				fb_ids.push(f.id);
			}
			SessionManager.instance.request(Protocol.USER_BIND, {uuid:GameConfig.UUID,
				device:GameConfig.DEVICE, email:_fbInfo.email, friends:fb_ids, fb_id:_fbInfo.id,
				fb_name:_fbInfo.name, gender:_fbInfo.gender, age_range_min:_fbInfo.age_range.min}, onBind, onFail);
		}
		
		private function onBind(e:NetEvent):void
		{
			trace('onBind '+e.data);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,bind);
			//			_view.hideLoading();
			var net:NetPacket = new NetPacket(e.data);
			
			if(net.result == 1){
				var b:Bind = new Bind(e.data);
				/// 舊會員
				if(b.uuid != GameConfig.UUID){
					GameConfig.UUID = b.uuid;
					var strVersion:String = VersionNumber.fnGetVersionNumber();
					signin();
					trace('onBind old user ');
				}else{/// 新會員
					_index = 0;
					reqFriendRecord();	
				}
			}else{
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "on FB bind "+e.data;
				EventManager.instance.trigger(evError);
			}
		}
		
		private function signin(e:GameEvent = null):void
		{
			trace('signin');
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,signin);
			var strVersion:String = VersionNumber.fnGetVersionNumber();
			SessionManager.instance.request(Protocol.USER_SIGNIN, {uuid:GameConfig.UUID, device:GameConfig.DEVICE, version:strVersion},onSignin,onFail);
		}
		
		private function onSignin(e:NetEvent):void
		{
			trace('onSignin ' + e.data);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,signin);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				GameConfig.game_playerInfo = new PlayerInfo(e.data);
				GameConfig.game_playerInfo.boardcast();
				/// For refill
				GameConfig.game_playerInfo.skill_raise(0,GameConfig.game_playerInfo.skills[0],GameConfig.game_playerInfo.skills_next[0]);
				GameConfig.game_playerRecord = GameConfig.game_playerInfo.player_record
				selfFillInfo();
				_index = 0;
				reqFriendRecord();
			}
			else
			{
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "onSignin "+ data.result
				EventManager.instance.trigger(evError);
			}
		}
		
		private function onFail(e:NetEvent):void{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
			evError.m_eventArgs = "Faceboook "+ e.data
			EventManager.instance.trigger(evError);
		}
		
		private function selfFillInfo():void
		{
			if(!_self)return;
			
			_self.record = GameConfig.game_playerRecord;
			if(GameConfig.game_playerInfo)
			{
				_self.level = GameConfig.game_playerInfo.level;
				_self.coin = GameConfig.game_playerInfo.coin;
			}
		}
		
	}
}
class fbsingleton{}