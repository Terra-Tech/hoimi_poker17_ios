package handler
{
	import com.hoimi.util.EventManager;
	
	import flash.events.MouseEvent;
	
	import Enum.EnumMsg;
	
	import event.GameEvent;
	import event.GameMessageEvent;

	public class YesNoPanelHandler
	{
		public function YesNoPanelHandler()
		{
		}
		// coin
		public static function fnCoinNotEnough():void
		{
			var ev:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
			ev.m_strMsg = TxtLangTransfer.COIN_NOT_ENOUGH
			ev.m_strType = EnumMsg.MSG_TYPE_YES_NO;
			ev.m_callYesFn = fnCoinNotEnoughYesEvent
			EventManager.instance.trigger(ev);
		}
		
		private static function fnCoinNotEnoughYesEvent():void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_GAIN_COIN_EVENT));
		}
		
		
		// diamond
		public static function fnDiamondNotEnough():void
		{
			var ev:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
			ev.m_strMsg = TxtLangTransfer.DIAMOND_NOT_ENOUGH
			ev.m_strType = EnumMsg.MSG_TYPE_YES_NO;
			ev.m_callYesFn = fnDiamondNotEnoughYesEvent
			EventManager.instance.trigger(ev);
		}
		
		private static function fnDiamondNotEnoughYesEvent():void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_IAP_EVENT));
		}
		
		
	}
}