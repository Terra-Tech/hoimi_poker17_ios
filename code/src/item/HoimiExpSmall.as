package item
{
	
	import flash.display.Sprite;
	
	
	import lib.TiConvertNumber;
	
	public class HoimiExpSmall extends Sprite
	{
		private var m_sp:Sprite = new Sprite();
		public var m_nExp:int = 0;
		
		public function HoimiExpSmall()
		{
			super();
			this.addChild(m_sp);
		}
		
		public function fnSetValue(value:int):void
		{
			m_sp.removeChildren();
			m_nExp = value;
			var sp:Sprite = TiConvertNumber.fnConvertValueToUI(value,Poker_Number_Exp_Small,0,false,false);
			m_sp.addChild(sp);
			
		}
		
		public function fnSetValue2(value:Number):void
		{
			m_sp.removeChildren();
			m_nExp = value;
			var sp:Sprite = TiConvertNumber.fnConvertValueToUI(value,Poker_Number_Exp_Small,0,false,false);
			m_sp.addChild(sp);
			sp.x += sp.width
		}
		
		
		public function fnAddExpValue(value:int):void
		{
			m_nExp += value
			fnSetValue2(m_nExp);
		}
	}
}