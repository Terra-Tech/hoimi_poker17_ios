package Refill.Model
{
	import com.hoimi.lib.net.JsonObject;

	public class RefillSync extends JsonObject
	{
		public var second_pass:int = 0;
		
		public function RefillSync(time:* = null)
		{
			super(time);
		}
	}
}