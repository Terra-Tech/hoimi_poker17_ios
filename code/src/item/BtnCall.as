package item
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import Enum.EnumLang;
	
	import base.TiButtonState;
	import base.TiDisableTextField;
	
	import lib.MultiLanguagleUISelect;
	
	public class BtnCall extends Sprite
	{
		
		private var m_mc:MovieClip
		private var m_buttonState:TiButtonState = new TiButtonState();
		
		public function BtnCall()
		{
			super();
			
			var m_mcCall:Poker_Btn_Call = new Poker_Btn_Call();
			var mc:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mcCall);
			m_mc = mc;
			this.addChild(m_mcCall);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
		}
		public function fnBtnNormalState():void
		{
			m_mc.gotoAndStop(1);
		}
		
		public function fnDisableBtn():void
		{
			m_buttonState.fnDisableAndDarkMcState(m_mc);
		}
	}
	
	
}