package Skill.skillItem
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	
	import Skill.SkillBase;
	import Skill.SkillRaiseEvent;
	
	
	import lib.MultiLanguagleUISelect;
	
	public class SkillItem_04 extends SkillBase
	{
		
		private var m_mcSkill:Poker_Btn_Skill_04 = new Poker_Btn_Skill_04();
		public function SkillItem_04()
		{
			super();
			
			
			var mc:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mcSkill);
			m_mc = mc;
			this.addChild(m_mcSkill);
			this.addChild(m_sp);
			//m_txtExplanation.text = "聚寶盆每次補幣時增加的金幣數量";
			m_txtExplanation.text = "鑽石換金幣,高額獎勵(爆擊)發生時相乘的倍率";
			m_strSkillName = "[點石成金獎勵倍率]";
		}
		
		override protected function fnRaiseSkill():void
		{
			var ev:SkillRaiseEvent = new SkillRaiseEvent(3)
			EventManager.instance.trigger(ev);
		}
		
		override protected function fnDataDisplay(nInfo1:Number,nInfo2:Number):void
		{
			m_txtCurrent.text = "x"+nInfo1
			if(nInfo2 == -1)
			{
				m_txtNext.text = "----"		
				m_mcSkill.arrow.visible=false;
			}
			else
			{
				m_txtNext.text = "x"+nInfo2	
			}
		
		}
	}
}