package lib
{
	public class TTCConvertCharacterIndex
	{
		public function TTCConvertCharacterIndex()
		{
		}
		private static var arCharacter:Array=["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o"
			,"p","q","r","s","t","u","v","w","x","y","z"]
		
			
		public static function fnConvertCharacterToIndex(strCharacter:String):int
		{
			strCharacter = strCharacter.toLocaleLowerCase();
			for (var index:int = 0; index <arCharacter.length ;index++)
			{
				if(strCharacter == arCharacter[index])
				{
					return index+1;
				}
			}
			
			return index+1;
		}
		
		public static function fnConvertIndexToCharacter(nIndex,bCaptial:Boolean = false):String
		{
			var str:String = arCharacter[nIndex-1]; 
			if(bCaptial)
			{
				return str.toLocaleUpperCase();
			}
			return str;
		}
	}
}