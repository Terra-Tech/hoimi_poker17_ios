package com.hoimi.util{
	import com.chartboost.plugin.air.CBLoadError;
	import com.chartboost.plugin.air.CBLocation;
	import com.chartboost.plugin.air.Chartboost;
	import com.chartboost.plugin.air.ChartboostEvent;
	
	public class ChartboostUtil
	{
		static private var _callback:Function = null;
		static private var _videoReady:Boolean = false;
		
		public static function get instance():Chartboost
		{
			return Chartboost.getInstance();
		}
		
		static public function init(appId:String, appSignature:String):void
		{
			Chartboost.getInstance().startWith(appId, appSignature);
		}
		
		public static function cacheVideo():void
		{
			Chartboost.getInstance().cacheRewardedVideo(CBLocation.MAIN_MENU);
		}
		
		public static function showVideo():void
		{
			Chartboost.getInstance().showRewardedVideo(CBLocation.MAIN_MENU);
		}
		
		public static function hasRewardedVideo():Boolean
		{
			return Chartboost.getInstance().hasRewardedVideo(CBLocation.MAIN_MENU);
		}
		
		public static function cacheInterstitial():void
		{
			Chartboost.getInstance().cacheInterstitial(CBLocation.MAIN_MENU);
		}
		
		public static function showInterstitial():void
		{
			Chartboost.getInstance().showInterstitial(CBLocation.MAIN_MENU);
		}
		
		public static function dispose():void
		{
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_FAIL_TO_LOAD_INTERSTITIAL);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_CLICK_INTERSTITIAL);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_CACHE_INTERSTITIAL);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_DISMISS_INTERSTITIAL);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_CACHE_REWARDED_VIDEO);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_FAIL_TO_LOAD_REWARDED_VIDEO);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_CLICK_REWARDED_VIDEO);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_COMPLETE_REWARDED_VIDEO);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_DISPLAY_REWARDED_VIDEO);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_DISMISS_REWARDED_VIDEO);
			
			Chartboost.getInstance().removeDelegateEvent(ChartboostEvent.DID_CLOSE_REWARDED_VIDEO);
			
			_callback = null;
		}
		
		public static function hasInterstitial():Boolean
		{
			return Chartboost.getInstance().hasInterstitial(CBLocation.MAIN_MENU);
		}
		
		public static function addDelegateMethods(eventCB:Function = null):void 
		{
			_callback = eventCB || onRecvEvent;
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_FAIL_TO_LOAD_INTERSTITIAL, function (location:String, error:CBLoadError):void{_callback(ChartboostEvent.DID_FAIL_TO_LOAD_INTERSTITIAL,location,error)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_CLICK_INTERSTITIAL, function (location:String):void{_callback(ChartboostEvent.DID_CLICK_INTERSTITIAL,location)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_CACHE_INTERSTITIAL, function (location:String):void{_callback(ChartboostEvent.DID_CACHE_INTERSTITIAL,location)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_DISMISS_INTERSTITIAL, function (location:String):void{_callback(ChartboostEvent.DID_DISMISS_INTERSTITIAL,location)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_CACHE_REWARDED_VIDEO, function (location:String):void{_callback(ChartboostEvent.DID_CACHE_REWARDED_VIDEO,location)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_FAIL_TO_LOAD_REWARDED_VIDEO, function (location:String, error:CBLoadError):void{_callback(ChartboostEvent.DID_FAIL_TO_LOAD_REWARDED_VIDEO,location,error)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_CLICK_REWARDED_VIDEO, function (location:String):void{_callback(ChartboostEvent.DID_CLICK_REWARDED_VIDEO,location)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_COMPLETE_REWARDED_VIDEO, function (location:String, reward:int):void{_callback(ChartboostEvent.DID_COMPLETE_REWARDED_VIDEO,location)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_DISPLAY_REWARDED_VIDEO, function (location:String):void{_callback(ChartboostEvent.DID_DISPLAY_REWARDED_VIDEO,location)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_DISMISS_REWARDED_VIDEO, function (location:String):void{_callback(ChartboostEvent.DID_DISMISS_REWARDED_VIDEO,location)});
			
			Chartboost.getInstance().addDelegateEvent(ChartboostEvent.DID_CLOSE_REWARDED_VIDEO, function (location:String):void{_callback(ChartboostEvent.DID_CLOSE_REWARDED_VIDEO,location)});
		}
		
		private static function onRecvEvent(type:String, location:String, err:CBLoadError = null):void
		{
			trace("type : "+ type + " location:"+location);
			
			if(err)trace( err.Text);
			
			
			/*switch(e.type)
			{
				case ChartboostEvent.DID_FAIL_TO_LOAD_INTERSTITIAL:
					trace("Chartboost: on Interstitial failed to load ");
					break;
				case ChartboostEvent.DID_CLICK_INTERSTITIAL:
					trace("Chartboost: on Interstitial clicked " );
					break;
				case ChartboostEvent.DID_CACHE_INTERSTITIAL:
					trace("Chartboost: on Interstitial recv " );
					break;
				case ChartboostEvent.DID_DISMISS_INTERSTITIAL:
					trace("Chartboost: on Interstitial dismiss " );
					cacheInterstitial();
					break;
				case ChartboostEvent.DID_CACHE_REWARDED_VIDEO:
					trace(hasRewardedVideo() + " Chartboost: on video cahced " );
					break;
				case ChartboostEvent.DID_COMPLETE_REWARDED_VIDEO:
					trace("Chartboost: on video completed " );
					break;
				case ChartboostEvent.DID_FAIL_TO_LOAD_REWARDED_VIDEO:
					trace("Chartboost: on video load fail " );
					break;
				case ChartboostEvent.DID_CLICK_REWARDED_VIDEO:
					trace("Chartboost: on video click " );
					break;
				case ChartboostEvent.DID_DISPLAY_REWARDED_VIDEO:
					trace("Chartboost: on video present " );
					break;
				case ChartboostEvent.DID_DISMISS_REWARDED_VIDEO:
					trace("Chartboost: on video dismiss " );
					break;
				case ChartboostEvent.DID_CLOSE_REWARDED_VIDEO:
					trace("Video is close ? "+hasRewardedVideo());
					break;
				
			}*/
		}

	}
}