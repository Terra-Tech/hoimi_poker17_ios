package Skill.skillItem
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	
	import Skill.SkillBase;
	import Skill.SkillRaiseEvent;
	
	
	import lib.MultiLanguagleUISelect;
	
	public class SkillItem_03 extends SkillBase
	{
		
		private var m_mcSkill:Poker_Btn_Skill_03 = new Poker_Btn_Skill_03();
		public function SkillItem_03()
		{
			super();
			
		
			var mc:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mcSkill);
			m_mc = mc;
			this.addChild(m_mcSkill);
			this.addChild(m_sp);
			//m_txtExplanation.text = "鑽石換金幣,高額獎勵(爆擊)發生的倍率";
			m_txtExplanation.text = "鑽石換金幣，獲得高額獎勵(爆擊)出現的機率";
			m_strSkillName = "[點石成金獎勵機率]";
		}
		
		override protected function fnRaiseSkill():void
		{
			var ev:SkillRaiseEvent = new SkillRaiseEvent(2)
			EventManager.instance.trigger(ev);
		}
		
		override protected function fnDataDisplay(nInfo1:Number,nInfo2:Number):void
		{
			m_txtCurrent.text = nInfo1/10+"%"
			if(nInfo2 == -1)
			{
				m_txtNext.text = "----"
				m_mcSkill.arrow.visible=false;
			}
			else
			{
				m_txtNext.text = nInfo2/10+"%"
			}
			
		}
		
	}
}