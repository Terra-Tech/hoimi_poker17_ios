package section
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	
	import flash.utils.setTimeout;
	
	import base.TTCSectionBase;
	
	import caurina.transitions.Tweener;
	
	import conf.musicConfig;
	
	import event.GameEvent;
	
	import handler.TTCMusicHandler;
	import handler.TxtLangTransfer;
	
	import item.Loading;
	
	import lib.RandomMachine;
	import lib.TTCModeCheck;
	
	import model.OpenBetInfo;
	
	public class SectionConnecting extends TTCSectionBase
	{
		private var m_mcConnecting:Poker_Connecting = new Poker_Connecting();
		private var m_mcBg:Poker_Lobby_Bg = new Poker_Lobby_Bg
		private var m_mcLoading:Poker_Loading = new Poker_Loading();

		public function SectionConnecting()
		{
			super();
			this.addChild(m_mcBg);
			this.addChild(m_mcLoading);
			this.addChild(m_mcConnecting);
			
			m_mcLoading.x = 360;
			m_mcLoading.y = 600
			fnTweenCoin()
			m_mcConnecting.visible=false;
			m_mcLoading.visible=false;
			fnInit();
		}
		
		private function fnTweenCoin():void
		{
			Tweener.addTween(m_mcLoading,{y:550,time:1,transition:"easeOutQuart",onComplete:fnAfterTween});
		}
		
		private function fnAfterTween():void
		{
			Tweener.addTween(m_mcLoading,{y:600,time:1,transition:"easeInQuart",onComplete:fnTweenCoin});
		}
		
		
		private function fnInit():void
		{
			if(GameConfig.bAgain)
			{
				TTCMusicHandler.fnGetInstance().fnResume();
				fnNetAgain();
			}
			else if(GameConfig.bOther)
			{
				TTCMusicHandler.fnGetInstance().fnResume();
				fnNetOther();
			}
			else
			{
				GameConfig.change_enemy_remind = RandomMachine.fnGetRanOneValue(GameConfig.ar_enemy_leave);
				TTCMusicHandler.fnGetInstance().fnSelectMusic(musicConfig.BATTLE_MUSIC,0.5);

				fnSearchingEnemy();
			}
		}
		
		private function fnSearchingEnemy():void
		{
			m_mcConnecting.visible=true
			m_mcLoading.visible=true;
			m_mcConnecting.txt_connecting.text  = TxtLangTransfer.SEARCHING_ENEMY;
			var nTime:int
			if(TTCModeCheck.isDebugBuild())
			{
				nTime = RandomMachine.fnGetRanOneValue([100]);
			}
			else
			{
				nTime = RandomMachine.fnGetRanOneValue([1000,1500,2000,2500]);
			}
			
			setTimeout(function():void 
			{
				fnConnecting();
			}, nTime)
		}
		
		private function fnConnecting():void
		{
			m_mcConnecting.visible=true
			m_mcLoading.visible=true;
			m_mcConnecting.txt_connecting.text  = TxtLangTransfer.CONNECTING;
			var nTime:int
			if(TTCModeCheck.isDebugBuild())
			{
				nTime = RandomMachine.fnGetRanOneValue([100]);
			}
			else
			{
				nTime = RandomMachine.fnGetRanOneValue([200,400,800,1000,1500,2000]);
			}
			setTimeout(function():void 
			{
				var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NEXT_SECTION_EVENT);
				dispatchEvent(ev);
			}, nTime)
		}
		
		private function fnNetOther(e:GameEvent = null):void
		{
			GameConfig.change_enemy_remind = RandomMachine.fnGetRanOneValue(GameConfig.ar_enemy_leave);
			m_mcConnecting.visible=false
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnNetOther);
			SessionManager.instance.request(Protocol.GAME_OPEN_BET, {token:GameConfig.game_playerInfo.token, table_id:GameConfig.game_table.id,assign:"", exclude:GameConfig.game_openBetInfo.enemy_uuid},onOpenBet,onNet);	
		}
		
		private function fnNetAgain(e:GameEvent = null):void
		{
			m_mcConnecting.visible=false
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnNetAgain);
			SessionManager.instance.request(Protocol.GAME_OPEN_BET, {token:GameConfig.game_playerInfo.token, table_id:GameConfig.game_table.id,assign:GameConfig.game_openBetInfo.enemy_uuid, exclude:""},onOpenBet,onNet);	
		}
		
		/** open_bet
		 * 1:{"result" : 1,'game_id' : 92, 'player_coin':230,'enemy_uuid' : 12h123lkjhj123h, 'enemy_name' : Aries, 'player_cards':[0,1,2,3,4,5]})
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 * -4:param miss
		 * -5:no such table
		 * -6:coin not enought
		 * -7:coin over max limit
		 */
		private function onOpenBet(e:NetEvent):void
		{
			trace('onOpenBet '+e.data);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnNetOther);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnNetAgain);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				if(GameConfig.bAgain)
				{
					fnConnecting();
				}
				else
				{
					fnSearchingEnemy();
				}
				GameConfig.game_openBetInfo = new OpenBetInfo(e.data);
				GameConfig.game_playerInfo.coin = GameConfig.game_openBetInfo.player_coin + GameConfig.game_table.bet;
			}
			else if(data.result == -6)
			{
				trace("coin not enought");
				var ev2:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_PREVIOUS_SECTION_EVENT);
				dispatchEvent(ev2);

			}
			else if(data.result == -7)
			{
				trace("coin over max limit");	
				var ev3:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_PREVIOUS_SECTION_EVENT);
				dispatchEvent(ev3);
			}
			else
			{
				var ev4:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				ev4.m_eventArgs = "onOpenBet "+data.result
				EventManager.instance.trigger(ev4);
			}
		}
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
		
		
		override public function fnResize():void
		{
			m_mcBg.x = 360 +GameConfig.object_adjuct_position_x;
			m_mcConnecting.x+= GameConfig.object_adjuct_position_x;
			m_mcLoading.x+=GameConfig.object_adjuct_position_x;
		}
		
		
		
		override public function fnPause():void
		{
			super.fnPause();
		}
		
		override public function fnResume():void
		{
			super.fnResume();
		}
		
		override public function fnSkip():void
		{
			super.fnSkip();
		}
		
		
		override public function fnReplay():void
		{
			super.fnReplay();
		}
		
		override public function fnExit():void
		{
			super.fnExit()
			this.removeChildren();
		}
		
	}
}