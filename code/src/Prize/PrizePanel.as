package Prize
{
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.freshplanet.lib.ui.util.RectangleSprite;
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.utils.setTimeout;
	
	import Enum.EnumSection;
	
	import Prize.Model.Prize;
	import Prize.Model.PrizeInfo;
	
	import base.TiDisableTextField;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	
	import handler.TTCSoundHandler;
	
	import item.PrizeItem;
	import item.ResultPanelCoin;
	
	
	
	public class PrizePanel extends Sprite implements IPrizeView
	{
		private var m_mc:Poker_Prize_Base = new Poker_Prize_Base();
		private var m_mcTreasureBox:Poker_Prize_Treasure_Box = new Poker_Prize_Treasure_Box();
		private var _open:Boolean = false;
		private var m_sp:Sprite = new Sprite();
		private var _scroll:ScrollController;
		private var m_nMaskHeight:Number = 850
		private var m_nDefaultSpY:Number = 150
		private var m_coinMsgSp:Sprite = new Sprite();	
		private var m_arList:Array
		private var m_scrollSp:Sprite = new Sprite();
			
		public function PrizePanel()
		{
			super();
			this.addChild(m_mcTreasureBox);
			this.addChild(m_mc);
			//this.addChild(m_sp);
			this.addChild(m_scrollSp);
			this.addChild(m_coinMsgSp);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcTreasureBox);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_ok);
			m_mcTreasureBox.addEventListener(MouseEvent.CLICK,open_panel);
			m_mc.btn_ok.addEventListener(MouseEvent.CLICK,gain);
			m_mc.visible=false;
			m_mc.x = 360
			m_mcTreasureBox.new_prize.visible=false;
			m_mcTreasureBox.y = 1050;
			m_mcTreasureBox.x = 610
			m_mcTreasureBox.visible=false;
			m_sp.x = 0  
			m_sp.y = 0
			this.addEventListener(Event.ADDED_TO_STAGE, onAdd);
		}
		
		
		public function netError(msg:String):void
		{
		//	var ev:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT);
		//	ev.m_eventArgs = "netError \n" + msg
		//	EventManager.instance.trigger(ev);	
		}
		
		public function serverError(msg:String):void
		{
		//	var ev:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT);
		//	ev.m_eventArgs = "serverError \n" +msg
		//	EventManager.instance.trigger(ev);	
		}
		
		public function showLoading():void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
		}
		
		public function hideLoading():void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
		}
		
		public function alert():void
		{
			m_mcTreasureBox.new_prize.visible=true;
			m_mcTreasureBox.visible=true;
		}
		
		public function showList(list:Array):void
		{
			m_sp.removeChildren();
			m_arList = list
			var defs:Array = [];
			var i:int,len:int = list.length;
			var p:PrizeInfo;
			var prizeItem:PrizeItem 
			for(i =0 ;i <len;i++)
			{
				p = list[i];
				prizeItem = new PrizeItem(p.id,p.create_date,p.item_id,p.amount,p.friend);
				prizeItem.x =360
				prizeItem.y =( prizeItem.height)* i 
				m_sp.addChild(prizeItem);
			}
			if(len>0)
			{
				fnInitScroll();
			}
			
		}
		
		public function showGain(prize:Prize):void
		{
			trace("已領取 id : " + prize.prize_id);
		//	statusCoin.text = "Coin:"+prize.player_coin.toString();
		//	statusDiamond.text = "Diamond:"+prize.player_diamond.toString();
			
			/// 請自行檢查list是否都領取完
			/// 領取完
			var nIndex:int = 0
			for(var i:int = 0 ;i < m_arList.length ; i++)
			{
				if(m_arList[i].id == prize.prize_id)
				{
					nIndex = i
					break;	
				}
			}
			var nAddCoin:Number = prize.player_coin - GameConfig.game_playerInfo.coin 
			fnSetData(nAddCoin,nIndex);
			GameConfig.game_playerInfo.coin = prize.player_coin
		}
		
		private function fnSetData(nCoin:Number,nIndex:int):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSoundCoin();
			if(nIndex <10)
			{
				var m_coMsgGainCoin:ResultPanelCoin = new ResultPanelCoin();
				m_coMsgGainCoin.fnSetValue(nCoin,true,false);
				m_coMsgGainCoin.x = 200
				m_coMsgGainCoin.y = 150 + (nIndex*100)
				m_coinMsgSp.addChild(m_coMsgGainCoin);
				setTimeout(function():void 
				{
					fnTweenEffect(m_coMsgGainCoin);
				}, 2000)
			}
		}
		
		private function fnTweenEffect(mc:Sprite):void
		{
			var nY:Number = mc.y - 200 
			Tweener.addTween(mc,{y:nY,time:1,alpha:0,transition:"linear",onComplete:fnTweenEffectFinish});
		}
		
		private function fnTweenEffectFinish():void
		{
			m_coinMsgSp.removeChildren();
		}
		
		public function get open():Boolean
		{
			return _open;
		}
		
		public function gained():void
		{	trace("領取完畢")
			//statusStatus.text = 'Status : 領取完畢';
			m_mcTreasureBox.new_prize.visible=false;
			m_mcTreasureBox.visible=false;
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_SECTION_REFRESH));
			setTimeout(function():void 
			{
				close_panel();
			}, 2000)
		
		}
		
		protected function nothing(e:Event):void
		{
			
		}
		
		protected function gain(e:MouseEvent):void
		{
			if(m_arList.length == 0)
			{
				close_panel();
			}
			EventManager.instance.trigger(new Event(PrizeController.EVENT_PRIZE_GAIN));
		}
		
		protected function onAdd(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAdd);
			this.addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			EventManager.instance.trigger(new Event(PrizeController.EVENT_PRIZE_INIT));
			
		}
		
		protected function open_panel(e:MouseEvent):void
		{
			EventManager.instance.trigger(new Event(PrizeController.EVENT_PRIZE_SHOW));
			m_mc.visible=true;
			m_sp.visible=true;
			_open = true;
		}
		
		protected function close_panel(e:Event = null):void
		{
			_open = false;
			m_mc.visible=false;
			m_sp.visible=false;
			EventManager.instance.trigger(new Event(PrizeController.EVENT_PRIZE_HIDE));
			
		}
		
		protected function onRemove(event:Event):void
		{
			EventManager.instance.trigger(new Event(PrizeController.EVENT_PRIZE_DESTORY));
			this.removeEventListener(Event.REMOVED_FROM_STAGE, onRemove);
		}
		
		private function fnInitScroll():void
		{
			var container:RectangleSprite = new RectangleSprite(0xFFFFFF, 0, m_nDefaultSpY, 0, m_nMaskHeight);//red background
			m_scrollSp.addChild(container);
			container.addChild(m_sp);
			var containerViewport:Rectangle = new Rectangle(0, 0, 720 , m_nMaskHeight);
			var containerRect:Rectangle = new Rectangle(0, 0, 720 , m_nMaskHeight);
			this._scroll = new ScrollController();
			this._scroll.horizontalScrollingEnabled = false;
			this._scroll.addScrollControll(m_sp, container, containerViewport,containerRect);
		}
		
		
		
	}
}