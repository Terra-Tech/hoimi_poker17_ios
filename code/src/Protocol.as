package 
{
	public class Protocol
	{
		static public const ROUTE:Array = [	
			"/users/signup",
			"/users/bind",
			"/users/signin",
			"/game/table",
			"/game/open_bet",
			"/game/pick",
			"/game/first_bet",
			"/game/buying",
			"/game/second_bet",
			"/users/refill",
			"/iap/product",
			"/iap/key",
			"/iap",
			"/users/exchange",
			"/users/skill_raise",
			"/version/",
			"/game/jackpot",
			"/users/prize_list",
			"/users/prize",
			"/users/facebook",
			"/game/lotto",
			"/users/coin_back_pool",
			"/users/coin_back_withdraw",
			"/users/prize_amount",
			"/users/refill_timestamp",
			"/users/facebook_signup",
			"/prize/draw",
			"/double/config",
			"/double/bet",
			"/rank/money"

		];
		
		static public const USER_SIGNUP:int = count();
		static public const USER_BIND:int = count();
		static public const USER_SIGNIN:int = count();
		static public const GAME_TABLE:int = count();
		static public const GAME_OPEN_BET:int = count();
		static public const GAME_PICK:int = count();
		static public const GAME_FIRST_BET:int = count();
		static public const GAME_BUYING:int = count();
		static public const GAME_SECOND_BET:int = count();
		static public const USER_REFILL:int = count();
		static public const IAP_PRODUCT:int = count();
		static public const IAP_KEY:int = count();
		static public const IAP:int = count();
		static public const USER_EXCHANGE:int = count();
		static public const SKILL_RAISE:int = count();
		static public const VERSION:int = count();
		static public const JACKPOT:int = count();
		static public const PRIZE_LIST:int = count();
		static public const PRIZE:int = count();
		static public const FACEBOOK:int = count();
		static public const LOTTO_BET:int = count();
		static public const COIN_BACK_POOL:int = count();
		static public const COIN_BACK_WITHDRAW:int = count();
		static public const PRIZE_AMOUNT:int = count();
		static public const REFILL_TIMESTAMP:int = count();
		static public const FACEBOOK_SIGNUP:int = count();
		static public const DRAW_SN_RPIZE:int = count();
		static public const DOUBLE_CONFIG:int = count();
		static public const DOUBLE_BET:int = count();
		static public const MONEY_RANK:int = count();


		static public function get max():int
		{
			return COUNTER+1;
		}
		
		static private var COUNTER:int = 0;
		
		static private function count():int
		{
			trace("route["+COUNTER+"] : "+ ROUTE[COUNTER]);
			return COUNTER++;
		}
	}
}