package Skill
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	
	import flash.events.Event;
	
	import Skill.Model.Skill;
	import Skill.Model.SkillRaise;
	
	import event.GameEvent;

	public class SkillController
	{
		/// UI被加到螢幕上
		static public const EVENT_SKILL_INIT:String = 'SkillInit';
		/// UI被移除
		static public const EVENT_SKILL_DESTORY:String = 'SkillDestory';
		/// 領取
		static public const EVENT_SKILL_RAISE:String = 'SkillRaise';
		
		static public const SKILL_MAX_LEVEL:int = 20;
		
		private var _view:ISkillView;
		private var m_nSkillId:int
		
		public function SkillController(view:ISkillView){
			_view = view;
			EventManager.instance.register(EVENT_SKILL_INIT, init);
		}
		
		private function destory(e:Event):void
		{
			EventManager.instance.remove(EVENT_SKILL_DESTORY, destory);
			EventManager.instance.remove(EVENT_SKILL_RAISE, raise);
			EventManager.instance.register(EVENT_SKILL_INIT, init);
		}
		
		private function init(e:Event):void
		{
			EventManager.instance.remove(EVENT_SKILL_INIT, init);
			EventManager.instance.register(EVENT_SKILL_DESTORY, destory);
			EventManager.instance.register(EVENT_SKILL_RAISE, raise);
		}
		
		private function raise(e:SkillRaiseEvent):void
		{
			var idx:int = e.index;
			if(idx < 0 || idx >= GameConfig.game_playerInfo.skills_next.length){
				_view.showIndexError();
				return;
			}
			var cur:Skill = GameConfig.game_playerInfo.skills[idx];
			if(cur.level == SKILL_MAX_LEVEL){
				_view.showArrivedTopLevel();
				return;
			}
			var next:Skill = GameConfig.game_playerInfo.skills_next[idx];
			if(next.coin > GameConfig.game_playerInfo.coin){
				_view.showCoinNotEnought();
				return;
			}
			if(next.diamond > GameConfig.game_playerInfo.diamond){
				_view.showDiamondNotEnought();
				return;
			}
			m_nSkillId = idx
			_view.showLoading();
			fnNetRaise();
		}
		
		private function fnNetRaise(e:GameEvent = null):void
		{
			trace("m_nSkillId :" + m_nSkillId);
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnNetRaise);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			SessionManager.instance.request(Protocol.SKILL_RAISE, {token:GameConfig.game_playerInfo.token, skill:m_nSkillId}, onSuccess, onFail);
		}
		
		private function onSuccess(e:NetEvent):void{
			trace("skill raise recv : " + e.data);
			_view.hideLoading();
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnNetRaise);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			var net:NetPacket = new NetPacket(e.data);
			switch(net.result){
				case 1:
					var sr:SkillRaise = new SkillRaise(e.data);
					GameConfig.game_playerInfo.skill_raise(sr.skill, sr.current_skill, sr.next_skill);
					GameConfig.game_playerInfo.coin = sr.player_coin;
					GameConfig.game_playerInfo.diamond = sr.player_diamond
					_view.showRaiseSuccess(sr);
					break;
				case -5:
					_view.showIndexError();
					break;
				case -6:
					_view.showArrivedTopLevel();
					break;
				case -7:
					_view.showCoinNotEnought();
					break;
				case -8:
					_view.showDiamondNotEnought();
					break;
				default:
					_view.serverError("protocol :"+e.protocol+" inner error : " + net.result);
					break;
			}
		}
		
		private function onFail(e:NetEvent):void{
			_view.netError("net error : " + e.protocol);
		}
	}
}