package Skill.skillItem
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	
	import Skill.SkillBase;
	import Skill.SkillRaiseEvent;
	
	
	import lib.MultiLanguagleUISelect;
	
	public class SkillItem_06 extends SkillBase
	{
		
		private var m_mcSkill:Poker_Btn_Skill_06 = new Poker_Btn_Skill_06();
		public function SkillItem_06()
		{
			super();
			
			
			var mc:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mcSkill);
			m_mc = mc;
			this.addChild(m_mcSkill);
			this.addChild(m_sp);
			m_txtExplanation.text = "牌局落敗時，會返還部分金幣，可在返利金庫領取";
			m_strSkillName = "[返利金庫]";
		}
		
		override protected function fnRaiseSkill():void
		{
			var ev:SkillRaiseEvent = new SkillRaiseEvent(5)
			EventManager.instance.trigger(ev);
		}
		
		override protected function fnDataDisplay(nInfo1:Number,nInfo2:Number):void
		{
			m_txtCurrent.text = nInfo1/10+"%"
			if(nInfo2 == -1)
			{
				m_txtNext.text = "----"
				m_mcSkill.arrow.visible=false;
			}
			else
			{
				m_txtNext.text = nInfo2/10+"%"
			}
			
		}
	}
}