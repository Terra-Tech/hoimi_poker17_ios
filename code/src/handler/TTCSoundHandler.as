package handler
{
	import com.hoimi.util.LocalSaver;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	
	import Enum.EnumLocalSave;
	
	import event.TTCSoundEvent;
	
	import lib.RandomMachine;
	
	public class TTCSoundHandler extends EventDispatcher
	{
		
	
	
		private static var m_Instance:TTCSoundHandler;
		private var m_Sound:Sound = null
		private var m_soundCollection:Array = new Array();
		private var m_soundLoader:Array = new Array();
		private var m_soundChannel:SoundChannel
		private var m_sound:Sound = new Sound();
		private var m_bIsPlaying:Boolean = false;
		private var m_nPosition:Number=0;
		private var m_nSelectedSound:int=0;
		private var _m_bMute:Boolean=false;
		private var m_bLoop:Boolean =false;
		private var m_bRandom:Boolean
		private var m_musicName:String=""
		private var _m_nVolume:Number = 1;
		public function TTCSoundHandler()
		{
			if(LocalSaver.instance.getValue(EnumLocalSave.SOUND_MUTE) == undefined)
			{
				LocalSaver.instance.save(EnumLocalSave.SOUND_MUTE,false);
			}
			var bSoundMute:Boolean = LocalSaver.instance.getValue(EnumLocalSave.SOUND_MUTE);
			m_bMute  = bSoundMute
		}
		
		public static function fnGetInstance(): TTCSoundHandler 
		{
			if ( m_Instance == null )
			{
				m_Instance = new TTCSoundHandler();
				
			}
			return m_Instance;
		}
		
		public function get m_bMute():Boolean
		{
			return _m_bMute;
		}
		
		public function set m_bMute(value:Boolean):void
		{
			_m_bMute = value;
			fnVolumeChange();
		}
		
		public function get m_nVolume():Number
		{
			return _m_nVolume;
		}
		
		public function set m_nVolume(value:Number):void
		{
			_m_nVolume = value;
			fnVolumeChange();
		}
		
		public function fnInit(strName:String):void
		{
			m_musicName = strName;
		}
		
		protected function fnSoundDownLoad(e:Event=null):void
		{
			m_soundCollection=new Array();
			m_soundCollection.push({url:m_musicName});
			m_soundLoader=new Array();
			
			m_sound=new Sound();
			if(!m_soundCollection)
			{
				return;
			}
			var request:URLRequest= new URLRequest(m_soundCollection[m_nSelectedSound].url);
			m_sound.load(request);
			m_sound.addEventListener( IOErrorEvent.IO_ERROR, handleIOErr );
			m_sound.addEventListener(Event.COMPLETE,fnDownloadSoundComplete)
		}
		
		private function handleIOErr( e: IOErrorEvent ): void
		{
			trace("sound IO error:"+ e.currentTarget)
			fnBtnSoundNext();
		}
		
		private function fnDownloadSoundComplete(e:Event):void
		{
			//var key:String= m_soundCollection[m_nSelectedSound].name
			//var url:String=	m_soundCollection[m_nSelectedSound].url;
			m_soundLoader.push(e.currentTarget);
			if(m_nSelectedSound<m_soundCollection.length-1)
			{
				m_nSelectedSound++
					fnSoundDownLoad();
			}
			else
			{
				fnDownloadComplete();
			}
			
		}
		
		private function fnDownloadComplete():void
		{
			m_nSelectedSound=0;
			m_nPosition = 0;
			fnBtnSoundPlay();
		}
		private function fnBtnSoundPlay(e:MouseEvent=null):void
		{
			if(m_soundChannel)
			{
				m_soundChannel.stop();
				m_soundChannel=null
			}
			
			m_sound=null;
			m_sound=new Sound();
			
			/* var request:URLRequest = new URLRequest(m_soundCollection[m_nSelectedSound].url);
			m_sound.load(request);
			m_sound.addEventListener( IOErrorEvent.IO_ERROR, handleIOErr ); */
			if(m_nSelectedSound < m_soundLoader.length)
			{
				m_sound=m_soundLoader[m_nSelectedSound]
			}
			else
			{
				return;
			}
			if(m_nPosition!=0)
			{
				m_soundChannel = m_sound.play(m_nPosition);
				m_soundChannel.addEventListener(Event.SOUND_COMPLETE, fnSoundEnd); 
			}
			else
			{
				m_soundChannel=m_sound.play();
				//	mcSoundControlPanel.Txt_MusicName.text=m_soundCollection[m_nSelectedSound].name;
				m_soundChannel.addEventListener(Event.SOUND_COMPLETE, fnSoundEnd); 
				
			}
			if(m_bMute==true)
			{
				m_soundChannel.soundTransform=new SoundTransform(0)
			}
			else
			{
				m_soundChannel.soundTransform=new SoundTransform(m_nVolume)
			}
			
			m_bIsPlaying=true;
			
		}
		
		private  function fnSoundEnd(event:Event):void 
		{ 
			if(m_bLoop)
			{
				fnBtnSoundPlay();
			}
			else if(m_bRandom)
			{
				m_nSelectedSound=Math.round(Math.random()*(m_soundCollection.length-1));
				m_nPosition=0
				fnBtnSoundPlay();
			}
			else
			{
				//	fnBtnSoundNext();
			}
			
		}
		private function fnBtnSoundNext(e:MouseEvent=null):void
		{
			if(m_nSelectedSound==m_soundCollection.length-1)
			{
				m_nSelectedSound=0;
			}
			else
			{
				m_nSelectedSound++;
			}
			m_nPosition=0
			fnBtnSoundPlay();
		}
		
		public function fnVolumeChange():void
		{
			if(m_soundChannel)
			{
				
				if(m_bMute==true)
				{
					m_soundChannel.soundTransform=new SoundTransform(0)
					return;
				}
				m_soundChannel.soundTransform=new SoundTransform(m_nVolume)
			}
		}
		
		public function fnStopSound():void
		{
			if(m_soundChannel)
			{
				m_soundChannel.stop();
				m_soundChannel.removeEventListener(Event.SOUND_COMPLETE,fnSoundCompleteEventHandler);
				m_soundChannel=null
			}
		}
		
		
		public function fnPauseSound():void
		{
			if(m_soundChannel)
			{
				m_nPosition = m_soundChannel.position;
				m_soundChannel.stop();
			}
		}
		
		public function fnResume():void
		{
			if(m_soundChannel && m_Sound && m_bMute==false)
			{
				m_soundChannel = m_Sound.play(m_nPosition,1,new SoundTransform(m_nVolume));
			}	
		}
		
	
		
		
		
		private function fnSoundEventHandler(e:TTCSoundEvent):void
		{
			
			if(e.detail == TTCSoundEvent.SOUND_PLAY_EVENT)
			{
				fnSoundPlay(e.m_eventArgs)
			}
		}
		
		
		
		public function fnSelectSound(strName:String):void
		{
			fnStopSound();
			fnInit(strName);
			fnSoundDownLoad();
		}  
		
		
	
		[Embed(source="/resource/sound/result.mp3")] 
		private var soundResult:Class; 
		
		public function fnEmbeddedSoundResult():void 
		{ 
			var sound:Sound = new soundResult() as Sound; 
			fnSoundPlay(sound);
		} 
		
		/*[Embed(source="/resource/sound/button.mp3")] 
		private var soundButton:Class; 
		
		public function fnEmbeddedSoundButton():void 
		{ 
			var sound:Sound = new soundButton() as Sound; 
			fnSoundPlay(sound);
		} */
		
		[Embed(source="/resource/sound/number.mp3")] 
		private var soundNumber:Class; 
		
		public function fnEmbeddedSoundNumber():void 
		{ 
			var sound:Sound = new soundNumber() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/page.mp3")] 
		private var soundPage:Class; 
		
		public function fnEmbeddedSoundPage():void 
		{ 
			var sound:Sound = new soundPage() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/skip.mp3")] 
		private var soundSkip:Class; 
		
		public function fnEmbeddedSoundSkip():void 
		{ 
			var sound:Sound = new soundSkip() as Sound; 
			fnSoundPlay(sound);
		} 
		
		/*[Embed(source="/resource/sound/bubble.mp3")] 
		private var soundBubble:Class; 
		
		public function fnEmbeddedSoundBubble():void 
		{ 
			var sound:Sound = new soundBubble() as Sound; 
			fnSoundPlay(sound);
		} 
		*/
		[Embed(source="/resource/sound/wrong.mp3")] 
		private var soundWrong:Class; 
		
		public function fnEmbeddedSoundWrong():void 
		{ 
			var sound:Sound = new soundWrong() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/cheer.mp3")] 
		private var soundCheer:Class; 
		
		public function fnEmbeddedSoundCheer():void 
		{ 
			var sound:Sound = new soundCheer() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/coin.mp3")] 
		private var soundCoin:Class; 
		
		public function fnEmbeddedSoundCoin():void 
		{ 
			var sound:Sound = new soundCoin() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/000.mp3")] 
		private var sound000:Class; 
		
		public function fnEmbeddedSound000():void 
		{ 
			var sound:Sound = new sound000() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/001.mp3")] 
		private var sound001:Class; 
		
		public function fnEmbeddedSound001():void 
		{ 
			var sound:Sound = new sound001() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/003.mp3")] 
		private var sound003:Class; 
		
		public function fnEmbeddedSound003():void 
		{ 
			var sound:Sound = new sound003() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/020.mp3")] 
		private var sound020:Class; 
		
		public function fnEmbeddedSound020():void 
		{ 
			var sound:Sound = new sound020() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/055.mp3")] 
		private var sound055:Class; 
		
		public function fnEmbeddedSound055():void 
		{ 
			var sound:Sound = new sound055() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/206.mp3")] 
		private var sound206:Class; 
		
		public function fnEmbeddedSound206():void 
		{ 
			var sound:Sound = new sound206() as Sound; 
			fnSoundPlay(sound);
		} 
		
		
		[Embed(source="/resource/sound/213.mp3")] 
		private var sound213:Class; 
		
		public function fnEmbeddedSound213():void 
		{ 
			var sound:Sound = new sound213() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/261.mp3")] 
		private var sound261:Class; 
		
		public function fnEmbeddedSound261():void 
		{ 
			var sound:Sound = new sound261() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/281.mp3")] 
		private var sound281:Class; 
		
		public function fnEmbeddedSound281():void 
		{ 
			var sound:Sound = new sound281() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/357.mp3")] 
		private var sound357:Class; 
		
		public function fnEmbeddedSound357():void 
		{ 
			var sound:Sound = new sound357() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/381.mp3")] 
		private var sound381:Class; 
		
		public function fnEmbeddedSound381():void 
		{ 
			var sound:Sound = new sound381() as Sound; 
			fnSoundPlay(sound);
		} 
		
		
		[Embed(source="/resource/sound/410.mp3")] 
		private var sound410:Class; 
		
		public function fnEmbeddedSound410():void 
		{ 
			var sound:Sound = new sound410() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/win.mp3")] 
		private var soundWin:Class; 
		
		public function fnEmbeddedSoundWin():void 
		{ 
			var sound:Sound = new soundWin() as Sound; 
			fnSoundPlay(sound);
		} 
		
		[Embed(source="/resource/sound/lose.mp3")] 
		private var soundLose:Class; 
		
		public function fnEmbeddedSoundLose():void 
		{ 
			var sound:Sound = new soundLose() as Sound; 
			fnSoundPlay(sound);
		}
		
		[Embed(source="/resource/sound/se_maoudamashii_system46.mp3")] 
		private var se_maoudamashii_system46:Class; 
		
		public function fnEmbeddedSoundse_maoudamashii_system46():void 
		{ 
			var sound:Sound = new se_maoudamashii_system46() as Sound; 
			fnSoundPlay(sound);
		}
		
		private function fnSoundPlay(sound:Sound):void
		{
			m_Sound = sound
			if(m_bMute)
			{
				m_soundChannel = sound.play(0,0,new SoundTransform(0));
				return;
			}
			m_soundChannel = sound.play(0,0,new SoundTransform(m_nVolume));
			m_soundChannel.addEventListener(Event.SOUND_COMPLETE,fnSoundCompleteEventHandler);
		}
		
		private function fnSoundCompleteEventHandler(e:Event):void
		{
			m_Sound = null
			if(m_soundChannel)
			m_soundChannel.removeEventListener(Event.SOUND_COMPLETE,fnSoundCompleteEventHandler);
			var ev:TTCSoundEvent = new TTCSoundEvent(TTCSoundEvent.SOUND_EVENT,TTCSoundEvent.SOUND_PLAY_FINISHED_EVENT);
			dispatchEvent(ev);
		}
		
	
		public function fnDestory():void
		{
			if(m_sound)
			{
				m_sound.removeEventListener( IOErrorEvent.IO_ERROR, handleIOErr );
				m_sound.removeEventListener(Event.COMPLETE,fnDownloadSoundComplete)
				m_sound = new Sound();
				
			}
			if(m_soundChannel)
			{
				m_soundChannel.removeEventListener(Event.SOUND_COMPLETE, fnSoundEnd);
				m_soundChannel.stop();
				m_soundChannel=null;
			}
			m_nPosition = 0;
			m_nSelectedSound = 0;
		}
		
	}
}