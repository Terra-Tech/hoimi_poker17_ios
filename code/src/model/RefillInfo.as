package model
{
	import com.hoimi.lib.net.NetPacket;

	public class RefillInfo extends NetPacket
	{
		/** refill
		* 1:{"result" : 1, 'coin':10, 'player_coin':210}
		* -1:token param miss
		* -2:can't find token
		* -3:token expire
		**/
		public var coin:int = 0;
		public var player_coin:Number = 0;
		
		public function RefillInfo(refill:String)
		{
			super(refill);
		}
	}
}