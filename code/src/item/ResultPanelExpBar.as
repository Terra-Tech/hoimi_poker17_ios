package item
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	
	import event.GameEvent;
	
	import lib.TweenerBashAni;
	import lib.TweenerRepeat;
	
	public class ResultPanelExpBar extends Sprite
	{
		public var m_mc:Poker_Exp_Bar = new Poker_Exp_Bar();
		private var m_sp:Sprite = new Sprite();
		private var m_spLV:Sprite = new Sprite();
		private var m_spExp:Sprite = new Sprite();
		private var m_tweener:TweenerRepeat= new TweenerRepeat();
		private var exp:HoimiExpSmall = new HoimiExpSmall();
		private var lv:HoimiExpSmall = new HoimiExpSmall();
		private var expNext:HoimiExpSmall = new HoimiExpSmall();
		private var m_bLevelUp:Boolean =false;
		
		public function ResultPanelExpBar()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_sp);
			m_sp.addChild(m_spLV);
			m_sp.addChild(m_spExp);
			m_spExp.y = 30
			
			m_mc.txt_name.text = GameConfig.game_playerInfo.nickname;
			m_mc.txt_name.mouseEnabled=false;
			
			fnInit();
		}
		
		private function fnInit():void
		{
			//	m_tweener.fnStopTween();
			m_spLV.removeChildren();
			m_spExp.removeChildren();
			
			lv.fnSetValue(GameConfig.game_playerInfo.level);
			lv.y = 34
			if(GameConfig.game_playerInfo.level <10)
			{
				lv.x = 112
			}
			else if(GameConfig.game_playerInfo.level <100)
			{
				lv.x = 125
			}
			else
			{
				lv.x = 137
			}
			m_spLV.addChild(lv);
			
			var slash:Poker_Number_Exp_Small
			if(GameConfig.game_playerInfo.next_exp ==0)
			{
				exp.fnSetValue2(0);
				m_spExp.addChild(exp);
				slash = new Poker_Number_Exp_Small();
				slash.x = exp.width+22
				slash.gotoAndStop(13);
				m_spExp.addChild(slash);
				expNext.fnSetValue2(0);
				expNext.x = exp.width + slash.width
				m_spExp.addChild(expNext);
				m_spExp.x = 680 - m_spExp.width
				m_mc.exp.gotoAndStop(m_mc.exp.totalFrames);
			}
			else
			{
				exp.fnSetValue2(GameConfig.game_playerInfo.exp);
				m_spExp.addChild(exp);
				slash = new Poker_Number_Exp_Small();
				slash.x = exp.width+22
				slash.gotoAndStop(13);
				m_spExp.addChild(slash);
				expNext.fnSetValue2(GameConfig.game_playerInfo.next_exp);
				expNext.x = exp.width + slash.width
				m_spExp.addChild(expNext);
				m_spExp.x = 680 - m_spExp.width
				
				fnSetExpBar();
			}
			//fnSetResultExp(1000)
		}
		
		private function fnSetExpBar():void
		{
			var nExp:Number = GameConfig.game_playerInfo.exp;
			var nNextExp:Number =GameConfig.game_playerInfo.next_exp;
			var ratio:int = nExp/nNextExp * 100
			m_mc.exp.gotoAndStop(ratio);
		}
		
		public function fnSetResultExp(nLevel:int, nAddExp:int):void
		{
			var nConut:int 
			if(lv.m_nExp == nLevel)
			{
				nConut = nAddExp
				m_bLevelUp=false;
			}
			else
			{
				nConut = expNext.m_nExp - exp.m_nExp; 
				m_bLevelUp = true
			}
			trace("add exp count :" + nConut);
			if(nConut>0)
			{
				var nValue:int = Math.ceil(nConut/20);
				m_tweener.fnSetInit(nConut/nValue,0.02,nValue,fnItemTweenerComplete,fnAllPlsueTweenFinish);
			}
		}
		
		private function fnItemTweenerComplete(nValue:int):void
		{
			m_spExp.removeChildren();
			exp.fnAddExpValue(nValue);
			var nExp:Number = exp.m_nExp
			var nNextExp:Number = expNext.m_nExp;
			var ratio:int = nExp/nNextExp * 100
			m_mc.exp.gotoAndStop(ratio);
			
			m_spExp.addChild(exp);
			var slash:Poker_Number_Exp_Small = new Poker_Number_Exp_Small();
			slash.x = exp.width+22
			slash.gotoAndStop(13);
			m_spExp.addChild(slash);
			expNext.x = exp.width + slash.width
			m_spExp.addChild(expNext);
			m_spExp.x = 680 - m_spExp.width
		}
		
		
		
		private function fnAllPlsueTweenFinish():void
		{
			fnInit();
			var ev2:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_EXP_TWEEN_FINISH_EVENT);
			dispatchEvent(ev2);
			if(m_bLevelUp)
			{
				GameConfig.game_playerInfo.boardcast();
				var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_LEVEL_UP_EVENT);
				dispatchEvent(ev);
			}
			
			
		}
		
		public function fnDestory():void
		{
		}
	}
}