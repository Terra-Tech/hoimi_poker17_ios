package lib
{
	import flash.display.Sprite;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	
	public class TweenerBashAni extends Sprite
	{
		private var m_nIndex:int= 0;
		private var m_spAni:Sprite = new Sprite();	
		private var m_ar:Array = new Array(); 
		private var m_nStartX:Number;
		private var m_nStartY:Number;
		private var m_nEndX:Number;
		private var m_nEndY:Number;
		private var m_nScaleX:Number;
		private var m_nScaleY:Number;
		private var m_nAlpha:Number;
		private var m_nTime:Number
		private var m_strEffect:String = "easeOutExpo"
		private var m_callFnItem:Function
		private var m_callFn:Function;
		
		public function TweenerBashAni()
		{
			super();
			this.addChild(m_spAni);
		}
	
		public function fnSetInit(ar:Array,nStartX:Number,nStartY:Number,nEndX:Number,nEndY:Number,nScaleX:Number,nScaleY:Number,nAlpha:Number, nTime:Number, strEffect:String,callFnItem:Function, callFn:Function):void
		{
		
			m_nIndex = 0
			m_ar = ar;
			m_nStartX = nStartX
			m_nStartY = nStartY
			m_nEndX = nEndX
			m_nEndY = nEndY
			m_nScaleX = nScaleX;
			m_nScaleY = nScaleY;
			m_nAlpha = nAlpha;
			m_strEffect = strEffect;
			m_nTime = nTime
			m_callFnItem = callFnItem
			m_callFn = callFn;
			fnTweenerStart();
		}
		
		
		private function fnTweenerStart():void
		{
			
			var mc:Sprite = m_ar[m_nIndex]
			mc.x = m_nStartX;
			mc.y = m_nStartY;
			m_spAni.addChild(mc);
			Tweener.addTween(mc,{x:m_nEndX,y:m_nEndY,scaleX:m_nScaleX,scaleY:m_nScaleY,alpha:m_nAlpha,time:m_nTime,transition:m_strEffect,onComplete:fnTweenerFinish});
		}
		
		private function fnTweenerFinish():void
		{
			m_nIndex++
			m_spAni.removeChildren();
			m_callFnItem.call(m_callFnItem, m_ar.length,m_nIndex);
			
	
			if(m_nIndex<m_ar.length)
			{
				fnTweenerStart();
			}
			else
			{
				m_callFn();
			
			}
		}
		
		
		
		
	}
}