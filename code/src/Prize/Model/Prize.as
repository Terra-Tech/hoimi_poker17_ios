package Prize.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class Prize extends JsonObject
	{
		public var prize_id:int;
		public var player_coin:Number;
		public var player_diamond:Number;
		
		public function Prize(p:* = null){
			super(p);
		}
	}
}