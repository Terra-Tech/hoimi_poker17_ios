package com.hoimi.util
{
	import flash.globalization.DateTimeFormatter;

	public class DateUtil
	{
		public function DateUtil()
		{
		}
		
		static public function utcString2LocalString(utc:String, localzation:String):String
		{
			var d:Date, ary:Array, dAry:Array, tAry:Array;
			ary = utc.split('T');
			dAry = ary[0].split('-');
			tAry = ary[1].split('.');
			tAry = tAry[0].split(':');
			d = new Date();
			d.setUTCFullYear(dAry[0], dAry[1], dAry[2]);
			d.setUTCHours(tAry[0], tAry[1], tAry[2]);
			var dtf:DateTimeFormatter = new DateTimeFormatter(localzation);
			dtf.setDateTimePattern("yyyy-MM-dd hh:mm:ss");
			
			return dtf.format(d);
		}
	}
}