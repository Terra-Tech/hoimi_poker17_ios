package item
{
	import flash.display.Sprite;
	
	import lib.TiConvertNumber;
	
	public class ResultPanelDiamond extends Sprite
	{
		private var m_mc:Poker_Hoimi_Diamond = new Poker_Hoimi_Diamond();
		private var m_sp:Sprite = new Sprite();
		public function ResultPanelDiamond()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_sp);
			m_sp.x = 700;
			//fnSetValue(67722)
		}
		
		public function fnSetValue(value:Number):void
		{
			m_sp.removeChildren();
			var sp:Sprite = TiConvertNumber.fnConvertValueToUI(value,Poker_Number_Diamond,0,true,false);
			
			if(value > 0 && value > 9999999 || value < 0 && value < -9999999)
			{
				sp.scaleX = 0.4;
				sp.scaleY = 0.4
			}
			else
			{
				sp.scaleX = 0.5;
				sp.scaleY = 0.5
			}
			m_sp.addChild(sp);
			m_sp.x = 200+ m_sp.width/2 ;
			
			
		}
	}
}