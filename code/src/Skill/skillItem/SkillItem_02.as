package Skill.skillItem
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	
	import Skill.SkillBase;
	import Skill.SkillRaiseEvent;
	

	import lib.MultiLanguagleUISelect;
	
	public class SkillItem_02 extends SkillBase
	{
		private var m_mcSkill:Poker_Btn_Skill_02 = new Poker_Btn_Skill_02();
		
		public function SkillItem_02()
		{
			super();
			
			
			var mc:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mcSkill);
			m_mc = mc;
			this.addChild(m_mcSkill);
			this.addChild(m_sp);
			m_txtExplanation.text = "聚寶盆每分鐘補幣的金額";
			m_strSkillName = "[聚寶盆補幣金額]";
		}
		
		override protected function fnRaiseSkill():void
		{
			var ev:SkillRaiseEvent = new SkillRaiseEvent(1)
			EventManager.instance.trigger(ev);
		}
		
		override protected function fnDataDisplay(nInfo1:Number,nInfo2:Number):void
		{
			m_txtCurrent.text = nInfo1+""
			if(nInfo2 == -1)
			{
				m_txtNext.text = "----"
				m_mcSkill.arrow.visible=false;
			}
			else
			{
				m_txtNext.text = nInfo2+""	
			}
			
		}
	}
}