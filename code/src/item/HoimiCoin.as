package item
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	
	import lib.TiConvertNumber;
	
	public class HoimiCoin extends Sprite
	{
		private var m_sp:Sprite = new Sprite();
		public var m_nCoinValue:Number = 0
		public function HoimiCoin()
		{
			super();
			this.addChild(m_sp);
			//EventManager.instance.register(GameEvent.GAME_COIN_CHANGE_EVENT, fnCoinChange);
			//fnSetValue(12345678)
		}
		
		
		
		public function fnSetValue(value:Number):void
		{
			m_nCoinValue = value
			m_sp.removeChildren();
			var sp:Sprite = TiConvertNumber.fnConvertValueToUI(value,Poker_Number_Coin_Small,0,false,true);
			m_sp.addChild(sp);
			
		}
		
		public function fnSetValue2(value:Number):void
		{
			m_nCoinValue = value
			m_sp.removeChildren();
			var sp:Sprite = TiConvertNumber.fnConvertValueToUI(value,Poker_Number_Coin_Small,0,false,true);
			m_sp.addChild(sp);
			sp.x += sp.width
		}
		
		public function fnAddCoinValue(value:Number):void
		{
			m_nCoinValue += value
			fnSetValue(m_nCoinValue);
		}
		
		public function fnMinusCoinValue(value:Number):void
		{
			m_nCoinValue -= value
			fnSetValue(m_nCoinValue);
		}
		
		public function fnDestory():void
		{
		//	EventManager.instance.remove(GameEvent.GAME_COIN_CHANGE_EVENT, fnCoinChange);
		}
	}
}