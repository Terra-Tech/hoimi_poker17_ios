package com.hoimi.util
{
	public class VersionUtil
	{
		/* 
			-2:not vaild value
			-1:local newer than remote
		    0:equal
			1:remote newer than local
		*/
		static public function compare(local:String ,remote:String):int
		{
			if(local.indexOf(".") == -1 || remote.indexOf(".") == -1)
				return -2;
			var l_:Array = local.split(".");
			var r_:Array = remote.split(".");
			if(l_.length != r_.length)
				return -2;
			var ln_:int, rn_:int;
			var len_:uint = remote.length;
			for(var i:int = 0;i<len_;i++)
			{
				ln_ = int(l_[i]);
				rn_ = int(r_[i]);
				if(ln_ > rn_)
					return -1;
				else if(ln_ < rn_)
					return 1;
			}
			
			return 0;
		}
	}
}