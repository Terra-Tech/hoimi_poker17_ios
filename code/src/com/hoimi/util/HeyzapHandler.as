package com.hoimi.util
{
	import com.heyzap.sdk.HeyzapEvent;
	import com.heyzap.sdk.ads.BannerAd;
	import com.heyzap.sdk.ads.HeyzapAds;
	import com.heyzap.sdk.ads.IncentivizedAd;
	import com.heyzap.sdk.ads.InterstitialAd;
	import com.heyzap.sdk.ads.VideoAd;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;

	public class HeyzapHandler extends EventDispatcher
	{
		static public const EVENT_REWARD_COMPLETED:String = "RewardCompleted";
		static public const EVENT_REWARD_COLSED:String = "RewardClosed";
		static public const EVENT_REWARD_CACHED:String = "RewardCached";
		static public const EVENT_REWARD_LOAD_FAILED:String = "RewardLoadFailed";
		
		static private var _instance:HeyzapHandler;
		
		static public function get instance():HeyzapHandler
		{
			if(!_instance)
			{
				_instance = new HeyzapHandler(new singletonforcer);
			}
			return _instance;
		}
		
		public function HeyzapHandler(innerClass:singletonforcer){}
		
		public function init(publiherID:String):void
		{
			try
			{ 
				HeyzapAds.getInstance().start(publiherID);
				
				cacheInterstitial();
				
				cacheRewardVideo();
				
				cacheVideo();
				
				// -OR- initialize including both ios and android ids for multiplatform apps 
				// Vungle.create(["your_ios_vungle_id","your_android_vungle_id"]); 
			} catch (error:Error) {
				// could not create extension. Are you running on something besides iOS/Android?
				trace("HeyzapAds init error " +  error.message);
			}
		}
		
		public function destroy():void
		{
			IncentivizedAd.getInstance().removeEventListener(HeyzapEvent.FETCH_FAILED, onRewardVideoFetchFailed);
			IncentivizedAd.getInstance().removeEventListener(HeyzapEvent.DID_HIDE, onRewardVideoHide);
			IncentivizedAd.getInstance().removeEventListener(HeyzapEvent.ON_LOADED, onRewardVideoLoaded);
			IncentivizedAd.getInstance().removeEventListener(HeyzapEvent.IS_COMPLETED, onRewardVideoCompleted);
		}
		
		public function showTestPanel():void
		{
			HeyzapAds.getInstance().showMediationTestSuite();
		}
		
		public function cacheVideo():void
		{
			VideoAd.getInstance().fetch();
		}
		
		public function showVideo():Boolean
		{
			if (VideoAd.getInstance().isAvailable()) 
			{
				VideoAd.getInstance().show();
				cacheVideo();
				return true;
			}
			
			return false;
		}
		
		public function RewardVideoAvaible():Boolean
		{
			return IncentivizedAd.getInstance().isAvailable();
		}
		
		public function cacheRewardVideo():void
		{
			IncentivizedAd.getInstance().addEventListener(HeyzapEvent.FETCH_FAILED, onRewardVideoFetchFailed);
			
			IncentivizedAd.getInstance().fetch();
		}
		
		public function showRewardVideo():Boolean
		{
			if (IncentivizedAd.getInstance().isAvailable()) 
			{
				IncentivizedAd.getInstance().addEventListener(HeyzapEvent.IS_COMPLETED, onRewardVideoCompleted);
				IncentivizedAd.getInstance().addEventListener(HeyzapEvent.DID_HIDE, onRewardVideoHide);
				IncentivizedAd.getInstance().addEventListener(HeyzapEvent.ON_LOADED, onRewardVideoLoaded);
				
				IncentivizedAd.getInstance().show();
				
				cacheRewardVideo();
				
				return true;
			}
			return false;
		}
		
		private function onRewardVideoFetchFailed(e:HeyzapEvent):void
		{
			IncentivizedAd.getInstance().removeEventListener(HeyzapEvent.FETCH_FAILED, onRewardVideoFetchFailed);
			
			e.preventDefault();
			
			this.dispatchEvent(new Event(HeyzapHandler.EVENT_REWARD_LOAD_FAILED));
		}
		
		private function onRewardVideoHide(e:HeyzapEvent):void
		{
			IncentivizedAd.getInstance().removeEventListener(HeyzapEvent.DID_HIDE, onRewardVideoHide);
			
			e.preventDefault();
			
			this.dispatchEvent(new Event(HeyzapHandler.EVENT_REWARD_COLSED));
		}
		
		private function onRewardVideoLoaded(e:HeyzapEvent):void
		{
			IncentivizedAd.getInstance().removeEventListener(HeyzapEvent.ON_LOADED, onRewardVideoLoaded);
			
			e.preventDefault();
			
			this.dispatchEvent(new Event(HeyzapHandler.EVENT_REWARD_CACHED));
		}
		
		private function onRewardVideoCompleted(e:HeyzapEvent):void
		{
			IncentivizedAd.getInstance().removeEventListener(HeyzapEvent.IS_COMPLETED, onRewardVideoCompleted);
			
			e.preventDefault();
			
			this.dispatchEvent(new Event(HeyzapHandler.EVENT_REWARD_COMPLETED));
		}
		
		public function showBannerTop():void
		{
			BannerAd.getInstance().show(BannerAd.POSITION_TOP);
		}
		
		public function showBannerBottom():void
		{
			BannerAd.getInstance().show(BannerAd.POSITION_BOTTOM);
		}
		
		public function hideBanner():void
		{
			BannerAd.getInstance().hide();
		}
		
		public function cacheInterstitial():void
		{
			InterstitialAd.getInstance().fetch();
		}
		
		public function showInterstitialAd():Boolean
		{
			if(InterstitialAd.getInstance().isAvailable())
			{
				InterstitialAd.getInstance().show();
				
				cacheInterstitial();
				return true;
			}
			return false;
		}
	}
}

class singletonforcer{}