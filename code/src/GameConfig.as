package
{
	
	import com.hoimi.util.DeviceUtil;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	
	import Enum.EnumDevice;
	import Enum.EnumLang;
	import Enum.EnumLocalSave;
	import Enum.EnumURL;
	
	import event.GameEvent;
	
	import lib.TTCModeCheck;
	
	import model.BuyingInfo;
	import model.FirstBetResult1;
	import model.FirstBetResult2;
	import model.GameTable;
	import model.IAPKey;
	import model.IAPProductInfo;
	import model.OpenBetInfo;
	import model.PickInfo;
	import model.PlayerInfo;
	import model.PlayerRecord;
	import model.SecondBetInfo;
	
	

	public class GameConfig
	{
		public function GameConfig()
		{
		}
		public static var APP_URL:String = "http://onelink.to/7mjnd2";
		public static const APP_ID:String = "air.com.hoimi.17poker";
		public static const APPLE_ID:String = "1073840752";
		public static const GOOGLE_ANALYTICS_ID:String = "UA-62997900-8"
//		public static const SERVER_URL:String = "http://localhost:8888";
		public static const SERVER_URL:String = EnumURL.SERVER_PRODUCTION_URL;	
		private static var _DEVICE:String	
		public static var device_Language:String = EnumLang.EN;
		private static var _server_Language:String
		public static var stage_displacement_x:Number = 0
		public static var stage_scale_ratio:Number = 1	
		public static var object_adjuct_position_x:Number = 0	
		private static var _UUID:String ="";
		public static var first_sign_in:Boolean = false;
		public static var game_table:GameTable; 	
		public static var game_tableList:Array; 	
		public static var game_playerInfo:PlayerInfo
		public static var game_openBetInfo:OpenBetInfo
		public static var game_pickInfo:PickInfo
		public static var game_1stBetInfo1:FirstBetResult1;
		public static var game_1stBetInfo2:FirstBetResult2
		public static var game_buyingInfo:BuyingInfo;
		public static var game_2ndBetInfo:SecondBetInfo;
		private static var _game_playerRecord:PlayerRecord
		public static var game_iapProductInfo:IAPProductInfo;
		public static var game_iapKey:IAPKey;
		public static var bOther:Boolean=false;
		public static var bAgain:Boolean=false;
		public static var bFacebookReady:Boolean = false;
		public static const SESSION_TIME:int = 10000;
		public static var appPause:Boolean = false
		public static const ar_enemy_leave:Array = [5,6,7,8,9,10,11,12]	
		public static var change_enemy_remind:int = 0	
		
		
		public static function get UUID():String
		{
			return _UUID;
		}

		public static function set UUID(value:String):void
		{
			_UUID = value;
			LocalSaver.instance.save(EnumLocalSave.UUID,value);
		}

		public static function get game_playerRecord():PlayerRecord
		{
			return _game_playerRecord;
		}

		public static function set game_playerRecord(value:PlayerRecord):void
		{
			_game_playerRecord = value;
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_PLAYER_RECORD_CHANGE));
		}

		public static function get server_Language():String
		{
			if(device_Language == EnumLang.EN)
			{
				return EnumLang.SERVER_EN
			}
			else if(device_Language == EnumLang.TW || device_Language == EnumLang.CH || device_Language == EnumLang.HK)
			{
				return EnumLang.SERVER_TW;
			}
			return EnumLang.SERVER_EN;
		}

		

		public static function get DEVICE():String
		{
			if(!DeviceUtil.getInstance().isOnDevice)
			{
				return EnumDevice.ANDROID
			}else{
				if(DeviceUtil.getInstance().isOnAndroid)
				{
					return EnumDevice.ANDROID
				}
				else
				{
					return EnumDevice.IOS;
				}	
			}
		}


		public static function fnClearBattelData():void
		{
			 game_pickInfo = null
			 game_1stBetInfo1 = null
			 game_1stBetInfo2 = null
			 game_buyingInfo = null;
			 game_2ndBetInfo = null;
		}
	}
}