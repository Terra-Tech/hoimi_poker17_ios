package Prize.Model
{
	import com.hoimi.lib.net.JsonObject;
	import flash.system.Capabilities;
	import com.hoimi.util.DateUtil;
	
	public class PrizeList extends JsonObject
	{
		public var prizes:Array = [];
		
		public function PrizeList(pl:* = null){
			super(pl);
		}
		
		override public function byObject(obj:Object):void
		{
			for (var p:String in obj)
			{
				if(this.hasOwnProperty(p))
				{
					if(this[p] is JsonObject)
						this[p].input(obj[p]);
					else
						this[p] = obj[p];
				}
			}
			var i:int, len:int = prizes.length, pi:PrizeInfo;
			var d:Date, ary:Array, dAry:Array, tAry:Array;
			for(i = 0 ; i < len ;i++)
			{
				pi = new PrizeInfo(prizes[i]);
				pi.create_date = DateUtil.utcString2LocalString(pi.create_date, Capabilities.language);
				prizes[i] = pi;
			}
			
		}
	}
}