package FB.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class FacebookPicture extends JsonObject
	{
		public var data:Data = new Data();
		
		public function FacebookPicture(obj:*=null)
		{
			super(obj);
		}
	}
}
import com.hoimi.lib.net.JsonObject;

class Data extends JsonObject{
	public var is_silhouette:Boolean;
	public var url:String;
	public function Data(data:* = null){
		
	}
}