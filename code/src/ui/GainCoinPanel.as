package ui
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import Skill.Model.Skill;
	
	import base.TiDisableTextField;
	
	import event.GameEvent;
	
	import handler.TTCSoundHandler;
	
	import item.CheckSelect;
	import item.GainCoinItem;
	import item.MsgGainCoin;
	
	
	public class GainCoinPanel extends Sprite
	{
		private var m_mc:Poker_Gain_Coin_Panel = new Poker_Gain_Coin_Panel();
		private var m_mcMask:Poker_Mask_Base = new Poker_Mask_Base();
		private var m_arItem:Array = new Array();
		private var m_coMsgGainCoin:MsgGainCoin = new MsgGainCoin();
		private var m_coCheck:CheckSelect = new CheckSelect();
		private var m_bInit:Boolean = false;
		
		public function GainCoinPanel()
		{
			super();
			this.addChild(m_mcMask);
			this.addChild(m_mc);
			this.addChild(m_coMsgGainCoin);
			this.addChild(m_coCheck);
			m_coCheck.x = 80
			m_coCheck.y = 317 
			m_mcMask.x = 360
			m_mcMask.alpha = 0.5
			m_mc.y = 160;
			m_mc.x = 360
			m_coMsgGainCoin.x = 360
			m_coMsgGainCoin.y = 1125
			this.visible =false
			m_coMsgGainCoin.visible=false;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_close);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_iap);
			m_mc.btn_close.addEventListener(MouseEvent.CLICK,fnClickClose);
			EventManager.instance.register(GameEvent.GAME_GAIN_COIN_EVENT,fnDisplayIapPanel);
			m_mc.btn_iap.addEventListener(MouseEvent.CLICK,fnIapClick);
			m_coCheck.addEventListener(MouseEvent.CLICK,fnCheckSelected);
			
			m_mc.txt_check.text = "跳過確認視窗";
			EventManager.instance.register(GameEvent.GAME_SECTION_REFRESH,fnRefreshData);		
		}
		
		private function fnIapClick(e:MouseEvent):void
		{
			this.visible =false;
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_IAP_EVENT));
		}
		
		private function fnDisplayIapPanel(e:GameEvent):void
		{
			this.visible =true
			var skillInfo1:Skill =  GameConfig.game_playerInfo.skills[2]
			m_mc.txt_gain_coin.txt_1.text = skillInfo1.level
			m_mc.txt_gain_coin.txt_2.text = skillInfo1.value/10+" %"
			var skillInfo2:Skill =  GameConfig.game_playerInfo.skills[3]
			m_mc.txt_gain_coin.txt_3.text = skillInfo2.level
			m_mc.txt_gain_coin.txt_4.text = "x "+skillInfo2.value
			fnInitGainCoinItem();
			fnCheckItemEnable();
			fnCheckSelected();
			
		}
		
		private function fnClickClose(e:MouseEvent):void
		{
			this.visible =false
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound003();
		}
		
		private function fnInitGainCoinItem():void
		{
			if(m_bInit)
			{
				return;
			}
			for(var i:int = 0; i < 3;i++)
			{
				var gainItem:GainCoinItem
				if(i==0)
				{
					gainItem = new GainCoinItem(10,10*GameConfig.game_playerInfo.exchange_rate);
				}
				if(i==1)
				{
					gainItem = new GainCoinItem(100,100*GameConfig.game_playerInfo.exchange_rate);
				}
				if(i==2)
				{
					gainItem = new GainCoinItem(1000,1000*GameConfig.game_playerInfo.exchange_rate);
				}
				/*if(i==3)
				{
					gainItem = new GainCoinItem(10000,10000*GameConfig.game_playerInfo.exchange_rate);
				}*/
				gainItem.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
				gainItem.x = 360;
				gainItem.y = i *180 + 460;
				this.addChild(gainItem);
				m_arItem.push(gainItem);
			}
			m_bInit = true;
		}
		
		private function fnGameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_DIAMIOND_CHANGE_COIN_FINISH_EVENT)
			{
				var ar:Array = e.m_eventArgs
				
				m_coMsgGainCoin.fnSetData(ar[0],ar[1],ar[2]);
				m_coMsgGainCoin.visible=true;
				TTCSoundHandler.fnGetInstance().fnEmbeddedSoundCoin();
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_SECTION_REFRESH));
			}
		}
		
		private function fnCheckSelected(e:MouseEvent = null):void
		{
			var gainItem:GainCoinItem
			for(var i:int = 0; i< m_arItem.length ;i++)
			{
				gainItem = m_arItem[i] as GainCoinItem
				gainItem.m_bSelected = m_coCheck.m_bSelected;	
			}
			
		}
		
		private function fnCheckItemEnable():void
		{
			var gainItem:GainCoinItem
			for(var i:int = 0; i< m_arItem.length ;i++)
			{
				gainItem = m_arItem[i] as GainCoinItem
				gainItem.fnCheckButtonEnable();
			}
		}
		
		private function fnRefreshData(e:GameEvent):void
		{
			fnCheckItemEnable();
		}
	}
}