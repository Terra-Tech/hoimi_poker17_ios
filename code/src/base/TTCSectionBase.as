package base
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	
	
	import event.GameEvent;
	import event.TTCSubtitleEvent;
	import event.TTCVoiceEvent;
	
	import handler.TTCVoiceHandler;
	
	

	
	
	public class TTCSectionBase extends Sprite
	{
		public function TTCSectionBase()
		{
			super();
			fnInit();
		}
		
		private function fnInit():void
		{
			
		
			this.addEventListener(Event.ADDED_TO_STAGE,fnAddToStageEventHandler);	
			TTCVoiceHandler.fnGetInstance().addEventListener(TTCVoiceEvent.VOICE_EVENT,fnVoiceEventHandler);
		}
		
		protected function fnAddToStageEventHandler(e:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,fnAddToStageEventHandler);	
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_ADD_TO_STAGE_EVENT,true);
			dispatchEvent(ev);
		}
		
		protected function fnVoiceEventHandler(e:TTCVoiceEvent):void
		{
			if(e.detail == TTCVoiceEvent.VOICE_CONFIT_COMPLETE)
			{
			
			}
			else if(e.detail == TTCVoiceEvent.VOICE_DOWNLOAD_COMPLETE)
			{
				var ev:TTCVoiceEvent = new TTCVoiceEvent(TTCVoiceEvent.VOICE_EVENT,TTCVoiceEvent.VOICE_DOWNLOAD_COMPLETE);
				dispatchEvent(ev);
				TTCVoiceHandler.fnGetInstance().removeEventListener(TTCVoiceEvent.VOICE_EVENT,fnVoiceEventHandler);
			}
			
		}
		
		
		private function fnSubtitleEventHandler(e:TTCSubtitleEvent):void
		{
			switch(e.detail)
			{
				case TTCSubtitleEvent.SUBTITLE_FINISH_EVENT:
					fnSubtitleFinishHandler();
					break;
				case TTCSubtitleEvent.SUBTITLE_NEXT_EVENT:
					fnNextSubtitleHandler(e.m_eventArgs);
					break
			}
		}
		
		protected function fnSubtitleFinishHandler():void
		{
		}
		
		public function fnResize():void
		{
			
		}
		
		protected function fnNextSubtitleHandler(nCurrentSentenceIndex:int):void
		{
			//be override
		}
		
		public function fnPause():void
		{
			//be override
		}
		
		public function fnResume():void
		{
			//be override
		}
		
		public function fnSkip():void
		{
			fnExit();
			//be override
		}
		
		public function fnExit():void
		{
			//be override
		}
		
		public function fnReplay():void
		{
			fnExit();
			//be override
		}
		
	}
}