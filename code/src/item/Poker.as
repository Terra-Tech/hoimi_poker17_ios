package item
{
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import caurina.transitions.Tweener;
	
	import handler.TTCSoundHandler;
	
	public class Poker extends Sprite
	{
		/*		Mod 	
		Divid	0   1   2   3
		---------------
			0	CJ  DJ  HJ  SJ
				0   1   2   3 
			1	CQ  DQ  HQ  SQ
				4   5   6   7 
			2	CK  DK  HK  SK
				8   9   10  11
			3	CA  DA  HA  SA
				12  13  14  15 
			4	Jorker
				16  
		*/
		private var m_mcPorkerBase:Poker_Card_Base = new Poker_Card_Base();
		private var m_mcPorker:Poker_Card = new Poker_Card();
		private var _m_nCardIndex:int = 0
		private var _m_nDivid:int = 0;	
		private var _m_nMod:int = 0;
		public var m_bSelected:Boolean=false;	
		public var m_nCardIndexAtSort:int
		private var m_coArrow:PorkerArrow = new PorkerArrow();
		
		public function Poker(nIndex:int)
		{
			super();
			this.addChild(m_mcPorker);
			this.addChild(m_mcPorkerBase)
			this.addChild(m_coArrow);
			m_coArrow.y = -158
			m_nCardIndex = nIndex
			m_mcPorker.gotoAndStop(nIndex+1);
			var nDivid:int = nIndex/4
			var nMod:int = nIndex%4
			m_nDivid = nDivid;
			m_nMod = nMod;
			m_mcPorker.eye.visible=false;
			m_coArrow.visible=false;
		}
		
		
		
		public function get m_nMod():int
		{
			return _m_nMod;
		}

		public function set m_nMod(value:int):void
		{
			_m_nMod = value;
		}

		public function get m_nDivid():int
		{
			return _m_nDivid;
		}

		public function set m_nDivid(value:int):void
		{
			_m_nDivid = value;
		}

		public function get m_nCardIndex():int
		{
			return _m_nCardIndex;
		}

		public function set m_nCardIndex(value:int):void
		{
			_m_nCardIndex = value;
		}

		public function fnCardShow():void
		{
			m_mcPorkerBase.visible = false;
			m_coArrow.visible=false
		}
		
		public function fnEnemyCardShow(nIndex:int):void
		{
			m_coArrow.visible=false
			m_mcPorkerBase.visible = false;
			m_nCardIndex = nIndex
			m_mcPorker.gotoAndStop(nIndex+1);
			var nDivid:int = nIndex/4
			var nMod:int = nIndex%4
			m_nDivid = nDivid;
			m_nMod = nMod;
		}
		
	
		
		public function fnCardHide():void
		{
			m_mcPorkerBase.visible=true;
		}
		
		public function fnCardSelect():void
		{
			if(m_mcPorker.y == 0)
			{
				TTCSoundHandler.fnGetInstance().fnEmbeddedSound020();
				m_mcPorker.y = -50; 
				m_coArrow.visible=false
				m_bSelected = true;
			}
			
		}
		
		public function fnCardDisSelect():void
		{
			if(m_mcPorker.y == -50)
			{
				TTCSoundHandler.fnGetInstance().fnEmbeddedSound020();
				m_mcPorker.y = 0; 
				m_coArrow.visible=true
				m_bSelected = false;
			}
		}
		
		public function fnShowEye():void
		{
			m_mcPorker.eye.visible=true;
			m_mcPorker.y = 0; 
		}
		
		public function fnHideEye():void
		{
			m_mcPorker.eye.visible=false;
		}
		
		public function fnShowArrow():void
		{
			m_coArrow.visible=true;
		}
		
		public function fnHideArrow():void
		{
			m_coArrow.visible=false;
		}
		
	}
}