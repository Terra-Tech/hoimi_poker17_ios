package item
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	
	import Enum.EnumStatus;
	
	import caurina.transitions.Tweener;
	
	import event.PokerEvent;
	
	import handler.TxtLangTransfer;
	
	import lib.MultiLanguagleUISelect;
	
	public class MsgGameStatus extends Sprite
	{
		private var _m_strGameStatus:String ="";	
		private var m_mc:MovieClip = new MovieClip();
		private var m_mcTxt:Poker_Msg_Game_Status2 = new Poker_Msg_Game_Status2();
		private var m_timer:Timer
		private var m_mcTxtAni:Poker_Msg_Ani = new Poker_Msg_Ani();
		public function MsgGameStatus()
		{
			super();
			this.addChild(m_mcTxt);
			var mcStatus:Poker_Msg_Game_Status = new Poker_Msg_Game_Status();
			var mc:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(mcStatus);
			m_mc = mc
			this.addChild(mcStatus);
		
			m_mc.x=-720;
			m_mcTxt.x=720;
			m_mcTxtAni.visible=false
			m_mcTxtAni.x = -217.5
			m_mcTxtAni.y = 520	
			this.addChild(m_mcTxtAni);
		}
		
		public function get m_strGameStatus():String
		{
			return _m_strGameStatus;
		}

		public function set m_strGameStatus(value:String):void
		{
			_m_strGameStatus = value;
			switch(value)
			{
				case EnumStatus.STATUS_BET:
					fnBet();
					break;
				case EnumStatus.STATUS_DEAL:
					fnDeal();
					break;
				case EnumStatus.STATUS_DEAL_FINISH:
					fnDealFinish();
					break;
				case EnumStatus.STATUS_PICK:
					fnPick();
					break;
				case EnumStatus.STATUS_PICK_FINISH:
					fnPickFinish();
					break;
				case EnumStatus.STATUS_1STBET:
					fn1stBet()
					break;
				case EnumStatus.STATUS_CALL_DEAL:
				case EnumStatus.STATUS_CALL_DEAL2:
					fnCallDeal();
					break;
				case EnumStatus.STATUS_BUYING:
					fnBuying()
					break;
				case EnumStatus.STATUS_2NDBET:
					fn2ndBet()
					break;
				case EnumStatus.STATUS_OPEN_CARD:
					fnOpenCard()
					break;
				case EnumStatus.STATUS_PLAYER_WIN:
					fnShowWin()
					break;
				case EnumStatus.STATUS_PLAYER_LOSE:
					fnShowLose()
					break;
				case EnumStatus.STATUS_SHOW_RESULT_PANEL:
					fnShowResultPanel();
					break;
			}
		}
	

		private function fnBet():void
		{
			m_timer = new Timer(2000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			m_mc.gotoAndStop(1);
			m_mcTxt.visible=true;
			fnSetMsg(TxtLangTransfer.OPEN_BET)
			m_timer.start();
			setTimeout(function():void 
			{
				fnTweenHide();
			}, 3000)
		}
		
	
		
		private function fnDeal():void
		{
			m_timer = new Timer(2000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			m_timer.start();
		}
		
		private function fnDealFinish():void
		{
			m_timer = new Timer(1000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			fnTweenHide();
			m_timer.start();
		}
		
		private function fnPick():void
		{
			m_mc.gotoAndStop(2);
			m_timer = new Timer(2000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			m_mcTxt.visible=true;
			fnSetMsg(TxtLangTransfer.PICK_TIME)
			m_timer.start();
		
		}
		
		private function fnPickFinish():void
		{
			fnTweenHide();
			m_timer = new Timer(1000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			m_timer.start();
		}
		
		private function fn1stBet():void
		{
			m_mc.gotoAndStop(3);
			m_mcTxt.visible=true;
			fnSetMsg(TxtLangTransfer.FIRST_SECOND_BET_TIME)
			m_timer = new Timer(2000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			m_timer.start();
			setTimeout(function():void 
			{
				fnTweenHide();
			}, 2000)
		
		}
		
		
		private function fnBuying():void
		{
			m_timer = new Timer(2000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			m_mc.gotoAndStop(4);
			m_mcTxt.visible=true;
			fnSetMsg(TxtLangTransfer.BUYING)
			m_timer.start();
			setTimeout(function():void 
			{
				fnTweenHide();
			}, 3000)
		}
		
		private function fn2ndBet():void
		{
			m_mc.gotoAndStop(5);
			m_mcTxt.visible=true;
			fnSetMsg(TxtLangTransfer.FIRST_SECOND_BET_TIME)
			m_timer = new Timer(2000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			m_timer.start();
			setTimeout(function():void 
			{
				fnTweenHide();
			}, 2000)
		}
		
		private function fnCallDeal():void
		{
			m_timer = new Timer(1000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			//m_mc.gotoAndStop(6);
			//fnSetMsg(TxtLangTransfer.CALL_DEAL)
			m_timer.reset();
			m_timer.start();
		}
		
		private function fnOpenCard():void
		{
			m_timer = new Timer(1000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			fnTweenHide();
			m_timer.reset();
			m_timer.start();
		}
	
		
		private function fnShowWin():void
		{
			m_mcTxtAni.y = 600;
			m_timer = new Timer(10,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			m_mc.gotoAndStop(7);
			m_mcTxt.visible=false;
			fnSetMsg("")
			m_timer.start();
		}
		
		private function fnShowLose():void
		{
			m_mcTxtAni.y = 600;
			m_timer = new Timer(10,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);	
			m_mc.gotoAndStop(8);
			m_mcTxt.visible=false;
			fnSetMsg("")
			m_timer.start();
		}
		
		private function fnShowResultPanel():void
		{
			m_timer = new Timer(2000,1)
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTiemrComplete);
			m_timer.start();
		}
		
		private function fnSetMsg(strMsg:String):void
		{
			m_mcTxt.txt.text = strMsg;
			fnTweenDisplay();
		}
		
		private function fnTweenDisplay():void
		{
			m_mc.x = -800 
			Tweener.addTween(m_mc,{x:0,time:0.15,transition:"easeOutExpo",onComplete:fnDisplayFinish});
			m_mcTxtAni.visible=true;
			m_mcTxtAni.gotoAndPlay(2);
				
		}
		
		private function fnDisplayFinish():void
		{
			m_mcTxt.x = 720 
			m_mcTxt.gotoAndPlay(2)
			Tweener.addTween(m_mcTxt,{x:0,time:0.15,transition:"easeOutExpo"});
		}
		
		private function fnTweenHide():void
		{
			Tweener.addTween(m_mc,{x:720,time:0.15,transition:"easeOutExpo",onComplete:fnTweenHideFinish});
			Tweener.addTween(m_mcTxt,{x:-800,time:0.15,transition:"easeOutExpo"});
		}
		
		private function fnTweenHideFinish():void
		{
		
		}
		
		private function fnTiemrComplete(e:TimerEvent):void
		{
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_STATUS_GAP_TIME_EVENT)
			dispatchEvent(ev);
		}
			
	}
}