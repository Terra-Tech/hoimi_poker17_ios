package Skill.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class Skill extends JsonObject
	{
		public var level:int = 0;
		public var value:Number = 0;
		public var coin:Number = 0;
		public var diamond:int = 0;
		
		public function Skill(obj:*=null)
		{
			super(obj);
		}
	}
}