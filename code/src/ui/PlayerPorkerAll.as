package ui
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	import event.PokerEvent;
	
	import handler.TTCSoundHandler;
	
	import item.Poker;
	
	import lib.TweenerBashAni;
	
	import model.BuyingInfo;
	
	
	public class PlayerPorkerAll extends Sprite
	{
		private const PICK_NUMBER:int = 2;
		private var m_sp:Sprite = new Sprite();
		private var m_cardA:Poker
		private var m_cardB:Poker
		private var m_cardC:Poker
		private var m_cardD:Poker
		private var m_cardE:Poker
		private var m_nIndexCard:int = 0;
		private var m_arCard:Array 
		private var m_bBuying:Boolean=false;
		private var m_arBuying:Array = new Array();
		private var m_tweenerBuying:TweenerBashAni = new TweenerBashAni();
		private var m_pickQueue:Array = [];
		
		public function PlayerPorkerAll()
		{
			super();
			this.addChild(m_sp);
			
		}
		
		public function fnNewGame():void
		{
			m_sp.removeChildren();
			m_bBuying =false;
			m_cardA = null;
			m_cardB = null;
			m_cardC = null;
			m_cardD = null;
			m_cardE = null;
		}
		
		public function fnDeal(ar:Array,bPlayer:Boolean):void
		{
			m_sp.removeChildren();
			m_arCard = new Array();
			var porker:Poker
			m_nIndexCard = 0;
			for(var i:int = 0 ; i< 5 ;i++)
			{
				porker = new Poker(ar[i])
		//		porker.m_nCardIndexAtSort = i
				if(bPlayer)	
				{
					porker.fnCardShow();
				}
				porker.x = (i-2)*142 +10
				m_arCard.push(porker);
				//m_sp.addChild(porker);
				porker.addEventListener(MouseEvent.CLICK,fnCardSelect);
				
			}
			fnTweenCardAddToStage();
			m_sp.mouseChildren = false;
			m_sp.mouseEnabled = false;
		}
		
		private function fnTweenCardAddToStage():void
		{
			var card:Poker = m_arCard[m_nIndexCard] as Poker
			var nX:Number = card.x
			card.x = -720
			m_sp.addChild(card);
			Tweener.addTween(card,{x:nX,time:0.3,transition:"easeOutBack",onComplete:fnTweenCardAddtoStageFinish});
		}
		
		private function fnTweenCardAddtoStageFinish():void
		{
			m_nIndexCard++
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound055();
			if(m_nIndexCard<5)
			{
				fnTweenCardAddToStage();
			}
			else
			{
				setTimeout(function():void {fnSortCard()}, 500)
			}
		}
		
		private function fnSortCard():void
		{
			GameConfig.game_openBetInfo.player_cards.sort(Array.NUMERIC);
			var arCardIndex:Array = GameConfig.game_openBetInfo.player_cards
			fnSortCardHandler(arCardIndex,m_arCard);
			
			setTimeout(function():void 
			{
				var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_DEAL_FINISH_EVENT,true);
				dispatchEvent(ev)
			}, 1000)
		}
		
		private function fnSortCardHandler(arCardIndex:Array,arCard:Array):void
		{
			
			//GameConfig.game_openBetInfo.player_cards.reverse();
			var porker:Poker
			var arNew:Array = new Array();
			for(var i:int = 0 ; i< 5 ;i++)
			{
				var nIndex:int = arCardIndex[i]
				for(var j:int = 0; j<5 ;j++)
				{
					porker = arCard[j] as Poker
					if(nIndex == porker.m_nCardIndex)
					{
						var newX:Number = (i-2)*142 +10
							porker.m_nCardIndexAtSort = i	
						fnTweenSortCard(porker,newX);
						arNew.push(porker);
					}
				}
			}
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound055();
			m_arCard = arNew;
		}
		
		private function fnTweenSortCard(card:Poker,nX:Number):void
		{
			
			Tweener.addTween(card,{x:nX,time:0.15,transition:"easeOutExpo"});
		}
		
		
	
		public function fnPickCard():void
		{
			m_sp.mouseChildren = true;
			m_sp.mouseEnabled = true;
//			m_cardA = null;
//			m_cardB = null;
			m_pickQueue = [];
			for(var i:int = 0; i<5;i++)
			{
				var porker:Poker =  m_arCard[i]
					porker.fnShowArrow();
			}
		}
		
		private function pickQueueRemove(p:Poker):void{
			var card:Poker;
			for( var idx:int in m_pickQueue){
				card = m_pickQueue[idx];
				if(card.m_nCardIndex == p.m_nCardIndex){
					m_pickQueue.removeAt(idx);
				}
			}
			p.fnCardDisSelect();
		}
		
		private function pickQueuePush(p:Poker):void{
			var c:Poker;
			if(m_pickQueue.length == PICK_NUMBER){
				c = m_pickQueue.shift();
				c.fnCardDisSelect();
			}
			m_pickQueue.push(p);
			p.fnCardSelect();
		}
		
		
		
		private function fnCardSelect(e:MouseEvent):void
		{
			var porker:Poker = e.currentTarget as Poker
			if(m_bBuying)
			{
				fnBuyingPorkerClick(e);
				return;
			}
			/// pick state
			if(porker.m_bSelected)
			{
				pickQueueRemove(porker);
			}
			else
			{
				pickQueuePush(porker);
			}
		}
		
		public function fnShowPickCard():Array
		{
			
			m_sp.mouseChildren = false;
			m_sp.mouseEnabled = false;
			var p:Poker;
			var left:Array = [], right:Array = [], rightPoker:Array = [];
			var i:int, len:int = m_pickQueue.length;
			for(i = 0; i<len; i++){
				p = m_pickQueue[i];
				left.push(p.m_nCardIndex);
				p.fnShowEye();
				p.fnCardDisSelect();	
				p.mouseChildren=false;
				p.mouseEnabled=false;
			}
			len = m_arCard.length;
			for(i = 0; i< len ; i++)
			{
				p =  m_arCard[i];
				if(left.length >= PICK_NUMBER){
					if(left.indexOf(p.m_nCardIndex) == -1){
						right.push(p.m_nCardIndex);
						rightPoker.push(p);
					}
				}else{
					if(left.indexOf(p.m_nCardIndex) == -1){
						m_pickQueue.push(p);
						left.push(p.m_nCardIndex);	
						p.fnShowEye();
						p.fnCardDisSelect();	
						p.mouseChildren=false;
						p.mouseEnabled=false;
					}
				}
				p.fnHideArrow();
			}
			
			right.sort(Array.NUMERIC);
			
				
			var arIndex:Array = new Array();
			arIndex.push(left[0],left[1],right[0],right[1],right[2]);
			
			fnSortCardHandler(arIndex,m_arCard);
			m_cardA = m_arCard[0];
			m_cardB = m_arCard[1];
			
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound055();
			
			var ar:Array = new Array();
			ar.push(m_pickQueue[0].m_nCardIndex,m_pickQueue[0].m_nDivid,m_pickQueue[0].m_nMod);
			ar.push(m_pickQueue[1].m_nCardIndex,m_pickQueue[1].m_nDivid,m_pickQueue[1].m_nMod);
			return ar
			
		}
		
		
		private function fnPickCardAni(card:Poker,nX:Number):void
		{
			Tweener.addTween(card,{x:nX,time:0.15,transition:"easeOutExpo",onComplete:fnPickCardAniFinish});
		}
		
		private function fnPickCardAniFinish():void
		{
			
		}
		
		public function fnBuyingTime():void
		{
			m_sp.mouseChildren=true;
			m_sp.mouseEnabled=true
			m_bBuying = true
			for(var i:int = 2; i<5;i++)
			{
				var porker:Poker =  m_arCard[i]
				porker.fnShowArrow();
			}
		}
		
		private function fnBuyingPorkerClick(e:MouseEvent):void
		{
			var porker:Poker = e.currentTarget as Poker
				if(porker.m_nCardIndexAtSort == 2)
				{
					if(m_cardC)
					{
						m_cardC = null
						porker.fnCardDisSelect()	
					}
					else
					{
						m_cardC = porker
						porker.fnCardSelect();	
					}
				}
				if(porker.m_nCardIndexAtSort == 3)
				{
					if(m_cardD)
					{
						m_cardD = null
						porker.fnCardDisSelect()	
					}
					else
					{
						m_cardD = porker
						porker.fnCardSelect();	
					}
				}
				if(porker.m_nCardIndexAtSort == 4)
				{
					if(m_cardE)
					{
						m_cardE = null
						porker.fnCardDisSelect()	
					}
					else
					{
						m_cardE = porker
						porker.fnCardSelect();	
					}
				}
		}
		
		public function fnBuyingConfirm():void
		{
			for(var i:int = 2; i<5;i++)
			{
				var porker:Poker =  m_arCard[i]
				porker.fnHideArrow();
			}
			m_sp.mouseChildren=false;
			m_sp.mouseEnabled=false
			m_arBuying = new Array();
			if(m_cardC)
			{
				m_arBuying.push(m_cardC.m_nCardIndex);
				fnBuyingConfirmAni(m_cardC);
			}
			if(m_cardD)
			{
				m_arBuying.push(m_cardD.m_nCardIndex);
				fnBuyingConfirmAni(m_cardD);
			}
			if(m_cardE)
			{
				m_arBuying.push(m_cardE.m_nCardIndex);
				fnBuyingConfirmAni(m_cardE);
			}
			
		//	var newX:Number = card.x + 500
		//	Tweener.addTween(card,{x:newX,time:0.15,transition:"easeOutBack"});
		//	m_tweenerBuying.fnSetInit(m_arBuying,-500,);
			
			fnBuying();
			
		}
		
		private function fnBuying(e:GameEvent = null):void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnBuying);
			SessionManager.instance.request(Protocol.GAME_BUYING, {token:GameConfig.game_playerInfo.token, game_id:GameConfig.game_openBetInfo.game_id, player_buying:m_arBuying},onBuying,onNet);
		}
	
		private function onBuying(e:NetEvent):void
		{
			trace('onBuying '+e.data);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnBuying);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				
				GameConfig.game_buyingInfo = new BuyingInfo(e.data);
				GameConfig.game_buyingInfo.player_replace_cards
				fnTweenBuyingConfirmFinish();
			}
			else
			{
				var ev2:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				ev2.m_eventArgs = "onBuying "+e.data
				EventManager.instance.trigger(ev2);
			}
		}
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
		
		private function fnBuyingConfirmAni(card:Poker):void
		{
			var newX:Number = card.x + 800
			Tweener.addTween(card,{x:newX,time:0.15,transition:"easeOutBack"});
		}
		
	
		
		private function fnTweenBuyingConfirmFinish():void
		{
			var newCard:Array = GameConfig.game_buyingInfo.player_replace_cards;
			var nIndex:int 
			var porker:Poker
			if(m_cardC)
			{
				porker = new Poker(newCard[0])
				newCard.shift();
				porker.x = 10
				m_cardC = porker
				fnBuyingCardAddToStage(porker);
			}
			if(m_cardD)
			{
				porker = new Poker(newCard[0])
				newCard.shift();
				porker.x = 152
				m_cardD = porker
				fnBuyingCardAddToStage(porker);
			}
			if(m_cardE)
			{
				porker = new Poker(newCard[0])
				newCard.shift();
				porker.x = 294
				m_cardE = porker
				fnBuyingCardAddToStage(porker);
			}
			setTimeout(function():void 
			{
				fnTweenBuyingCardAddtoStageFinish();
			}, 0.5)
			
			setTimeout(function():void 
			{
				fnSortCardAfterBuying();
			}, 1000)
			
		}
		
		private function fnBuyingCardAddToStage(card:Poker):void
		{
			m_sp.addChild(card);
			var nX:Number = card.x
			card.x = -720
			Tweener.addTween(card,{x:nX,time:0.3,transition:"easeOutExpo"});
		}
		
		private function fnTweenBuyingCardAddtoStageFinish():void
		{
			if(m_cardC)
			{
				m_cardC.fnCardShow()
			}
			else
			{
				m_cardC =m_arCard[2]
			}
			if(m_cardD)
			{
				m_cardD.fnCardShow()
			}
			else
			{
				m_cardD =m_arCard[3]
			}
			if(m_cardE)
			{
				m_cardE.fnCardShow()
			}
			else
			{
				m_cardE =m_arCard[4]
			}
		}
		
		private function fnSortCardAfterBuying():void
		{
		
			if(m_cardC == null && m_cardD==null && m_cardE == null)
			{
				m_cardA.fnHideEye();
				m_cardB.fnHideEye();
				m_arCard = new Array();
				m_arCard.push(m_cardA);
				m_arCard.push(m_cardB);
				m_arCard.push(m_cardC);
				m_arCard.push(m_cardD);
				m_arCard.push(m_cardE);
			}
			else
			{
				var arIndex:Array = new Array(m_cardA.m_nCardIndex,m_cardB.m_nCardIndex,m_cardC.m_nCardIndex,m_cardD.m_nCardIndex,m_cardE.m_nCardIndex);
				var arPorker:Array = new Array(m_cardA,m_cardB,m_cardC,m_cardD,m_cardE);
				arIndex.sort(Array.NUMERIC);
			//	arIndex.reverse();
				var porker:Poker
				var arNew:Array = new Array();
				for(var i:int = 0 ; i< 5 ;i++)
				{
					var nIndex:int = arIndex[i]
					for(var j:int = 0; j<5 ;j++)
					{
						porker = arPorker[j] as Poker
						if(nIndex == porker.m_nCardIndex)
						{
						//	var newX:Number = (i-2)*142 +10
						//	porker.m_nCardIndexAtSort = i	
						//	fnTweenSortCard(porker,newX);
							porker.fnHideEye();
							arNew.push(porker);
						}
					}
				}
				m_arCard = arNew;
			}
			var ar:Array = GameConfig.game_buyingInfo.player_cards
			fnSortCardHandler(ar,m_arCard);
			
			setTimeout(function():void 
			{
				var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_BUYING_CONFIRM_EVENT,true);
				dispatchEvent(ev)
			}, 500)
		}
		
		
		
	}
}