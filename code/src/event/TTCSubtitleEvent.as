package event
{
	public class TTCSubtitleEvent extends TiBasicEvent
	{
		public static const SUBTITLE_EVENT:String = "SUBTITLE_EVENT"; 
		public static const SUBTITLE_FINISH_EVENT:String="SUBTITLE_FINISH_EVENT";
		public static const SUBTITLE_NEXT_EVENT:String="SUBTITLE_NEXT_EVENT";
		public static const SUBTITLE_LOAD_FONT_COMPLETE_EVENT:String="SUBTITLE_LOAD_FONT_COMPLETE_EVENT";

		public function TTCSubtitleEvent(type:String, detail:String="")
		{
			super(type, detail);
		}
	}
}