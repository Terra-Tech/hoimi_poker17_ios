package com.hoimi.util
{
	public class StringUtil
	{
		public function StringUtil()
		{
		}
		
		static public function convertNumber(number:uint):String
		{
			var str:String = number.toString();
			var rtn:String = "";
			var len:uint = str.length;
			
			for(var i:int = 0, j:int = len -1 ; i < len ;i++,j--)
			{
				rtn += convertNumberToChinese2(uint(str.charAt(i)), j);
			}
			
			return rtn;
		}
		
		static public function convertNumberToChinese2(number:uint, digit:uint):String
		{
			var rtn:String = "";
			
			switch(number)
			{
				case 0:
					rtn += "零";
					break;
				case 1:
					if(digit == 1) 
						rtn = "";
					else 
						rtn += "一";
					break;
				case 2:
					rtn += "二";
					break;
				case 3:
					rtn += "三";
					break;
				case 4:
					rtn += "四";
					break;
				case 5:
					rtn += "五";
					break;
				case 6:
					rtn += "六";
					break;
				case 7:
					rtn += "七";
					break;
				case 8:
					rtn += "八";
					break;
				case 9:
					rtn += "九";
					break;
			}
			
			switch(digit)
			{
				case 0:
					if(number == 0)
						rtn = "";
					else 
						rtn += "";
					break;
				case 1:
					rtn += "十";
					break;
				case 2:
					rtn += "百";
					break;
				case 3:
					rtn += "千";
					break;
				case 4:
					rtn += "萬";
					break;
				case 5:
					rtn += "十萬";
					break;
				case 6:
					rtn += "百萬";
					break;
				case 7:
					rtn += "千萬";
					break;
				case 8:
					rtn += "億";
					break;
				case 9:
					rtn += "十億";
					break;
			}
			
			return rtn;
		}
		
		static public function convertNumberToChinese(number:uint):String
		{
			switch(number)
			{
				case 1:
					return "一";
				case 2:
					return "二";
				case 3:
					return "三";
				case 4:
					return "四";
				case 5:
					return "五";
				case 6:
					return "六";
				case 7:
					return "七";
				case 8:
					return "八";
				case 9:
					return "九";
				case 10:
					return "十";
				case 11:
					return "十一";
				case 12:
					return "十二";
				case 13:
					return "十三";
				case 14:
					return "十四";
				case 15:
					return "十五";
				case 16:
					return "十六";
				case 17:
					return "十七";
				case 18:
					return "十八";
				case 19:
					return "十九";
				case 20:
					return "二十";
				case 21:
					return "二十一";
				case 22:
					return "二十二";
				case 23:
					return "二十三";
				case 24:
					return "二十四";
				case 25:
					return "二十五";

			}
			
			return "?";
		}
		
		static public function format(str:String,... args):String
		{
			var len:uint = args.length;
			var replace:String = str;
			for(var i:int = 0; i < len ;i++)
			{
				replace = replace.replace("{"+i+"}",args[i]);
			}
			
			return replace;
		}
	}
}