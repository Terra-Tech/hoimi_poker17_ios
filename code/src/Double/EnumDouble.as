package Double
{
	public class EnumDouble
	{
		static public const  JOKER:int = 1;
		static public const  BLACK:int = 2;
		static public const  RED:int = 3;
		static public const  SPADE:int = 4;
		static public const  HEART:int = 5;
		static public const  DIAMOND:int = 6;
		static public const  CLUB:int = 7;
	}
}