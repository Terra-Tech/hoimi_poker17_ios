package com.hoimi.util
{
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.Capabilities;

	public class Rating
	{
		// App ID - Replace with your app id
		private static const APPLE_APP_ID:String= "123456789";
		private static const PLAY_APP_ID:String= "air.com.sjdigital.eTake"; 
		
		// Store URIs
		private static const PLAY_STORE_BASE_URI:String= "market://details?id=";
		private static const PLAY_REVIEW:String= "&reviewId=0";
		private static const APP_STORE_BASE_URI:String= "http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?onlyLatestVersion=false&type=Purple+Software&id=";
		
		// Open the review page in the app store
		//
		static public function openRatePage(app_id:String):void
		{
			var appUrl:String = APP_STORE_BASE_URI + app_id;
			if (isAndroid()) {
				appUrl = PLAY_STORE_BASE_URI + app_id + PLAY_REVIEW;
			}
			
			// Open store URI 	
			var req:URLRequest = new URLRequest(appUrl);
			navigateToURL(req);
		}
		
		static public function isAndroid():Boolean
		{
			return Capabilities.manufacturer.indexOf('Android') > -1;
		}
	}
}