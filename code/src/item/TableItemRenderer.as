package item
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import Enum.EnumLang;
	import Enum.EnumMsg;
	
	
	import event.GameEvent;
	import event.GameMessageEvent;
	
	import handler.HoimiFont01LoadHandler;
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	import handler.YesNoPanelHandler;
	
	import lib.CoinValueTransfer;
	
	import model.GameTable;
	import model.OpenBetInfo;
	
	public class TableItemRenderer extends Sprite
	{
		private var m_mc:Lobby_Table = new Lobby_Table();
		private var _m_nIndex:int = 0
		private var m_nMinLimit:Number = 0
		private var m_nMaxLimit:Number = 0
		private var m_nBet:Number = 0
			
		public function TableItemRenderer(index:int,min:Number,max:Number,bet:Number)
		{
			super();
			this.addChild(m_mc);
			m_mc.x = 360
			m_nIndex = index
			m_mc.gotoAndStop(index);	
			m_nMinLimit = min
			m_nMaxLimit = max
			m_nBet = bet
			EventManager.instance.register(GameEvent.GAME_COIN_CHANGE_EVENT,fnCheckCoinEnable);
			fnCheckCoinEnable();
			m_mc.city_EN.gotoAndStop(index);
			m_mc.city_TW.gotoAndStop(index);
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				m_mc.city_EN.visible=false;
				m_mc.city_TW.visible=true;
			}
			else
			{
				m_mc.city_EN.visible=true;
				m_mc.city_TW.visible=false;
			}
			fnSetText();
			//HoimiFont01LoadHandler.fnGetInstance().addEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			//HoimiFont01LoadHandler.fnGetInstance().fnLoadFont();
			
		}
		
		private function fnCheckCoinEnable(e:GameEvent = null):void
		{
			if(GameConfig.game_playerInfo.coin < m_nMinLimit)
			{
				m_mc.table_mask.visible=true
				m_mc.table_mask.addEventListener(MouseEvent.CLICK,fnCoinNotEnough);	
			}
			else if(GameConfig.game_playerInfo.coin > m_nMaxLimit)
			{
				m_mc.table_mask.visible=true
				m_mc.table_mask.addEventListener(MouseEvent.CLICK,fnCoinExceedMax);
			}
			else
			{
				m_mc.table_mask.visible=false
				m_mc.addEventListener(MouseEvent.CLICK,fnClickTable);
			}
		}
		
		private function GameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_FONT_01_LOAD_COMPLETE_EVENT)
			{
				fnEmbededFont();
			}
			if(e.detail == GameEvent.GAME_NUMBER_FONT_LOAD_COMPLETE_EVENT)
			{
				fnEmbededFont();
			}
		}
		
		private function fnEmbededFont():void
		{
			HoimiFont01LoadHandler.fnGetInstance().removeEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			var tf:TextFormat = new TextFormat();
			tf.font = HoimiFont01LoadHandler.fnGetInstance().embeddedFont.fontName;
			//tf.size = 40;
			//tf.color = "0xFF8800";
			m_mc.txt_limit.embedFonts = true; // very important to set
			m_mc.txt_limit.defaultTextFormat = tf;
			m_mc.txt_min.embedFonts = true; // very important to set
			m_mc.txt_min.defaultTextFormat = tf;
			m_mc.txt_max.embedFonts = true; // very important to set
			m_mc.txt_max.defaultTextFormat = tf;
			m_mc.txt_bet.embedFonts = true; // very important to set
			m_mc.txt_bet.defaultTextFormat = tf;
			fnSetText();
		}
		
		private function fnSetText():void
		{
			//m_mc.txt_limit.text = TxtLangTransfer.COIN_LIMIT;
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				m_mc.txt_min.text = TxtLangTransfer.MIN_LIMIT + CoinValueTransfer.fnCoinValueTransferUnitTW(m_nMinLimit);
				m_mc.txt_max.text = TxtLangTransfer.MAX_LIMIT + CoinValueTransfer.fnCoinValueTransferUnitTW(m_nMaxLimit);
				m_mc.txt_bet.text = TxtLangTransfer.BASE_BET + CoinValueTransfer.fnCoinValueTransferUnitTW(m_nBet);
			}
			else
			{
				if(m_nMinLimit >= 1000000)
				{
					m_mc.txt_min.text = TxtLangTransfer.MIN_LIMIT + CoinValueTransfer.fnCoinValueTransferUnit(m_nMinLimit);
				}
				else
				{
					m_mc.txt_min.text = TxtLangTransfer.MIN_LIMIT + CoinValueTransfer.fnCoinValueTransferTS(m_nMinLimit);
				}
				
				if(m_nMaxLimit >= 1000000)
				{
					m_mc.txt_max.text = TxtLangTransfer.MAX_LIMIT + CoinValueTransfer.fnCoinValueTransferUnit(m_nMaxLimit);
				}
				else
				{
					m_mc.txt_max.text = TxtLangTransfer.MAX_LIMIT + CoinValueTransfer.fnCoinValueTransferTS(m_nMaxLimit);
				}
				
				if(m_nBet >=1000000)
				{
					m_mc.txt_bet.text = TxtLangTransfer.BASE_BET + CoinValueTransfer.fnCoinValueTransferUnit(m_nBet);
				}
				else
				{
					m_mc.txt_bet.text = TxtLangTransfer.BASE_BET + CoinValueTransfer.fnCoinValueTransferTS(m_nBet);
				}
			}
		}
		
		private function fnCoinNotEnough(e:MouseEvent = null):void
		{
			trace("coin not enought");
			YesNoPanelHandler.fnCoinNotEnough();
		
		}
		
		private function fnCoinExceedMax(e:MouseEvent =null):void
		{
			trace("coin over max limit");	
			var ev3:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
			ev3.m_strMsg = TxtLangTransfer.COIN_OVER_MAX_LIMIT;
			ev3.m_strType = EnumMsg.MSG_TYPE_OK;
			EventManager.instance.trigger(ev3);
		}

		
		private function fnClickTable(e:MouseEvent):void
		{
			
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			fnNetTable();
		}
		
		private function fnNetTable(e:GameEvent = null):void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnNetTable);
			var index:int = _m_nIndex-1
			var table:GameTable = GameConfig.game_tableList[index] as GameTable;
			GameConfig.game_table = table
			SessionManager.instance.request(Protocol.GAME_OPEN_BET, {token:GameConfig.game_playerInfo.token, table_id:table.id,assign:'', exclude:''},onOpenBet,onNet);
		}
		
		/** open_bet
		 * 1:{"result" : 1,'game_id' : 92, 'player_coin':230,'enemy_uuid' : 12h123lkjhj123h, 'enemy_name' : Aries, 'player_cards':[0,1,2,3,4,5]})
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 * -4:param miss
		 * -5:no such table
		 * -6:coin not enought
		 * -7:coin over max limit
		 */
		private function onOpenBet(e:NetEvent):void
		{
			trace('onOpenBet '+e.data);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnNetTable);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				GameConfig.game_openBetInfo = new OpenBetInfo(e.data);
				var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT, GameEvent.GAME_NEXT_SECTION_EVENT,true)
				dispatchEvent(ev);
			}
			else if(data.result == -6)
			{
				fnCoinNotEnough();
			}
			else if(data.result == -7)
			{
				fnCoinExceedMax();
			}
			else
			{
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "onOpenBet "+data.result;
				EventManager.instance.trigger(evError);
			}
		}
		
	
		public function get m_nIndex():int
		{
			return _m_nIndex;
		}

		public function set m_nIndex(value:int):void
		{
			_m_nIndex = value;
		}
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
		
		public function fnDestory():void
		{
			EventManager.instance.remove(GameEvent.GAME_COIN_CHANGE_EVENT,fnCheckCoinEnable);
		}

	}
}