package Skill.skillItem
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	
	import Skill.SkillBase;
	import Skill.SkillRaiseEvent;
	
	import lib.MultiLanguagleUISelect;
	
	public class SkillItem_07 extends SkillBase
	{
		private var m_mcSkill:Poker_Btn_Skill_07 = new Poker_Btn_Skill_07();
		public function SkillItem_07()
		{
			super();
			var mc:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mcSkill);
			m_mc = mc;
			this.addChild(m_mcSkill);
			this.addChild(m_sp);
			m_txtExplanation.text = "每局獲勝後系統抽成的金額%降低";
			m_strSkillName = "[獲勝金幣抽成降低]";
		}
		
		override protected function fnRaiseSkill():void
		{
			var ev:SkillRaiseEvent = new SkillRaiseEvent(6)
			EventManager.instance.trigger(ev);
		}
		
		override protected function fnDataDisplay(nInfo1:Number,nInfo2:Number):void
		{
			m_txtCurrent.text = (nInfo1*100).toFixed(1)+"%"
			if(nInfo2 == -1)
			{
				m_txtNext.text = "----"
				m_mcSkill.arrow.visible=false;
			}
			else
			{
				m_txtNext.text = (nInfo2*100).toFixed(1)+"%"
			}
			
		}
	}
}