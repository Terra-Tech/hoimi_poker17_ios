package com.hoimi.util
{
	import com.hurlant.crypto.Crypto;
	import com.hurlant.crypto.symmetric.ICipher;
	import com.hurlant.crypto.symmetric.IPad;
	import com.hurlant.crypto.symmetric.PKCS5;
	import com.hurlant.util.Base64;
	import com.hurlant.util.Hex;
	
	import flash.utils.ByteArray;

	public class CryptoHelper
	{
		public function CryptoHelper()
		{
			throw new Error("This is static class!!");
		}
		
		static private const algorithm:String = "des-ecb";
		
		static public function encrypt(key:String, source:String):String
		{
			var keyBytes:ByteArray = new ByteArray();
	
			keyBytes.writeUTFBytes(key);
			var pad:IPad = new PKCS5();
			var cipher:ICipher = Crypto.getCipher(algorithm, keyBytes, pad);
			pad.setBlockSize(cipher.getBlockSize());
			
			var data:ByteArray = new ByteArray();
			data.writeUTFBytes(source);
			cipher.encrypt(data);
			return Base64.encodeByteArray(data);
		}
		
		static public function decrypt(key:String, source:String):String
		{
			var keyBytes:ByteArray = new ByteArray();
			
			keyBytes.writeUTFBytes(key);
			var pad:IPad = new PKCS5();
			var cipher:ICipher = Crypto.getCipher(algorithm, keyBytes, pad);
			pad.setBlockSize(cipher.getBlockSize());
			
			var data:ByteArray = Base64.decodeToByteArray(source);
			cipher.decrypt(data);
			return Hex.toString(Hex.fromArray(data));
		}
	}
}