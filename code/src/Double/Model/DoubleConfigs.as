package Double.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class DoubleConfigs extends JsonObject
	{
		/// 1:Joker, 2:Black, 3:Red, 4:Spade, 5:Heart, 6:Diamond, 7:Club
		public var configs:Array = [];
		
		public function DoubleConfigs(obj:*=null)
		{
			super(obj);
		}
		
		override public function byObject(obj:Object):void
		{
			for (var p:String in obj)
			{
				if(this.hasOwnProperty(p))
				{
					if(this[p] is JsonObject)
						this[p].input(obj[p]);
					else
						this[p] = obj[p];
				}
			}
			var i:int, len:int = configs.length, dc:DoubleConfig;
			var d:Date, ary:Array, dAry:Array, tAry:Array;
			for(i = 1 ; i < len ;i++)
			{
				dc = new DoubleConfig(configs[i]);
				configs[i] = dc;
			}
			
		}
	}
}