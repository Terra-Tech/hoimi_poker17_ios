package ui
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import Skill.SkillView;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	
	import handler.TxtLangTransfer;
	
	import item.HoimiCoin;
	import item.HoimiDiamond;
	
	import lib.TweenerBashAni;
	
	public class PlayerStatusFooter extends Sprite
	{
		private var m_mc:Poker_Game_Footer = new Poker_Game_Footer();
		private var m_coCoin:HoimiCoin = new HoimiCoin();
		private var m_coDiamond:HoimiDiamond = new HoimiDiamond();
		private var m_tweener:TweenerBashAni= new TweenerBashAni();
		private var m_tweenerMinus:TweenerBashAni = new TweenerBashAni();
		
		public function PlayerStatusFooter()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_coCoin);
			this.addChild(m_coDiamond);
			this.addChild(m_tweener);	
			m_coCoin.x = 440;
			m_coCoin.y = 1242;
			m_coDiamond.x = 635;
			m_coDiamond.y = 1242;
			fnCoinChange();
			fnDiamondChange(null);
			
//			EventManager.instance.register(GameEvent.GAME_COIN_CHANGE_EVENT, fnCoinChange);
			EventManager.instance.register(GameEvent.GAME_DIAMOND_CHANGE_EVENT, fnDiamondChange);
			EventManager.instance.register(GameEvent.GAME_PLAYER_RECORD_CHANGE, fnPlayerRecordChange);
		}
		
		private function fnPlayerRecordChange(e:GameEvent):void
		{
			fnSetWinLoseRate(GameConfig.game_playerRecord.win,GameConfig.game_playerRecord.lose,GameConfig.game_playerRecord.rate);
		}
		
		public function fnCoinChange():void
		{
			m_coCoin.fnSetValue(GameConfig.game_playerInfo.coin)
		}
		
		private function fnDiamondChange(e:GameEvent):void
		{
			m_coDiamond.fnSetValue(GameConfig.game_playerInfo.diamond);
		}
		
		public function fnBetCoins(nValue:int):void
		{
			if(nValue>0)
			{
				fnAddBetAni(nValue);
				
			}
			else
			{
				fnMinusBetAni(-nValue);
			}
				
		}
		
		private function fnAddBetAni(nCoinValue:int):void
		{
			var nConut:int = nCoinValue/GameConfig.game_table.bet
			var ar:Array = new Array();	
			for(var i:int = 0; i< nConut;i++)
			{
				var mcBet:Poker_Bet_Coin = new Poker_Bet_Coin();
				mcBet.coin_base.visible=false;
				ar.push(mcBet);
				
			}
			if(nConut>0)
			{
				m_tweener.visible=true;
				m_tweener.fnSetInit(ar,360,700,481,1251,0.5,0.5,1,0.25,"easeOutExpo",fnItemTweenerComplete,fnAllPlusTweenFinish);
			}
		}
		
		private function fnItemTweenerComplete(nAll:int,nIndex:int):void
		{
			m_coCoin.fnAddCoinValue(GameConfig.game_table.bet);
		}
		
		private function fnAllPlusTweenFinish():void
		{
			m_tweener.visible=false;
			m_coCoin.fnSetValue(GameConfig.game_playerInfo.coin);
		}
		
		private function fnMinusBetAni(nCoinValue:Number):void
		{
			m_tweenerMinus = new TweenerBashAni();
			var nConut:int = nCoinValue/GameConfig.game_table.bet
			var ar:Array = new Array();	
			for(var i:int = 0; i< nConut;i++)
			{
				var mcBet:Poker_Bet_Coin = new Poker_Bet_Coin();
				ar.push(mcBet);
				
			}
			if(nConut>0)
			{
				m_tweenerMinus.fnSetInit(ar,0,0,0,0,1,1,1,0.25,"easeOutExpo",fnItemMinusTweenerComplete,fnAllMinusTweenFinish);
			}
		}
		
		private function fnItemMinusTweenerComplete(nAll:int,nIndex:int):void
		{
			m_coCoin.fnMinusCoinValue(GameConfig.game_table.bet);
		}
	
		private function fnAllMinusTweenFinish():void
		{
			m_tweener.visible=false;
			m_coCoin.fnSetValue(GameConfig.game_playerInfo.coin);
		}
		
		public function fnSetWinLoseRate(nWin:int,nLose:int,nRate:Number):void
		{
			m_mc.txt_win.text =  TxtLangTransfer.WIN+": "
			m_mc.txt_win_number.text = nWin.toString();
			m_mc.txt_lose.text = TxtLangTransfer.LOSE+": "
			m_mc.txt_lose_number.text = nLose.toString();
			
			m_mc.txt_rate.text = TxtLangTransfer.WIN_LOSE_RATE+":" 
			if(nWin == 0)
			{
				m_mc.txt_rate_number.text =  "0.0%"	
			}
			else
			{
				m_mc.txt_rate_number.text =  ((nWin/(nWin+nLose))*100).toFixed(1) +"%"	
			}
			
		}
		
		
		public function fnDestory():void
		{
			EventManager.instance.remove(GameEvent.GAME_DIAMOND_CHANGE_EVENT, fnDiamondChange);
			EventManager.instance.remove(GameEvent.GAME_PLAYER_RECORD_CHANGE, fnPlayerRecordChange);
			m_coCoin.fnDestory();
			m_coDiamond.fnDestory();
		}
	}
}