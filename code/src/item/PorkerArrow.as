package item
{
	import flash.display.Sprite;
	
	import caurina.transitions.Tweener;
	
	public class PorkerArrow extends Sprite
	{
		private var m_mcArrow:Poker_Arrow_Down = new Poker_Arrow_Down();
	
		public function PorkerArrow()
		{
			super();
			this.addChild(m_mcArrow);
			fnTweenEffect();
		}
		
		public function fnTweenEffect():void
		{
			Tweener.addTween(m_mcArrow,{y:-15,time:0.5,transition:"linear",onComplete:fnAfterTweenEffect});
		}
		
		private function fnAfterTweenEffect():void
		{
			Tweener.addTween(m_mcArrow,{y:0,time:0.5,transition:"linear",onComplete:fnTweenEffect});
		}
	}
}