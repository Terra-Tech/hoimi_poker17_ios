package Double
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	
	import event.GameEvent;
	
	import handler.TxtLangTransfer;
	
	import item.HoimiCoin;
	import item.HoimiDiamond;
	
	import lib.TweenerBashAni;
	
	public class DoublePlayerStatusFooter extends Sprite
	{
		private var m_mc:Poker_Game_Footer = new Poker_Game_Footer();
		private var m_coCoin:HoimiCoin = new HoimiCoin();
		private var m_coDiamond:HoimiDiamond = new HoimiDiamond();
		private var m_tweener:TweenerBashAni= new TweenerBashAni();
		private var m_tweenerMinus:TweenerBashAni = new TweenerBashAni();
		private var m_nBet:Number = 0;
		
		public function DoublePlayerStatusFooter()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_coCoin);
			this.addChild(m_coDiamond);
			this.addChild(m_tweener);	
			this.addChild(m_tweenerMinus);
			m_coCoin.x = 440;
			m_coCoin.y = 1242;
			m_coDiamond.x = 635;
			m_coDiamond.y = 1242;
			fnCoinChange();
			fnDiamondChange();
			
		}
		
	
		
		public function fnCoinChange():void
		{
			m_coCoin.fnSetValue(GameConfig.game_playerInfo.coin)
		}
		
		public function fnDiamondChange():void
		{
			m_coDiamond.fnSetValue(GameConfig.game_playerInfo.diamond);
		}
		
		public function fnBetCoins(nValue:int):void
		{
			if(nValue>0)
			{
				fnAddBetAni(nValue);
			}
			else
			{
				fnMinusBetAni(-nValue);
			}
		}
		
		private function fnAddBetAni(nCoinValue:int):void
		{
			m_nBet = nCoinValue/10
			var nConut:int = 10
			var ar:Array = new Array();	
			for(var i:int = 0; i< nConut;i++)
			{
				var mcBet:Poker_Bet_Coin = new Poker_Bet_Coin();
				mcBet.coin_base.visible=false;
				mcBet.scaleX = 0.5
				mcBet.scaleY = 0.5
				ar.push(mcBet);
				
			}
			if(nConut>0)
			{
				m_tweener.visible=true;
				m_tweener.fnSetInit(ar,300,1085,481,1251,0.5,0.5,1,0.25,"easeOutExpo",fnItemTweenerComplete,fnAllPlusTweenFinish);
			}
		}
		
		private function fnItemTweenerComplete(nAll:int,nIndex:int):void
		{
			m_coCoin.fnAddCoinValue(m_nBet);
		}
		
		private function fnAllPlusTweenFinish():void
		{
			m_tweener.visible=false;
			m_coCoin.fnSetValue(GameConfig.game_playerInfo.coin);
		}
		
		private function fnMinusBetAni(nCoinValue:Number):void
		{
			m_nBet = nCoinValue
			var nConut:int =1
			var ar:Array = new Array();	
			for(var i:int = 0; i< nConut;i++)
			{
				var mcBet:Poker_Bet_Coin = new Poker_Bet_Coin();
				mcBet.coin_base.visible=false;
				ar.push(mcBet);
				
			}
			if(nConut>0)
			{
				m_tweenerMinus.fnSetInit(ar,481,1251,300,1085,0.5,0.5,1,0.25,"easeOutExpo",fnItemMinusTweenerComplete,fnAllMinusTweenFinish);
			}
		}
		
		private function fnItemMinusTweenerComplete(nAll:int,nIndex:int):void
		{
			//m_coCoin.fnMinusCoinValue(m_nBet);
		}
		
		private function fnAllMinusTweenFinish():void
		{
			m_tweenerMinus.visible=false;
			m_coCoin.fnSetValue(GameConfig.game_playerInfo.coin-m_nBet);
		}
		
		
		public function fnSetWinLoseRate(nWin:int,nLose:int):void
		{
			m_mc.txt_win.text =  TxtLangTransfer.WIN+": "
			m_mc.txt_win_number.text = nWin.toString();
			m_mc.txt_lose.text = TxtLangTransfer.LOSE+": "
			m_mc.txt_lose_number.text = nLose.toString();
			
			m_mc.txt_rate.text = TxtLangTransfer.WIN_LOSE_RATE+":" 
			if(nWin == 0)
			{
				m_mc.txt_rate_number.text =  "0.0%"	
			}
			else
			{
				m_mc.txt_rate_number.text =  ((nWin/(nWin+nLose))*100).toFixed(1) +"%"	
			}

		}
		
		
		public function fnDestory():void
		{
			m_coCoin.fnDestory();
			m_coDiamond.fnDestory();
		}
	}
}