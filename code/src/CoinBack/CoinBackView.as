package CoinBack
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.setTimeout;
	
	import caurina.transitions.Tweener;
	
	import event.GameEvent;
	
	import handler.TTCSoundHandler;
	
	import item.HoimiCoin;
	import item.ResultPanelCoin;
	
	public class CoinBackView extends Sprite implements ICoinBackView
	{
		private static var m_Instance:CoinBackView;
		private var statusSp:Sprite= new Sprite();;
		
		private var statusPool:TextField;
		private var statusDiamond:TextField;
		private var statusCoin:TextField;
		private var statusMsg:TextField = new TextField();;
		private var m_coCoinValue:HoimiCoin = new HoimiCoin();
		private var m_sp:Sprite = new Sprite();
		private var m_mcHoimiCoin:Poker_Hoimi_Coin = new Poker_Hoimi_Coin();
		
		public function CoinBackView()
		{
			this.addEventListener(Event.ADDED_TO_STAGE, onAdd);
			EventManager.instance.register(GameEvent.GAME_COIN_BACK_CHANGE, onPoolChange);
		}
		
		private function onPoolChange(e:GameEvent):void
		{
			showPool(GameConfig.game_playerInfo.coin_back_pool);
		}
		
		public static function fnGetInstance(): CoinBackView 
		{
			if ( m_Instance == null )
			{
				m_Instance = new CoinBackView();
			}
			return m_Instance;
		}
		
		private function init():void
		{
			this.addChild(m_mcHoimiCoin);
			this.addChild(m_coCoinValue);
			this.addChild(statusMsg);
			this.addChild(m_sp);
			m_mcHoimiCoin.x = 60
			m_mcHoimiCoin.y = 111.5
			m_mcHoimiCoin.scaleX = 0.3
			m_mcHoimiCoin.scaleY =0.3
			m_coCoinValue.x = 70
			m_coCoinValue.y = 112
			
			statusMsg.defaultTextFormat = new TextFormat( "Arial", 30, 0xFF0000, false, false, false, null, null, TextFormatAlign.CENTER );
			statusMsg.filters = [new DropShadowFilter(1,45,0xFFFFFF,1,1,1,1,1)];
			statusMsg.x = 20
			statusMsg.y = 152;
			statusMsg.width = 300
			
			
			
//			EventManager.instance.trigger(new Event(CoinBackController.EVENT_COIN_BACK_INIT));
		}
		
		
		
		// 網路發生問題
		public function netError(msg:String):void{
			//statusMsg.text = msg;
		}
		/// Server 吐錯誤訊息
		public function serverError(msg:String):void{
			//statusMsg.text = msg;
		}
		/// 如打開領獎Panel show loading
		public function showLoading():void{
			//statusExp.text = "Loading...";
		}
		/// 如打開領獎Panel hide loading
		public function hideLoading():void{
			//statusExp.text = "complete!";
		}
		/// 領到錢（withdraw_coin : 領了多少, player_coin : 現在玩家財產）
		public function showWithdraw(withdraw_coin:Number,player_coin:Number):void{
			//statusDiamond.text = 'Withdraw : '+withdraw_coin.toString();
			if(withdraw_coin > 0)
			{
				//statusCoin.text = 'Coin : ' + player_coin.toString();
				GameConfig.game_playerInfo.coin = player_coin
//				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_COIN_CHANGE_EVENT));	
				var m_coMsgGainCoin:ResultPanelCoin = new ResultPanelCoin();
				m_coMsgGainCoin.fnSetValue(withdraw_coin,true,false);
				m_coMsgGainCoin.x = -200
				m_coMsgGainCoin.y = 220
			
				TTCSoundHandler.fnGetInstance().fnEmbeddedSoundCoin();
				m_sp.addChild(m_coMsgGainCoin);
				setTimeout(function():void 
				{
					fnTweenEffect(m_coMsgGainCoin);
				}, 2000)
			}
			
		}
		
		private function fnTweenEffect(mc:Sprite):void
		{
			Tweener.addTween(mc,{y:-150,time:1,alpha:0,transition:"easeInQuart",onComplete:fnTweenEffectFinish});
		}
		
		private function fnTweenEffectFinish():void
		{
			m_sp.removeChildren();
		}
		/// 更新錢池
		public function showPool(coin:Number):void{
		//	statusPool.text = 'Pool : '+coin.toString();
			
			m_coCoinValue.fnSetValue2(coin);
		}
		/// 顯示底於最小值所以不能領取（withdraw_min:最小值）
		public function showBelowWithdrawMin(withdraw_min:Number):void{
			statusMsg.text = "大於 "+withdraw_min.toString()+" 才可領取!!";
			setTimeout(function():void 
			{
				statusMsg.text="";
			}, 2000)
		}
		
		private function onAdd(e:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAdd);
			this.addEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			init();
		}
		
		private function onRemove(e:Event):void
		{
			this.removeEventListener(Event.REMOVED_FROM_STAGE, onRemove);
			this.addEventListener(Event.ADDED_TO_STAGE, onAdd);
		}
		
		
	
		
		
	}
}