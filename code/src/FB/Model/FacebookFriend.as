package FB.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	import flash.display.DisplayObject;
	
	import model.PlayerRecord;
	
	public class FacebookFriend extends JsonObject
	{
		public var id:String;
		public var name:String;
		public var level:int;
		public var record:PlayerRecord = new PlayerRecord(null);
		public var picture:FacebookPicture = new FacebookPicture();
		public var image:DisplayObject = null;
		public var coin:Number = 0;
		
		public function FacebookFriend(obj:*=null)
		{
			super(obj);
		}
		
		public function get win():int
		{
			return record.win;
		}
		
		public function get lose():int
		{
			return record.lose;
		}
		
		public function get rate():Number
		{
			return record.rate;
		}
		
		public function get url():String
		{
			return picture.data.url;
		}
	}
}