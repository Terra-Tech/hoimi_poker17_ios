package model
{
	import com.hoimi.lib.net.NetPacket;

	public class BuyingInfo extends NetPacket
	{
		/** buying
		 * 1:{"result" : 1, 'game_id':92, 'player_replace_cards':[0,2], 'whos_turn':0, 'enemy_max_multiple':6}
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 * -4:param miss
		 * -5:state error
		 * -6:buying cards not in hand
		 **/ 
		public var game_id:int = 0;
		/// 0:Player, 1:Enemy
		public var whos_turn:int = 0;
		public var player_replace_cards:Array = [];
		public var player_cards:Array = [];
		public var player_reward:String;
		public var enemy_max_multiple:int = 0;
		
		public function BuyingInfo(buying:String)
		{
			super(buying);
		}
	}
}