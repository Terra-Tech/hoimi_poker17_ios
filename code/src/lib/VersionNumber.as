package lib
{
	
	import flash.desktop.NativeApplication;
	

	public class VersionNumber
	{
		public function VersionNumber()
		{
		}
		
		public static function fnGetVersionNumber():String
		{
			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			var vs_no:String = appXml.ns::versionNumber;
			return vs_no;
		}
		
		public static function fnGetVersionWeights(nValue1:int,nValue2:int,nValue3:int):int
		{
			return nValue1*100+ nValue2*10+nValue3;
		}
	}
}