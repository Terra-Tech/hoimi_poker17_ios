package section
{
	import com.hoimi.util.DeviceUtil;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	import com.hoimi.util.Rating;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import Enum.EnumLocalSave;
	import Enum.EnumMsg;
	import Enum.EnumSection;
	
	import base.TTCSectionBase;
	
	import event.GameEvent;
	import event.GameMessageEvent;
	
	import handler.TTCSoundHandler;
	import handler.TxtLangTransfer;
	
	import menu.MenuBank;
	import menu.MenuBattle;
	import menu.MenuCoinBack;
	import menu.MenuFriends;
	import menu.MenuRank;
	import menu.MenuSkill;
	
	import ui.UnCompletePanel;
	
	
	

	
	
	
	public class SectionMenu extends TTCSectionBase
	{
	
		private var m_mcBg:Poker_Menu_Bg = new Poker_Menu_Bg();
		private var m_coBattle:MenuBattle = new MenuBattle();
		private var m_coFriends:MenuFriends = new MenuFriends();
		private var m_coRank:MenuRank = new MenuRank();
		private var m_coSkill:MenuSkill = new MenuSkill();
		private var m_coBank:MenuBank = new MenuBank();
		private var m_coCoinBack:MenuCoinBack = new MenuCoinBack();
		private var m_coUncompletePanel:UnCompletePanel = new UnCompletePanel();
		private var m_spNew:Sprite = new Sprite();
		private var m_spOriginal:Sprite = new Sprite();
		
		public function SectionMenu()
		{
			super();
			this.addChild(m_mcBg);
			
			this.addChild(m_coFriends);
			this.addChild(m_coRank);
			this.addChild(m_coSkill);
			this.addChild(m_coBattle);
			this.addChild(m_coCoinBack);
			this.addChild(m_spOriginal);	
			this.addChild(m_coBank);
			this.addChild(m_spNew);
			this.addChild(m_coUncompletePanel);
			
			m_coUncompletePanel.fnDisplay(GameConfig.game_playerInfo.uncomplete_winlose,GameConfig.game_playerInfo.reward_coin,GameConfig.game_playerInfo.reward_diamond);
			m_coFriends.addEventListener(MouseEvent.CLICK,fnClickFriends);
			m_coBattle.addEventListener(MouseEvent.CLICK,fnClickBattle);
			m_coSkill.addEventListener(MouseEvent.CLICK,fnClickSkill);
			m_coCoinBack.addEventListener(MouseEvent.CLICK,fnClickCoinBack);
			m_coBank.addEventListener(MouseEvent.CLICK,fnClickBank);
			m_coRank.addEventListener(MouseEvent.CLICK,fnClickRank);
			fnCheckGiveRate();
		}
		
		private function fnCheckGiveRate():void
		{
			if(GameConfig.game_playerRecord.win >= 3 && LocalSaver.instance.getValue(EnumLocalSave.APP_RATE)==undefined && !DeviceUtil.getInstance().isOnIOS)
			{
				var ev:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
				ev.m_strMsg = TxtLangTransfer.GIVE_RATE;
				ev.m_strType =  EnumMsg.MSG_TYPE_YES_NO;
				ev.m_callYesFn = fnNavigateRateYes;
				LocalSaver.instance.save(EnumLocalSave.APP_RATE,true);
				EventManager.instance.trigger(ev);
					
			}
			
		}
		
		private function fnNavigateRateYes():void
		{
			Rating.openRatePage(GameConfig.APPLE_ID);
		}
		
		private function fnClickCoinBack(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			m_spNew.addChild(m_coCoinBack);
		}
		
		private function fnClickBank(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			m_spOriginal.addChild(m_coCoinBack);
		}
		
		protected function fnClickBattle(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NEXT_SECTION_EVENT);
			ev.m_eventArgs = EnumSection.SECTION_LOBBY;
			dispatchEvent(ev);
		}		
		
		protected function fnClickFriends(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NEXT_SECTION_EVENT);
			ev.m_eventArgs = EnumSection.SECTION_FRIENDS;
			dispatchEvent(ev);
		}		
		
		protected function fnClickSkill(event:MouseEvent):void
		{
			// TODO Auto-generated method stub
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NEXT_SECTION_EVENT);
			ev.m_eventArgs = EnumSection.SECTION_SKILL;
			dispatchEvent(ev);
		}		
		
		private function fnClickRank(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_NEXT_SECTION_EVENT);
			ev.m_eventArgs = EnumSection.SECTION_RANK;
			dispatchEvent(ev);
		}
		
		override public function fnResize():void
		{
			m_mcBg.x = 360 +  GameConfig.object_adjuct_position_x 
			m_coFriends.x = GameConfig.object_adjuct_position_x
			m_coFriends.y = 218.5 
			m_coRank.x = 239 +  GameConfig.object_adjuct_position_x
			m_coRank.y = 218.5 
			m_coSkill.x = 486 +  GameConfig.object_adjuct_position_x
			m_coSkill.y = 218.5 
			m_coBattle.x = GameConfig.object_adjuct_position_x
			m_coBattle.y = 512.45	
			m_coBank.x = GameConfig.object_adjuct_position_x
			m_coBank.y = 926	
			m_coCoinBack.x = 362 +  GameConfig.object_adjuct_position_x
			m_coCoinBack.y = 926 
			m_coUncompletePanel.x = GameConfig.object_adjuct_position_x
		
		
		}
		
		override public function fnPause():void
		{
			super.fnPause();
		}
		
		override public function fnResume():void
		{
			super.fnResume();
		}
		
		override public function fnSkip():void
		{
			super.fnSkip();
		}
		
		
		override public function fnReplay():void
		{
			super.fnReplay();
		}
		
		override public function fnExit():void
		{
			super.fnExit()
			this.removeChildren();
		}
		
		
	}
}