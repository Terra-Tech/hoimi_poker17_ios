package com.hoimi.util
{
	import com.vungle.extensions.Vungle;
	import com.vungle.extensions.VungleAdConfig;
	import com.vungle.extensions.events.VungleEvent;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;

	public class VungleHandler extends EventDispatcher
	{
		static public const EVENT_REWARD_COMPLETED:String = "EVENT_REWARD_COMPLETED";
		
		static private var _instance:VungleHandler;
		
		static public function get instance():VungleHandler
		{
			if(!_instance)
			{
				_instance = new VungleHandler(new singletonforcer);
			}
			return _instance;
		}
		
		public function init(appid:Array):void
		{
			try
			{ 
				trace("Vungle init " + appid);
				// initialize with your app id 
				Vungle.create(appid); 
				
				listenEvents();
				
				// -OR- initialize including both ios and android ids for multiplatform apps 
				// Vungle.create(["your_ios_vungle_id","your_android_vungle_id"]); 
			} catch (error:Error) {
				// could not create extension. Are you running on something besides iOS/Android?
				trace("Vungle error " +  error.message);
			}
		}
		
		public function destroy():void
		{
			removeEvents();
		}
		
		public function playIncentivized():void
		{
			var config:VungleAdConfig = new VungleAdConfig();
			config.incentivized = true;
			Vungle.vungle.playAd(config);
		}
		
		public function playInterstitial():void
		{
			Vungle.vungle.playAd();
		}
		
		public function available():Boolean
		{
			return Vungle.vungle.isAdAvailable();
		}
		
		public function VungleHandler(innerClass:singletonforcer){}
		
		private function listenEvents():void
		{
			Vungle.vungle.addEventListener(VungleEvent.AD_PLAYABLE, onAdPlayable);
			/// The AD_STARTED and AD_FINISHED events are dispatched when an ad is displayed and dismissed
			Vungle.vungle.addEventListener(VungleEvent.AD_STARTED, onAdStarted); 
			Vungle.vungle.addEventListener(VungleEvent.AD_FINISHED, onAdFinished); 
			/// For incentivized ads, Vungle considers a viewing where more than 80% of the video was seen as a completed view
			Vungle.vungle.addEventListener(VungleEvent.AD_VIEWED, onAdViewed);
		}
		
		private function removeEvents():void
		{
			Vungle.vungle.removeEventListener(VungleEvent.AD_PLAYABLE, onAdPlayable);
			/// The AD_STARTED and AD_FINISHED events are dispatched when an ad is displayed and dismissed
			Vungle.vungle.removeEventListener(VungleEvent.AD_STARTED, onAdStarted); 
			Vungle.vungle.removeEventListener(VungleEvent.AD_FINISHED, onAdFinished); 
			/// For incentivized ads, Vungle considers a viewing where more than 80% of the video was seen as a completed view
			Vungle.vungle.removeEventListener(VungleEvent.AD_VIEWED, onAdViewed);
		}
		
		private function onAdPlayable(e:VungleEvent):void
		{
			trace("on ad able");
		}
		
		private function onAdStarted(e:VungleEvent):void 
		{ 
			trace("ad displayed"); 
		} 
		
		private function onAdFinished(e:VungleEvent):void
		{ 
			trace("ad dismissed: " + e.wasCallToActionClicked);
		}
		
		private function onAdViewed(e:VungleEvent):void 
		{ 
			trace("watched"+e.watched+" of "+e.length+" second video."); 
			var percentComplete:Number=e.watched/e.length; 
			if(percentComplete>0.80) 
			{ 
				trace("counts a completed view- present reward.");
				this.dispatchEvent(new Event(VungleHandler.EVENT_REWARD_COMPLETED));
			} 
		}
	}
}

class singletonforcer{}