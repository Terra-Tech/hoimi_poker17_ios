package item
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import event.GameEvent;
	
	public class DecisionTiemr extends Sprite
	{
		private var m_mcTimer:Poker_Timer = new Poker_Timer();
		private var m_timeTick:Timer = new Timer(1000)
		private var m_nCountTime:int
			
		public function DecisionTiemr()
		{
			super();
			this.addChild(m_mcTimer);
			m_mcTimer.visible=false;
		}
		
		public function fnStartPcik():void
		{
			m_mcTimer.visible = true;
			m_nCountTime = GameConfig.game_table.time_pick
			m_mcTimer.Txt_Timer.text = 	m_nCountTime+"";
			m_timeTick.addEventListener(TimerEvent.TIMER,fnTimeComplete);
			if(!GameConfig.appPause)
			{
				m_timeTick.reset();
				m_timeTick.start()
			}
		}
		
		public function fnStartDecision(nDecisionTime:Number):void
		{
			m_mcTimer.visible = true;
			m_nCountTime = nDecisionTime
			m_mcTimer.Txt_Timer.text = 	m_nCountTime+"";
			m_timeTick.addEventListener(TimerEvent.TIMER,fnTimeComplete);
			if(!GameConfig.appPause)
			{
				m_timeTick.reset();
				m_timeTick.start()
			}
		}
		
		public function fnStartBuying():void
		{
			m_mcTimer.visible = true;
			m_nCountTime = GameConfig.game_table.time_buying
			m_mcTimer.Txt_Timer.text = 	m_nCountTime+"";
			m_timeTick.addEventListener(TimerEvent.TIMER,fnTimeComplete);
			if(!GameConfig.appPause)
			{
				m_timeTick.reset();
				m_timeTick.start()
			}
		}
		
		public function fnStartResultTime():void
		{
			m_mcTimer.visible = true;
			m_nCountTime = GameConfig.game_table.time_result
			m_mcTimer.Txt_Timer.text = 	m_nCountTime+"";
			m_timeTick.addEventListener(TimerEvent.TIMER,fnTimeComplete);
			if(!GameConfig.appPause)
			{
				m_timeTick.reset();
				m_timeTick.start()	
			}
		}
		
		private function fnTimeComplete(e:TimerEvent):void
		{
			m_nCountTime--
			m_mcTimer.Txt_Timer.text = 	m_nCountTime+"";		
			if(m_nCountTime < 0)
			{
				m_mcTimer.visible = false;
				m_timeTick.stop()
				m_timeTick.removeEventListener(TimerEvent.TIMER_COMPLETE,fnTimeComplete);
				var ev:GameEvent =new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_TIME_FINISH)
				dispatchEvent(ev);
			}
		}
		
		public function fnCountDownStop():void
		{
			m_mcTimer.visible = false;
			m_timeTick.stop()
			m_timeTick.removeEventListener(TimerEvent.TIMER_COMPLETE,fnTimeComplete);
		}
		
		public function fnResume():void
		{
			if(!m_timeTick.running && m_mcTimer.visible)
			{
				m_timeTick.start();
			}
		}
		
		public function fnPause():void
		{
			if(m_timeTick.running)
			{
				m_timeTick.stop();
			}
		}
		
		public function fnDestory():void
		{
			m_timeTick.stop()
			m_timeTick.removeEventListener(TimerEvent.TIMER_COMPLETE,fnTimeComplete);
		}
	}
}