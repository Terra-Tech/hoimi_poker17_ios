package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class IAPDiamondInfo extends NetPacket
	{
		/** iap diamond
		 * 1:{"result" : 1, 'product_id':twd_150, 'vaild':true, player_diamond:1560}
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 * -4:param miss
		 * -5:data lost
		 */
		public var product_id:String;
		public var valid:Boolean = false;
		public var player_diamond:int = 0;
		public function IAPDiamondInfo(iap:String)
		{
			super(iap);
		}
	}
}