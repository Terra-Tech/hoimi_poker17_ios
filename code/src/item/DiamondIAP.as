package item
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import base.TiDisableTextField;
	
	import event.GameEvent;
	
	import handler.TTCSoundHandler;
	
	public class DiamondIAP extends Sprite
	{
		private var m_coDiamondValue:HoimiDiamond = new HoimiDiamond();
		private var m_mcBtnIAP:Poker_Btn_IAP = new Poker_Btn_IAP();
		
		public function DiamondIAP()
		{
			this.addChild(m_mcBtnIAP);
			this.addChild(m_coDiamondValue);
			m_coDiamondValue.mouseEnabled=false;
			m_coDiamondValue.mouseChildren=false;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcBtnIAP);
			m_mcBtnIAP.addEventListener(MouseEvent.CLICK,fnClickHandler);
			m_coDiamondValue.y = 23
			m_coDiamondValue.x = 132
			EventManager.instance.register(GameEvent.GAME_DIAMOND_CHANGE_EVENT, fnDiamondChange);
			
		}
		
		
		private function fnDiamondChange(e:GameEvent):void
		{
			m_coDiamondValue.fnSetValue(GameConfig.game_playerInfo.diamond)
		}
		
		
		
		private function fnClickHandler(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_IAP_EVENT));
			
		}
		
		
		
		public function fnDestory():void
		{
		}
	}
}