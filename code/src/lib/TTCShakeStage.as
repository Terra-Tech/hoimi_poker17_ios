package lib
{
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	public class TTCShakeStage
	{
		private static var m_time:Timer
		private static var m_direction:int =1;
		private static var m_co:Sprite = new Sprite();
		private static var m_nMoviePixel:int = 20
		
		public function TTCShakeStage()
		{
			
		}
		
	
		public static function fnShake(co:Sprite,nInterval:int = 15,nTimes:int = 10, nMovePixel:int = 20):void
		{
			// TODO Auto-generated method stub
			m_co = co
			m_direction = 1
			m_nMoviePixel = nMovePixel 
			m_time = new Timer(nInterval,nTimes);
			m_time.reset();
			m_time.addEventListener(TimerEvent.TIMER,fnMove);
			m_time.addEventListener(TimerEvent.TIMER_COMPLETE,fnMoveComplete);
			m_time.start();
		}
		
		private static function fnMove(e:TimerEvent):void
		{
			
			//var ran:int = Math.round(Math.random()*3)+1;
			switch(m_direction)
			{
				case 1:
					m_co.x -=m_nMoviePixel 
					m_direction++
					break;
				case 2 :
					m_co.y -=m_nMoviePixel 
					m_direction++
					break;
				case 3 :
					m_co.x +=m_nMoviePixel 
					m_direction++
					break;
				case 4:
					m_co.y +=m_nMoviePixel
					m_direction = 1;
					break;
				
			}
		}
			
		protected static function fnMoveComplete(event:TimerEvent):void
		{
			// TODO Auto-generated method stub
			m_co.x = 0;
			m_co.y = 0;
		}
	
	}
}