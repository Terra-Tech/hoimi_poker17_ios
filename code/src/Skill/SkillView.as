package Skill
{
	import com.freshplanet.lib.ui.scroll.mobile.ScrollController;
	import com.freshplanet.lib.ui.util.RectangleSprite;
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import Enum.EnumMsg;
	
	import Refill.RefillController;
	import Refill.RefillView;
	
	import Skill.Model.Skill;
	import Skill.Model.SkillRaise;
	import Skill.skillItem.SkillItem_01;
	import Skill.skillItem.SkillItem_02;
	import Skill.skillItem.SkillItem_03;
	import Skill.skillItem.SkillItem_04;
	import Skill.skillItem.SkillItem_05;
	import Skill.skillItem.SkillItem_06;
	import Skill.skillItem.SkillItem_07;
	
	import event.GameEvent;
	import event.GameMessageEvent;
	
	import handler.TTCSoundHandler;
	
	import section.SectionSkill;
	
	public class SkillView extends Sprite implements ISkillView
	{
		public function SkillView()
		{
		}
		private static var m_Instance:SkillView;
		private var m_sp:Sprite = new Sprite();
		private var m_coSkill1:SkillItem_01 = new SkillItem_01();
		private var m_coSkill2:SkillItem_02 = new SkillItem_02();
		private var m_coSkill3:SkillItem_03 = new SkillItem_03();
		private var m_coSkill4:SkillItem_04 = new SkillItem_04();
		private var m_coSkill5:SkillItem_05 = new SkillItem_05();
		private var m_coSkill6:SkillItem_06 = new SkillItem_06();
		private var m_coSkill7:SkillItem_07 = new SkillItem_07();
		private var m_skills:Array 
		private var m_skillsNext:Array
		private var m_nMaskHeight:Number = 1030
		private var m_nDefaultSpY:Number = 250
		private var m_scrollSp:Sprite = new Sprite();
		private var _scroll:ScrollController;
		private var m_nScrollPoint:Point = new Point(0,0);
		private const SKILL_AMOUNT:int = 7;

		public static function fnGetInstance(): SkillView 
		{
			if ( m_Instance == null )
			{
				m_Instance = new SkillView();
			}
			return m_Instance;
		}
		
		public function init():void
		{
		//	this.addChild(m_sp);
			//m_sp.y = 250
			m_sp = new Sprite();
			m_scrollSp = new Sprite();
			this.addChild(m_scrollSp);
			m_sp.addChild(m_coSkill1);
			m_sp.addChild(m_coSkill2);
			m_sp.addChild(m_coSkill3);
			m_sp.addChild(m_coSkill4);
			m_sp.addChild(m_coSkill5);
			m_sp.addChild(m_coSkill6);
			m_sp.addChild(m_coSkill7);
			m_coSkill1.cacheAsBitmap = true;
			m_coSkill2.cacheAsBitmap = true;
			m_coSkill3.cacheAsBitmap = true;
			m_coSkill4.cacheAsBitmap = true;
			m_coSkill5.cacheAsBitmap = true;
			m_coSkill6.cacheAsBitmap = true;
			m_coSkill7.cacheAsBitmap = true;

			m_coSkill1.x = 360
			m_coSkill2.x = 360
			m_coSkill3.x = 360
			m_coSkill4.x = 360
			m_coSkill5.x = 360
			m_coSkill6.x = 360
			m_coSkill7.x = 360
			m_coSkill1.y = 0
			m_coSkill2.y = 200
			m_coSkill3.y = 400
			m_coSkill4.y = 600
			m_coSkill5.y = 800
			m_coSkill6.y = 1000
			m_coSkill7.y = 1200
				
			m_skills = 	GameConfig.game_playerInfo.skills
			m_skillsNext  = GameConfig.game_playerInfo.skills_next
			fnInitData();
			fnInitScroll();
			EventManager.instance.trigger(new Event(SkillController.EVENT_SKILL_INIT));
		}
		
		private function fnInitData():void
		{
			var skill:Skill
			var skillNext:Skill
			for(var i :int = 0 ;i< SKILL_AMOUNT;i++)
			{
				skill= m_skills[i]
				skillNext = m_skillsNext[i]
				this["m_coSkill"+(i+1)].fnSetData(skill.level, skill.value,skillNext.value,skillNext.coin,skillNext.diamond);
			}
		}
		
		private function fnInitScroll():void
		{
			var container:RectangleSprite = new RectangleSprite(0xFFFFFF, 0, m_nDefaultSpY, 0, m_nMaskHeight);//red background
			m_scrollSp.addChild(container);
			container.addChild(m_sp);
			var containerViewport:Rectangle = new Rectangle(0, 0, 720 , m_nMaskHeight);
			var containerRect:Rectangle = new Rectangle(0, 0, 720 , m_nMaskHeight);
			this._scroll = new ScrollController();
			this._scroll.horizontalScrollingEnabled = false;
			this._scroll.addScrollControll(m_sp, container, containerViewport,containerRect);
			_scroll.scrollTo(m_nScrollPoint,false);
			m_nScrollPoint = new Point(0,0);
		}
	
		
		public function netError(msg:String):void
		{
		}
		
		public function serverError(msg:String):void
		{
		}
		
		public function showLoading():void
		{
			trace('loading...');
		}
		
		public function hideLoading():void
		{
		}
		
		public function showIndexError():void
		{
			trace('no such index!');
		}
		
		public function showCoinNotEnought():void
		{
			trace('coin not enought');
		}
		
		public function showDiamondNotEnought():void
		{
			trace('diamond not enought');
		}
		
		public function showArrivedTopLevel():void
		{
			trace('already top level');
		}
		
		public function showRaiseSuccess(skill:SkillRaise):void
		{
			trace('raise success ' + skill.toString());
			TTCSoundHandler.fnGetInstance().fnEmbeddedSoundResult();
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound261()
			var ev:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
			ev.m_strMsg = "\n\n技能升級成功！"
			ev.m_strType= EnumMsg.MSG_TYPE_OK;
			m_nScrollPoint = _scroll.scrollPosition
			EventManager.instance.trigger(ev);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_SECTION_REFRESH));
		}
		
		
		public function destory():void
		{
			this.removeChildren();
			EventManager.instance.trigger(new Event(SkillController.EVENT_SKILL_DESTORY));
		}
	}
}