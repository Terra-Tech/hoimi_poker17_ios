package Double.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class DoubleResult extends JsonObject
	{
		public var game_id:Number;
		public var result_card:int;
		public var win_lose:Number;
		public var player_coin:Number;
		
		public function DoubleResult(obj:*=null)
		{
			super(obj);
		}
	}
}