package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class IAPKey extends NetPacket
	{
		/** key
		 * 1:{"result" : 1, 'publicKey':'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqF8I5NBTBAgVY7T7UoH9PKLcGdOJJjDXoBroLmRPxXvtnI+FzFOGc+OVLuIsZOTx9sG6TjooI3j/Zyprpqc9uO61IIOhBbPWTSQSp7GXfUpXQ5GwbYYOliJFftU7ElPwTnyj98B7Vi1dtdYafgyFK0RTiSVfq/67aHXHmMWGwbWQ+cjXh0W232wsda8r8fiBHwCWU1WjImpZ2r6y+ouZMi23InrmyyFXA/gweoyPvS4MZlI4DTDmyxisY+jGgwtaOHB+iBpmL3QXPQ12PNKqn10JOjIJylL4wkqrc0CVGtub/7KxvNJUld1V4vr7cu8tv0B7LZ4r61E5UB15jSdXGwIDAQAB'}
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 */
		public var publicKey:String;
		public function IAPKey(key:String)
		{
			super(key);
		}
	}
}