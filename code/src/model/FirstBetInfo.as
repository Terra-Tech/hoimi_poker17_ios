package model
{
	import com.hoimi.lib.net.NetPacket;

	public class FirstBetInfo extends NetPacket
	{
		/** first_bet
		 * 1:{'result' : 1, 'game_id':92, 'winlose':0, 'player_coin':330, 'enemy_buying_amount':2}
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 * -4:param miss
		 * -5:state error
		 * -6:bet mulitple over max
		 * -7:player coin not enought
		 **/ 
		public var game_id:int = -1;
		public var winlose:Number = 0;
		public var player_coin:Number = 0;
		public var enemy_buying_amount:int = 0;
		
		public function FirstBetInfo(bet:String)
		{
			super(bet);
		}
	}
}