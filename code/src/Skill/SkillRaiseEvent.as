package Skill
{
	import flash.events.Event;
	
	public class SkillRaiseEvent extends Event
	{
		public var index:int = -1;
		
		public function SkillRaiseEvent(index:int)
		{
			this.index = index;
			super(SkillController.EVENT_SKILL_RAISE);
		}
	}
}