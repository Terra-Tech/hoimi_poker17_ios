package Enum
{
	public class EnumSection
	{
		public function EnumSection()
		{
		
		}
		public static const SECTION_OPENING:String = "SECTION_OPENING" 
		public static const SECTION_MENU:String = "SECTION_MENU" 
		public static const SECTION_FRIENDS:String = "SECTION_FRIENDS" 
		public static const SECTION_RANK:String = "SECTION_RANK" 
		public static const SECTION_SKILL:String = "SECTION_SKILL" 
		public static const SECTION_LOBBY:String = "SECTION_LOBBY"
		public static const SECTION_CONNECTING:String = "SECTION_CONNECTING"
		public static const SECTION_BATTLE:String = "SECTION_BATTLE"
	}
}