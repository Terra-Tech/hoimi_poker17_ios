package Prize
{
	import Prize.Model.Prize;

	public interface IPrizeView
	{
		/// 網路發生問題
		function netError(msg:String):void;
		/// Server 吐錯誤訊息
		function serverError(msg:String):void;
		/// 如打開領獎Panel show loading
		function showLoading():void;
		/// 如打開領獎Panel hide loading
		function hideLoading():void;
		/// 寶箱有驚嘆號顯示有獎可領
		function alert():void;
		/// 顯示獎品列表 list of PrizeInfo, 為空則秀已領完
		function showList(list:Array):void
		/// 領到獎的項目
		function showGain(prize:Prize):void
		/// Panel是否打開
		function get open():Boolean
		/// 已經領取完
		function gained():void
	}
}