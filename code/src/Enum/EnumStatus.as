package Enum
{
	import flash.display.Sprite;
	
	public class EnumStatus extends Sprite
	{
		public function EnumStatus()
		{
			super();
		}
		public static const STATUS_END:String = "STATUS_END";
		public static const STATUS_BET:String = "STATUS_BET";
		public static const STATUS_DEAL:String = "STATUS_DEAL";
		public static const STATUS_DEAL_FINISH:String = "STATUS_DEAL_FINISH"
		public static const STATUS_PICK:String = "STATUS_PICK";
		public static const STATUS_PICK_FINISH:String = "STATUS_PICK_FINISH";
		public static const STATUS_1STBET:String = "STATUS_1STBET";
		public static const STATUS_CALL_DEAL:String = "STATUS_CALL_DEAL";
		public static const STATUS_BUYING:String = "STATUS_BUYING";
		public static const STATUS_2NDBET:String = "STATUS_2NDBET";
		public static const STATUS_CALL_DEAL2:String = "STATUS_CALL_DEAL2";
		public static const STATUS_OPEN_CARD:String = "STATUS_OPEN_CARD";
		public static const STATUS_PLAYER_WIN:String = "STATUS_PLAYER_WIN";
		public static const STATUS_PLAYER_LOSE:String = "STATUS_PLAYER_LOSE";
		public static const STATUS_SHOW_RESULT_PANEL:String = "STATUS_SHOW_RESULT_PANEL";
		
	}
}