package event
{
	import flash.events.Event;
	
	import Enum.EnumMsg;

	public class GameMessageEvent extends Event
	{
		public static const MESSAGE_EVENT:String = "GAME_MESSAGE_EVENT";
		
		private var m_strDetail:String;
		public var m_strType:String=""
		public var m_strMsg:String = "";
		public var m_callYesFn:Function
		public var m_callNoFn:Function
		public var m_callOkFn:Function
		
		
		
		public function get detail() : String
		{
			return this.m_strDetail;
		}
		
		
		public function GameMessageEvent(type:String, detail:String = "",bubbles:Boolean = false,cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			this.m_strDetail = detail;
		}
		
	}
}