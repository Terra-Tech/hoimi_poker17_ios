package model
{
	import com.hoimi.lib.net.JsonObject;
	
	import flash.display.DisplayObject;
	
	public class CoinRankInfo extends JsonObject
	{
		//uuid":"A8C29F5AAAFE186D","coin":183524347,"name":"jay","fb_id":"10206208188189937"},
		public var uuid:String;
		public var coin:Number;
		public var name:String;
		public var fb_id:String;
		public var image:DisplayObject = null;
		public var rank:int
		
		public function CoinRankInfo(data:*)
		{
			super(data);
		}
	}
}