﻿package com.hoimi.util.sound
{
	import flash.media.Sound;
	import flash.utils.Dictionary;
	
	public class SoundManager
	{
		private var _soundMap: Dictionary;
		private var _volume:uint;
		static private var _instance:SoundManager;
		
		static public function get instance():SoundManager
		{
			if(_instance == null)
			{
				_instance = new SoundManager(new singleton());
			}
			return _instance;
		}

		public function SoundManager(s:singleton)
		{
			_soundMap = new Dictionary(true);
		}

		public function add(soundName: String, sound: Sound):void
		{
			_soundMap[soundName] = new SoundEx(sound);
		}
		
		public function getSound(soundName: String):SoundEx
		{
			return _soundMap[soundName];
		}
		
		public function set volume(value:Number):void
		{
			if(value < 0 || value > 1) return;
			_volume = value;
			for each(var sound:SoundEx in _soundMap)
			{
				sound.volume = value;
			}
		}
		
		public function get volume():Number
		{
			return _volume;
		}
		
		public function stopAll():void
		{
			for each(var sound:SoundEx in _soundMap)
			{
				sound.stop(true,true);
			}
		}
	}

}

class singleton{}