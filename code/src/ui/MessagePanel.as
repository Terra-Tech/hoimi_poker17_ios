package ui
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextFormat;
	
	import Enum.EnumLang;
	import Enum.EnumMsg;
	
	import base.TiDisableTextField;
	
	import event.GameEvent;
	import event.GameMessageEvent;
	
	import handler.HoimiFont01LoadHandler;
	import handler.TTCSoundHandler;
	
	public class MessagePanel extends Sprite
	{
		private var m_mc:Poker_Msg_Panel = new Poker_Msg_Panel();
		private var m_callYesFn:Function;
		private var m_callNoFn:Function;
		private var m_callOkFn:Function;
		
		public function MessagePanel()
		{
			super();
			fnSetMask();
			this.addChild(m_mc);
			m_mc.x = 360
			m_mc.y = 300
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_no);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_yes);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_ok);
			m_mc.btn_no.visible = false;
			m_mc.btn_yes.visible = false;
			m_mc.btn_ok.visible = false;
			m_mc.btn_no.addEventListener(MouseEvent.CLICK,fnNo);
			m_mc.btn_yes.addEventListener(MouseEvent.CLICK,fnYes);
			m_mc.btn_ok.addEventListener(MouseEvent.CLICK,fnOk);
			this.visible = false;
			m_mc.txt_msg.mouseEnabled=false;
			
			EventManager.instance.register(GameMessageEvent.MESSAGE_EVENT,fnDisplayPanel);
		//	HoimiFont01LoadHandler.fnGetInstance().addEventListener(GameEvent.GAME_EVENT,GameEventHandler);
		//	HoimiFont01LoadHandler.fnGetInstance().fnLoadFont();
		}
		
		private function GameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_FONT_01_LOAD_COMPLETE_EVENT)
			{
				fnEmbededFont();
			}
		}
		
		private function fnEmbededFont():void
		{
			HoimiFont01LoadHandler.fnGetInstance().removeEventListener(GameEvent.GAME_EVENT,GameEventHandler);
			var tf:TextFormat = new TextFormat();
			tf.font = HoimiFont01LoadHandler.fnGetInstance().embeddedFont.fontName;
			//tf.size = 40;
			//tf.color = "0xFF8800";
			m_mc.txt_msg.embedFonts = true; // very important to set
			m_mc.txt_msg.defaultTextFormat = tf;
				
		}
		
		private function fnSetMask():void
		{
			var masker:Sprite = new Sprite();
			masker.graphics.beginFill(0);
			masker.graphics.drawRect( 0 , 0 , 720 , 1280);
			masker.graphics.endFill();
			masker.alpha = 0
			this.addChild(masker);
			
		}
		
		private function fnDisplayPanel(e:GameMessageEvent):void
		{
			if(e.m_strType == EnumMsg.MSG_TYPE_YES_NO)
			{
				fnDisplayYesNoPanel(e)
			}
			else if(e.m_strType == EnumMsg.MSG_TYPE_OK)
			{
				fnDisplayOkPanel(e)
			}
		}
		
		private function fnDisplayYesNoPanel(e:GameMessageEvent):void
		{
			this.visible =true;
			m_mc.txt_msg.text = e.m_strMsg;	
			m_mc.btn_no.visible = true;
			m_mc.btn_yes.visible = true;
			m_mc.btn_ok.visible = false;
			m_callYesFn = e.m_callYesFn;
			m_callNoFn = e.m_callNoFn;
		}
		
		private function fnDisplayOkPanel(e:GameMessageEvent):void
		{
			this.visible =true;
			m_mc.txt_msg.text = e.m_strMsg;	
			m_mc.btn_no.visible = false;
			m_mc.btn_yes.visible = false;
			m_mc.btn_ok.visible = true;
			m_callOkFn = e.m_callOkFn
		}
		
		
		
		private function fnNo(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound003()
			this.visible = false;
			if(m_callNoFn)
			{
				m_callNoFn();
			}
		}
		
		private function fnYes(e:MouseEvent):void
		{
			trace('message yes ');
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			this.visible = false;
			if(m_callYesFn)
			{
				m_callYesFn();
			}
		}
		
		private function fnOk(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			this.visible = false;
			if(m_callOkFn)
			{
				m_callOkFn();
			}
		}
		
		
	}
}