package handler
{
	import flash.display.Sprite;
	import flash.utils.setTimeout;
	
	public class MyScrollController extends Sprite
	{
		public function MyScrollController()
		{
			super();
			setTimeout(showScrollContrller, 100);
		}
		
		private function showScrollContrller():void
		{
			this.removeChildren();
			this.addChild(new ScrollControllerExample());
		}
	}
}