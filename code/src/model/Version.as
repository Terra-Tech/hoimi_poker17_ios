package model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class Version extends JsonObject
	{
		public var value:String;
		public var message:String;
		public var url:String;
		public function Version(version:String = null)
		{
			super(version);
		}
	}
}