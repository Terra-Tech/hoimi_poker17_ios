package com.hoimi.lib.net 
{
	import flash.events.Event;
	
	public class NetEvent extends Event
	{
		static public const STATE_IO_ERROR:String = "NetEventIOError";
		static public const STATE_TIMEOUT:String = "NetEventTimeout";
		static public const STATE_COMPLETE:String = "NetEventComplete";
		static public const STATE_FAIL:String = "NetEventFail";
		
		private var _data:String = null;
		public var protocol:int = -1;
		public var result:int = 0;
		
		public function NetEvent(type:String, protocol:int, data:String)
		{
			this.protocol = protocol;
			_data = data;
			super(type, false, false);
		}

		public function get data():String
		{
			return _data;
		}

		public function set data(value:String):void
		{
			var net:NetPacket = new NetPacket(value);
			result = net.result;
			_data = value
		}

	}
}