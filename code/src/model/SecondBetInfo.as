package model
{
	import com.hoimi.lib.net.NetPacket;

	public class SecondBetInfo extends NetPacket
	{
		/** second_bet
		 * 1:{'result' : 1, 'game_id':game_id, 'winlose':-60, 'player_coin':3320, 'player_cards':[4,7,9,14,15], 'enemy_cards':[0,1,2,5,6], 'player_reward':2pair, 'enemy_reward':fullhouse}
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 * -4:param miss
		 * -5:bet mulitple over max
		 * -6:player coin not enought
		 * -7:state error
		 **/
		public var game_id:int = 0;
		public var winlose:Number = 0;
		public var player_coin:Number = 0;
		public var player_diamond:int = 0;
		public var player_cards:Array = [];
		public var enemy_cards:Array = [];
		public var player_reward:String = '';
		public var enemy_reward:String = '';
		public var level:int = 0;
		public var exp:int;
		public var next_exp:int;
		/// 升級獎勵
		public var reward_coin:int;
		/// 升級獎勵
		public var reward_diamond:int;
		/// JP 獎勵
		public var jp_bonus:Number;
		public var player_record:PlayerRecord = new PlayerRecord(null);
		/// 返利錢池
		public var coin_back_pool:Number;
		
		public function SecondBetInfo(bet:String)
		{
			super(bet);
		}
	}
}