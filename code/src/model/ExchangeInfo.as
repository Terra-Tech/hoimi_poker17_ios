package model
{
	import com.hoimi.lib.net.NetPacket;
	
	/** exchange
	 * onExchange {"result":1,"player_coin":33036,"player_diamond":167,"exchange_rate":1}
	 * -1:token param miss
	 * -2:can't find token
	 * -3:token expire
	 * -4:param miss
	 * -5:diamond amount <=0
	 * -6:diamond not enought
	 **/ 
	
	public class ExchangeInfo extends NetPacket
	{
		public var exchange_rate:Number = 1;
		public var player_coin:Number = 0;
		public var player_diamond:int = 0;
		public function ExchangeInfo(ex:String)
		{
			super(ex);
		}
	}
}