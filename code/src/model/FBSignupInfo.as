package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class FBSignupInfo extends NetPacket
	{
		/**   signup
		 *  1:success {result:1}
		 * -2:already exist
		 * -3:name duplicate
		 **/
		public var uuid:String;
		public function FBSignupInfo(signup:String){
			super(signup);
		}
	}
}