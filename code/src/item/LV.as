package item
{
	import flash.display.Sprite;
	
	public class LV extends Sprite
	{
		private var m_mc:Poker_LV = new Poker_LV();
		private var m_spLV:Sprite = new Sprite();
		
		public function LV()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_spLV);
			m_spLV.x = m_mc.width
			m_spLV.y = 13
				
		}
		
		public function fnSetLV(nLevel):void
		{
			m_spLV.removeChildren();
			var lv:HoimiExpSmall = new HoimiExpSmall();
			lv.fnSetValue2(nLevel);
			m_spLV.addChild(lv);
		}
	}
}