package com.hoimi.util
{
	public class Random
	{
		static public function randomize(a:*, b:*):int
		{
			return ( Math.random() > .5 ) ? 1 : -1;
		}
	}
}