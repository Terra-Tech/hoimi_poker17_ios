package item
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.utils.setTimeout;
	
	import Enum.EnumLang;
	
	import caurina.transitions.Tweener;
	
	public class CardResult extends Sprite
	{
		private var m_mcResult:Poker_Msg_Card_Result = new Poker_Msg_Card_Result();
		private var m_mcBase:Poker_Msg_Card_Result_Base = new Poker_Msg_Card_Result_Base();
		private var m_mcTxtAni:Poker_Msg_Ani = new Poker_Msg_Ani();
		private var m_mc:MovieClip;
		
		public function CardResult()
		{
			super();
			this.addChild(m_mcBase);
			this.addChild(m_mcResult);
			this.addChild(m_mcTxtAni);
			m_mcBase.x=0;
			m_mcResult.x=360;
		//	m_mc.y = 18;
			m_mcResult.visible =false;
			m_mcBase.visible=false
			m_mcTxtAni.visible=false;
			m_mcTxtAni.x = 50
			m_mcTxtAni.y = 80
			m_mcResult.EN.visible=false;
			m_mcResult.TW.visible=false;
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				m_mcResult.TW.visible = true
				m_mc = m_mcResult.TW
			}
			else
			{
				m_mcResult.EN.visible = true
				m_mc = m_mcResult.EN
			}
			
		}
		
		public function fnShowResult(strEnum:String):void
		{
			/*
		/	1,"5Cards",8,20,100
		/	2,"RoyalStraightFlush",7,10,99
		/	4,"4Cards",6,2,96
		/	5,"Fullhouse",5,1,90
		/	6,"Straight",4,1,80
		/	7,"3Cards",3,1,66
		/	8,"2Pair",2,1,48
		/	9,"1Pair",1,1,26
			10,"Joker4Cards",6,2,96
			11,"JokerFullhouse",5,1,90
			12,"Joker3Cards",3,1,66
			*/
			m_mcResult.visible=true;
			m_mcBase.visible = true;
			switch(strEnum)
			{
				case "1Pair":
					m_mc.gotoAndStop(1);
					break;
				case "2Pair":
					m_mc.gotoAndStop(2);
					break;
				case "3Cards":
				case "Joker3Cards":	
					m_mc.gotoAndStop(3);
					break;
				case "Straight":
					m_mc.gotoAndStop(4);
					break;
				case "Fullhouse":
				case "JokerFullhouse":
					m_mc.gotoAndStop(5);
					break;
				case "4Cards":
				case "Joker4Cards":	
					m_mc.gotoAndStop(6);
					break;
				case "RoyalStraightFlush":
					m_mc.gotoAndStop(7);
					break;
				case "5Cards":
					m_mc.gotoAndStop(8);
					break;
			}
			fnTweenDisplay();
		}
		
		private function fnTweenDisplay():void
		{
		//	m_mcBase.y = -200 
		//	Tweener.addTween(m_mcBase,{y:0,time:0.15,transition:"easeInSine",onComplete:fnDisplayFinish});
			m_mcTxtAni.visible=true;
			m_mcTxtAni.gotoAndPlay(2);
		/*	setTimeout(function():void 
			{
				fnTweenHide();
			}, 3000)*/
		}
		
		private function fnDisplayFinish():void
		{
			//m_mcResult.y = -200 
		//	Tweener.addTween(m_mcResult,{x:360,time:0.15,transition:"easeInSine",onComplete:fnWordDisplayFinish});
		
		}
		
		private function fnWordDisplayFinish():void
		{
			setTimeout(function():void 
			{
				fnTweenHide();
			}, 1000)

		}
		
		private function fnTweenHide():void
		{
			m_mcResult.visible =false;
			m_mcBase.visible=false;
			//Tweener.addTween(m_mcResult,{y:-200,time:0.15,transition:"easeOutExpo",onComplete:fnTweenHideFinish});
			//Tweener.addTween(m_mcBase,{y:-200,time:0.15,transition:"easeOutExpo"});
		}
		
		private function fnTweenHideFinish():void
		{
			
		}
	}
}