package conf
{
	import lib.RandomMachine;

	public class musicConfig
	{
		public function musicConfig()
		{
		}
		
		public static const GAME_MUSIC_MENU:String = "resource/music/menu.mp3";
		public static const GAME_MUSIC_01:String = "resource/music/01.mp3";
		public static const GAME_MUSIC_02:String = "resource/music/02.mp3";
		public static const GAME_MUSIC_03:String = "resource/music/03.mp3";
		public static const GAME_MUSIC_DOUBLE:String = "resource/music/double.mp3";

		private static var _BATTLE_MUSIC:String

		public static function get BATTLE_MUSIC():String
		{
			var nRan:int = RandomMachine.fnGetRanOneValue([1,2,3]);
			_BATTLE_MUSIC =  "resource/music/0"+nRan+".mp3";
			return _BATTLE_MUSIC;
		}

		

	}
}