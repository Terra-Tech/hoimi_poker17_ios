package lib
{
	public class RandomMachine
	{
		public function RandomMachine()
		{
		}
		
		public static function fnGetRanArray(ar:Array,nRan:int):Array
		{
			if(nRan > ar.length)
			{
				trace("fnGetRanArray nRan: "+nRan +" > ar.length:"+ar.length);
				return ar;
			}
			var arRetrun:Array = new Array(); 
			var arCopy:Array = ar.concat();
			for(var i:int =0 ;;i++)
			{
				var ran:int = Math.round(Math.random()*(ar.length-1));
				if(arCopy[ran] != "mark")
				{
					arRetrun.push(arCopy[ran]);
					arCopy[ran] = "mark";
				}
				if(arRetrun.length == nRan)
				{
					break;
				}
			}
			return arRetrun;	
		}
		
		public static function fnGetRanOneValue(ar:Array):int
		{
			var nValue:int = 0;
			if(ar.length == 0)
			{
				trace("fnGetRanOneValue ar.length == 0");
				return nValue;
			}
			var ran:int = Math.round(Math.random()*(ar.length-1));
			var returnValue:Number = ar[ran]
			return returnValue;	
		}
		
		public static function fnGetRanOneValueAndRemove(ar:Array):int
		{
			var nValue:int = 0;
			if(ar.length == 0)
			{
				trace("fnGetRanOneValue ar.length == 0");
				return nValue;
			}
			var ran:int = Math.round(Math.random()*(ar.length-1));
			var returnValue:Number = ar[ran]
			ar.splice(ran,1);
			return returnValue;	
		}
		
		public static function fnGetRanArrayAndRemove(ar:Array,nRan:int):Array
		{
			if(nRan > ar.length)
			{
				trace("fnGetRanArrayAndRemove nRan: "+nRan +" > ar.length:"+ar.length);
			}
			var arRetrun:Array = new Array(); 
			for(var i:int =0 ;;i++)
			{
				var ran:int = Math.round(Math.random()*(ar.length-1));
				if(ar[ran] != "mark")
				{
					arRetrun.push(ar[ran]);
					ar[ran] = "mark";
				}
				if(arRetrun.length == nRan)
				{
					break;
				}
			}
			for(var j:int = ar.length -1 ;j>=0 ;j--)
			{
				if(ar[j] == "mark")
				{
					ar.splice(j,1);
				}
			}
			return arRetrun;	
		}
	}
}