package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class FacebookInfo extends NetPacket
	{/** facebook
		* 1:{"result" : 1, 'name':廖國捷, 'level':3, 'record':{win:0,lose:1,rate:0}}
		* -3:token expire
		* -5:can't find anyone
		**/
		public var name:String = '';
		public var level:int = 1;
		public var record:PlayerRecord = new PlayerRecord(null);
		
		public function FacebookInfo(fb:*){
			super(fb);
		}
	}
}