package ui
{
	import com.hoimi.lib.net.NetEvent;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import base.TiDisableTextField;
	
	import handler.TTCSoundHandler;
	
	import item.SoundPanel;
	
	import model.SNPrize;
	
	public class SettingPanel extends Sprite
	{
		private var m_mc:Poker_Btn_Setting = new Poker_Btn_Setting();
		private var m_mcBase:Poker_Setting_Base = new Poker_Setting_Base();
		private var m_soundPanel:SoundPanel = new SoundPanel();
		private var m_helpPanel:HelpPanel = new HelpPanel();
		private var m_sp:Sprite = new Sprite();
		private var m_strSerialNumber:String = "";
		
		public function SettingPanel()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_sp);
			m_sp.addChild(m_mcBase);
			m_sp.addChild(m_soundPanel);
			m_sp.addChild(m_helpPanel);
			m_sp.visible =false
			m_mc.x = 641
			m_mc.y = 13
			m_mcBase.x = 360
			m_mcBase.y = 80
			m_soundPanel.x = 110
			m_soundPanel.y = 140
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
			m_mc.addEventListener(MouseEvent.CLICK,fnClickSetting);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcBase.sn.btn_ok);
			m_mcBase.sn.btn_ok.addEventListener(MouseEvent.CLICK,drawSNPrize);
			m_mcBase.sn.txt_input.restrict = "A-Za-z0-9"
			m_mcBase.sn.txt_input.maxChars = 18
			m_mcBase.sn.txt_msg.mouseEnabled =false;
		}
		
		private function fnClickSetting(e:MouseEvent):void
		{
			m_sp.visible = !m_sp.visible
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();	
			m_mcBase.sn.txt_input.text = "";
			m_mcBase.sn.txt_msg.text = ""
			if(GameConfig.game_playerInfo.level >=10)
			{
				m_mcBase.sn.visible = true;
			}
			else
			{
				m_mcBase.sn.visible = false;
			}
		}
		
		private function drawSNPrize(e:MouseEvent):void
		{
			m_strSerialNumber = m_mcBase.sn.txt_input.text;
			//32152G3SFEFAFY11A2
			if(m_strSerialNumber.charAt(17) == "")
			{
				m_mcBase.sn.txt_msg.text = "請輸入英數字共18個組合之序號"
				return;
			}
			SessionManager.instance.request(Protocol.DRAW_SN_RPIZE, {token:GameConfig.game_playerInfo.token, sn:m_strSerialNumber}, onDraw, null);
		}	
		
		/** draw
		 * -5:no such sn
		 * -6:already taken
		 * -7:expired
		 * -8:draw over limit times
		 */
		private function onDraw(e:NetEvent):void
		{
			switch(e.result){
				case -5:
					trace('沒有這組sn');
					m_mcBase.sn.txt_msg.text = "序號輸入錯誤"
					break;
				case -6:
					trace('sn已被領過');
					m_mcBase.sn.txt_msg.text = "此序號已被領過"
					break;
				case -7:
					trace('sn已過期');
					m_mcBase.sn.txt_msg.text = "此序號已過期"
					break;
				case -8:
					trace('超過可領獎次數');
					m_mcBase.sn.txt_msg.text = "超過可領獎次數"
					break;
				case 1:
					trace(e.data);
					var sn:SNPrize = new SNPrize(e.data);
					GameConfig.game_playerInfo.coin = sn.player_coin;
					GameConfig.game_playerInfo.diamond = sn.player_diamond;
					m_mcBase.sn.txt_msg.text = "領獎成功";
					m_mcBase.sn.txt_input.text = "";
					GiftPanel.fnGetInstance().fnSetData(sn.coin,sn.diamond);
					break;
				default:
					break;
			}
		}		
		
		
	}
}