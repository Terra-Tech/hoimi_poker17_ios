package ui
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import Skill.SkillView;
	
	import base.TiDisableTextField;
	
	import handler.TTCSoundHandler;
	
	import item.ResultPanelCoin;
	import item.ResultPanelDiamond;
	
	public class GiftPanel extends Sprite
	{
		private var m_mc:Poker_Msg_Panel = new Poker_Msg_Panel();
		private var m_coCoin:ResultPanelCoin = new ResultPanelCoin();
		private var m_coDiamond:ResultPanelDiamond = new ResultPanelDiamond();
		private static var m_Instance:GiftPanel;
		
		public static function fnGetInstance(): GiftPanel 
		{
			if ( m_Instance == null )
			{
				m_Instance = new GiftPanel();
			}
			return m_Instance;
		}
		
		public function GiftPanel()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_coCoin);
			this.addChild(m_coDiamond);
		
			m_coCoin.x = -200
			m_coCoin.y = 250;
			m_coDiamond.x = -200;
			m_coDiamond.y = 350;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_ok);
			m_mc.btn_ok.addEventListener(MouseEvent.CLICK,fnClickOk);
			m_mc.btn_no.visible =false;
			m_mc.btn_yes.visible =false;
			this.visible = false;
		}
		
		private function fnClickOk(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			this.visible=false;
		}
		
		public function fnSetData(nRewardCoin:int,nRewardDiamond:int):void
		{	
			this.visible=true;
			m_coCoin.fnSetValue(nRewardCoin,true,false);
			m_coDiamond.fnSetValue(nRewardDiamond);
			TTCSoundHandler.fnGetInstance().fnEmbeddedSoundResult();
			m_coCoin.visible =false;
			m_coDiamond.visible =false;
			if(nRewardCoin > 0)
			{
				m_coCoin.visible = true;
			}
			if(nRewardDiamond > 0)
			{
				m_coDiamond.visible = true;
			}
		}
		
	}
}