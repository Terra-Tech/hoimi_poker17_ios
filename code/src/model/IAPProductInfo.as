package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class IAPProductInfo extends NetPacket
	{
		/** product
		 * 1:{"result":1,"product":{"twd_60":6,"twd_150":18,"twd_300":40,"twd_600":85,"twd_1500":225,"twd_3000":500},"bonus":{"uuid":"5275E2A753E6A41C","twd_60":6,"twd_150":18,"twd_300":40,"twd_600":85,"twd_1500":225,"twd_3000":500}}
		 * -1:token param miss
		 * -2:can't find token
		 * -3:token expire
		 */
		

		
		public var product:IAPProduct = new IAPProduct(null);
		public var bonus:IAPBonus = new IAPBonus(null);
		public function IAPProductInfo(iap:String)
		{
			super(iap);
		}
	}
}