package
{
	import com.hoimi.util.DeviceUtil;
	import com.hoimi.util.FacebookConnect;
	import com.hoimi.util.LocalSaver;
	
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.system.Capabilities;
	import flash.ui.Keyboard;
	
	import Enum.EnumLocalSave;
	
	import FB.FacebookConfig;
	
	import Refill.RefillView;
	
	import lib.TTCModeCheck;
	
	import section.SectionAll;
	
	public class Hoimi_Poker17_iOS extends Sprite
	{
		private var main:SectionAll
		public function Hoimi_Poker17_iOS()	
		{
			super();
			if(stage)
			{
			if(TTCModeCheck.isDebugBuild())
			{
				LocalSaver.instance.clear();
			}
				fnInit();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, fnInit);
			}
		}
		
		
		private function fnInit(e:Event = null):void
		{
			GameConfig.device_Language = Capabilities.language;
			//	GameConfig.device_Language = "en"
			main = new SectionAll();
			this.addChild(main);
			this.removeEventListener(Event.ADDED_TO_STAGE, fnInit);
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.addEventListener(Event.RESIZE,onResize);
			stage.addEventListener(Event.ACTIVATE,fnActiveApplication);
			stage.addEventListener(Event.DEACTIVATE,fnDeActiveApplication);
			NativeApplication.nativeApplication.addEventListener(KeyboardEvent.KEY_DOWN, fnOnKeyDown);
			if(FacebookConnect.instance.isSupported && DeviceUtil.getInstance().isOnDevice){
				FacebookConnect.instance.init(FacebookConfig.appID);
				GameConfig.bFacebookReady = true;
			}
		}
		
		
		private function onResize(e:Event):void
		{
			
			var scaleHeightRatio:Number = stage.stageHeight/1280
			var nExpectWidth:Number = 720 * scaleHeightRatio 
			var nActualWidthGap:Number = stage.stageWidth - nExpectWidth	
			GameConfig.stage_displacement_x = nActualWidthGap/2.0
			GameConfig.stage_scale_ratio = scaleHeightRatio
			GameConfig.object_adjuct_position_x = GameConfig.stage_displacement_x/GameConfig.stage_scale_ratio
			main.scaleX = scaleHeightRatio
			main.scaleY = scaleHeightRatio
			main.onResize();
			main.fnResize();
			
			
		}
		
		private function fnActiveApplication(e:Event):void
		{
			trace("Resume");
			GameConfig.appPause = false
			main.fnResume()
			RefillView.fnGetInstance().fnResume();
		}
		
		private function fnDeActiveApplication(e:Event):void
		{
			trace("Pause")
			GameConfig.appPause = true
			main.fnPause();
			RefillView.fnGetInstance().fnPause();
			
		}
		
		private function fnOnKeyDown(e:KeyboardEvent):void
		{
			if(e.keyCode == Keyboard.BACK)
			{
				e.preventDefault();
				main.fnPhysicalBack();
			}
		}
		
	}
}