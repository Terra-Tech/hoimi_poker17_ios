﻿package com.hoimi.util
{
	import flash.net.SharedObject;

	public class LocalSaver
	{
		static private var _instance:LocalSaver;
		private var _so:SharedObject;
		
		public function LocalSaver(singleton:inner)
		{
			_so = SharedObject.getLocal("LocalSave");
		}
		
		static public function get instance():LocalSaver
		{
			if(!_instance)
			{
				_instance = new LocalSaver(new inner);
			}
			
			return _instance;
		}
		
		public function save($ID:String, $value:*):void
		{
			_so.data[$ID] = $value;
			_so.flush();
		}
		
		public function getValue($ID:String):*
		{
			var value_:* = _so.data[$ID];
			return value_;
		}
		
		public function clear():void
		{
			_so.clear();
		}
	}
}

class inner{}