package ui
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import base.TiDisableTextField;
	
	import handler.TTCSoundHandler;
	
	import item.HoimiDiamond;
	import item.LevelUpArrow;
	import item.ResultPanelCoin;
	import item.ResultPanelDiamond;
	
	public class LevelUpPanel extends Sprite
	{
		private var m_mc:Poker_Result_Level_Up_Panel = new Poker_Result_Level_Up_Panel();
		private var m_coLevel:ResultPanelCoin = new ResultPanelCoin();
		private var m_coCoin:ResultPanelCoin = new ResultPanelCoin();
		private var m_coDiamond:ResultPanelDiamond = new ResultPanelDiamond();
		private var m_coArrow:LevelUpArrow = new LevelUpArrow();
		
		public function LevelUpPanel()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_coLevel);
			this.addChild(m_coArrow);
			this.addChild(m_coCoin);
			this.addChild(m_coDiamond);
			m_coArrow.x = 115;
			m_coArrow.y = -130
			m_coLevel.x = -200;
			m_coLevel.y = -100;
			m_coCoin.x = -150
			m_coCoin.y = 0;
			m_coDiamond.x = -150;
			m_coDiamond.y = 100;
			m_coLevel.m_mc.visible=false;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_ok);
			m_mc.btn_ok.addEventListener(MouseEvent.CLICK,fnClickOk);
		
		}
		
		private function fnClickOk(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			this.visible=false;
		}
		
		public function fnSetData(nLevel:int,nRewardCoin:int,nRewardDiamond:int):void
		{	
			m_coArrow.fnTweenEffect();
			m_coLevel.fnSetValue(nLevel,false,false);
			m_coCoin.fnSetValue(nRewardCoin,true,false);
			m_coDiamond.fnSetValue(nRewardDiamond);
			TTCSoundHandler.fnGetInstance().fnEmbeddedSoundResult();
		}
			
	}
}