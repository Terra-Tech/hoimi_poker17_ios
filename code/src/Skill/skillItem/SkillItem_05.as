package Skill.skillItem
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	
	import Skill.SkillBase;
	import Skill.SkillRaiseEvent;
	
	
	import lib.MultiLanguagleUISelect;
	
	public class SkillItem_05 extends SkillBase
	{
		private var m_mcSkill:Poker_Btn_Skill_05 = new Poker_Btn_Skill_05();
		
		public function SkillItem_05()
		{
			super();
			
			
			var mc:MovieClip = MultiLanguagleUISelect.fnMultiLanguageUi(m_mcSkill);
			m_mc = mc;
			this.addChild(m_mcSkill);
			this.addChild(m_sp);
			m_txtExplanation.text = "每場遊戲結算時獲得的經驗值加成";
			m_strSkillName = "[經驗值獲得]";
		}
		
		override protected function fnRaiseSkill():void
		{
			var ev:SkillRaiseEvent = new SkillRaiseEvent(4)
			EventManager.instance.trigger(ev);
		}
		
		override protected function fnDataDisplay(nInfo1:Number,nInfo2:Number):void
		{
			m_txtCurrent.text = "x"+nInfo1
			if(nInfo2 == -1)
			{
				m_txtNext.text = "----"	
				m_mcSkill.arrow.visible=false;
			}
			else
			{
				m_txtNext.text = "x"+nInfo2
			}
			
		}
	}
}