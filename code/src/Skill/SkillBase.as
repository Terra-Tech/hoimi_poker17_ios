package Skill
{
	import com.hoimi.util.EventManager;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	
	import Enum.EnumMsg;
	
	import base.TiButtonState;
	import base.TiDisableTextField;
	
	import event.GameEvent;
	import event.GameMessageEvent;
	
	import handler.TxtLangTransfer;
	import handler.YesNoPanelHandler;
	
	import item.HoimiCoin;
	import item.HoimiDiamond;
	import item.LV;
	

	public class SkillBase extends Sprite
	{
		
		protected var m_mc:MovieClip
		private var m_buttonState:TiButtonState = new TiButtonState();
		protected var m_txtExplanation:TextField = new TextField();
		protected var m_txtCurrent:TextField = new TextField();
		protected var m_txtNext:TextField = new TextField();
		private var m_coLevel:LV = new LV();
		private var m_coDiamondNumber:HoimiDiamond = new HoimiDiamond();
		private var m_coCoinNumber:HoimiCoin = new HoimiCoin();
		private var m_coHoimiCoin:Poker_Hoimi_Coin = new Poker_Hoimi_Coin();
		private var m_coHoimiDiamond:Poker_Hoimi_Diamond = new Poker_Hoimi_Diamond();
		protected var m_sp:Sprite = new Sprite();
		protected var m_strSkillName:String="";
		
		public function SkillBase()
		{
		
			this.addChild(m_sp);
			m_sp.addChild(m_txtExplanation);
			m_sp.addChild(m_txtCurrent);
			m_sp.addChild(m_txtNext);
			m_sp.addChild(m_coLevel);
			m_sp.addChild(m_coHoimiCoin);
			m_sp.addChild(m_coHoimiDiamond);
			m_sp.addChild(m_coCoinNumber);
			m_sp.addChild(m_coDiamondNumber);
			m_sp.mouseChildren=false;
			m_sp.mouseEnabled=false;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
			m_txtExplanation.defaultTextFormat = new TextFormat( "Arial", 24, 0x000000, false, false, false, null, null, TextFormatAlign.LEFT );
			m_txtExplanation.filters = [new DropShadowFilter(2,45,0,0.3,1,1,1,1)];
			m_txtCurrent.defaultTextFormat = new TextFormat( "Arial", 30, 0xCC0000, false, false, false, null, null, TextFormatAlign.LEFT );
			m_txtCurrent.filters = [new DropShadowFilter(2,45,0,0.3,1,1,1,1)];
			m_txtNext.defaultTextFormat = new TextFormat( "Arial", 30, 0xCC0000, false, false, false, null, null, TextFormatAlign.LEFT );
			m_txtNext.filters = [new DropShadowFilter(2,45,0,0.3,1,1,1,1)];
			m_txtExplanation.width = 600
			m_txtExplanation.x = -330
			m_txtExplanation.y = 65
			m_txtCurrent.width = 300
			m_txtCurrent.x = -190
			m_txtCurrent.y = 100
			m_txtNext.x = -190
			m_txtNext.y = 135
			m_txtNext.width = 300
			m_coLevel.x = 200
			m_coLevel.y = 15
			m_coLevel.scaleX = 1.3
			m_coLevel.scaleY = 1.3
			m_coHoimiCoin.x = 55;
			m_coHoimiCoin.y = 150
			m_coHoimiCoin.scaleX = 0.3
			m_coHoimiCoin.scaleY = 0.3
			m_coHoimiDiamond.x = 55;
			m_coHoimiDiamond.y = 115
			m_coHoimiDiamond.scaleX = 0.3
			m_coHoimiDiamond.scaleY = 0.3
			m_coCoinNumber.x = 65
			m_coCoinNumber.y = 151
			m_coDiamondNumber.x = 65
			m_coDiamondNumber.y = 115
		}
		
		
		public function fnBtnNormalState():void
		{
			m_mc.gotoAndStop(1);
		}
		
		public function fnDisableBtn():void
		{
			m_buttonState.fnDisableAndDarkMcState(m_mc);
		}
		
		public function fnSetData(nLevel:int,nInfo1:Number,nInfo2:Number,nCoin:Number,nDiamond:Number):void
		{ 	
		/*	trace("nLevel: "+nLevel);
			trace("nInfo1: "+nInfo1);
			trace("nInfo2: " + nInfo2);
			trace("nCoin: "+nCoin);
			trace("nDiamond: "+nDiamond);*/
			this.removeEventListener(MouseEvent.CLICK,fnCoinNotEnough);
			this.removeEventListener(MouseEvent.CLICK,fnDiamondNotEnough);
			if(nCoin == 0)
			{
				m_buttonState.fnEnableMcState(m_mc);
			}
			else if(nDiamond > GameConfig.game_playerInfo.diamond)
			{
				m_buttonState.fnDisableAndDarkMcState(m_mc);
				this.addEventListener(MouseEvent.CLICK,fnDiamondNotEnough);
			}
			else if(nCoin > GameConfig.game_playerInfo.coin)
			{
				m_buttonState.fnDisableAndDarkMcState(m_mc);
				this.addEventListener(MouseEvent.CLICK,fnCoinNotEnough);
			}
			else
			{
				m_buttonState.fnEnableMcState(m_mc);
				this.addEventListener(MouseEvent.CLICK,fnSkillRaise);
			}
			m_coLevel.fnSetLV(nLevel);
		//	m_txtCurrent.text = nInfo1+""
		//	m_txtNext.text = nInfo2+""
			if(nCoin == 0)
			{
				fnDataDisplay(nInfo1,-1);
			}
			else
			{
				fnDataDisplay(nInfo1,nInfo2);
			}
		
			
			if(nCoin == 0)
			{
				m_coHoimiCoin.visible=false;
				m_coCoinNumber.visible =false;
			}
			else
			{
				m_coCoinNumber.fnSetValue2(nCoin);
			}
			
			if(nDiamond == 0)
			{
				m_coDiamondNumber.visible=false;
				m_coHoimiDiamond.visible=false;
			}
			else
			{
				m_coDiamondNumber.visible=true;
				m_coHoimiDiamond.visible=true;
				m_coDiamondNumber.fnSetValue2(nDiamond);
			}
		}
		
		protected function fnDataDisplay(nInfo1:Number,nInfo2:Number):void
		{
			// be override
		}
		
		private function fnCoinNotEnough(e:MouseEvent):void
		{
			YesNoPanelHandler.fnCoinNotEnough();
		}
		
		private function fnDiamondNotEnough(e:MouseEvent):void
		{
			YesNoPanelHandler.fnDiamondNotEnough();
		}
		
		private function fnSkillRaise(e:MouseEvent):void
		{
			var ev:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
			ev.m_strMsg = m_strSkillName + "\n"+TxtLangTransfer.SKIIL_RAISE
			ev.m_strType =  EnumMsg.MSG_TYPE_YES_NO
			ev.m_callYesFn = fnRaiseSkill
			EventManager.instance.trigger(ev);
			
			
		}
		
		protected function fnRaiseSkill():void
		{
			//be override
		}
		
	
	}
}