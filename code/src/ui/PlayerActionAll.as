package ui
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	
	import Enum.EnumLang;
	
	import base.TiButtonState;
	import base.TiDisableTextField;
	
	import event.GameEvent;
	import event.PokerEvent;
	
	import handler.HoimiFontNumberLoadHandler;
	import handler.TTCSoundHandler;
	
	import item.BtnCall;
	import item.BtnFold;
	
	import lib.CoinValueTransfer;
	
	public class PlayerActionAll extends Sprite
	{
		private var m_mcRaise:Poker_Btn_Raise_All = new Poker_Btn_Raise_All();
		private var m_coCall:BtnCall = new BtnCall();
		private var m_coFold:BtnFold = new BtnFold();
		private var m_mcOk:Poker_Btn_Ok = new Poker_Btn_Ok();
		private var m_sp:Sprite = new Sprite();
		private var m_mcAni1:Poker_Circle_Ani = new Poker_Circle_Ani();
		private var m_mcAni2:Poker_Circle_Ani = new Poker_Circle_Ani();
		private var m_mcAni3:Poker_Circle_Ani = new Poker_Circle_Ani();
		private var m_mcAni4:Poker_Circle_Ani = new Poker_Circle_Ani();
		private var m_mcAni5:Poker_Circle_Ani = new Poker_Circle_Ani();
		private var m_mcAni6:Poker_Circle_Ani = new Poker_Circle_Ani();
		private var m_buttonState:TiButtonState = new TiButtonState();
		private var m_timer:Timer = new Timer(2000,1)
		
		public function PlayerActionAll()
		{
			super();
			this.addChild(m_sp);
			m_sp.addChild(m_mcRaise);
			m_sp.addChild(m_coCall);
			m_sp.addChild(m_coFold);
			m_sp.addChild(m_mcAni1);
			m_sp.addChild(m_mcAni2);
			m_sp.addChild(m_mcAni3);
			m_sp.addChild(m_mcAni4);
			m_sp.addChild(m_mcAni5);
			m_sp.addChild(m_mcAni6);
			this.addChild(m_mcOk);
			m_mcOk.visible=false;
			m_mcOk.x = 360
			m_mcAni1.x = m_mcRaise.btn_1x.x+(m_mcRaise.btn_1x.width/2)
			m_mcAni1.y = m_mcRaise.btn_1x.y+(m_mcRaise.btn_1x.height/2)
			m_mcAni2.x = m_mcRaise.btn_2x.x+(m_mcRaise.btn_2x.width/2)
			m_mcAni2.y = m_mcRaise.btn_2x.y+(m_mcRaise.btn_2x.height/2)
			m_mcAni3.x = m_mcRaise.btn_3x.x+(m_mcRaise.btn_3x.width/2)
			m_mcAni3.y = m_mcRaise.btn_3x.y+(m_mcRaise.btn_3x.height/2)
			m_mcAni4.x = m_mcRaise.btn_4x.x+(m_mcRaise.btn_4x.width/2)
			m_mcAni4.y = m_mcRaise.btn_4x.y+(m_mcRaise.btn_4x.height/2)
			m_mcAni5.x = m_mcRaise.btn_5x.x+(m_mcRaise.btn_5x.width/2)
			m_mcAni5.y = m_mcRaise.btn_5x.y+(m_mcRaise.btn_5x.height/2)
			m_mcAni6.x = m_mcRaise.btn_6x.x+(m_mcRaise.btn_6x.width/2)
			m_mcAni6.y = m_mcRaise.btn_6x.y+(m_mcRaise.btn_6x.height/2)
			m_mcAni1.mouseChildren=false;
			m_mcAni1.mouseEnabled=false;
			m_mcAni2.mouseChildren=false;
			m_mcAni2.mouseEnabled=false;
			m_mcAni3.mouseChildren=false;
			m_mcAni3.mouseEnabled=false;
			m_mcAni4.mouseChildren=false;
			m_mcAni4.mouseEnabled=false;
			m_mcAni5.mouseChildren=false;
			m_mcAni5.mouseEnabled=false;
			m_mcAni6.mouseChildren=false;
			m_mcAni6.mouseEnabled=false;
		
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcOk);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcRaise.btn_1x);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcRaise.btn_2x);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcRaise.btn_3x);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcRaise.btn_4x);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcRaise.btn_5x);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mcRaise.btn_6x);
			m_mcRaise.btn_1x.addEventListener(MouseEvent.CLICK,fnRaise1x);
			m_mcRaise.btn_2x.addEventListener(MouseEvent.CLICK,fnRaise2x);
			m_mcRaise.btn_3x.addEventListener(MouseEvent.CLICK,fnRaise3x);
			m_mcRaise.btn_4x.addEventListener(MouseEvent.CLICK,fnRaise4x);
			m_mcRaise.btn_5x.addEventListener(MouseEvent.CLICK,fnRaise5x);
			m_mcRaise.btn_6x.addEventListener(MouseEvent.CLICK,fnRaise6x);
			
			m_coCall.addEventListener(MouseEvent.CLICK,fnCall);
			m_coFold.addEventListener(MouseEvent.CLICK,fnFold);
			m_mcOk.addEventListener(MouseEvent.CLICK,fnOk);
			fnDisableAction();
			if(GameConfig.device_Language == EnumLang.CH || GameConfig.device_Language == EnumLang.HK || GameConfig.device_Language == EnumLang.TW)
			{
				m_mcRaise.btn_1x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnitTW(GameConfig.game_table.bet)
				m_mcRaise.btn_2x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnitTW(GameConfig.game_table.bet*2)
				m_mcRaise.btn_3x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnitTW(GameConfig.game_table.bet*3)
				m_mcRaise.btn_4x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnitTW(GameConfig.game_table.bet*4)
				m_mcRaise.btn_5x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnitTW(GameConfig.game_table.bet*5)
				m_mcRaise.btn_6x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnitTW(GameConfig.game_table.bet*6)
			}
			else
			{
				m_mcRaise.btn_1x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet)
				m_mcRaise.btn_2x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*2)
				m_mcRaise.btn_3x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*3)
				m_mcRaise.btn_4x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*4)
				m_mcRaise.btn_5x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*5)
				m_mcRaise.btn_6x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*6)
			}
			
			
		//	HoimiFontNumberLoadHandler.fnGetInstance().addEventListener(GameEvent.GAME_EVENT,GameEventHandler);
		//	HoimiFontNumberLoadHandler.fnGetInstance().fnLoadFont();
			
		}
		
		private function GameEventHandler(e:GameEvent):void
		{
			if(e.detail == GameEvent.GAME_NUMBER_FONT_LOAD_COMPLETE_EVENT)
			{
				fnSetCoinTextField();
			}
		}
		
		private function fnSetCoinTextField():void
		{
			HoimiFontNumberLoadHandler.fnGetInstance().removeEventListener(GameEvent.GAME_EVENT,GameEventHandler);
		
			fnSetTextFont(m_mcRaise.btn_1x.btn.txt_coin);
			fnSetTextFont(m_mcRaise.btn_2x.btn.txt_coin);
			fnSetTextFont(m_mcRaise.btn_3x.btn.txt_coin);
			fnSetTextFont(m_mcRaise.btn_4x.btn.txt_coin);
			fnSetTextFont(m_mcRaise.btn_5x.btn.txt_coin);
			fnSetTextFont(m_mcRaise.btn_6x.btn.txt_coin);
			m_mcRaise.btn_1x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet)
			m_mcRaise.btn_2x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*2)
			m_mcRaise.btn_3x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*3)
			m_mcRaise.btn_4x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*4)
			m_mcRaise.btn_5x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*5)
			m_mcRaise.btn_6x.btn.txt_coin.text = CoinValueTransfer.fnCoinValueTransferUnit(GameConfig.game_table.bet*6)
		
		}
		
		private function fnSetTextFont(txt:TextField):void
		{
			var tf:TextFormat = new TextFormat();
			tf.font = HoimiFontNumberLoadHandler.fnGetInstance().embeddedFont.fontName;
			tf.size = 38;
		//	tf.align = TextFormatAlign.RIGHT;
			//tf.color = "0xFFFF33";
			txt.embedFonts = true; // very important to set
			txt.defaultTextFormat = tf;
		//	txt.width = 150
			//txt.x = 37
			//txt.y = 2
			txt.mouseEnabled =false;
			//m_txtCoin.text = 99999+""
			//txt.text ="100" 
		}
		
		
		private function fnRaise1x(e:MouseEvent):void
		{
			fnDisableAction();
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_RAISE_EVENT)
				ev.m_eventArgs = 1
			dispatchEvent(ev);
		}
		
		private function fnRaise2x(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			fnDisableAction();
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_RAISE_EVENT)
			ev.m_eventArgs = 2
			dispatchEvent(ev);
		}
		
		private function fnRaise3x(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			fnDisableAction();
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_RAISE_EVENT)
			ev.m_eventArgs = 3
			dispatchEvent(ev);
		}
		
		private function fnRaise4x(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			fnDisableAction();
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_RAISE_EVENT)
			ev.m_eventArgs = 4
			dispatchEvent(ev);
		}
		
		private function fnRaise5x(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			fnDisableAction();
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_RAISE_EVENT)
			ev.m_eventArgs = 5
			dispatchEvent(ev);
		}
		
		private function fnRaise6x(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			fnDisableAction();
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_RAISE_EVENT)
			ev.m_eventArgs = 6
			dispatchEvent(ev);
		}
		
		private function fnCall(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			fnDisableAction();
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_CALL_EVENT)
			dispatchEvent(ev);
		}
		
		private function fnFold(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			fnDisableAction();
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_FOLD_EVENT)
			dispatchEvent(ev);
		}
		
		private function fnOk(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			m_mcOk.visible=false;
			var ev:PokerEvent = new PokerEvent(PokerEvent.POKER_EVENT,PokerEvent.POKER_PLAYER_OK_EVENT)
			dispatchEvent(ev);
		}
		
		public function fnShow1STBetAction(nPlayerMultiple:int,nEnemyMultiple:int,nMaxRatio):void
		{
			m_sp.mouseEnabled=true;
			m_sp.mouseChildren=true
			m_sp.visible = true;
			fnEnableRaiseBtnAgain(nPlayerMultiple,nEnemyMultiple,nMaxRatio)
			m_coCall.fnBtnNormalState();
			m_coFold.fnBtnNormalState(); 
			
		}
		
		private function fnEnableRaiseBtnAgain(nPlayerMultiple:int,nEnemyMultiple:int,nMaxRatio):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSoundse_maoudamashii_system46();
			var nMinValue:int = (nEnemyMultiple -nPlayerMultiple) +1
			var nMaxValue:int = nMaxRatio- nPlayerMultiple
			for(var i:int = 1 ; i <=6 ;i++)
			{
				if(i>=nMinValue && i<=nMaxValue)
				{
					fnEnabelRaiseBtn(m_mcRaise["btn_"+i+"x"],this["m_mcAni"+i]);
				}
				else
				{
					fnDisableRaiseBtn(m_mcRaise["btn_"+i+"x"]);
				}
				
			}
			m_timer.addEventListener(TimerEvent.TIMER_COMPLETE,fnTimerComplete);
			if(!GameConfig.appPause)
			{
				m_timer.start();	
			}
		}
		
		private function fnTimerComplete(e:TimerEvent):void
		{
			m_mcAni1.gotoAndPlay(2);
			m_mcAni2.gotoAndPlay(2);
			m_mcAni3.gotoAndPlay(2);
			m_mcAni4.gotoAndPlay(2);
			m_mcAni5.gotoAndPlay(2);
			m_mcAni6.gotoAndPlay(2);
			m_timer.reset();
			m_timer.start();
		}
		
		private function fnEnabelRaiseBtn(mc:MovieClip,mcAni:MovieClip):void
		{
			mc.mouseChildren=true;
			mc.mouseEnabled=true;
			mc.gotoAndStop(1);
			mcAni.gotoAndPlay(2);
			mcAni.visible = true;
		}
		
		private function fnDisableRaiseBtn(mc:MovieClip):void
		{
			mc.mouseChildren=false;
			mc.mouseEnabled=false;
			mc.gotoAndStop(4);
		}
		
		
		public function fnDisableAction():void
		{
			m_sp.mouseEnabled=false;
			m_sp.mouseChildren=false
			m_buttonState.fnDisableAndDarkMcState(m_mcRaise.btn_1x)
			m_buttonState.fnDisableAndDarkMcState(m_mcRaise.btn_2x)
			m_buttonState.fnDisableAndDarkMcState(m_mcRaise.btn_3x)
			m_buttonState.fnDisableAndDarkMcState(m_mcRaise.btn_4x)
			m_buttonState.fnDisableAndDarkMcState(m_mcRaise.btn_5x)
			m_buttonState.fnDisableAndDarkMcState(m_mcRaise.btn_6x)
			m_mcAni1.gotoAndStop(1);
			m_mcAni2.gotoAndStop(1);
			m_mcAni3.gotoAndStop(1);
			m_mcAni4.gotoAndStop(1);
			m_mcAni5.gotoAndStop(1);
			m_mcAni6.gotoAndStop(1);
			m_mcAni1.visible = false;
			m_mcAni2.visible = false;
			m_mcAni3.visible = false;
			m_mcAni4.visible = false;
			m_mcAni5.visible = false;
			m_mcAni6.visible = false;
			m_coCall.fnDisableBtn();
			m_coFold.fnDisableBtn();
			m_timer.stop();
			m_timer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnTimerComplete);
		}
		
		public function fnHideAction():void
		{
			m_sp.visible=false;
			fnDisableAction();
		}
		
		public function fnShowOk():void
		{
			fnDisableAction();
			m_sp.visible=false;
			m_mcOk.visible = true;
		}
		
		public function fnHideOk():void
		{
			fnDisableAction();
			m_mcOk.visible = false;
		}
		
		public function fnResume():void
		{
			if(!m_timer.running && m_sp.mouseEnabled )
			{
				m_timer.start();
			}
		}
		
		public function fnPause():void
		{
			if(m_timer.running)
			{
				m_timer.stop();
			}
		}
		
		public function fnDestory():void
		{
			m_mcRaise.btn_1x.removeEventListener(MouseEvent.CLICK,fnRaise1x);
			m_mcRaise.btn_2x.removeEventListener(MouseEvent.CLICK,fnRaise2x);
			m_mcRaise.btn_3x.removeEventListener(MouseEvent.CLICK,fnRaise3x);
			m_mcRaise.btn_4x.removeEventListener(MouseEvent.CLICK,fnRaise4x);
			m_mcRaise.btn_5x.removeEventListener(MouseEvent.CLICK,fnRaise5x);
			m_mcRaise.btn_6x.removeEventListener(MouseEvent.CLICK,fnRaise6x);
			
			m_coCall.removeEventListener(MouseEvent.CLICK,fnCall);
			m_coFold.removeEventListener(MouseEvent.CLICK,fnFold);
			m_mcOk.removeEventListener(MouseEvent.CLICK,fnOk);
			m_timer.stop();
			m_timer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnTimerComplete);
		}
	}
}