package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class OpenBetInfo extends NetPacket
	{
		/** open_bet
		 * 1:{"result" : 1,'game_id' : game_id, 'player_coin':coin,'enemy_uuid' : enemy.uuid, 'enemy_name' : enemy.name, 'player_cards':player_cards}
		 * -1:param miss
		 * -2:no such table
		 * -3:coin not enought
		 * -4:coin over max limit
		 * -101:token miss
		 * -103:token expire
		 * -201:finance get error
		 * -301:finance update error
		 * -401:member getRandom error
		 * -501:gameUserPoolHolder get error
		 * -601:gameUserPoolHolder update error
		 * -701:gameHolder add error
		 */

		public var game_id:int = 0;
		public var player_coin:Number = 0;
		public var enemy_uuid:String = '';
		public var enemy_name:String = '';
		public var enemy_level:int = 0;
		public var player_cards:Array=[];
		
		
		public function OpenBetInfo(game:String)
		{
			super(game);
			trace(player_cards);
		}
	}
}