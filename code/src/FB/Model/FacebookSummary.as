package FB.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class FacebookSummary extends JsonObject
	{
		public var total_count:int;
		public function FacebookSummary(obj:*=null)
		{
			super(obj);
		}
	}
}