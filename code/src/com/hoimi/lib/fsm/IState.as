﻿package com.hoimi.lib.fsm
{
	public interface IState
	{
		function Enter(): void;
		function Exit(): void;
		function Update(time: Number): void;
	}
}