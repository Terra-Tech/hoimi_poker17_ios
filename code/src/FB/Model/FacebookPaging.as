package FB.Model
{
	import com.hoimi.lib.net.JsonObject;
	
	public class FacebookPaging extends JsonObject
	{
		public var next:String;
		public var previous:String;
		public function FacebookPaging(obj:*=null)
		{
			super(obj);
		}
	}
}