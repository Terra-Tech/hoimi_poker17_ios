package CoinBack
{
	public interface ICoinBackView
	{
		// 網路發生問題
		function netError(msg:String):void;
		/// Server 吐錯誤訊息
		function serverError(msg:String):void;
		/// 如打開領獎Panel show loading
		function showLoading():void;
		/// 如打開領獎Panel hide loading
		function hideLoading():void;
		/// 領到錢（withdraw_coin : 領了多少, player_coin : 現在玩家財產）
		function showWithdraw(withdraw_coin:Number,player_coin:Number):void
		/// 更新錢池
		function showPool(coin:Number):void
		/// 顯示底於最小值所以不能領取（withdraw_min:最小值）
		function showBelowWithdrawMin(withdraw_min:Number):void
	}
}