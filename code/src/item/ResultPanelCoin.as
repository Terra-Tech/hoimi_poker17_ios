package item
{
	import com.hoimi.util.EventManager;
	
	import flash.display.Sprite;
	
	import event.GameEvent;
	
	import lib.TiConvertNumber;
	
	public class ResultPanelCoin extends Sprite
	{
		public var m_mc:Poker_Hoimi_Coin = new Poker_Hoimi_Coin();
		private var m_sp:Sprite = new Sprite();
		public function ResultPanelCoin()
		{
			super();
			this.addChild(m_mc);
			this.addChild(m_sp);
			m_mc.y = -5
			m_sp.x = 700;
			//fnSetValue(-67722,true,true)
		}
		
		public function fnSetValue(value:Number,bUsePlusMinus:Boolean,bUseTS:Boolean):void
		{
			m_sp.removeChildren();
			var sp:Sprite = TiConvertNumber.fnConvertValueToUI(value,Poker_Number_Coin,0,bUsePlusMinus,bUseTS);
			
			if(value < 0)
			{
				value = -value;
			}
			if(value >0 && value < 100000)
			{
				sp.scaleX = 0.5;
				sp.scaleY = 0.5
			}
			else if(value >= 100000 && value < 1000000)
			{
				sp.scaleX = 0.45;
				sp.scaleY = 0.45
			}
			else if(value >= 1000000 && value < 10000000)
			{
				sp.scaleX = 0.4;
				sp.scaleY = 0.4
			}
			else
			{
				sp.scaleX = 0.35;
				sp.scaleY = 0.35
			}
			m_sp.addChild(sp);
			m_sp.x = 200+ m_sp.width/2 ;
		
			
		}
	}
}