package ui
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import base.TiDisableTextField;
	
	import item.ResultPanelCoin;
	
	public class UnCompletePanel extends Sprite
	{
		private var m_mc:Poker_Msg_Panel = new Poker_Msg_Panel();
	//	private var m_mcReaultCoin:ResultPanelCoin = new ResultPanelCoin();
		private var m_coLevelUp:LevelUpPanel = new LevelUpPanel();
	//	private var m_mcMask:Poker_Mask_Base = new Poker_Mask_Base();
		
		public function UnCompletePanel()
		{
			super();
			this.visible = false;
		//	this.addChild(m_mcMask);
			//this.addChild(m_mc);
			//this.addChild(m_mcReaultCoin);
			this.addChild(m_coLevelUp);
			m_mc.btn_no.visible=false;
			m_mc.btn_yes.visible=false;
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_ok);
		//	m_mc.x = 360
		//	m_mc.y = 300
			//m_mcReaultCoin.x = 200;
			//m_mcReaultCoin.y = 650;
			m_coLevelUp.x = 360
			m_coLevelUp.y = 600
			m_coLevelUp.visible=false;
			m_mc.btn_ok.addEventListener(MouseEvent.CLICK,fnOk);
		}
		
		private function fnOk(e:MouseEvent):void
		{
			this.visible=false;
		}
		
		public function fnDisplay(nWinLose:Number,nRewardCoin:Number,nRewardDiamond:Number):void
		{
			if(nWinLose<0)
			{
				GameConfig.game_playerInfo.uncomplete_winlose = 0;
				GameConfig.game_playerInfo.reward_coin = 0;
				GameConfig.game_playerInfo.reward_diamond = 0;
				this.visible = true;
				m_mc.txt_msg.text="\n\n上次未完成比賽的結果!"
			//	m_mcReaultCoin.fnSetValue(nWinLose,true,false);
			//	nRewardCoin=10
				if(nRewardCoin>0 || nRewardDiamond>0)
				{
					m_coLevelUp.visible = true;
					m_coLevelUp.fnSetData(GameConfig.game_playerInfo.level,nRewardCoin,nRewardDiamond);
				}
			}
		}
	}
}