package menu
{
	import flash.display.Sprite;
	
	import base.TiDisableTextField;
	
	public class MenuSkill extends Sprite
	{
		private var m_mc:Poker_Btn_Menu_Skill = new Poker_Btn_Menu_Skill();
		public function MenuSkill()
		{
			super();
			this.addChild(m_mc);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc);
		}
	}
}