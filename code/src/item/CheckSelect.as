package item
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import handler.TTCSoundHandler;
	
	public class CheckSelect extends Sprite
	{
		private var m_mc:Poker_Check_Select = new Poker_Check_Select();
		public var m_bSelected:Boolean =false
		public function CheckSelect()
		{
			super();
			this.addChild(m_mc);
			m_mc.gotoAndStop(2);
			m_mc.addEventListener(MouseEvent.CLICK,fnClick);
		}
		
		private function fnClick(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000();
			m_bSelected = !m_bSelected
			if(m_bSelected)
			{
				m_mc.gotoAndStop(1);
			}
			else
			{
				m_mc.gotoAndStop(2);
			}
			
		}
		
		
	}
}