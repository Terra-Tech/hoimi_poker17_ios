package section
{
	import com.hoimi.lib.net.NetEvent;
	import com.hoimi.lib.net.NetPacket;
	import com.hoimi.util.EventManager;
	import com.hoimi.util.LocalSaver;
	
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import flash.utils.flash_proxy;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	
	import Enum.EnumAction;
	import Enum.EnumLocalSave;
	import Enum.EnumMsg;
	import Enum.EnumStatus;
	
	import base.TTCSectionBase;
	
	import caurina.transitions.Tweener;
	
	import conf.musicConfig;
	
	import event.GameEvent;
	import event.GameMessageEvent;
	import event.NetConnectionEvent;
	import event.PokerEvent;
	
	import handler.TTCMusicHandler;
	import handler.TTCSoundHandler;
	
	import item.DecisionTiemr;
	import item.Marquee;
	import item.MsgGameStatus;
	
	import lib.RandomMachine;
	import lib.VersionNumber;
	
	import model.BuyingInfo;
	import model.FirstBetResult1;
	import model.FirstBetResult2;
	import model.GameTable;
	import model.OpenBetInfo;
	import model.PickInfo;
	import model.PlayerInfo;
	import model.SecondBetInfo;
	
	import module.EnemyModule;
	import module.PlayerModule;
	
	import ui.PlayerActionAll;
	import ui.PlayerStatusFooter;
	import ui.ResultPanel;
	
	
	
	
	public class SectionBattle extends TTCSectionBase
	{
		private var m_mcBg:Poker_BG = new Poker_BG();
		private var m_coGameStatus:MsgGameStatus = new MsgGameStatus();
		private var m_decisionGapTimer:Timer = new Timer(1000,1);
		private var m_coPlayerActionAll:PlayerActionAll = new PlayerActionAll();
		private var m_modulePlayer:PlayerModule = new PlayerModule();
		private var m_moduleEnemy:EnemyModule = new EnemyModule();
		private var m_coDecisionTime:DecisionTiemr = new DecisionTiemr();
		private var m_bPlayerBuyingTurn:Boolean=false;
		private var m_bPlayerTurn:Boolean=false;
		private var m_gameTable:GameTable
		private var m_arActionQueue:Array = new Array();
		private var m_nFold:int = -1
		private var m_nActionMaxMutiple:int = 0
		private var m_nPlayerActionMaxSendServer1ST:int = 0
		private var m_nPlayerActionMaxSendServer2ND:int = 0
		private var m_nEnemyActionMaxSendServer1ST:int = 0
		private var m_nEnemyActionMaxSendServer2ND:int = 0
		private var m_coFooter:PlayerStatusFooter = new PlayerStatusFooter();	
		private var m_coMarquee:Marquee = new Marquee();
		private var m_coResult:ResultPanel = new ResultPanel();
		private var m_mask:Poker_Mask_Base = new Poker_Mask_Base();
		private var m_pausePassTime:Number
		
		public function SectionBattle()
		{
			super();
			m_gameTable = GameConfig.game_table;
			fnInit();
		}
		
		private function fnInit():void
		{
			m_coFooter.fnSetWinLoseRate(GameConfig.game_playerRecord.win,GameConfig.game_playerRecord.lose,GameConfig.game_playerRecord.rate);
			GameConfig.game_playerInfo.coin = GameConfig.game_openBetInfo.player_coin + GameConfig.game_table.bet;
			
			this.addChild(m_mcBg);
			this.addChild(m_coMarquee);
			this.addChild(m_coPlayerActionAll);
			this.addChild(m_moduleEnemy);
			this.addChild(m_modulePlayer);
			
		
		
			this.addChild(m_coGameStatus);
			m_coGameStatus.visible=false;
			
			this.addChild(m_coDecisionTime);
			this.addChild(m_coFooter);
			this.addChild(m_mask);
			this.addChild(m_coResult);
			m_mask.visible=false;
			//m_mask.addEventListener(MouseEvent.CLICK,fnClickMask);
			m_coResult.y = 500;
			m_coDecisionTime.x = 580;
			m_coDecisionTime.y = 540
				
			m_coDecisionTime.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_coResult.addEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_coGameStatus.addEventListener(PokerEvent.POKER_EVENT,fnPokerEventHandler);
			m_modulePlayer.addEventListener(PokerEvent.POKER_EVENT,fnPokerEventHandler);
			m_modulePlayer.addEventListener(NetConnectionEvent.NET_EVENT,fnNetEventHandler);
			m_moduleEnemy.addEventListener(PokerEvent.POKER_EVENT,fnPokerEventHandler);
			m_coPlayerActionAll.addEventListener(PokerEvent.POKER_EVENT,fnPokerEventHandler);
			m_moduleEnemy.fnSetName(GameConfig.game_openBetInfo.enemy_name,GameConfig.game_openBetInfo.enemy_level);
			m_coPlayerActionAll.visible=false;
			fnGameStart();
			//m_coFooter.fnAddBetCoins(1000,-100)
		}
		
		private function fnExitTable(e:MouseEvent):void
		{
			GameConfig.game_table = null;
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_PREVIOUS_SECTION_EVENT)
				dispatchEvent(ev);
		}
		
		
		
		
		
		private function fnPokerEventHandler(e:PokerEvent):void
		{
			switch(e.detail)
			{
				case PokerEvent.POKER_PLAYER_RAISE_EVENT:
					fnPlayerRaise(e.m_eventArgs);
					break;
				case PokerEvent.POKER_PLAYER_CALL_EVENT:
					fnPlayerCall();
					break;
				case PokerEvent.POKER_PLAYER_FOLD_EVENT:
					fnPlayerFold();
					break;
				case PokerEvent.POKER_PLAYER_OK_EVENT:
					fnPokerOk();
					break;
				case PokerEvent.POKER_STATUS_GAP_TIME_EVENT:
					fnStatusGapTimerComplete();
					break;
				case PokerEvent.POKER_DEAL_FINISH_EVENT:
					fnDealFinish();
					break;
				case PokerEvent.POKER_ENEMY_RAISE_EVENT:
					fnEnemyRaise(e.m_eventArgs);
					break;
				case PokerEvent.POKER_ENEMY_CALL_EVENT:
					fnEnemyCall(e.m_eventArgs);
					break;
				case PokerEvent.POKER_ENEMY_FOLD_EVENT:
					fnEnemyFold();
					break;
				case PokerEvent.POKER_PLAYER_BUYING_CONFIRM_EVENT:
				case PokerEvent.POKER_ENEMY_BUYING_CONFIRM_EVENT:
					fnBuying();
					break;
				case PokerEvent.POKER_ENEMY_OPEN_CARD_FINISH_EVENT:
					fnEnemyOpenCardFinish();
					break;
			}
		}
		
		private function fnNetEventHandler(e:NetConnectionEvent):void
		{
			switch(e.detail)
			{
				case NetConnectionEvent.NET_PICK_FINISH_EVENT:
					fnNetPickFinish();
					break;
			}
		}
	
		
		private function fnGameStart(e:MouseEvent = null):void
		{
			m_coGameStatus.visible = true;
			m_nFold = -1
			m_nEnemyActionMaxSendServer1ST = 0
			m_nEnemyActionMaxSendServer2ND = 0
			m_moduleEnemy.m_nActionMaxMutiple = 0;
			m_nPlayerActionMaxSendServer1ST = 0;
			m_nPlayerActionMaxSendServer2ND = 0 
			m_nActionMaxMutiple = 0;
			m_moduleEnemy.fnHideDecision();
			m_modulePlayer.fnHideDecision()
			m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_BET
			m_coPlayerActionAll.visible=false;
		}
		
		
		private function fnStatusGapTimerComplete():void
		{
			switch(m_coGameStatus.m_strGameStatus)
			{
				case EnumStatus.STATUS_BET:
					fnBet();
					break;
				case EnumStatus.STATUS_DEAL:
					fnDeal();
					break;
				case EnumStatus.STATUS_PICK:
					fnPickCard();
					break;
				case EnumStatus.STATUS_PICK_FINISH:
					fnPickCardFinish();
					break;
				case EnumStatus.STATUS_1STBET:
					fn1STBet()
					break;
				case EnumStatus.STATUS_CALL_DEAL:
					fnCallDeal();
					break;
				case EnumStatus.STATUS_BUYING:
					fnBuying();
					break;
				case EnumStatus.STATUS_2NDBET:
					fn2NDBet()
					break;
				case EnumStatus.STATUS_CALL_DEAL2:
					fnCallDeal2();
					break;
				case EnumStatus.STATUS_OPEN_CARD:
					fnOpenCard();
					break;
				case EnumStatus.STATUS_PLAYER_WIN:
					fnWin();
					break;
				case EnumStatus.STATUS_PLAYER_LOSE:
					fnLose();
					break;
				case EnumStatus.STATUS_SHOW_RESULT_PANEL:
					fnShowResultPanel();
					break;
			}
		}
		
		private function fnBet():void
		{
			m_moduleEnemy.fnGameStart();
			m_modulePlayer.fnGameStart()
			
			m_coGameStatus.visible = true;
			GameConfig.game_playerInfo.coin = GameConfig.game_openBetInfo.player_coin 
			m_coFooter.fnBetCoins(-GameConfig.game_table.bet);
			m_moduleEnemy.m_nCoin = m_gameTable.bet;
			m_modulePlayer.m_nCoin = m_gameTable.bet;
			m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_DEAL;
		}
	
		
		private function fnDeal():void
		{
			var arRan:Array = RandomMachine.fnGetRanArray([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10);
			//var arPlayer:Array = new Array(arRan[0],arRan[1],arRan[2],arRan[3],arRan[4])
			var arEnemy:Array = new Array(arRan[5],arRan[6],arRan[7],arRan[8],arRan[9])
			m_moduleEnemy.fnDeal(arEnemy);
			m_modulePlayer.fnDeal(GameConfig.game_openBetInfo.player_cards);
		//	fnGameNextStatus();
		}
		
		private function fnDealFinish():void
		{
			m_coGameStatus.m_strGameStatus  = EnumStatus.STATUS_PICK;
		
		}
		
		private function fnPickCard():void
		{
			m_modulePlayer.fnPickCard();
			m_coDecisionTime.fnStartPcik();
		}
		
		private function fnGameEventHandler(e:GameEvent):void
		{
			switch(e.detail)
			{
				case GameEvent.GAME_TIME_FINISH:
						fnTimerCountDownFinish();
					break;
				case GameEvent.GAME_DOUBLE_FINISH_EVENT:
						m_coFooter.fnCoinChange();
					break;

			}
		}
		
		private function fnTimerCountDownFinish():void
		{
			m_coPlayerActionAll.fnDisableAction()
			switch(m_coGameStatus.m_strGameStatus)
			{
				case EnumStatus.STATUS_PICK:
					fnPickCardTimeUp();
					break;
				case EnumStatus.STATUS_1STBET:
				case EnumStatus.STATUS_2NDBET:
					fnPlayerCall();
					break;
				case EnumStatus.STATUS_BUYING:
					fnPlayerBuyingConfirm();
					break;
			}
		}
		
		private function fnPokerOk():void
		{
			if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_BUYING)
			{
				fnPlayerBuyingConfirm();
			}
		}
		
		private function fnPickCardTimeUp():void
		{
			m_modulePlayer.fnPickCardFinish();
			
		}
		
		private function fnNetPickFinish():void
		{
			m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PICK_FINISH
				
		}
		
		private function fnPickCardFinish():void
		{
			m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_1STBET
			m_moduleEnemy.fnPickCardFinish();
			if(GameConfig.game_pickInfo.whos_turn == 0)
			{
				m_bPlayerTurn = true;
				m_bPlayerBuyingTurn = true;
			}
			else
			{
				m_bPlayerTurn = false;
				m_bPlayerBuyingTurn = false;
			}
		}
		
		private function fn1STBet():void
		{
			m_moduleEnemy.m_strAction = "";
			m_modulePlayer.m_strAction = "";
			m_arActionQueue.length = 0;
			m_nPlayerActionMaxSendServer1ST = 0;
			m_nEnemyActionMaxSendServer1ST = 0
			if(m_bPlayerTurn)
			{
				fnPlayer1STBet();
			}
			else
			{
				fnEnemy1STBet();
			}
		}
		
		private function fn2NDBet():void
		{
			m_moduleEnemy.m_strAction = "";
			m_modulePlayer.m_strAction = "";
			m_arActionQueue.length=0;
			m_nPlayerActionMaxSendServer2ND = 0;
			m_nEnemyActionMaxSendServer2ND = 0
			m_nActionMaxMutiple = 0
			m_moduleEnemy.m_nActionMaxMutiple = 0
			if(GameConfig.game_buyingInfo.whos_turn == 1)
			{
				fnEnemy2NDBet();
			}
			else
			{
				fnPlayer2NDBet();
			}
		}
		
		private function fnEnemy1STBet():void
		{
			m_moduleEnemy.fnEnemyDecision(m_moduleEnemy.m_nActionMaxMutiple,m_nActionMaxMutiple,GameConfig.game_pickInfo.enemy_max_multiple,GameConfig.game_table.time_1st_bet);
		}
		
		private function fnEnemy2NDBet():void
		{
			m_moduleEnemy.fnEnemyDecision(m_moduleEnemy.m_nActionMaxMutiple,m_nActionMaxMutiple,GameConfig.game_buyingInfo.enemy_max_multiple,GameConfig.game_table.time_2nd_bet);
		}
		
		private function fnCheck1STBetCoin():void
		{
			if(m_modulePlayer.m_nCoin > m_moduleEnemy.m_nCoin)
			{
				fnEnemy1STBet()
			}
			else if(m_modulePlayer.m_nCoin < m_moduleEnemy.m_nCoin)
			{
				fnPlayer1STBet();
			}
			else
			{
				if(m_moduleEnemy.m_strAction == "")
				{
					fnEnemy1STBet();
				}
				else if(m_modulePlayer.m_strAction == "")
				{
					fnPlayer1STBet();
				}
				else
				{
					trace("call deal")	
					fnNetfirstBet();
					
				}
				
			}
		}
		
		private function fnNetfirstBet(e:GameEvent = null):void
		{
			/// player first
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnNetfirstBet);
			trace("fnNetfirstBet m_nPlayerActionMaxSendServer1ST :"+m_nPlayerActionMaxSendServer1ST);
			trace("fnNetfirstBet m_nEnemyActionMaxSendServer1ST :"+m_nEnemyActionMaxSendServer1ST);
			SessionManager.instance.request(Protocol.GAME_FIRST_BET, {token:GameConfig.game_playerInfo.token, game_id:GameConfig.game_openBetInfo.game_id, actions:m_arActionQueue, player_mutiple:m_nPlayerActionMaxSendServer1ST, enemy_mutiple:m_nEnemyActionMaxSendServer1ST, fold:m_nFold},onFirstBet,onNet);
		}
		
		private function onFirstBet(e:NetEvent):void
		{
			trace('onFirstBet '+e.data);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnNetfirstBet);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				
				GameConfig.game_1stBetInfo1 = new FirstBetResult1(e.data);
				GameConfig.game_playerInfo.coin = GameConfig.game_1stBetInfo1.player_coin
				m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_CALL_DEAL;	
			}
			else if(data.result == 2)
			{
				GameConfig.game_1stBetInfo2 = new FirstBetResult2(e.data);
					
				/*if(GameConfig.game_1stBetInfo2.winlose > 0 )
				{
					//GameConfig.game_playerInfo.m_nCoinChange = GameConfig.game_1stBetInfo2.winlose
				}
				GameConfig.game_playerInfo.diamond = GameConfig.game_1stBetInfo2.player_diamond;
				GameConfig.game_playerInfo.level = GameConfig.game_1stBetInfo2.level;
				GameConfig.game_playerInfo.exp = GameConfig.game_1stBetInfo2.exp*/
			
				if(GameConfig.game_1stBetInfo2.winlose < 0)
				{
					GameConfig.game_playerInfo.coin_back_pool = GameConfig.game_1stBetInfo2.coin_back_pool;
					setTimeout(function():void 
					{
						m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PLAYER_LOSE;
					}, 1500)
				}
				else if(GameConfig.game_1stBetInfo2.winlose > 0)
				{
					setTimeout(function():void 
					{
						m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PLAYER_WIN;
					}, 1500)
				}
			}
			else
			{
				trace("onFirstBet error");
				var ev2:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				ev2.m_eventArgs = "FirstBet "+data.result
				EventManager.instance.trigger(ev2);
			}
		}
		
		
		private function fnCheck2NDBetCoin():void
		{
			if(m_modulePlayer.m_nCoin > m_moduleEnemy.m_nCoin)
			{
				fnEnemy2NDBet()
			}
			else if(m_modulePlayer.m_nCoin < m_moduleEnemy.m_nCoin)
			{
				fnPlayer2NDBet();
			}
			else
			{
				if(m_moduleEnemy.m_strAction == "")
				{
					fnEnemy2NDBet();
				}
				else if(m_modulePlayer.m_strAction == "")
				{
					fnPlayer2NDBet();
				}
				else
				{
					trace("call deal2")	
					fnNetSecondBet();
				}
				
			}
		}
		
		private function fnNetSecondBet(e:GameEvent = null):void
		{
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_DISPLAY_EVENT));
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnNetSecondBet);
			trace("fnNetSecondBet");
			trace("m_nPlayerActionMaxSendServer2ND :" + m_nPlayerActionMaxSendServer2ND);
			trace("m_nEnemyActionMaxSendServer2ND :" + m_nEnemyActionMaxSendServer2ND);
	
			SessionManager.instance.request(Protocol.GAME_SECOND_BET, {token:GameConfig.game_playerInfo.token, game_id:GameConfig.game_openBetInfo.game_id, actions:m_arActionQueue, player_mutiple:m_nPlayerActionMaxSendServer2ND, enemy_mutiple:m_nEnemyActionMaxSendServer2ND, fold:m_nFold},onSecondBet,onNet);
			
		}
		
		private function onSecondBet(e:NetEvent):void
		{
			trace('onSecondBet '+e.data);
			EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnNetSecondBet);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				
				GameConfig.game_2ndBetInfo = new SecondBetInfo(e.data);
				GameConfig.game_playerInfo.coin_back_pool = GameConfig.game_2ndBetInfo.coin_back_pool;
				
				if(m_nFold == -1)
				{
					m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_CALL_DEAL2;
				}
				else
				{
					m_mask.visible=true
					m_mask.alpha = 0
					if(GameConfig.game_2ndBetInfo.winlose < 0)
					{
						GameConfig.game_playerInfo.coin_back_pool = GameConfig.game_2ndBetInfo.coin_back_pool;
						setTimeout(function():void 
						{
							m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PLAYER_LOSE;
						}, 2000)
					}
					else if(GameConfig.game_2ndBetInfo.winlose > 0)
					{
						setTimeout(function():void 
						{
							m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PLAYER_WIN;
						}, 2000)
					}
				}
			}
			else
			{
				var ev2:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				ev2.m_eventArgs = "SecondBet" + data.result+"\nm_nPlayerActionMaxSendServer2ND:"+m_nPlayerActionMaxSendServer2ND+"\nm_nEnemyActionMaxSendServer2ND"+m_nEnemyActionMaxSendServer2ND;
				EventManager.instance.trigger(ev2);
			}
			
		}		
		
		private function fnPlayer1STBet():void
		{
			m_modulePlayer.fnPlayerDecision();
			m_coPlayerActionAll.visible =true;
			var nCanBetCoin:int
			m_coPlayerActionAll.fnShow1STBetAction(m_nActionMaxMutiple,m_moduleEnemy.m_nActionMaxMutiple,GameConfig.game_table.max_ratio1);
			m_coDecisionTime.fnStartDecision(GameConfig.game_table.time_1st_bet);
		}
		
		private function fnPlayer2NDBet():void
		{
			m_modulePlayer.fnPlayerDecision();
			m_coPlayerActionAll.visible =true;
			m_coPlayerActionAll.fnShow1STBetAction(m_nActionMaxMutiple,m_moduleEnemy.m_nActionMaxMutiple,GameConfig.game_table.max_ratio2);
			m_coDecisionTime.fnStartDecision(GameConfig.game_table.time_2nd_bet);
		}
		
		private function fnPlayerRaise(nRaise:int):void
		{
			var nCoin:int = GameConfig.game_table.bet * nRaise
			m_coFooter.fnBetCoins(-nCoin);	
			m_nActionMaxMutiple +=nRaise
			trace("fnPlayerRaise m_nActionMaxMutiple :" +m_nActionMaxMutiple);	
			if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_1STBET)
			{
				m_nPlayerActionMaxSendServer1ST +=nRaise

				trace("fnPlayerRaise m_nPlayerActionMaxSendServer1ST:"+m_nPlayerActionMaxSendServer1ST);
			}
			else if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_2NDBET)
			{
				m_nPlayerActionMaxSendServer2ND +=nRaise

				trace("fnPlayerRaise m_nPlayerActionMaxSendServer2ND:"+m_nPlayerActionMaxSendServer2ND);
			}
				
			m_modulePlayer.m_strAction = EnumAction.ACTION_RAISE	
			m_modulePlayer.fnAction(EnumAction.ACTION_RAISE);	
			m_coDecisionTime.fnCountDownStop();
			m_arActionQueue.push(EnumAction.ACTION_RAISE);
		
			m_modulePlayer.m_nCoin = nCoin;
			GameConfig.game_playerInfo.coin -=nCoin
			trace("player raise :" + nCoin)
			fnDecisionGapTime();
		}
		
		private function fnDecisionGapTime():void
		{
			m_decisionGapTimer.addEventListener(TimerEvent.TIMER_COMPLETE,fnDecisionGapTimeComplete);
			m_decisionGapTimer.reset();
			if(!GameConfig.appPause)
			{
				m_decisionGapTimer.start();	
			}
			
		}
		
		private function fnDecisionGapTimeComplete(e:TimerEvent):void
		{
			m_decisionGapTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnDecisionGapTimeComplete);
			if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_BUYING)
			{
				fnBuying();
			}
			else if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_1STBET)
			{
				fnCheck1STBetCoin();	
			}
			else if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_2NDBET)
			{
				fnCheck2NDBetCoin();
			}
		}
		
		private function fnPlayerCall():void
		{
			m_modulePlayer.fnAction(EnumAction.ACTION_CALL);	
			m_arActionQueue.push(EnumAction.ACTION_CALL);
			var nValue:int = m_moduleEnemy.m_nCoin - m_modulePlayer.m_nCoin
			if(nValue > 0)
			{
				m_modulePlayer.m_nCoin = nValue;
				m_coFooter.fnBetCoins(-nValue);	
				GameConfig.game_playerInfo.coin -= nValue;
				var nRaise:int = nValue/GameConfig.game_table.bet
				m_nActionMaxMutiple += nRaise
				trace("fnPlayerCall m_nActionMaxMutiple:"+m_nActionMaxMutiple);
				if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_1STBET)
				{
					m_nPlayerActionMaxSendServer1ST += nRaise
					trace("fnPlayerCall m_nPlayerActionMaxSendServer1ST:"+m_nPlayerActionMaxSendServer1ST);
				}
				else if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_2NDBET)
				{
					m_nPlayerActionMaxSendServer2ND += nRaise
					trace("fnPlayerCall m_nPlayerActionMaxSendServer2ND:"+m_nPlayerActionMaxSendServer2ND);
				}
			}
			m_coDecisionTime.fnCountDownStop();
			fnDecisionGapTime();
		}
		
		private function fnPlayerFold():void
		{
			m_nFold = 0
			m_modulePlayer.fnAction(EnumAction.ACTION_FOLD);	
			m_arActionQueue.push(EnumAction.ACTION_FOLD);
			m_coDecisionTime.fnCountDownStop();
			m_coPlayerActionAll.fnHideAction();
			if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_1STBET)
			{
				fnNetfirstBet();
			}
			else if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_2NDBET)
			{
				fnNetSecondBet();
			}
		//	fnDecisionGapTime();
		}
		
		
		private function fnEnemyRaise(nRaise:int):void
		{
			if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_1STBET)
			{
				m_nEnemyActionMaxSendServer1ST += nRaise
				trace("m_nEnemyActionMaxSendServer1ST:"+m_nEnemyActionMaxSendServer1ST);
			}
			else if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_2NDBET)
			{
				m_nEnemyActionMaxSendServer2ND += nRaise
				trace("player m_nEnemyActionMaxSendServer2ND:"+m_nEnemyActionMaxSendServer2ND);
			}
			m_moduleEnemy.fnAction(EnumAction.ACTION_RAISE);
			m_arActionQueue.push(EnumAction.ACTION_RAISE);
			fnDecisionGapTime();
		}
		
		private function fnEnemyCall(nRaise:int):void
		{
			if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_1STBET)
			{
				m_nEnemyActionMaxSendServer1ST += nRaise
				trace("m_nEnemyActionMaxSendServer1ST:"+m_nEnemyActionMaxSendServer1ST);
			}
			else if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_2NDBET)
			{
				m_nEnemyActionMaxSendServer2ND += nRaise
				trace("player m_nEnemyActionMaxSendServer2ND:"+m_nEnemyActionMaxSendServer2ND);
			}
			m_moduleEnemy.fnAction(EnumAction.ACTION_CALL);
			m_arActionQueue.push(EnumAction.ACTION_CALL);
			var nValue:int = m_modulePlayer.m_nCoin - m_moduleEnemy.m_nCoin
			m_moduleEnemy.m_nCoin = nValue;
			trace("enemy call")
			fnDecisionGapTime();
		}
		
		private function fnEnemyFold():void
		{
			m_nFold = 1
			m_moduleEnemy.fnAction(EnumAction.ACTION_FOLD);
			m_arActionQueue.push(EnumAction.ACTION_FOLD);
			if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_1STBET)
			{
				fnNetfirstBet();
			}
			else if(m_coGameStatus.m_strGameStatus == EnumStatus.STATUS_2NDBET)
			{
				fnNetSecondBet();
			}
			//m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PLAYER_WIN;
			//fnDecisionGapTime();
		}
		
		private function fnCallDeal():void
		{
			m_modulePlayer.fnHideDecision();
			m_moduleEnemy.fnHideDecision();
			m_coPlayerActionAll.fnHideAction();
			m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_BUYING
		}
		
		private function fnCallDeal2():void
		{
			m_modulePlayer.fnHideDecision();
			m_moduleEnemy.fnHideDecision();
			m_coPlayerActionAll.fnHideAction();
			m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_OPEN_CARD
		}
		
		private function fnOpenCard():void
		{
			m_moduleEnemy.fnOpenCard();
			//fnShowCardResult
			//m_modulePlayer.fnShowCardResult();
		}
		
		private function fnBuying():void
		{
			if(m_bPlayerBuyingTurn)
			{
				m_bPlayerBuyingTurn = !m_bPlayerBuyingTurn
				if(m_modulePlayer.m_bBuyingConfirm)
				{
					m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_2NDBET;	
				}
				else
				{
					fnPlayerBuying();	
				}
			}
			else 
			{
				m_bPlayerBuyingTurn = !m_bPlayerBuyingTurn
				if(m_moduleEnemy.m_bBuyingConfirm)
				{
					m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_2NDBET;	
				}
				else
				{
					fnEnemyBuying();
				}
			}
		}
		
		private function fnPlayerBuying():void
		{
			m_coPlayerActionAll.fnShowOk();
			m_modulePlayer.fnBuyingTime();
			m_coDecisionTime.fnStartBuying()
		}
		
		private function fnEnemyBuying():void
		{
			m_moduleEnemy.fnBuying()
		}
		
		private function fnPlayerBuyingConfirm():void
		{
			m_modulePlayer.fnBuyingConfirm();
			m_coPlayerActionAll.fnHideOk();
			m_coDecisionTime.fnCountDownStop();
		
		}
		
		private function fnEnemyOpenCardFinish():void
		{
			m_moduleEnemy.fnShowCardResult();
			m_modulePlayer.fnShowCardResult();
			
			if(GameConfig.game_2ndBetInfo.winlose >0 )
			{
				setTimeout(function():void 
				{
					m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PLAYER_WIN
				}, 1500)
			
			}
			else
			{
				setTimeout(function():void 
				{
					m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_PLAYER_LOSE
				}, 1500)
				
			}
		}
		
		private function fnWin():void
		{
			m_modulePlayer.fnHideDecision();
			m_moduleEnemy.fnHideDecision();
			m_coPlayerActionAll.visible=false;
			m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_SHOW_RESULT_PANEL
			m_mask.visible=true
			m_mask.alpha = 0
			TTCMusicHandler.fnGetInstance().fnPauseMusic();
			TTCSoundHandler.fnGetInstance().fnEmbeddedSoundWin();
			fnResultCoin();
			
		}
		
		private function fnLose():void
		{
			m_coPlayerActionAll.visible=false;
			m_modulePlayer.fnHideDecision();
			m_moduleEnemy.fnHideDecision();
			m_coGameStatus.m_strGameStatus = EnumStatus.STATUS_SHOW_RESULT_PANEL
			m_mask.visible=true
			m_mask.alpha = 0
			TTCMusicHandler.fnGetInstance().fnPauseMusic();
			TTCSoundHandler.fnGetInstance().fnEmbeddedSoundLose();
			fnResultCoin();
		}
		
		private function fnResultCoin():void
		{
			if(m_nFold !=-1 && GameConfig.game_1stBetInfo2)
			{
				if(GameConfig.game_1stBetInfo2.winlose > 0 )
				{
					m_coFooter.fnBetCoins(GameConfig.game_1stBetInfo2.winlose);
				}
				GameConfig.game_playerInfo.coin = GameConfig.game_1stBetInfo2.player_coin;
				trace("GameConfig.game_playerInfo.coin :" + GameConfig.game_playerInfo.coin);
			}
			else
			{
				if(GameConfig.game_2ndBetInfo.winlose > 0 )
				{
					m_coFooter.fnBetCoins(GameConfig.game_2ndBetInfo.winlose);
				}
				GameConfig.game_playerInfo.coin = GameConfig.game_2ndBetInfo.player_coin;
				trace("GameConfig.game_playerInfo.coin :" + GameConfig.game_playerInfo.coin);
			}
		}
		
		private function fnClickMask(e:MouseEvent):void
		{
			fnShowResultPanel();
		}
		
		private function fnShowResultPanel():void
		{
			if(	m_mask.alpha == 1)
			{
				return;
			}
			m_mask.visible = true;
			m_mask.alpha = 1
			
			var nAddExp:int
			if(m_nFold !=-1 && GameConfig.game_1stBetInfo2)
			{
				if(GameConfig.game_playerInfo.level == GameConfig.game_1stBetInfo2.level)
				{
					nAddExp = GameConfig.game_1stBetInfo2.exp - GameConfig.game_playerInfo.exp	
				}
				else
				{
					nAddExp = GameConfig.game_playerInfo.next_exp - GameConfig.game_playerInfo.exp + GameConfig.game_1stBetInfo2.exp	
				}
				m_coResult.fnDisplayPanel(GameConfig.game_1stBetInfo2.winlose,nAddExp,GameConfig.game_1stBetInfo2.level,GameConfig.game_1stBetInfo2.next_exp,GameConfig.game_1stBetInfo2.reward_coin,GameConfig.game_1stBetInfo2.reward_diamond);
				GameConfig.game_playerInfo.diamond = GameConfig.game_1stBetInfo2.player_diamond;
				GameConfig.game_playerInfo.level = GameConfig.game_1stBetInfo2.level;
				GameConfig.game_playerInfo.exp = GameConfig.game_1stBetInfo2.exp
				GameConfig.game_playerInfo.next_exp = GameConfig.game_1stBetInfo2.next_exp 
				GameConfig.game_playerRecord =GameConfig.game_1stBetInfo2.player_record
					
				
				
			}
			else if(GameConfig.game_2ndBetInfo)
			{
				if(GameConfig.game_playerInfo.level == GameConfig.game_2ndBetInfo.level)
				{
					nAddExp = GameConfig.game_2ndBetInfo.exp - GameConfig.game_playerInfo.exp	
				}
				else
				{
					nAddExp = GameConfig.game_playerInfo.next_exp - GameConfig.game_playerInfo.exp + GameConfig.game_2ndBetInfo.exp	
				}
				m_coResult.fnDisplayPanel(GameConfig.game_2ndBetInfo.winlose,nAddExp,GameConfig.game_2ndBetInfo.level,GameConfig.game_2ndBetInfo.next_exp,GameConfig.game_2ndBetInfo.reward_coin,GameConfig.game_2ndBetInfo.reward_diamond);

				GameConfig.game_playerInfo.diamond = GameConfig.game_2ndBetInfo.player_diamond;
				GameConfig.game_playerInfo.level = GameConfig.game_2ndBetInfo.level;
				GameConfig.game_playerInfo.exp = GameConfig.game_2ndBetInfo.exp
				GameConfig.game_playerInfo.next_exp = GameConfig.game_2ndBetInfo.next_exp
				GameConfig.game_playerRecord =GameConfig.game_2ndBetInfo.player_record
					
				
			}
			m_coFooter.fnSetWinLoseRate(GameConfig.game_playerRecord.win,GameConfig.game_playerRecord.lose,GameConfig.game_playerRecord.rate);
		}
		
		private function onNet(e:NetEvent):void
		{
			trace(e.protocol+ " " +e.type + " " + e.data);
		}
		
		
		override public function fnResize():void
		{
			m_mcBg.x =360+ GameConfig.object_adjuct_position_x;
			m_modulePlayer.x += GameConfig.object_adjuct_position_x;
			m_moduleEnemy.x += GameConfig.object_adjuct_position_x
			m_coPlayerActionAll.x+=GameConfig.object_adjuct_position_x;
			m_coDecisionTime.x+= GameConfig.object_adjuct_position_x
			m_coGameStatus.x=360+ GameConfig.object_adjuct_position_x
			m_mask.x= 360+GameConfig.object_adjuct_position_x
			m_coFooter.x+=GameConfig.object_adjuct_position_x;
			m_coMarquee.x =360+GameConfig.object_adjuct_position_x;
			m_coResult.x = 360+GameConfig.object_adjuct_position_x;
		}
		
	
		override public function fnExit():void
		{
			super.fnExit();
			GameConfig.game_pickInfo = null
			GameConfig.game_1stBetInfo1 = null
			GameConfig.game_1stBetInfo2 = null
			GameConfig.game_buyingInfo = null;
			GameConfig.game_2ndBetInfo = null;
			Tweener.removeAllTweens(); // just removing whatever Tweens might already exist
			m_coResult.fnDestory();
			m_coFooter.fnDestory();
			m_moduleEnemy.fnDestory();
			m_coDecisionTime.fnDestory();
			m_coPlayerActionAll.fnDestory();
			//m_mask.removeEventListener(MouseEvent.CLICK,fnClickMask);
			m_decisionGapTimer.stop();
			m_decisionGapTimer.removeEventListener(TimerEvent.TIMER_COMPLETE,fnDecisionGapTimeComplete);
			m_decisionGapTimer = null;
			m_coDecisionTime.removeEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_coResult.removeEventListener(GameEvent.GAME_EVENT,fnGameEventHandler);
			m_coGameStatus.removeEventListener(PokerEvent.POKER_EVENT,fnPokerEventHandler);
			m_modulePlayer.removeEventListener(PokerEvent.POKER_EVENT,fnPokerEventHandler);
			m_modulePlayer.removeEventListener(NetConnectionEvent.NET_EVENT,fnNetEventHandler);
			m_moduleEnemy.removeEventListener(PokerEvent.POKER_EVENT,fnPokerEventHandler);
			m_coPlayerActionAll.removeEventListener(PokerEvent.POKER_EVENT,fnPokerEventHandler);
			this.removeChildren();
			
		}
		
		override public function fnResume():void
		{
			var resumeTime:int = getTimer()
			trace("passTime" +( resumeTime - m_pausePassTime));	
			if(resumeTime - m_pausePassTime > 20000)
			{
				if(m_coResult.visible)
				{
					fnPleaseLeaveTable();
				}
				else
				{
					var ev:GameMessageEvent = new GameMessageEvent(GameMessageEvent.MESSAGE_EVENT);
					ev.m_strMsg = "\n比賽中斷\n\n離開超過20秒視同放棄!"
					ev.m_strType = EnumMsg.MSG_TYPE_OK
					//ev.m_callOkFn = fnPleaseLeaveTable	
					EventManager.instance.trigger(ev);	
					fnPleaseLeaveTable();
				}
				
			}
			else
			{
				m_coDecisionTime.fnResume();
				m_coPlayerActionAll.fnResume();
				m_moduleEnemy.fnResume();
				m_coResult.fnResume()	
			}
			
				
		}
		
		private function fnPleaseLeaveTable():void
		{
			signin();
			TTCMusicHandler.fnGetInstance().fnSelectMusic(musicConfig.GAME_MUSIC_MENU,0.5);
			var ev:GameEvent = new GameEvent(GameEvent.GAME_EVENT,GameEvent.GAME_PREVIOUS_SECTION_EVENT);
			dispatchEvent(ev);
		}
		
		private function signin(e:GameEvent = null):void
		{
			trace('signin');
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,signin);
			var strVersion:String = VersionNumber.fnGetVersionNumber();
			SessionManager.instance.request(Protocol.USER_SIGNIN, {uuid:GameConfig.UUID, device:GameConfig.DEVICE, version:strVersion},onSignin,onNet);
		}
		
		private function onSignin(e:NetEvent):void
		{
			trace('onSignin ' + e.data);
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,signin);
			var data:NetPacket = new NetPacket(e.data);
			if(data.result == 1)
			{
				GameConfig.game_playerInfo = new PlayerInfo(e.data);
				GameConfig.game_playerInfo.boardcast();
				GameConfig.game_playerRecord = GameConfig.game_playerInfo.player_record
			}
			else
			{
				EventManager.instance.trigger(new GameEvent(GameEvent.GAME_LOADING_HIDE_EVENT));
				var evError:GameEvent = new GameEvent(GameEvent.GAME_ERROR_MESSAGE_EVENT)
				evError.m_eventArgs = "onSignin "+ data.result
				EventManager.instance.trigger(evError);
			}
		}
		
		override public function fnPause():void
		{
			m_coDecisionTime.fnPause();
			m_coPlayerActionAll.fnPause();
			m_moduleEnemy.fnPause();
			m_coResult.fnPause();
			if(m_decisionGapTimer.running)
			{
				m_decisionGapTimer.stop();
			}
			m_pausePassTime = getTimer()

		}
	}
}