package lib
{
	import com.hoimi.util.EventManager;
	import com.hoimi.util.HttpHelper;
	
	import Enum.EnumURL;
	
	import event.GameEvent;
	import event.MPEvent;

	public class GameConnectHandler
	{
		public function GameConnectHandler()
		{
		}
		
		private static var m_fn:Function
		
		public static function fnTimeOut(fn:Function):void
		{
			m_fn = fn
			EventManager.instance.register(GameEvent.GAME_RECONNECT_EVENT,fnReconectServer);
			var ev:GameEvent = new GameEvent(GameEvent.GAME_CONNECT_TIME_OUT_EVENT);
			EventManager.instance.trigger(ev);
		}
		
		private static function fnReconectServer(e:GameEvent):void
		{
			EventManager.instance.remove(GameEvent.GAME_RECONNECT_EVENT,fnReconectServer);
		//	var url:String = EnumURL.URL_TOURNAMENT_CONFIG+ m_strType
		//	HttpHelper.get(url,"",fnTimeOut,null,fnTimeOut,m_fn);
		}
	}
}