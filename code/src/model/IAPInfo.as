package model
{
	import com.hoimi.lib.net.NetPacket;
	
	public class IAPInfo extends NetPacket
	{
		/** iap 
		 * 1:{"result" : 1, 'product_id':twd_150, 'vaild':true, player_diamond:1560}
		 * -3:token expire
		 */
		public var product_id:String;
		public var valid:Boolean = false;
		public var player_diamond:int = 0;
		public function IAPInfo(iap:String)
		{
			super(iap);
		}
	}
}