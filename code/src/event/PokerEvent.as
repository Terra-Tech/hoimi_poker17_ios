package event
{
	public class PokerEvent extends TiBasicEvent
	{
		public static const POKER_EVENT:String = "POKER_EVENT";
		public static const POKER_BET_ACTION_EVENT:String = "POKER_BET_ACTION_EVENT";
		public static const POKER_PLAYER_RAISE_EVENT:String = "POKER_PLAYER_RAISE_EVENT";
		public static const POKER_PLAYER_CALL_EVENT:String = "POKER_PLAYER_CALL_EVENT";
		public static const POKER_PLAYER_FOLD_EVENT:String = "POKER_PLAYER_FOLD_EVENT";
		public static const POKER_PLAYER_OK_EVENT:String = "POKER_PLAYER_OK_EVENT";
		public static const POKER_ENEMY_RAISE_EVENT:String = "POKER_ENEMY_RAISE_EVENT";
		public static const POKER_ENEMY_CALL_EVENT:String = "POKER_ENEMY_CALL_EVENT";
		public static const POKER_ENEMY_FOLD_EVENT:String = "POKER_ENEMY_FOLD_EVENT";
		public static const POKER_STATUS_GAP_TIME_EVENT:String = "POKER_STATUS_GAP_TIME_EVENT";
		public static const POKER_DEAL_FINISH_EVENT:String = "POKER_DEAL_FINISH_EVENT";
		public static const POKER_PLAYER_BUYING_CONFIRM_EVENT:String = "POKER_PLAYER_BUYING_CONFIRM_EVENT";
		public static const POKER_ENEMY_BUYING_CONFIRM_EVENT:String = "POKER_ENEMY_BUYING_CONFIRM_EVENT";
		public static const POKER_ENEMY_OPEN_CARD_FINISH_EVENT:String = "POKER_ENEMY_OPEN_CARD_FINISH_EVENT";
		
		public function PokerEvent(type:String, detail:String="", bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, detail, bubbles, cancelable);
		}
	}
}