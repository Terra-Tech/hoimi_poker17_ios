package handler
{
	import com.hoimi.util.LocalSaver;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	
	import Enum.EnumLocalSave;
	
	import caurina.transitions.Tweener;
	
	public class TTCMusicHandler 
	{
		private static var m_Instance:TTCMusicHandler;
		private var m_soundCollection:Array = new Array();
		private var m_soundLoader:Array = new Array();
		private var m_SoundChannel:SoundChannel
		private var m_sound:Sound = new Sound();
		public var m_bIsPlaying:Boolean = false;
		private var m_nPosition:Number=0;
		private var m_nSelectedSound:int=0;
		private var _m_bMute:Boolean=false;
		private var m_bLoop:Boolean =true;
		private var m_bRandom:Boolean
		private var m_musicName:String=""
		private var _m_nVolume:Number = 1;
		private var m_nDefaultVolume:Number = 1;
		private var m_swf:MovieClip 
		private var _m_nSwfVolume:Number = 1;
		private var m_nSwfDefaultVolume:Number = 1;
		private var m_nFadeInTime:Number = 0;
		
		public function TTCMusicHandler()
		{
			if(LocalSaver.instance.getValue(EnumLocalSave.MUSIC_MUTE) == undefined)
			{
				LocalSaver.instance.save(EnumLocalSave.MUSIC_MUTE,false);
			}
			var bMusicMute:Boolean = LocalSaver.instance.getValue(EnumLocalSave.MUSIC_MUTE);
			m_bMute = bMusicMute;
		}
		
		public static function fnGetInstance(): TTCMusicHandler 
		{
			if ( m_Instance == null )
			{
				m_Instance = new TTCMusicHandler();
			}
			return m_Instance;
		}
		
		public function get m_bMute():Boolean
		{
			return _m_bMute;
		}
		
		public function set m_bMute(value:Boolean):void
		{
			_m_bMute = value;
			fnVolumeChange();
		}
		
		public function get m_nSwfVolume():Number
		{
			return _m_nSwfVolume * m_nSwfDefaultVolume;
		}
		
		public function set m_nSwfVolume(value:Number):void
		{
			_m_nSwfVolume = value;
		}
		
		public function get m_nVolume():Number
		{
			return _m_nVolume * m_nDefaultVolume;
		}
		
		public function set m_nVolume(value:Number):void
		{
			_m_nVolume = value;
			m_nSwfVolume = value
			fnVolumeChange();
		}
		
		public function fnInit(strName:String,nDefaultVolume:Number = 1):void
		{
			m_musicName = strName;
			m_nDefaultVolume = nDefaultVolume;
		}
		
		public function fnAddSwf(swf:MovieClip,nDefaultVolume:Number = 1):void
		{
			fnRemoveSwf();
			m_swf = new MovieClip();
			m_swf = swf
			m_nSwfDefaultVolume = nDefaultVolume;
			fnVolumeChange();
		}
		
		public function fnRemoveSwf():void
		{
			if(m_swf)
			{
				m_swf = null;
			}
		}
		
		public function fnSelectMusic(strName:String,nDefaultVolume:Number = 1,nFadeInTime:Number = 0):void
		{
			m_nFadeInTime = nFadeInTime;
			fnStopMusic();
			fnInit(strName,nDefaultVolume);
			fnSoundDownLoad();
		}  
		
		protected function fnSoundDownLoad(e:Event=null):void
		{
			m_nPosition = 0;
			m_soundCollection=new Array();
			m_soundCollection.push({url:m_musicName});
			m_soundLoader=new Array();
			
			m_sound=new Sound();
			if(!m_soundCollection)
			{
				return;
			}
			var request:URLRequest= new URLRequest(m_soundCollection[m_nSelectedSound].url);
			m_sound.load(request);
			m_sound.addEventListener( IOErrorEvent.IO_ERROR, handleIOErr );
			m_sound.addEventListener(Event.COMPLETE,fnDownloadSoundComplete)
		}
		
		private function handleIOErr( event: IOErrorEvent ): void
		{
			trace("sound IO error:"+ event.currentTarget)
			fnBtnSoundNext();
		}
		
		private function fnDownloadSoundComplete(e:Event):void
		{
			//var key:String= m_soundCollection[m_nSelectedSound].name
			//var url:String=	m_soundCollection[m_nSelectedSound].url;
			m_soundLoader.push(e.currentTarget);
			if(m_nSelectedSound < m_soundCollection.length-1)
			{
				m_nSelectedSound++
				fnSoundDownLoad();
			}
			else
			{
				fnDownloadComplete();
			}
			
		}
		
		private function fnDownloadComplete():void
		{
			m_nSelectedSound=0;
			m_nPosition = 0;
			fnBtnSoundPlay();
			if(m_nFadeInTime!=0)
			{
				fnFadeIn(m_nFadeInTime);
			}
		
		}
		private function fnBtnSoundPlay(e:MouseEvent=null):void
		{
			if(m_SoundChannel)
			{
				m_SoundChannel.stop();
				m_SoundChannel=null
			}
			
			m_sound=null;
			m_sound=new Sound();
			
			/* var request:URLRequest = new URLRequest(m_soundCollection[m_nSelectedSound].url);
			m_sound.load(request);
			m_sound.addEventListener( IOErrorEvent.IO_ERROR, handleIOErr ); */
			if(m_nSelectedSound < m_soundLoader.length)
			{
				m_sound=m_soundLoader[m_nSelectedSound]
			}
			else
			{
				return;
			}
			if(m_nPosition!=0)
			{
				m_SoundChannel = m_sound.play(m_nPosition);
				m_SoundChannel.addEventListener(Event.SOUND_COMPLETE, fnSoundEnd);
			}
			else
			{
				m_SoundChannel=m_sound.play();
				//	mcSoundControlPanel.Txt_MusicName.text=m_soundCollection[m_nSelectedSound].name;
				m_SoundChannel.addEventListener(Event.SOUND_COMPLETE, fnSoundEnd);
			}
			if(m_bMute==true)
			{
				m_SoundChannel.soundTransform=new SoundTransform(0)
			}
			else
			{
				m_SoundChannel.soundTransform=new SoundTransform(m_nVolume)
			}
			
			m_bIsPlaying=true;
			
		}
		
		private  function fnSoundEnd(event:Event):void 
		{ 
			m_bIsPlaying = false;
			if(m_bLoop)
			{
				m_nPosition=0
				fnBtnSoundPlay();
			}
			else if(m_bRandom)
			{
				m_nSelectedSound=Math.round(Math.random()*(m_soundCollection.length-1));
				m_nPosition=0
				fnBtnSoundPlay();
			}
			else
			{
				fnBtnSoundNext();
			}
			
		}
		private function fnBtnSoundNext(e:MouseEvent=null):void
		{
			if(m_nSelectedSound==m_soundCollection.length-1)
			{
				m_nSelectedSound=0;
			}
			else
			{
				m_nSelectedSound++;
			}
			m_nPosition=0
			fnBtnSoundPlay();
		}
		private function fnVolumeChange():void
		{
			if(m_SoundChannel)
			{
				
				if(m_bMute==true)
				{
					m_SoundChannel.soundTransform=new SoundTransform(0)
					return;
				}
				m_SoundChannel.soundTransform=new SoundTransform(m_nVolume)
				
			}
			if(m_swf)
			{
				m_swf.soundTransform = new SoundTransform(m_nSwfVolume);			
			}
			
			
		}
		
		public function fnStopMusic():void
		{
			if(m_SoundChannel)
			{
				m_SoundChannel.stop();
				m_SoundChannel=null
				m_bIsPlaying = false
			}
		}
		
		
		public function fnPauseMusic():void
		{
			if(m_SoundChannel)
			{
				m_nPosition = m_SoundChannel.position;
				m_SoundChannel.stop();
				m_bIsPlaying = false
			}
		}
		
		public function fnResume():void
		{
			fnBtnSoundPlay();
		}
		
		private function fnFadeIn(nTime:Number = 3):void
		{
			if(m_SoundChannel && !m_bMute)
			{
				trace("fade in ");
				var sTrans:SoundTransform = new SoundTransform();
				sTrans.volume = 0;
				Tweener.addTween(sTrans, {volume:m_nVolume, time:nTime, transition:"easeInOutQuad",onUpdate:function():void{m_SoundChannel.soundTransform = sTrans}});
			}
		}
		
		public function fnFadeOut(nTime:Number = 3):void
		{
			if(m_SoundChannel && !m_bMute)
			{
				var sTrans:SoundTransform = new SoundTransform();
				sTrans.volume=m_SoundChannel.soundTransform.volume;
				Tweener.addTween(sTrans, {volume:0, time:nTime, transition:"easeInOutQuad",onUpdate:function():void{m_SoundChannel.soundTransform = sTrans;trace("strans:"+sTrans.volume)},onComplete:function():void{fnStopMusic()}});
			}
		}
		
	
		
		public function fnDestory():void
		{
			if(m_sound)
			{
				m_sound.removeEventListener( IOErrorEvent.IO_ERROR, handleIOErr );
				m_sound.removeEventListener(Event.COMPLETE,fnDownloadSoundComplete)
				m_sound = new Sound();
				
			}
			if(m_SoundChannel)
			{
				m_SoundChannel.removeEventListener(Event.SOUND_COMPLETE, fnSoundEnd);
				m_SoundChannel.stop();
				m_SoundChannel = null;
			}
			if(m_swf)
			{
				m_swf = null;
			}
			m_nPosition = 0;
			m_nSelectedSound = 0;
		}
		
		
		
	}
}