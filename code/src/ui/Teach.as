package ui
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import base.TiDisableTextField;
	
	import handler.TTCSoundHandler;
	
	public class Teach extends Sprite
	{
		private var m_mcBg:Poker_BG = new Poker_BG();
		private var m_mc:Poker_Teach = new Poker_Teach();
		public function Teach()
		{
			super();
			this.addChild(m_mcBg);
			this.addChild(m_mc);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_next);
			TiDisableTextField.fnSetButtonAndDisableAllTextField(m_mc.btn_previous);
			m_mc.btn_next.addEventListener(MouseEvent.CLICK,fnClickNext);
			m_mc.btn_previous.addEventListener(MouseEvent.CLICK,fnClickPrevious);
			m_mc.btn_previous.visible=false;
			m_mc.txt_page.text = "1 / 13"
		}
		
		
		
		private function fnClickNext(e:MouseEvent):void
		{
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			if(m_mc.teach.currentFrame == m_mc.teach.totalFrames)
			{
				this.visible =false;	
			}
			m_mc.btn_previous.visible=true;
			m_mc.teach.nextFrame();
			m_mc.txt_page.text = m_mc.teach.currentFrame +" / 13"
		}
		
		private function fnClickPrevious(e:MouseEvent):void
		{
			m_mc.teach.prevFrame();
			TTCSoundHandler.fnGetInstance().fnEmbeddedSound000()
			if(m_mc.teach.currentFrame == 1)
			{
				m_mc.btn_previous.visible =false;
			}
			else
			{
				m_mc.btn_previous.visible = true;
			}
			m_mc.txt_page.text = m_mc.teach.currentFrame +" / 13"
		}
		
		
	}
}